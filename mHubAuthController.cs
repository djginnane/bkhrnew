﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using umbraco.cms.businesslogic.member;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco.Core;
using Umbraco.Web.PublishedContentModels;
using System.ComponentModel.DataAnnotations;

/// <summary>
/// Summary description for mHubLoginController
/// </summary>
public class mHubAuthSurfaceController : SurfaceController
{
    [HttpPost]
    public ActionResult HandleLogin([Bind(Prefix = "loginModel")]MhubLoginModel model)
    {
        if (ModelState.IsValid == false)
        {
            return CurrentUmbracoPage();
        }

        if (Members.Login(model.Username, model.Password) == false)
        {
            FormsAuthentication.SetAuthCookie(model.Username, model.RememberMe);
            //don't add a field level error, just model level
            ModelState.AddModelError("loginModel", "Invalid username or password");
            return CurrentUmbracoPage();
        }

        TempData["LoginSuccess"] = true;
        //   var uName= Membership.GetUser().UserName;

        //if there is a specified path to redirect to then use it
        //if (Members.IsLoggedIn())
        //{
        //    var member = MhubMemberHelper.LoggedInMember;

        //    if (member.GetValue<int>("passwordChecker") == 1)

        //    {
        //        System.Console.WriteLine("test");
        //        return Redirect("/Account/");
        //    }
        //}

        if (model.RedirectUrl.IsNullOrWhiteSpace() == false)
        {
            return Redirect(model.RedirectUrl);
        }
        else
        {
            var referer = Request.UrlReferrer;
            var url = (referer.AbsoluteUri.Contains("logout")) ? "/" : referer.AbsoluteUri;
            return Redirect(url);
        }
    }

    [HttpGet]
    public ActionResult MemberLogout()
    {
        Session.Clear();
        FormsAuthentication.SignOut();
        return Redirect("/");
    }
}

public class MhubLoginModel : LoginModel
{
    [Display(Name = "Remember me")]
    public bool RememberMe { get; set; }
}