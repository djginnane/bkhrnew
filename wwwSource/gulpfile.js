﻿/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('default', function () {
    // place code for your default task here
});

gulp.task('sass', function () {
    return gulp.src('./SCSS/style.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./css'))
      .pipe(gulp.dest('../mhub.website/css/'));
});
gulp.task('sass2', function () {
    return gulp.src('./SCSS/style2.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./css'))
      .pipe(gulp.dest('../mhub.website/css/'));
});
gulp.task('sass3', function () {
    return gulp.src('./SCSS/style3.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./css'))
      .pipe(gulp.dest('../mhub.website/css/'));
});
gulp.task('sass4', function () {
    return gulp.src('./SCSS/style4.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./css'))
      .pipe(gulp.dest('../mhub.website/css/'));
});
gulp.task('sass5', function () {
    return gulp.src('./SCSS/style5.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./css'))
      .pipe(gulp.dest('../mhub.website/css/'));
});
gulp.task('sass:watch', function () {
    gulp.watch('./SCSS/*.scss', ['sass']);
});

