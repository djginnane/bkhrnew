var EditModes;
(function (EditModes) {
    EditModes[EditModes["InIdleMode"] = 0] = "InIdleMode";
    EditModes[EditModes["InEditMode"] = 1] = "InEditMode";
    EditModes[EditModes["InSaveMode"] = 2] = "InSaveMode";
})(EditModes || (EditModes = {}));
var EmployeeEntryCtrl = (function () {
    function EmployeeEntryCtrl($q, $routeParams, $http) {
        console.log("gets here");
        var _this = this;
        this.$q = $q;
        this.$routeParams = $routeParams;
        this.$http = $http;
        this.forms = {
            general: null,
            hrinfo: null,
            leave: null,
            addLeave: null,
            assessment: null,
            appraisals: null
        };
        this.calendars = {
            birthday: {
                opened: false,
                options: {
                    dateDisabled: false,
                    maxDate: new Date(),
                    minDate: new Date(1920, 1, 1),
                    startingDay: 1
                }
            },
            careerStart: {
                opened: false,
                options: {
                    dateDisabled: false,
                    maxDate: new Date(),
                    minDate: new Date(1920, 1, 1),
                    startingDay: 1
                }
            },
            companyStart: {
                opened: false,
                options: {
                    dateDisabled: false,
                    maxDate: new Date(),
                    minDate: new Date(1920, 1, 1),
                    startingDay: 1
                }
            }
        };
        this.employee = {
            annualLeave: [],
            appraisal: null,
            appraisalSchedule: null,
            birthday: null,
            birthday2: null,
            branch: '',
            careerStart: null,
            companyStart: null,
            division: '',
            email: '',
            lineManager: '',
            memberId: 0,
            name: '',
            parentId: 0,
            phone: '',
            photo: '',
            position: '',
            role: '',
            username: '',
            inActive: false
        };
        this.addLeaveModel = null;
        this.appraisals = [];
        this.roles = [];
        this.positions = [];
        this.members = [];
        this.divisions = [];
        this.branches = [];
        this.appraisalSchedules = [];
        this.pageId = 0;
        this.isLoading = true;
        this.Searching = false;
        this.DelSpin = false;
        this.createMode = false;
        this.editMode = {
            general: EditModes.InIdleMode,
            hrinfo: EditModes.InIdleMode,
            leave: EditModes.InIdleMode,
            assessment: EditModes.InIdleMode,
            appraisals: EditModes.InIdleMode
        };
        this.checkValidityAndFocus = function (strQuery) {
            var inputElements = document.querySelectorAll(strQuery), inputElementsLength = inputElements.length, tempEl = null;
            while (inputElementsLength--) {
                var inputEl = inputElements[inputElementsLength];
                inputEl.focus();
                if (inputEl.classList.contains('ng-invalid')) {
                    tempEl = inputEl;
                }
            }
            if (tempEl) {
                tempEl.focus();
            }
        };
        this.appraisalSchedules = [{
            id: '111',
            value: 'No appraisal'
        },
            {
                id: '112',
                value: 'Monthly'
            },
            {
                id: '113',
                value: 'Quarterly'
            },
            {
                id: '313',
                value: 'Bi-Annually'
            },
            {
                id: '114',
                value: 'Yearly'
            }];
        var bDate = new Date();
        var elPageId = document.getElementById('pageId');
        this.pageId = elPageId ? parseInt(elPageId.value) : 0;
        var request = $http.get('/umbraco/api/member/getEmployeeEntryModel?pageId=' + this.pageId + '&employeeId=' + (this.getQueryVariable('id') || 0));
        request.then(function (response) {
            if (response.status === 200 && response.data) {
                if (response.data['employee']) {
                    _this.employee = response.data['employee'];
                    bDate = new Date(response.data['employee']['birthday']);
                    bCar = new Date(response.data['employee']['careerStart']);
                    bCom = new Date(response.data['employee']['companyStart']);
                    _this.employee.birthday = new Date(bDate.getUTCFullYear(), bDate.getUTCMonth(), bDate.getUTCDate());
                    _this.employee.careerStart = new Date(bCar.getUTCFullYear(), bCar.getUTCMonth(), bCar.getUTCDate());
                    _this.employee.companyStart = new Date(bCom.getUTCFullYear(), bCom.getUTCMonth(), bCom.getUTCDate());
                    console.log(bDate.getUTCDate);
                }
                else {
                    _this.createMode = true;
                    _this.editMode.general = EditModes.InEditMode;
                    _this.editMode.hrinfo = EditModes.InEditMode;
                    _this.editMode.leave = EditModes.InEditMode;
                    _this.editMode.assessment = EditModes.InEditMode;
                    _this.editMode.appraisals = EditModes.InEditMode;
                }
                _this.members = response.data['members'] || [];
                _this.positions = response.data['positions'] || [];
                _this.appraisals = response.data['appraisals'] || [];
                _this.roles = response.data['roles'] || [];
                _this.divisions = response.data['divisions'] || [];
                _this.branches = response.data['branches'] || [];
            }
        })
            .finally(function () {
                _this.isLoading = false;
            });
    }
    EmployeeEntryCtrl.prototype.deactivateUser = function (ev, strFormSection) {
        var _this = this;
        ev.preventDefault();
     
        //swal({
        //    title: "Are you sure?",
        //    text: "You will not be able to recover this imaginary file!",
        //    type: "warning",
        //    showCancelButton: true,
        //    confirmButtonColor: "#DD6B55",
        //    confirmButtonText: "Yes, delete it!",
        //    closeOnConfirm: false,
        //    closeOnCancel: true
        //}, function (isConfirm) {
        //    if (isConfirm) {
              
               
        //        console.log("gets here");
        //        swal("Deleted!", "Your imaginary file has been deleted.", "success");
        //        console.log("gets here");
        //    }

        //});

        if (confirm("Are you sure you wish to deactivate this user?")) {
            this.Searching = true;
            this.$http.post('/umbraco/api/member/DeActivateUser?pageId=' + this.pageId, this.employee).then(function (response) {
                if (response.status === 200) {
                    _this.Searching = false;
                    _this.employee.inActive = false;
                    toastr.success('User Deactivated', 'Success');
                }
                else {
                    toastr.error('User not Deactivated, contact support', 'Success');
                }
            });
        }
    };
    EmployeeEntryCtrl.prototype.activateUser = function (ev, strFormSection) {
        var _this = this;
        ev.preventDefault();
        this.Searching = true;
        console.log(this.employee);
        this.$http.post('/umbraco/api/member/ActivateUser?pageId=' + this.pageId, this.employee).then(function (response) {
            if (response.status === 200) {
                _this.Searching = false;
                _this.employee.inActive = true;
                toastr.success('User Activated', 'Success');
            }
            else {
                toastr.error('User not Activated, contact support', 'Success');
            }
        });
    };
    EmployeeEntryCtrl.prototype.deleteUser = function (ev, strFormSection) {
        var _this = this;
        ev.preventDefault();
        this.DelSpin = true;
        console.log(this.employee);
        this.$http.post('/umbraco/api/member/DeleteUser?pageId=' + this.pageId, this.employee).then(function (response) {
            if (response.status === 200) {
                _this.DelSpin = false;
                // this.employee.inActive = false;
                toastr.success('User Deleted Successfully', 'Success');
            }
            else {
                toastr.error('User could not be deleted, contact support', 'Success');
            }
        });
    };
    EmployeeEntryCtrl.prototype.submitAll = function (ev, strFormSection) {
        var _this = this;
        ev.preventDefault();
        var keys = Object.keys(this.forms);
        var i = keys.length;
        var doSubmit = true;
        console.log(this.employee.annualLeave);
        while (i--) {
            var key = keys[i];
            var f = this.forms[key];
            if (!strFormSection || strFormSection === key) {
                if (!f.$valid) {
                    doSubmit = false;
                    this.checkValidityAndFocus('form[name="' + f.$name + '"] .form-control');
                }
            }
        }
        if (doSubmit) {
            if (strFormSection) {
                this.setMode(strFormSection, EditModes.InSaveMode);
            }
            var newbDate = this.employee.birthday;
            var newCar = this.employee.careerStart;
            var newCom = this.employee.companyStart;
            var utcDate = newbDate.getFullYear() + "-" + newbDate.getMonth() + "-" + newbDate.getDate() + "T17:00:00Z";
            this.employee.birthday = new Date(Date.UTC(newbDate.getFullYear(), newbDate.getMonth(), newbDate.getDate()));
            this.employee.careerStart = new Date(Date.UTC(newCar.getFullYear(), newCar.getMonth(), newCar.getDate()));
            this.employee.companyStart = new Date(Date.UTC(newCom.getFullYear(), newCom.getMonth(), newCom.getDate()));
            console.log(this.employee.birthday)

            this.$http.post('/umbraco/api/member/submitEmployee?pageId=' + this.pageId, this.employee).then(function (response) {
                _this.setMode(strFormSection, EditModes.InIdleMode);
                if (response.status === 200) {
                    if (strFormSection)
                        toastr.success('Successfully updated', 'Success');
                    else
                        toastr.success('Successfully added', 'Success');
                }
                else if (response.status === 406) {
                    toastr.warning(response.data);
                }
                else {
                    toastr.error(response.data);
                }
            });
        }
        else {
            if (strFormSection) {
                this.setMode(strFormSection, EditModes.InIdleMode);
            }
        }
    };
    EmployeeEntryCtrl.prototype.setMode = function (sectionName, val) {
        this.editMode[sectionName] = val;
    };
    EmployeeEntryCtrl.prototype.inEditMode = function (sectionName, val) {
        if (val === void 0) { val = EditModes.InEditMode; }
        return this.editMode[sectionName] >= val;
    };
    EmployeeEntryCtrl.prototype.getLeaves = function () {
        return this.employee.annualLeave;
    };
    EmployeeEntryCtrl.prototype.newLeaveForm = function (ev) {
        ev.preventDefault();
        if (!this.addLeaveModel) {
            this.addLeaveModel = {
                key: '',
                value: ''
            };
        }
    };
    EmployeeEntryCtrl.prototype.addLeave = function (ev) {
        ev.preventDefault();
        if (this.forms.addLeave.$valid) {
            var duplicateLeave = angular.copy(this.addLeaveModel);
            this.employee.annualLeave.push(duplicateLeave);
            this.addLeaveModel = null;
        }
        else {
            this.checkValidityAndFocus('form[name="' + this.forms.addLeave.$name + '"] .form-control');
        }
    };
    EmployeeEntryCtrl.prototype.removeLeave = function (leaveIndex) {
        if (confirm('Are you sure you want to remove this leave from the list?')) {
            this.employee.annualLeave.splice(leaveIndex, 1);
        }
    };
    EmployeeEntryCtrl.prototype.getAvailableYears = function () {
        var currentYear = new Date().getFullYear(), fromYear = currentYear - 2, toYear = currentYear + 2, years = [];
        for (var i = fromYear; i <= toYear; i++) {
            if (!this.existAsKeyInArray(this.employee.annualLeave, i)) {
                years.push(i);
            }
        }
        return years;
    };
    EmployeeEntryCtrl.prototype.existAsKeyInArray = function (ar, v) {
        for (var j = 0; j < ar.length; j++) {
            if (ar[j].key == v) {
                return true;
            }
        }
        return false;
    };
    EmployeeEntryCtrl.prototype.openCalendar = function (calendarName) {
        if (this.calendars[calendarName]) {
            this.calendars[calendarName].opened = true;
        }
    };
   
    EmployeeEntryCtrl.prototype.getDateAsString = function (propertyName) {
        var result = 'Undefined';
        if (this.employee[propertyName]) {
            var strDateObj = this.employee[propertyName];
            var intDate = strDateObj.getDate();
            var strDate = intDate < 10 ? '0' + intDate : intDate;
            var intMonth = strDateObj.getMonth() + 1;
            var strMonth = intMonth < 10 ? '0' + intMonth : intMonth;
            var strYear = strDateObj.getFullYear();
            result = strDate + '-' + strMonth + '-' + strYear;
        }
        return result;
    };
    EmployeeEntryCtrl.prototype.getDateAsUtc = function (propertyName) {
        var result = 'Undefined';
        if (this.employee[propertyName]) {
            var strDateObj = this.employee[propertyName];
            var intDate = strDateObj.getUTCDate();
            var strDate = intDate < 10 ? '0' + intDate : intDate;
            var intMonth = strDateObj.getUTCMonth() + 1;
            var strMonth = intMonth < 10 ? '0' + intMonth : intMonth;
            var strYear = strDateObj.getUTCFullYear();
            result = strDate + '-' + strMonth + '-' + strYear;
        }
        return result;
    };
    EmployeeEntryCtrl.prototype.getScheduleName = function (scheduleId) {
        var result = 'Undefined';
        this.appraisalSchedules.forEach(function (element) {
            if (element.id == scheduleId)
                result = element.value;
        });
        return result;
    };
    EmployeeEntryCtrl.prototype.getLineManagerName = function (lineManagerId) {
        var result = 'Undefined';
        this.members.forEach(function (m) {
            if (m.id == lineManagerId)
                result = m.name;
        });
        return result;
    };
    EmployeeEntryCtrl.prototype.getAppraisalName = function (appraisalId) {
        var result = 'Undefined';
        this.appraisals.forEach(function (element) {
            if (element.Id == appraisalId)
                result = element.TemplateName;
        });
        return result;
    };
    EmployeeEntryCtrl.prototype.getRoleName = function (roleId) {
        var result = null;
        var filteredRoles = this.roles.filter(function (role) { return roleId == role.id; });
        if (filteredRoles && filteredRoles.length > 0) {
            result = filteredRoles[0].value;
        }
        return result;
    };
    EmployeeEntryCtrl.prototype.hasError = function (formName, fieldName) {
        var result = false;
        if (this.forms && this.forms[formName] && this.forms[formName][fieldName]) {
            result = this.forms[formName][fieldName].$touched && this.forms[formName][fieldName].$invalid;
        }
        return result;
    };
    EmployeeEntryCtrl.prototype.getErrorMessages = function (formName, fieldName) {
        var result = false;
        if (this.forms && this.forms[formName] && this.forms[formName][fieldName]) {
            result = this.forms[formName][fieldName].$touched && this.forms[formName][fieldName].$error;
        }
        return result;
    };
    EmployeeEntryCtrl.prototype.getQueryVariable = function (variable) {
        var result = null;
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                result = pair[1];
            }
        }
        return result;
    };
    return EmployeeEntryCtrl;
}());
angular.module('employee-entry-app', ['ngRoute', 'ngMessages', 'ui.bootstrap'])
    .config(function ($routeProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
    })
    .controller('employeeEntryCtrl', EmployeeEntryCtrl);
