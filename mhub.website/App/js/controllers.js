var phonecatApp = angular.module('phonecatApp', []);

phonecatApp.controller('PhoneListCtrl', function ($scope, $http) {
  $http.get('/Umbraco/Api/LeaveManager/Get/').success(function(data) {
    $scope.phones = data;
  });

  $scope.orderProp = 'LeaveEnd';
});

