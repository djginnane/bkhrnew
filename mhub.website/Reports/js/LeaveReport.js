
var appTemplate = angular.module('appTemplate', ['ui.bootstrap']);

appTemplate.controller('listController', function ($scope,$http) {
	$scope.leaveList = [];
	$scope.fail=0;

	 $http.get('/Umbraco/Api/LeaveReport/Get/').success(function (data) {
      $scope.leavelist = data;
     // alert("hello");
     // alert($scope.leavelist[0].memberId)
     var config ={}

     $scope.dRange = function(dtStart,dtEnd){
console.log(dtEnd);
data=$scope.dtEnd;
var m=dtStart.getMonth();
var d=dtStart.getDay();
var y=dtStart.getFullYear();
var dStart=d+"/"+m+"/"+y;
var m=dtEnd.getMonth();
var d=dtEnd.getDay();
var y=dtEnd.getFullYear();
var dEnd=d+"/"+m+"/"+y;
console.log(y);

data1={"StartDate":dStart, "EndDate":dEnd};

 $http.post('/Umbraco/Api/LeaveReport/GetDRange/', data1, config)
             .success(function (dataResult, status) {
             	console.log(dataResult);
             	 $scope.leavelist=dataResult;
                if (dataResult==null)
                {
              
               $scope.fail=1;
          		 }
          		 else
          		 {
          		 	$scope.fail=0;
          		 }
          		
             })
		
				//alert(reportings.SDate);
			};
 
});

	 $scope.exportData = function () {
	     var blob = new Blob([document.getElementById('exportable').innerHTML], {
	         type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	     });
	     saveAs(blob, "Report.xls");
	 };

	 $scope.today = function () {
	     var curDate = new Date();
	     var curYear = curDate.getFullYear();
	    // alert(curYear);

	     $scope.dt= new Date(curYear,0,1);
	    // $scope.dt2 = new Date(curYear,1,1);

    // var tomorrowDate = new Date();
  //tomorrowDate .setDate(tomorrowDate .getDate() + 3);
	     $scope.dt2 = new Date(curYear, 11, 31);
   // alert($scope.dtEnd);
  };
  $scope.today();

  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.inlineOptions = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };

  $scope.dateOptions = {
    dateDisabled: disabled,
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
  }

  $scope.toggleMin = function() {
    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
  };

  $scope.toggleMin();

  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };
   

  $scope.setDate = function(year, month, day) {
   $scope.dt = new Date(year, month, day);
  //  $scope.dtEnd = new Date(year, month, day);
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];
  $scope.altInputFormats = ['M!/d!/yyyy'];

  $scope.popup1 = {
    opened: false
  };

  $scope.popup2 = {
    opened: false
  };

  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 1);
  $scope.events = [
    {
      date: tomorrow,
      status: 'full'
    },
    {
      date: afterTomorrow,
      status: 'partially'
    }
  ];

   function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }

    return '';
  }

	 });
/*
   var appTemplate = angular.module('appTemplate');

appTemplate.controller('listController',
   function($scope, $http) {


  $scope.leaveList = [];

  $http.get('/Umbraco/Api/LeaveReport/Get/').success(function (data) {
      $scope.leavelist = data;
     // alert($scope.leavelist[0].memberId)

  });
 // $scope.qtemplates = [];
  // $scope.test = [{}];
  
 
	 // $scope.qtemplates =[];	
})// JavaScript Document
*/