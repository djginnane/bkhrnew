﻿
var phonecatApp = angular.module('phonecatApp', ['ui.calendar', 'ui.bootstrap']);

phonecatApp.controller('ModalInstanceCtrl', function ($scope, $modalInstance, items) {

    $scope.items = items;
    $scope.selected = {
        item: $scope.items[0]
    };

    $scope.ok = function () {
        $modalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

phonecatApp.controller('PhoneListCtrl', function ($scope, $http, $modal, $log) {

    //Turn this back on
    $http.get('/Umbraco/Api/LeaveManager/Get/').success(function(data) {


        $scope.phones = data;
	
	
    });

    $scope.orderProp = 'LeaveEnd';
   
  
    $scope.addApproved=function(appStatus, CreatedAt, MemberId, LeaveStart){
        //alert($scope.ApprovedLeave);
        var data = {
            ApprovedLeave: appStatus,
            id: MemberId,
            CreatedAt:CreatedAt ,
            start: LeaveStart
        };
        console.log(data);
        var config = {
              
        }
        $http.post('/Umbraco/Api/LeaveManager/ApproveLeave/', data, config)
       .success(function (data, status) {
           //$scope.ApproveLeave = data;
           //alert(data);
           if (appStatus == 2)
           { $scope.items = ['The Leave Request was successfully Rejected, an email has been sent to notify the employee']; }
           else if (appStatus == 1)
           {  $scope.items = ['The Leave Request was successfully Approved, an email has been sent to notify the employee'];   }
          

           $scope.animationsEnabled = true;

          

           var modalInstance = $modal.open({
               animation: $scope.animationsEnabled,
               templateUrl: 'myModalContent.html',
               controller: 'ModalInstanceCtrl',
               size: 'sm',
               resolve: {
                   items: function () {
                       return $scope.items;
                   }
               }
           });
				
       })
        //alert("gets here");
    };
 
});


phonecatApp.controller('CalendarCtrl',
   function($scope, $compile, $timeout, uiCalendarConfig, $http, $rootScope) {
       var date = new Date();
       var d = date.getDate();
       var m = date.getMonth();
       var y = date.getFullYear();

	
	
       $scope.uiConfig = {
           calendar:{
               height: 500,
               editable: true,
               eventLimit: true,
               header:{
                   left: 'title',
                   center: '',
                   right: 'today prev,next'
               },
               //  eventClick: $scope.alertOnEventClick,
               //        eventDrop: $scope.alertOnDrop,
               //        eventResize: $scope.alertOnResize,
               eventRender: $scope.eventRender
           }
       };

       $scope.events = [];
       $scope.getCalendarDetails = function () {
           $http.get('/Umbraco/Api/LeaveManager/GetCalendarDetails').success(function (data) {

               if (data) {
                   var list = data;
                   var someDate = new Date();
                   var i = 0;
                   for (i = 0; i < data.length; i++) {
                       someDate = new Date(data[i]['start']);
                       var d1 = someDate.getDate();
                       var m1 = someDate.getMonth();
                       var y1 = someDate.getFullYear();
                       var cTitle = data[i]['title']
                       $scope.events.push([{

                           Id: data[i].Id, type: 'party', title: cTitle, start: new Date(y1, m1, d1, 14, 0), allDay: true, stick: true

                       }]
                          );
                   }
               }



           }, function (error) { console.log(error); });

       }
	
       $rootScope.addDelete=function(idx){
           var list =  $scope.events;
           var ind=20;
           console.log(list);
           for (var i = 0; i < list.length ; i++) {
               //alert(list[i][0]['title']);
               if (list[i][0]['id'] === idx+1000) {
                   ind= i;
               }
           }
           //alert(ind);
           //var person_to_delete = $scope.events[idx];
           $scope.events.splice(ind, 1)
           myVar=true;
           // $('#R'+idx).hide();
           //  $('#A'+idx).show();
       };
	
       $rootScope.addTest= function(title3,startDay,endDay, index, status){
           uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', startDay);
           //alert(testIndex);
           //$scope.calendar.fullCalendar( 'gotoDate', date );
           //alert(index+1000);
           var someDate = new Date(endDay);
		
           var myDate = new Date(someDate.getTime() + (1 * 24 * 60 * 60 * 1000));
           var dColor = "Blue";
           var dText = "black";
           if (status == 0)
           {
               dColor = "orange"
               dtext = "white";
           }
           else if (status == 1)
           {
               dColor = "green"
               dtext = "white";
           }
           else if (status == 2) {
               dColor = "red"
               dtext = "white";
           }
		
           $scope.events.push([{
               id: index+1000,	 
               title: title3,
               allDay: true,
               color: dColor,
               textColor: dtext,
               start: startDay,// new Date(y, m, 29),
               end: myDate // new Date(y, m, 29)
		 
           }]);
           // $('#A'+index).hide();
           //  $('#R'+index).show();
           /*   $scope.events.push([{
                    
                color: 'orange',
              textColor: 'white',
              title:'Training',
              start:new Date(y, m, 21),
             end: new Date(y, m, 29)
            
                
             }])
             */
	  
           // $scope.events.splice(testIndex,1)
       };
	
       /* $scope.addHello=function(title3,startDay,endDay){
            
         
        
   
   uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', startDay);
           //alert(title);
           //$scope.calendar.fullCalendar( 'gotoDate', date );
            $scope.events.push([{
                
           title: title3,
           start:  new Date(y, m, 29),
           end: new Date(y, m, 29)
            
         }]);
          $scope.events.push([{
                
            color: 'orange',
          textColor: 'white',
          title:'Training',
          start:new Date(y, m, 21),
         end: new Date(y, m, 29)
        
            
         }])
           };
           */
       $scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];
       $scope.eventRender = function (event, element, view) {
           element.attr({
               'tooltip': event.title,
               'tooltip-append-to-body': true
           });
           $compile(element)($scope);
       };
	
       $scope.getCalendarDetails();
   });
   

