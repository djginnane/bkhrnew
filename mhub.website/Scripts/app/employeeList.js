﻿(function () {
    'use strict';

    var app = angular.module('employees', ['ui.bootstrap']);

    app.controller('EmployeesController', EmployeesController);

    EmployeesController.$inject = ['$scope', '$location', '$templateCache', '$http', '$uibModal'];
    function EmployeesController($scope, $location, $templateCache, $http, $uibModal)
    {
        /* jshint validthis:true */
        var vm = this;
        vm.members = [];
        vm.reverse = false;
        vm.predicate = 'name';
        vm.filterTeams = [];
        vm.selectedTeam = '';
        vm.textSearch = '';
        vm.toggleSearchFilter = false;
        vm.TeamList = [];
       // vm.Branch=@mBranch;

        activate();

        function activate()
        {
            $http.get('/umbraco/api/member/getall').then(function (result)
            {
            	vm.members = result.data.Members;

            	loadTeamList();

                loadDistinctTeamArray();
            });
        }

        vm.order = function(predicate)
        {
            vm.reverse = (vm.predicate === predicate) ? !vm.reverse : false;
            vm.predicate = predicate;
        };

        function loadDistinctTeamArray()
        {
            if (vm.members.length > 0)
            {
                var flags = [], i;
                for (i = 0; i < vm.members.length; i++)
                {
                    if (flags[vm.members[i].team] || vm.members[i].team === null)
                    {
                        continue;
                    }

                    flags[vm.members[i].team] = true;
                    vm.filterTeams.push(vm.members[i].team);
                }
            }
        }

        function loadTeamList() {
            $http.get('/umbraco/api/member/getteamlist').then(function (result) {
                vm.TeamList = result.data;
            });
        }

        //vm.showProfile = function (memberId)
        //{
        //    var modalScope = $scope.$new();
        //    modalScope.member = _.findWhere(vm.members, { id: memberId });

        //    if (modalScope.member !== undefined)
        //    {
        //        var modalInstance = $modal.open({
        //            animation: false,
        //            templateUrl: '/scripts/app/employees/profileModal.html',
        //            scope: modalScope
        //        });
        //    }
        //};
    }
})();
