declare var toastr;


interface IEmployee
{
	memberId: number;
	parentId: number;
	username: string;
	email: string;
    name: string;
    phone: string;
    photo: string;
	position: string;
    lineManager: string;
    role: string;
    division: string;
    branch: string;
	birthday: Date;
	companyStart: Date;
	careerStart: Date;
	appraisalSchedule: number;
    appraisal: number;
    inActive: boolean;
	annualLeave: Leave[];
}

interface FormModels
{
	general: angular.INgModelController;
	hrinfo: angular.INgModelController;
	leave: angular.INgModelController;
	addLeave: angular.INgModelController;
	assessment: angular.INgModelController;
	appraisals: angular.INgModelController;
}

interface Member
{
	id: number;
	name: string;
	username: string;
	email: string;
}

interface Role
{
	id: number;
	value: string;
}

interface Leave
{
	key: string;
	value: string;
}

interface AppraisalForm
{
	Id: string;
	TemplateName: string;
}

enum EditModes
{
	InIdleMode = 0,
	InEditMode = 1,
	InSaveMode = 2
}

class EmployeeEntryCtrl
{
	public forms: FormModels = {
		general: null,
		hrinfo: null,
		leave: null,
		addLeave: null,
		assessment: null,
		appraisals: null
	};

	public calendars = {
		birthday: {
			opened: false,
			options: {
				dateDisabled: false,
                maxDate: new Date(2030, 1, 1),
				minDate: new Date(1920, 1, 1),
				startingDay: 1
			}
		},
		careerStart: {
			opened: false,
			options: {
				dateDisabled: false,
                maxDate: new Date(2030, 1, 1),
				minDate: new Date(1920, 1, 1),
				startingDay: 1
			}
		},
		companyStart: {
			opened: false,
			options: {
				dateDisabled: false,
                maxDate: new Date(2030, 1, 1),
				minDate: new Date(1920, 1, 1),
				startingDay: 1
			}
		}
	};

	public employee: IEmployee = {
		annualLeave: [],
		appraisal: null,
		appraisalSchedule: null,
		birthday: null,
		branch: '',
		careerStart: null,
		companyStart: null,
		division: '',
		email: '',
		lineManager: '',
		memberId: 0,
		name: '',
		parentId: 0,
		phone: '',
		photo: '',
		position: '',
		role: '',
        username: '',
        inActive:false
	};

	public addLeaveModel: Leave = null;
	public appraisals: AppraisalForm[] = [];
	public roles: Role[] = [];
	public positions: string[] = [];
	public members: Member[] = [];
	public divisions: string[] = [];
	public branches: string[] = [];
	public appraisalSchedules: {id: string, value: string}[] = [];
	public pageId: number = 0;
    public isLoading = true;
    public Searching = false;
    public DelSpin = false;
	public createMode = false;
	public editMode = {
		general: EditModes.InIdleMode,
		hrinfo: EditModes.InIdleMode,
		leave: EditModes.InIdleMode,
		assessment: EditModes.InIdleMode,
		appraisals: EditModes.InIdleMode
	};

	public constructor(private $q: angular.IQService, private $routeParams: angular.route.IRouteParamsService, private $http: angular.IHttpService)
	{
		this.appraisalSchedules = [{
			id: '111',
			value: 'No appraisal'
		},
		{
			id: '112',
			value: 'Monthly'
		},
		{
			id: '113',
			value: 'Quarterly'
        },
        {
            id: '313',
            value: 'Bi-Annually'
        },
		{
			id: '114',
			value: 'Yearly'
		}];

		let elPageId = document.getElementById('pageId') as HTMLInputElement;
		this.pageId = elPageId ? parseInt(elPageId.value) : 0;

		let request = $http.get('/umbraco/api/member/getEmployeeEntryModel?pageId=' + this.pageId + '&employeeId=' + (this.getQueryVariable('id') || 0));
		request.then((response) =>
		{
			if (response.status === 200 && response.data)
			{
				if (response.data['employee'])
				{
					this.employee = response.data['employee'];
					this.employee.birthday = new Date(response.data['employee']['birthday']);
					this.employee.careerStart = new Date(response.data['employee']['careerStart']);
					this.employee.companyStart = new Date(response.data['employee']['companyStart']);
				}
				else
				{
					this.createMode = true;
					this.editMode.general = EditModes.InEditMode;
					this.editMode.hrinfo = EditModes.InEditMode;
					this.editMode.leave = EditModes.InEditMode;
					this.editMode.assessment = EditModes.InEditMode;
					this.editMode.appraisals = EditModes.InEditMode;
				}

				this.members = response.data['members'] || [];
				this.positions = response.data['positions'] || [];
				this.appraisals = response.data['appraisals'] || [];
				this.roles = response.data['roles'] || [];
				this.divisions = response.data['divisions'] || [];
				this.branches = response.data['branches'] || [];
			}
		})
		.finally(() => {
			this.isLoading = false;
		});
	}

    public deactivateUser(ev: Event, strFormSection: string) {
        ev.preventDefault();
             if (confirm("Are you sure you wish to deactivate this user?")) {
            this.Searching = true;
            this.$http.post('/umbraco/api/member/DeActivateUser?pageId=' + this.pageId, this.employee).then((response) => {


                if (response.status === 200) {
                    this.Searching = false;
                    this.employee.inActive = false;
                    toastr.success('User Deactivated', 'Success');
                }
                else {
                    toastr.error('User not Deactivated, contact support', 'Success');
                }

            });
        }
    }
    public activateUser(ev: Event, strFormSection: string) {
        ev.preventDefault();
        this.Searching = true;
        console.log(this.employee);
        this.$http.post('/umbraco/api/member/ActivateUser?pageId=' + this.pageId, this.employee).then((response) => {


            if (response.status === 200) {
                this.Searching = false;
                this.employee.inActive = true;
                toastr.success('User Activated', 'Success');
            }
            else {
                toastr.error('User not Activated, contact support', 'Success');
            }
        });
    }

    public deleteUser(ev: Event, strFormSection: string) {
        ev.preventDefault();
        this.DelSpin = true;
        console.log(this.employee);
        this.$http.post('/umbraco/api/member/DeleteUser?pageId=' + this.pageId, this.employee).then((response) => {


           if (response.status === 200) {
               this.DelSpin = false;
               // this.employee.inActive = false;
                toastr.success('User Deleted Successfully', 'Success');
            }
           else {
                toastr.error('User could not be deleted, contact support', 'Success');
            }
       });
    }

	public submitAll(ev: Event, strFormSection: string)
	{
		ev.preventDefault();

		let keys = Object.keys(this.forms);
		let i = keys.length;
		let doSubmit = true;
        console.log(keys);
		while (i--)
		{
			let key = keys[i];
			let f = this.forms[key] as ng.INgModelController;

			if (!strFormSection || strFormSection === key)
			{
				if (!f.$valid)
				{
					doSubmit = false;
					this.checkValidityAndFocus('form[name="' + f.$name + '"] .form-control');
				}
			}
		}

		if (doSubmit)
		{
			if (strFormSection)
			{
				this.setMode(strFormSection, EditModes.InSaveMode);
			}

			this.$http.post('/umbraco/api/member/submitEmployee?pageId=' + this.pageId, this.employee).then((response) =>
			{
				this.setMode(strFormSection, EditModes.InIdleMode);

				if (response.status === 200)
				{
					if (strFormSection)
						toastr.success('Successfully updated', 'Success');
					else
						toastr.success('Successfully added', 'Success');
				}
				else if (response.status === 406)
				{
					toastr.warning(response.data);
				}
				else
				{
					toastr.error(response.data);
				}
			});
		}
		else
		{
			if (strFormSection)
			{
				this.setMode(strFormSection, EditModes.InIdleMode);
			}
		}
	}

	public setMode(sectionName: string, val: EditModes): void
	{
		this.editMode[sectionName] = val;
	}

	public inEditMode(sectionName: string, val: EditModes = EditModes.InEditMode): boolean
	{
		return this.editMode[sectionName] >= val;
	}

	public getLeaves(): Leave[]
	{
		return this.employee.annualLeave;
	}

	public newLeaveForm(ev: Event): void
	{
		ev.preventDefault();

		if (!this.addLeaveModel)
		{
			this.addLeaveModel = {
				key: '',
				value: ''
			}
		}
	}

	public addLeave(ev: Event): void
	{
		ev.preventDefault();

		if (this.forms.addLeave.$valid)
		{
			let duplicateLeave = angular.copy(this.addLeaveModel);
			this.employee.annualLeave.push(duplicateLeave);
			this.addLeaveModel = null;
		}
		else
		{
			this.checkValidityAndFocus('form[name="' + this.forms.addLeave.$name + '"] .form-control');
		}
	}

	public removeLeave(leaveIndex: number): void
	{
		if (confirm('Are you sure you want to remove this leave from the list?'))
		{
			this.employee.annualLeave.splice(leaveIndex, 1);
		}
	}

	public getAvailableYears(): number[]
	{
		let currentYear = new Date().getFullYear(), 
			fromYear = currentYear -2,
			toYear = currentYear + 2,
			years = [];

		for (let i = fromYear; i <= toYear; i++)
		{
			if (!this.existAsKeyInArray(this.employee.annualLeave, i))
			{
				years.push(i);
			}
		}

		return years;
	}

	private existAsKeyInArray(ar: any[], v: any): boolean
	{
		for (let j = 0; j < ar.length; j++)
		{
			if (ar[j].key == v) // dont use double quotes, as it could be comparing between number and a string representation of a number
			{
				return true;
			}
		}

		return false;
	}

	public openCalendar(calendarName: string): void
	{
		if (this.calendars[calendarName])
		{
			this.calendars[calendarName].opened = true;
		}
	}

	public getDateAsString(propertyName: string): string
	{
		let result = 'Undefined';

		if (this.employee[propertyName])
		{
			let strDateObj = this.employee[propertyName] as Date;

			let intDate = strDateObj.getDate();
			let strDate = intDate < 10 ? '0' + intDate : intDate;

			let intMonth = strDateObj.getMonth() + 1;
			let strMonth = intMonth < 10 ? '0' + intMonth : intMonth;

			let strYear = strDateObj.getFullYear();

			result = strDate + '-' + strMonth + '-' + strYear;
		}

		return result;
	}

	public checkValidityAndFocus = (strQuery: string) =>
	{
		let inputElements = document.querySelectorAll(strQuery),
			inputElementsLength = inputElements.length,
			tempEl: HTMLElement = null;

		while (inputElementsLength--)
		{
			let inputEl = inputElements[inputElementsLength] as HTMLElement;
			inputEl.focus();

			if (inputEl.classList.contains('ng-invalid'))
			{
				tempEl = inputEl;
			}
		}

		if (tempEl)
		{
			tempEl.focus();
		}
	};

	public getScheduleName(scheduleId: string): string
	{
		let result = 'Undefined';

		this.appraisalSchedules.forEach(element =>
		{
			if (element.id == scheduleId)
				result = element.value;
		});

		return result;
	}

	public getLineManagerName(lineManagerId: number): string
	{
		let result = 'Undefined';

		this.members.forEach(m =>
		{
			if (m.id == lineManagerId)
				result = m.name;
		});

		return result;
	}

	public getAppraisalName(appraisalId: string): string
	{
		let result = 'Undefined';

		this.appraisals.forEach(element =>
		{
			if (element.Id == appraisalId)
				result = element.TemplateName;
		});

		return result;
	}

	public getRoleName(roleId: number): string
	{
		let result = null;

		let filteredRoles = this.roles.filter(role => roleId == role.id);
		if (filteredRoles && filteredRoles.length > 0)
		{
			result = filteredRoles[0].value;
		}

		return result;
	}

	public hasError(formName: string, fieldName: string): boolean
	{
		let result = false;
		if (this.forms && this.forms[formName] && this.forms[formName][fieldName])
		{
			result = this.forms[formName][fieldName].$touched && this.forms[formName][fieldName].$invalid;
		}

		return result;
	}

	public getErrorMessages(formName: string, fieldName: string): boolean
	{
		let result = false;
		if (this.forms && this.forms[formName] && this.forms[formName][fieldName])
		{
			result = this.forms[formName][fieldName].$touched && this.forms[formName][fieldName].$error;
		}

		return result;
	}

	private getQueryVariable(variable: any): string
	{
		let result: string = null;
		var query = window.location.search.substring(1);
		var vars = query.split("&");
		
		for (let i = 0; i < vars.length; i++)
		{
			var pair = vars[i].split("=");
			if (pair[0] == variable)
			{
				result = pair[1];
			}
		}

		return result;
	}
}


angular.module('employee-entry-app', ['ngRoute', 'ngMessages', 'ui.bootstrap'])

.config(($routeProvider, $locationProvider) =>
{
	$locationProvider.html5Mode(true);
})

.controller('employeeEntryCtrl', EmployeeEntryCtrl);