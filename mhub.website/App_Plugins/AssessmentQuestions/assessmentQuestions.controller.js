/// <reference path="../../Scripts/angular-drag-and-drop-lists.min.js" />
angular.module("umbraco")
.controller("AssessmentQuestions", AssessmentQuestions);

function AssessmentQuestions($scope, assetsService, dialogService) {
  var defaultQuestion = { answers: [{}, {}] };
  var defaultAssessment = { questions: [defaultQuestion] };

  if ($scope.model.value === null || $scope.model.value === "") {
    $scope.model.value = $scope.model.config.defaultValue || defaultAssessment;
  }

  if (!$scope.model.value.questions) {
    $scope.model.value = defaultAssessment;
  }

  $scope.addQuestion = function () {
    var newQuestion = {};
    angular.copy(defaultQuestion, newQuestion);
    $scope.model.value.questions.push(newQuestion);

    console.log($scope.model.value.questions);
  }

  $scope.addAnswer = function (question) {
    if (!question.answers) {
      question.answers = [];
    }
    question.answers.push({});
  }

  $scope.deleteQuestion = function (index) {
    $scope.model.value.questions.splice(index, 1);
  }

  $scope.duplicateQuestion = function (index) {
    var duplicate = {};
    angular.copy($scope.model.value.questions[index], duplicate)
    $scope.model.value.questions.splice(index, 0, duplicate);
  }

  assetsService.load(["/scripts/angular-drag-and-drop-lists.min.js"]);
  assetsService.loadCss("/App_Plugins/AssessmentQuestions/lib/markdown.css");
  assetsService.loadCss("/App_Plugins/AssessmentQuestions/lib/assessment.css");
}