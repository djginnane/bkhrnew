﻿angular.module("umbraco")
    .controller("My.AppraialPickerController", function ($scope, appraisalFormsResource) {
        appraisalFormsResource.getAll().then(function (response) {
            $scope.appraisals = response.data;
        });
    });