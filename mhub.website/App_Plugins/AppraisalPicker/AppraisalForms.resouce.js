﻿//adds the resource to umbraco.resources module:
angular.module('umbraco.resources').factory('appraisalFormsResource',
    function ($q, $http) {
        //the factory object returned
        return {
            //this calls the ApiController we setup earlier
            getAll: function () {
                return $http.get("backoffice/My/AppraisalApi/GetAll");
            }
        };
    }
);