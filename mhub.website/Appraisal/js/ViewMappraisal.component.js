(function () {

    "use strict";

    var module = angular.module("vApprais");


    function fetchMovies($http, $location) {


        return $http.get("Umbraco/Api/AppraisalMember/GetAllMembers/")
     	.then(function (response) {

     	    return response.data;
     	}
)
    }




    function controller($http, $location) {
        var model = this;
        model.questionAns = null;
        model.ManagerQ = null;
        model.appraisals = [];
        model.comments = null;
        model.users = null;
        model.Loading = true;
        model.message = "Hello test";


        model.questions2 = function (appraisal) {


            model.comments = "stuff";

            model.questionsAns = appraisal.AppQuest;
            model.ManagerQ = "Start";

        };
        model.questions = function (appraisal) {


            model.questionsAns = appraisal.AppQuest;

            model.ManagerQ = "Start";




        };


        model.switchOff = function () {


            model.questionsAns = null;
        };
        model.saveQuestions = function (questions) {


            var config = {

            }

            $http.post('/Umbraco/Api/AppraisalMember/SaveAppraisal/', questions, config)
               .success(function (data, status) {
                   toastr.success("Appraisal Saved Successfully ");


               })

        };

        model.submitQuestions = function (questions) {
            var config = {

            }

            $http.post('/Umbraco/Api/AppraisalMember/SubmitAppraisal/', questions, config)
               .success(function (data, status) {

                   toastr.success("Appraisal Submitted Successfully ");




                   model.questionsAns = null;
                   model.comments = null;



               })

        };


        model.$onInit = function () {




            var config = {

            }


            var userId = $location.search().userid;
            var AppId = $location.search().Appraisalid;


            var users = [{ "userId": userId }]
            //delete next line in live version
            //var users=[{"userId":1527}]
            $http.get('/Umbraco/Api/AppraisalMember/GetAllMembers/', config)
              .success(function (data, status) {
                  model.Loading = false;
                  model.users = data;
                  if (AppId) {
                      model.questionsAns = data.mAppraisals[0]
                  }


                  console.log(model.users);

              })





        };
    }

    module.component("userApp", {
        templateUrl: "/Appraisal/AllUser.html",
        controllerAs: "model",
        controller: ["$http", "$location", controller]

    });
}());