(function () {

    "use strict";

    var module = angular.module("vApprais");


    function fetchMovies($http, $location) {


        return $http.get("/Umbraco/Api/AppraisalMember/Get/")
     	.then(function (response) {

     	    return response.data;
     	}
)
    }




    function controller($http, $location) {
        var model = this;
        model.questionAns = null;
        model.ManagerQ = null;
        model.appraisals = [];
        model.comments = null;
        model.users = null;
        model.FaceToFace = 0;
        model.submitting = false;
        model.savingApp = false;
        model.tick = false;
        model.appLoading = true;
        model.appError = "";

        model.AppraiseView2 = null;


        model.questions2 = function (appraisal) {


            model.comments = "stuff";

            model.questionsAns = appraisal.AppQuest;
            model.ManagerQ = "Start";
        };
        model.questions = function (appraisal) {


            model.questionsAns = appraisal.AppQuest;
            model.ManagerQ = "Start";
            model.AppraiseView2 = appraisal.AppStatus;


        };


        model.switchOff = function () {


            model.questionsAns = null;
        };
        model.saveQuestions = function (questions) {
            model.tick = false;
            model.savingApp = true;

            var config = {

            }

            $http.post('/Umbraco/Api/AppraisalMember/SaveAppraisal/', questions, config)
               .success(function (data, status) {
                   toastr.success("Saved Successfully");
                   model.savingApp = false;
                   model.tick = true;

               })
            //model.questionsAns=appraisal.AppQuest;
        };

        model.submitQuestions = function (questions) {
            console.log(questions);
            model.submitting = true;
            var config = {

            }

            $http.post('/Umbraco/Api/AppraisalMember/SubmitAppraisal/', questions, config)
               .success(function (data, status) {
                   toastr.success("Appraisal Submitted Successfully");
                   model.submitting = false;


                   // model.appraisals = data;
                   for (var i = 0; i < model.appraisals.length; i++) {
                       if (model.appraisals[i].appraisalId == data.appraisalId) {
                           model.appraisals[i].AppStatus = data.AppStatus;

                       }
                   }
                   model.questionsAns = null;
                   model.comments = null;


               })

        };

        model.setCompleted = function (questions) {
            console.log(questions);
            var config = {

            }

            $http.post('/Umbraco/Api/AppraisalMember/Completed/', questions, config)
               .success(function (data, status) {
                   toastr.success("Appraisal Submitted Successfully ");
                   //alert("Appraisal Submitted Successfully ");
                


                   // model.appraisals = data;
                   for (var i = 0; i < model.appraisals.length; i++) {
                       if (model.appraisals[i].appraisalId == data.appraisalId) {
                           model.appraisals[i].AppStatus = data.AppStatus;

                       }
                   }
                   model.questionsAns = null;
                   model.comments = null;


               })

        };


        model.$onInit = function () {



            var config = {

            }


            var userId = $location.search().userid;
            var users = [{ "userId": userId }]
            var AppId = $location.search().Appraisalid;

            $http.post('/Umbraco/Api/AppraisalMember/AppraisalmUser/', users, config)
              .success(function (data, status) {

                  model.appLoading = false;
                  model.appraisals = data.mAppraisals;

                  // console.log(data.mAppraisals[0])
                  model.users = data;

                  var appraisalId = 0;

                  if (AppId) {
                      for (var i = 0; i < model.appraisals.length; i++) {
                          appraisalId = model.appraisals[i].Id;
                          if (appraisalId == AppId) {
                              model.questionsAns = model.appraisals[i].AppQuest;
                              model.AppraiseView2 = model.appraisals[i].AppStatus;
                              if (model.AppraiseView2 == 4) {
                                  model.comments = "stuff";
                              }
                              ;
                          }

                          //  console.log(model.appraisals[i]);
                      }

                  }

              }).
        error(function (data, status) {
            model.appLoading = false;
            model.appError="Error loading users, please click Manage All Appraisals from the left hand menu"
        });



        };
    }

    module.component("userApp", {
        templateUrl: "/Appraisal/UserAppraisal.html",
        controllerAs: "model",
        controller: ["$http", "$location", controller]

    });
}());