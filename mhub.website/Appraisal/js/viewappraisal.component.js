(function(){

	"use strict";

	var module = angular.module("vApprais");
     
     function fetchMovies($http){
     	return $http.get("/Umbraco/Api/AppraisalMember/Get/")
     	.then(function(response)
{

	return response.data;
}
     		)
     }

	 function controller ($http) {
       var model=this;
       model.questionAns=null;
        model.comments=null;

       model.appraisals=[ ];
		model.message = "Hello test";
		
		 model.questions = function(appraisal){

				//alert(model.questionsAns);
				model.questionsAns=appraisal.questions;
			};
			
		//alert(model.appraisals)
		model.$onInit = function(){

fetchMovies($http).then(function(appraisals){

	model.appraisals=appraisals;
	
	//alert(appraisals.appStatus);
});


 
/*model.questions = function(){

				alert("hello");
			}*/
		};
	}

	module.component("appList",{
	templateUrl:"../AppraisalList2.html",
	controllerAs: "model",
	controller: ["$http", controller]

});
}());