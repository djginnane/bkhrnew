(function(){

	"use strict";

	var module = angular.module("vApprais", ['ui.bootstrap']);
	
     
     function fetchMovies($http){
     	return $http.get("/Umbraco/Api/AppraisalMember/Get/")
     	.then(function(response)
{
     	  
     	    // console.log(response.data);
	return response.data;
}
     		)
     }

	 function controller ($http) {
       var model=this;
       model.questionAns=null;
       model.comments = null;
       model.past = null;
       model.submitting = false;
       model.savingApp = false;
       model.tick = false;
       model.appLoading= true;

       model.appraisals=[ ];
	    
       model.questionsPast = function (appraisal) {
           model.past = 1;
         

           model.questionsAns = appraisal.AppQuest;
       };
		
		 model.questions = function(appraisal){

		
		     model.questionsAns = appraisal.AppQuest;
		     model.past = 0;
			};
		 model.saveQuestions = function (questions) {
		     model.tick = false;
			     model.savingApp = true;
                   
		            var config = {

		  }
		 
		  $http.post('/Umbraco/Api/AppraisalMember/SaveAppraisal/', questions, config)
             .success(function (data, status) {
              
                 toastr.success("Appraisal Saved Successfully ");
                 model.savingApp = false;
                 model.tick = true;
               
             })
				
			};
			 model.submitQuestions = function (questions) {
			     console.log(questions);
			     model.submitting = true;
			 	  var config = {

		  }
		 
		  $http.post('/Umbraco/Api/AppraisalMember/SubmitAppraisal/', questions, config)
             .success(function (data, status) {
                
                 fetchMovies($http).then(function (appraisals) {
                     toastr.success("Appraisal Submitted Successfully ");
                    
                     model.submitting = false;

	model.appraisals=appraisals;
	model.questionsAns=null;
	
	
});

               
               
             })
		          
		};
			
		
		model.$onInit = function(){

fetchMovies($http).then(function(appraisals){

    model.appraisals = appraisals;
    model.appLoading = false;
	//console.log(appraisals)
	
});


 

		};
	}

	module.component("appList",{
	templateUrl:"/Appraisal/AppraisalList.html",
	controllerAs: "model",
	controller: ["$http", controller]

});
}());