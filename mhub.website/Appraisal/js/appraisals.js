var app = angular.module('appraisals-app', ['ui.bootstrap']);

app.controller('appTemplate', function($scope, $http, $modal, $log) {
   
    $scope.questions = [];
    $scope.qtemplates = [];
    $scope.elDetails = {};
    $scope.cEv = {};
    $scope.cIn = {};
    $scope.cIn.index2 ="";
    $scope.mInst = {};

    $http.get('/Umbraco/Api/AppraisalTemp/Get/').success(function (data) {
    //    console.log(data);
        $scope.qtemplates = data;

    });
    // $scope.qtemplates = [];
    // $scope.test = [{}];

    $scope.addQuestion = function (questions) {


        {
            //alert($scope.questions);
            if ($scope.questions == "") {


                $scope.questions.push({
                    QType: "number",
                    QWording: "What was your target this appraisal period?",
                    QSection: 1,// new Date(y, m, 29)
                    QCalc: 1

                },
                 {
                     QType: "number",
                     QWording: "What did you achieve this appraisal period?",
                     QSection: 1,
                     QCalc: 2 // new Date(y, m, 29)

                 }

                 );
            }

        }

        $scope.questions.push({
            QType: questions.type,
            QWording: questions.text,
            QSection: questions.section// new Date(y, m, 29)

        });

    }

      $scope.removeTemplate = function (index) {

        $scope.questions.splice(index, 1);

    }

    $scope.removeQuestion = function (index) {
        
        $scope.questions.splice(index, 1);

    }
    $scope.setIndex = function (index) {

        $scope.cIn.index2 = angular.copy(index);
        // alert($scope.cEv.index2);
       

    }
    $scope.saveTempChanges = function (tempDetails) {
        console.log("gets here");
        var config = {};
        $http.post('/Umbraco/Api/AppraisalTemp/saveChanges/', tempDetails, config)
       .success(function (data, status) {
        
           toastr.success("Template Updated Successfully");
       });

    }
    $scope.deleteQuestion = function (tempDetails) {
        var tId = $scope.elDetails.Id;
         
        var index2 = $scope.cIn.index2;
      
        var config = {};
        var eModel = {"key":tId,"value":""};
        $http.post('/Umbraco/Api/AppraisalTemp/DeleteQuestion/', eModel, config)
         .success(function (data, status) {
             toastr.success("Question Deleted");
             $scope.qtemplates[index2].AppQuest.splice($scope.cEv, 1);
         });
    }
    $scope.deleteTemplate = function (tempDetails)
    {
        var tId =$scope.elDetails.Id;

      
        var config = {};
        var eModel = {"key":tId,"value":""};
        $http.post('/Umbraco/Api/AppraisalTemp/GetDelete/', eModel, config)
         .success(function (data, status) {
             toastr.success("Template Deleted");
             $scope.qtemplates.splice($scope.cEv, 1);
      //  $http.get('/Umbraco/Api/AppraisalTemp/GetDelete/'+tId).success(function (data) {
         

    });
    }
    $scope.addTemplate = function (qtemplates) {
        TemplateQuestions = $scope.questions;
      //  console.log(TemplateQuestions);
        // alert(qtemplates.name);
        var data = {
            TemplateName: qtemplates.name,
            Templatedescription: "Description",
            AppQuest: TemplateQuestions
        };
       // $scope.qtemplates.push({ TemplateName: "hello", Id: 97,});
        var config = {

        }
        var tempId = 0;
        // alert(qtemplates.name);
        $http.post('/Umbraco/Api/AppraisalTemp/StoreAppraisal/', data, config)
           .success(function (data1, status) {
               // alert("success");
               $scope.qtemplates.push({
                   Id: data1.Id,
                   TemplateName: qtemplates.name,
                   AppQuest: data1.AppQuest// new Date(y, m, 29)
                   
               });
               
       

        });
       
        $scope.questions = [];
      

     

    }
    /* Modal Code*/
    $scope.items = ['item1', 'item2', 'item3'];
  


    $scope.animationsEnabled = true;
    $scope.openModal = function (size,activeEvent,index) {
         $scope.elDetails = activeEvent;
         $scope.cEv = index;
     
        $scope.modalInstance = $modal.open({
            templateUrl: 'myTestModal.tmpl.html',
            size: size,
            scope: $scope
        });
    }
    $scope.openModal1 = function (size, activeEvent, index, index2) {
        $scope.elDetails = activeEvent;
        $scope.cEv = index;
        
      //  $scope.cEv.index2 = index2;

        $scope.modalInstance = $modal.open({
            templateUrl: 'myTestModal.tmpl.html',
            size: size,
            scope: $scope
        });
    }


    $scope.close = function () {
        $scope.modalInstance.dismiss();//$scope.modalInstance.close() also works I think
    };
    // $scope.qtemplates =[];	
});// JavaScript Document