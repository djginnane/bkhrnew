﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web.WebApi;

public class SocialController : UmbracoApiController
{
    private SocialRepository _socialRepository;

    public SocialController()
    {
        _socialRepository = new SocialRepository();
    }

    [HttpPost]
    public HttpResponseMessage PostMessage(IntranetMessage message)
    {
        try
        {
            int currentMemberId = Members.GetCurrentMemberId();
            IMember currentMember = Services.MemberService.GetById(currentMemberId);

            if (currentMember == null)
            {
                return Request.CreateUserNoAccessResponse();
            }

            var savedMessage = _socialRepository.SaveMessage(SocialMessageType.Text, currentMemberId, message.MessageContent);

            if (savedMessage != null)
            {
                var responseMessage = IntranetMessage.From(savedMessage);
                return Request.CreateResponse(HttpStatusCode.OK, responseMessage, new JsonMediaTypeFormatter());
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<SocialController>("SocialController.PostMessage():", e);
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Sorry, but an error just occured. We are notified and will look into it.");
        }

        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No messages found");
    }

    [HttpGet]
    public HttpResponseMessage GetMessages()
    {
        try
        {
            int currentMemberId = Members.GetCurrentMemberId();
            IMember currentMember = Services.MemberService.GetById(currentMemberId);

            if (currentMember == null)
            {
                return Request.CreateUserNoAccessResponse();
            }

            IEnumerable<SocialMessage> messages = _socialRepository.GetMessagesByMember(currentMemberId);

            if (messages != null)
            {
                var jsonMessages = messages.Select(msg => IntranetMessage.From(msg));

                return Request.CreateResponse(HttpStatusCode.OK, jsonMessages, new JsonMediaTypeFormatter());
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<SocialController>("SocialController.GetMessages():", e);
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Sorry, but an error just occured. We are notified and will look into it.");
        }

        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No messages found");
    }
}