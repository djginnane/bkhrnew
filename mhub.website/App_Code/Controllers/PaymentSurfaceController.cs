﻿using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Web.Mvc;
using Umbraco.Core.Models;
using Newtonsoft.Json;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;


/// <summary>
/// Summary description for PaymentSurfaceController
/// </summary>
public class PaymentSurfaceController : SurfaceController
{

    [HttpPost]
    public ActionResult UploadPayments(eLearningModel model)//,List<InsuredPerson> Insured_Persons)
    {
        string fileName = model.value;
        var member = MhubMemberHelper.LoggedInMember;
        var db = ApplicationContext.Current.DatabaseContext.Database;
       // var hub = ContentHelpers.CurrentHub(member);
        if (!ModelState.IsValid)
        {
            ViewBag.Message = "Could not upload file, please ensure it is a csv file";
            return CurrentUmbracoPage();
        }

        for (int i = 0; i < Request.Files.Count; i++)
        {
            var file = Request.Files[i];

            if (file != null && file.ContentLength > 0)
            {
                fileName = Path.GetFileName(file.FileName);
                var extension = Path.GetExtension(fileName);
                var File_Id = Guid.NewGuid();




                var dirPath = Path.Combine(Server.MapPath("~/App_Data/TEMP/FileUploads/"));

                if (!Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }
                fileName = File_Id.ToString();
                var path = dirPath + fileName + extension;

                file.SaveAs(path);
                var paymentList=CFUploadHelper.Main(path);
                var Completed = CFUploadHelper.AddPayments(paymentList);
            }
        }
        TempData["customerval"] = "Employees Uploaded Successfully.";
        return RedirectToCurrentUmbracoPage();
    }
    public PaymentSurfaceController()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}