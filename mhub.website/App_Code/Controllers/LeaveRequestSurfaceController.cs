﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Core;
using Umbraco.Web.Mvc;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for LeaveRequestSurfaceController
/// </summary>
///
public class LeaveRequestSurfaceController : SurfaceController
{
    [HttpPost]
    public ActionResult CreateLeave(LeaveRequestStart model)
    {
        if (!ModelState.IsValid)
        {
            ViewBag.Message = "Form was was not submitted Successfully, Please review";
            return CurrentUmbracoPage();
        }

        String diff2 = (model.LeaveEnd - model.LeaveStart).TotalDays.ToString();
        int diffVal = Int32.Parse(diff2);

        System.Diagnostics.Debug.WriteLine("Gets to surface Controller");
        var requestGuid = new Guid();
        requestGuid=Guid.NewGuid();

        var db = ApplicationContext.Current.DatabaseContext.Database;
        if (diffVal < 30)
        {
            System.Diagnostics.Debug.WriteLine("Gets to database input");

            DateTime leaveDate = model.LeaveStart;
            DateTime createDate = DateTime.Now;
            Boolean addHalfDay = model.HalfDayStart;
            Boolean addHalfDay2 = model.HalfDayEnd;
            Boolean finalHalfDay = false;
            DateTime leaveEnd = model.LeaveEnd;
            DateTime leaveStart = leaveDate;
            string[] array = model.LeaveType.Split(':');
            string leaveName = array[0];
            string value = array[1];
            int leaveValue;
            int User_Id = Convert.ToInt16(model.user_id);

            var member = MhubMemberHelper.LoggedInMember;
            if (int.TryParse(value, out leaveValue))
            {
                // String is not a number.
                //leaveValue = value;
            }
            else { leaveValue = 0; }
            // int leaveValue= Convert
            int x = 0;
            int lApproved = 0;
            bool subByMan = false;
            if (User_Id!=member.Id)
            {
                lApproved = 1;
                subByMan = true;
            }
            var leaveRequestList = new List<LeaveRequest>();
            do
            {
                var dayOf1 = leaveDate.DayOfWeek;
                String dayOfW = dayOf1.ToString();
                if (x == 0)
                {
                    finalHalfDay = addHalfDay;
                }
                else if (x == diffVal)
                { finalHalfDay = addHalfDay2; }
                else
                {
                    finalHalfDay = false;
                }
                //don't record Saturdays or Sundays
                if (!(dayOfW == "Saturday") && !(dayOfW == "Sunday"))
                {
                    var leaveRequest = new LeaveRequest

                    {
                        LeaveDay = leaveDate,
                        LeaveType = leaveName,
                        LeaveCounted = leaveValue,
                        MemberId = User_Id,
                        HalfDay = finalHalfDay,
                        LeaveStart = leaveStart,
                        LeaveEnd = leaveEnd,
                        CreatedAt = createDate,
                        Comment = model.Comment,
                        hubRoot = member.GetValue<int>("hub"),
                        ApprovedLeave = lApproved,
                        Unique_Approver = requestGuid


                    };
                    
                    leaveRequestList.Add(leaveRequest);
                
                }
                leaveDate = (leaveDate).AddDays(1);
                x++;
            } while (x <= diffVal);
            leaveRequestList = LeaveHelpers.RemovePublicHol(leaveRequestList, member);
            foreach (var LR in leaveRequestList)
            {
                var result = db.Insert(LR);
            }

            string eMessage = LeaveHelpers.LeaveEmailMessage(member, model);
            string eSubject = "Leave request requires your approval";
            var SubmittedFor = ApplicationContext.Current.Services.MemberService.GetById(User_Id);
            var directManagerId =SubmittedFor.GetValue<int>("lineManager");
            var lineMan = ApplicationContext.Current.Services.MemberService.GetById(directManagerId);
            string eMail = "defaultemail@hotmail.com";
            if (ApplicationContext.Current.Services.MemberService.Exists(directManagerId))
            {
                eMail = directManagerId==User_Id? "defaultemail@hotmail.com" : lineMan.Email;

               
            }
        
            if (subByMan)
            {  eSubject = "Leave request submitted for "+SubmittedFor.Name+" successfully"; }
            else {
                AlertHelpers.emailAlert(eMessage, eMail, eSubject);
            }
            TempData["tempval"] = "Your leave request has been submitted to your manager and is pending review";
        }
        else
        {
            TempData["tempvalErr"] = "Leave Period is above the maximum allowed";
        }





        return RedirectToCurrentUmbracoPage();
    }


}