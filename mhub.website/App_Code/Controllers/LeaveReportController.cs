﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.WebApi;
using System.Net.Http;
using System.Net;
using System.Net.Http.Formatting;
using Umbraco.Core;
using UmbracoWithMvc.Controllers;
using UmbracoWithMvc.Entities;
using System.Web.Http;
using System.Web.Security;
using Umbraco.Core.Models;

/// <summary>
/// Summary description for LeaveReportController
/// </summary>
public class LeaveReportController : UmbracoApiController
{

    public HttpResponseMessage Get()
    {
        var memberList = MhubMemberHelper.GetAllMemberList();

        DateTime CurrentDate = DateTime.Today;
        var CurrentYear = CurrentDate.Year;
        // TODO: Add public holidays as array into query ie where leaveDate not in (array of dates)
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select memberid,leavetype, count(memberid) as TotalDays,sum(case  when halfDay='true' then 1 end) as LeaveCounted from mhubleaveRequest where YEAR(leaveDay)=@0 and ApprovedLeave=1 and MemberId in (@1) group by MemberId, leavetype order by memberid";
        var LReq = db.Fetch<LeaveRequest>(query, CurrentYear, memberList.ToArray());
        var remainLeaveQ = "select memberid, count(memberid) as TotalDays,sum(case  when halfDay='true' then 1 end) as LeaveCounted from mhubleaveRequest where YEAR(leaveDay)=@0 and ApprovedLeave=1 and LeaveCounted=1 and MemberId in (@1) group by MemberId,leaveCounted order by memberid";
        var remaining= db.Fetch<LeaveRequest>(remainLeaveQ, CurrentYear, memberList.ToArray());
        var query2 = "select leavetype as [LeaveType], count(leavetype) as [TotalDays] from mhubleaveRequest where YEAR(leaveDay)>=@0 and ApprovedLeave=1 and MemberId in (@1) group by leavetype order by [TotalDays] desc";
        var LeaveTypes = db.Fetch<LeaveCounts>(query2, CurrentYear, memberList.ToArray());
        var RepStruct = LeaveHelpers.ReportStructure(LReq, LeaveTypes, remaining);
        var Result = LeaveHelpers.GenerateLeaveReport(LReq, RepStruct);
        var ResultAll = LeaveHelpers.AddNoLeave(Result, LeaveTypes, memberList);
       

        return Request.CreateResponse(HttpStatusCode.OK, ResultAll, new JsonMediaTypeFormatter());
    }

    [HttpPost]
    public HttpResponseMessage GetDRange(LeaveRangeDates data)
    {
        // Add User validation, which values to get based on manager type


        var memberList = MhubMemberHelper.GetAllMemberList();
        Console.WriteLine(memberList.ToArray());
        var dstart = data.StartDate;
        DateTime dateTest = DateTime.Today;
        var db = ApplicationContext.Current.DatabaseContext.Database;
        //   var query = "select memberid,leavetype, count(memberid) as TotalDays from mhubleaveRequest where leaveStart>@0 and leaveStart<@1 and ApprovedLeave=1 and MemberId in (@2) group by MemberId, leavetype ";
        var query = "select memberid,leavetype, count(memberid) as TotalDays, sum(case  when halfDay='true' then 1 end) as LeaveCounted  from mhubleaveRequest where  ApprovedLeave=1 and leaveDay>@0 and leaveDay<=@1 and MemberId in (@2) group by MemberId, leavetype order by MemberId";

        var LReq = db.Fetch<LeaveRequest>(query, dstart, data.EndDate, memberList.ToArray());
        //  var query2 = "select leavetype as [LeaveType], count(leavetype) as [TotalDays] from mhubleaveRequest where leaveStart>@0 and leaveStart<@1 and ApprovedLeave=1 and MemberId in (@2) group by leavetype order by [TotalDays] desc";
        var remainLeaveQ = "select memberid, count(memberid) as TotalDays,sum(case  when halfDay='true' then 1 end) as LeaveCounted from mhubleaveRequest where YEAR(leaveDay)=@0 and ApprovedLeave=1 and LeaveCounted=1 and MemberId in (@1) group by MemberId,leaveCounted order by memberid";
        var remaining = db.Fetch<LeaveRequest>(remainLeaveQ, dateTest.Year, memberList.ToArray());
        var query2 = "select leavetype as [LeaveType], count(leavetype) as [TotalDays] from mhubleaveRequest where  ApprovedLeave=1 and leaveDay>@0 and  leaveDay<@1 and MemberId in (@2) group by leavetype order by [TotalDays] desc";
        //var LeaveTypes = db.Fetch<LeaveCounts>(query2, dstart, data.EndDate,1527);
        var LeaveTypes = db.Fetch<LeaveCounts>(query2, dstart, data.EndDate, memberList.ToArray());
        var Result = new List<LeaveReport>();
        if (LReq.Count != 0)
        {
          //  var LeaveTypes = db.Fetch<LeaveCounts>(query2, CurrentYear, memberList.ToArray());
          //  var RepStruct = LeaveHelpers.ReportStructure(LReq, LeaveTypes, remaining);
            //var Result = LeaveHelpers.GenerateLeaveReport(LReq, RepStruct);
             var RepStruct = LeaveHelpers.ReportStructure(LReq, LeaveTypes, remaining);
            Result = LeaveHelpers.GenerateLeaveReport(LReq, RepStruct);
        }
        else
        {
            Result = null;
        }
        var ResultAll = LeaveHelpers.AddNoLeave(Result, LeaveTypes, memberList);
        return Request.CreateResponse(HttpStatusCode.OK, ResultAll, new JsonMediaTypeFormatter());
    }

    /// <summary>
    /// gets user leave data for individual leave reports
    /// </summary>

    [HttpPost]
    public HttpResponseMessage GetUserRange(LeaveRangeDates data)
    {
        // Add User validation, which values to get based on manager type
        var dstart = data.StartDate;
        var MemberId = data.MemberId;
        if (data.MemberId == 33333)
        {
            MemberId = MhubMemberHelper.LoggedInMember.Id;
            data.MemberId = MhubMemberHelper.LoggedInMember.Id;
        }

        //TODO: Return empty list whith username and Name if they have no leave
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select leavetype, count(leavetype) as TotalDays,sum(case  when halfDay='true' then 1 end) as ApprovedLeave, leaveCounted from mhubleaveRequest where leaveDay>@0 and leaveDay<@1 and ApprovedLeave=1 and MemberId=@2 group by leavetype,leavecounted";
        var LReq = db.Fetch<LeaveRequest>(query, dstart, data.EndDate, MemberId);
        var query2 = "select leavetype, leavetype, leaveCounted, leaveDay, LeaveStart, halfday, comment from mhubleaveRequest where leaveDay>@0 and leaveDay<@1 and ApprovedLeave=1 and MemberId=@2";
        var LReq2 = db.Fetch<LeaveRequest>(query2, dstart, data.EndDate, MemberId);

        var result = new LeaveUserTotals();

        result = LeaveHelpers.UserLeaveReport(LReq, LReq2, data);


        return Request.CreateResponse(HttpStatusCode.OK, result, new JsonMediaTypeFormatter());
    }
    [HttpGet]
    public HttpResponseMessage UnpaidReport()
    {
        AlertHelpers.UnpaidLeaveEmail();
        return Request.CreateResponse(HttpStatusCode.OK, 1, new JsonMediaTypeFormatter());
    }

    public LeaveReportController()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}