﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.WebApi;
using Umbraco.Core.Logging;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using UmbracoWithMvc.Controllers;
using Newtonsoft.Json;
using System.Web.Security;
using Umbraco.Core;

public class MemberController : UmbracoApiController
{
    public MemberController()
    {
    }

    [HttpGet]
    [ActionName("getEmployeeEntryModel")]
    public HttpResponseMessage GetEmployeeEntryModel(int pageId, int? employeeId)
    {
        var result = new HttpResponseMessage();

        try
        {
            var currentPage = Services.ContentService.GetById(pageId);

            if (currentPage != null)
            {
                var rootPage = currentPage.Ancestors().FirstOrDefault();

                if (rootPage != null)
                {
                    var model = new MemberEntryModel();

                    // If memberID is passed, get the employee data from memberservice and populate properties.
                    if (employeeId.HasValue)
                    {
                        var member = Services.MemberService.GetById(employeeId.Value);

                        if (member != null)
                        {
                            model.Employee = new Employee(member);
                        }
                    }

                    model.Members = GetMembers();
                    model.Appraisals = GetAppraisals(rootPage);
                    model.Roles = GetRoles(2135);
                    model.Positions = GetPositions(rootPage);
                    model.Branches = GetBranches(rootPage);
                    model.Divisions = GetDivisions(rootPage);
                   

                    return Request.CreateResponse(HttpStatusCode.OK, model, new JsonMediaTypeFormatter());
                }
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<MemberController>("MemberController.GetAll():", e);
            return Request.CreateErrorResponse(HttpStatusCode.NoContent, e);
        }

        return result;
    }

    private IEnumerable<object> GetMembers()
    {
        IEnumerable<object> result = new List<object>();

        // Gets the list of members by memberType
        var loggedMember = MhubMemberHelper.LoggedInMember;
        var rootHub = loggedMember.GetValue<int>("hub");
        var members = Services.MemberService.GetMembersByPropertyValue("hub", rootHub); 
      //  var requestCache = ApplicationContext.Current.ApplicationCache.RuntimeCache;//RequestCache;
       // IEnumerable<IMember> members = (IEnumerable<IMember>)requestCache.GetCacheItem("hubMembers" + rootId.ToString());
        if (members != null)
        {
            result = members.Select(x => new
            {
                id = x.Id,
                name = x.Name,
                username = x.Username,
                email = x.Email,
                InActive=x.IsApproved
            })
            .OrderBy(x => x.name)
            .ToList();
        }

        return result;
    }

    private IEnumerable<AppraisalForms> GetAppraisals(IContent contentObj)
    {
        IEnumerable<AppraisalForms> result = new List<AppraisalForms>();

        using (var db = DatabaseContext.Database)
        {
            result = db.Fetch<AppraisalForms>("select * from mhubAppraisalForms where rootNode = @0", contentObj.Id);
        }

        return result;
    }

    private IEnumerable<object> GetRoles(int dataTypeId)
    {
        IEnumerable<object> result = new List<object>();

        // Gets the list of roles from DataTypeService
        var roles = Services.DataTypeService.GetPreValuesCollectionByDataTypeId(dataTypeId);
        if (roles != null)
        {
            result = roles.PreValuesAsDictionary.Select(x => new
            {
                id = x.Value.Id.ToString(),
                value = x.Value.Value
            })
            .OrderBy(x => x.value)
            .ToList();
        }

        return result;
    }

    private IEnumerable<string> GetPositions(IContent contentObj)
    {
        IEnumerable<string> result = new List<string>();

        // Get the list of branches from rootNode
        var companyPositions = contentObj.GetValue<string>("companyPositions");
        if (companyPositions != null)
        {
            result = companyPositions.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
        }

        return result;
    }

    private IEnumerable<string> GetBranches(IContent contentObj)
    {
        IEnumerable<string> result = new List<string>();

        // Get the list of branches from rootNode
        var branches = contentObj.GetValue<string>("branches");
        if (branches != null)
        {
            result = branches.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
        }

        return result;
    }

    private IEnumerable<string> GetDivisions(IContent contentObj)
    {
        IEnumerable<string> result = new List<string>();

        // Get the list of divisions from rootNode
        var divisions = contentObj.GetValue<string>("divisions");
        if (divisions != null)
        {
            result = divisions.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
        }

        return result;
    }

    [HttpPost]
    [ActionName("submitEmployee")]
    public HttpResponseMessage SubmitEmployee(int pageId, [FromBody] Employee employee)
    {
        try
        {
            var currentPage = Services.ContentService.GetById(pageId);

            if (currentPage != null && employee != null)
            {
                IMember member = null;
                if (employee.MemberId > 0)
                {
                    // Get an existing user. Properties will be edited below, before persisted to the database
                    member = Services.MemberService.GetById(employee.MemberId);

                    // Check if they are trying to change username / email, and if so, check for existence already or not
                    if (member.Email != employee.Email)
                    {
                        var memberByEmail = Services.MemberService.GetByEmail(employee.Email);
                        if (memberByEmail != null)
                        {
                            return Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, "Email already registered");
                        }
                    }

                    if (member.Username != employee.Username)
                    {
                        var memberByUsername = Services.MemberService.GetByUsername(employee.Username);
                        if (memberByUsername != null)
                        {
                            return Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, "Username already registered");
                        }
                    }
                }
                else
                {
                    var memberByEmail = Services.MemberService.GetByEmail(employee.Email);
                    if (memberByEmail != null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, "Email already registered");
                    }

                    var memberByUsername = Services.MemberService.GetByUsername(employee.Username);
                    if (memberByUsername != null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, "Username already registered");
                    }

                    // No MemberID, so will insert this employee as a new member in the database.
                    member = Services.MemberService.CreateMemberWithIdentity(employee.Username, employee.Email, employee.Name, "staff");
                    member.IsApproved = true;

                    var rootPage = currentPage.Ancestors().FirstOrDefault();
                    member.SetValue("hub", rootPage.Id);

                    var lineManager = Members.GetCurrentMemberId();
                    member.SetValue("lineManager", lineManager);

                    string passwd = employee.Birthday.ToLocalTime().ToString("ddMMyyyy"); // Sets the password to pattern of Month (1-12) + Day (1-31) + 4 digit Year (ie. 2001). No spaces or such (ie. 9241980)
                    Services.MemberService.SavePassword(member, passwd);

                    var rootpage = Umbraco.TypedContent(rootPage.Id);
                    string rootName = rootpage.Name;
                    int mId = member.Id;

                    ApplicationContext.Current.Services.MemberService.AssignRole(mId, rootName);
                }

                if (member != null)
                {
                    member.Username = employee.Username;
                    member.Email = employee.Email;
                    member.Name = employee.Name;
                    int LevelInt = 0;

                    bool result = int.TryParse(employee.AssessmentLevel, out LevelInt); //i now = 108  


                    member.SetValue("position", employee.Position);
                    member.SetValue("lineManager", employee.LineManager);
                    member.SetValue("roleLevel", employee.Role);
                    member.SetValue("division", employee.Division);
                    member.SetValue("branch", employee.Branch);
                    member.SetValue("birthday", employee.Birthday);
                    member.SetValue("companyStart", employee.CompanyStart.ToUniversalTime());
                    member.SetValue("careerStart", employee.CareerStart.ToUniversalTime());
                    member.SetValue("assessmentLevek", LevelInt); // Note a typo on this property. Must be fixed in Umbraco
                    member.SetValue("appraisalSchedule", employee.AppraisalSchedule);
                    member.SetValue("appraisal", employee.Appraisal);
                    // for annual leave issues
                    var currentYear = DateTime.UtcNow.Year;
                    //var annList = new List<eLearningModel>();
                    //var defaultAnnual = new eLearningModel();
                    //defaultAnnual.key = currentYear.ToString();
                    //defaultAnnual.value = "0";
                    //annList.Add(defaultAnnual);
                   var annualLeave = "{}";
                    var TempAnnualLeave = new List<eLearningModel>();
                    if (employee.AnnualLeave!=null)
                    {
                        //Add adjusted leave for the current staff if added midyear
                        if (employee.CompanyStart.Year == currentYear)
                        {
                            var annLeave = employee.AnnualLeave.ToList();
                            var annualAllocation = annLeave.Exists(x => x.key == currentYear.ToString()) ? annLeave.Find(x => x.key == currentYear.ToString()).value : "0";
                            foreach (var al in employee.AnnualLeave)
                            {
                                var anYear = new eLearningModel();
                                anYear.value = al.value;
                                anYear.key = al.key;
                                if (currentYear.ToString()==al.key)
                                {
                                    ///calculate prorata leave available for the new staff
                                    var intHol = System.Convert.ToDecimal(al.value);
                                    var activeMonths=intHol/12 * System.Convert.ToDecimal((12- employee.CareerStart.Month));
                                    var ProRataLeave = Math.Ceiling(activeMonths / 0.5m) * 0.5m;

                                    anYear.value = ProRataLeave.ToString();
                                    member.SetValue("holidayLeave", intHol);

                                }
                                TempAnnualLeave.Add(anYear);


                            }

                        }
                        else
                        {
                            TempAnnualLeave = employee.AnnualLeave.ToList();
                        }
                        annualLeave = JsonConvert.SerializeObject(TempAnnualLeave);
                       
                    }
                   

                    //else
                    //{
                    //    annualLeave= JsonConvert.SerializeObject(annList);
                    //}
                    member.SetValue("annualLeave", annualLeave);

                    if (member.IsValid())
                    {
                        Services.MemberService.Save(member);
                        return Request.CreateResponse(HttpStatusCode.OK, member, new JsonMediaTypeFormatter());
                    }

                    return Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, "Post data is not valid");
                }
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<MemberController>("MemberController.SubmitEmployee():", e);
        }

        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Could not modify emplyee. Contact support.");
    }

    [HttpPost]
    [ActionName("DeActivateUser")]
    public HttpResponseMessage DeActivateUser(int pageId, [FromBody] Employee employee)
    {
        try
        {
            var member = ApplicationContext.Services.MemberService.GetById(employee.MemberId);
            // var test = member.GetValue<bool>("inActive");
            // member.SetValue("inActive", true);
            var deactTime = DateTime.Today; 
            member.IsApproved = false;
            member.SetValue("dateInactive", deactTime);
            ApplicationContext.Services.MemberService.Save(member);
            return Request.CreateResponse(HttpStatusCode.OK, pageId, new JsonMediaTypeFormatter());
        }
        catch (Exception e)
        {
            LogHelper.Error<MemberController>("MemberController.DeActivateUser():", e);
        }
        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Could not deactivate staff. Contact support.");
    }
    [HttpPost]
    [ActionName("ActivateUser")]
    public HttpResponseMessage ActivateUser(int pageId, [FromBody] Employee employee)
    {
        try
        {
            var member = ApplicationContext.Services.MemberService.GetById(employee.MemberId);
            // var test = member.GetValue<bool>("inActive");
            member.IsApproved = true;
           // member.SetValue("inActive", false);
            ApplicationContext.Services.MemberService.Save(member);
            return Request.CreateResponse(HttpStatusCode.OK, pageId, new JsonMediaTypeFormatter());
        }
        catch (Exception e)
        {
            LogHelper.Error<MemberController>("MemberController.DeActivateUser():", e);
        }
        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Could not activate staff. Contact support.");
    }

    [HttpPost]
    [ActionName("DeleteUser")]
    public HttpResponseMessage DeleteUser(int pageId, [FromBody] Employee employee)
    {
        try
        {
            var uname=@Membership.GetUser().UserName;
            if (uname != null)
            {
                var member = ApplicationContext.Services.MemberService.GetById(employee.MemberId);
                // var test = member.GetValue<bool>("inActive");
                //member.SetValue("inActive", false);
                ApplicationContext.Services.MemberService.Delete(member);
                return Request.CreateResponse(HttpStatusCode.OK, pageId, new JsonMediaTypeFormatter());
            }
           
        }
        catch (Exception e)
        {
            LogHelper.Error<MemberController>("MemberController.DeActivateUser():", e);
        }
        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Could not delete staff. Contact support.");
    }

    [HttpPost]
    [ActionName("resetPass")]
    public HttpResponseMessage resetPass(userAssessment memReset)
    {
        var findUser = 0;
        if (Services.MemberService.Exists(memReset.Username))
        {
            var MemberRe = Services.MemberService.GetByUsername(memReset.Username);
            if (MemberRe.Email.ToLower() == memReset.Email.ToLower())
            {
                //TODO: change password to the commented out string, then send email with current password, then set the commented out restPass value to true.
                findUser = 1;
                string password = Membership.GeneratePassword(8, 0);

                Services.MemberService.SavePassword(MemberRe, password);


                //  MemberRe.SetValue("resetPass", true);
                //  Services.MemberService.Save(MemberRe);

                //TODO - make this into a html email.
                var eMessage = "Password Reset to " + password + " \r\n Please login to change password, the password must be typed and not copy and pasted";
                AlertHelpers.emailAlert(eMessage, MemberRe.Email, "Password reset");
                MemberRe.SetValue("passwordChecker", 1);
                MemberRe.IsLockedOut = false;
                MemberRe.FailedPasswordAttempts = 0;
                Services.MemberService.Save(MemberRe);

            }
            else
            {
                findUser = 2;
            }
        }
        return Request.CreateResponse(HttpStatusCode.OK, findUser, new JsonMediaTypeFormatter());
    }

    [HttpPost]
    [ActionName("resetTemp")]
    public HttpResponseMessage resetTemp(userAssessment memReset)
    {
        //TODO:Uncomment two lines below
        var MemberRe = MhubMemberHelper.LoggedInMember;

        var statusId = 0;
        if (!String.IsNullOrWhiteSpace(memReset.Password))
        {
            if (memReset.Password.Length >= 8)
            {
                Services.MemberService.SavePassword(MemberRe, memReset.Password);
                MemberRe.SetValue("passwordChecker", 0);
                Services.MemberService.Save(MemberRe);
                statusId = 1;
            }
            else
            {
                statusId = 2;
            }
            //  var loggedUsername = MemberRe.Username;
            //  var currentSession = HttpContext.Current.Session;
            // currentSession["MemberProfile"] = ApplicationContext.Current.Services.MemberService.GetByUsername(loggedUsername);
        }


        //  MemberRe.SetValue("resetPass", true);
        //  Services.MemberService.Save(MemberRe);


        return Request.CreateResponse(HttpStatusCode.OK, statusId, new JsonMediaTypeFormatter());
    }

    public HttpResponseMessage GetAll()
    {
        try
        {
            var member = MhubMemberHelper.LoggedInMember;
            int totalRecords = 0;
            IEnumerable<IMember> members = MhubMemberHelper.APIGetAll2(member);//Services.MemberService.GetAll(0, Int32.MaxValue, out totalRecords);

            if (members != null)
            {
                MemberModel model = new MemberModel();
                model.TotalRecords = totalRecords;
                model.Members = members.Select(x => new Employee(x, Services.MediaService));

                return Request.CreateResponse(HttpStatusCode.OK, model, new JsonMediaTypeFormatter());
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<MemberController>("MemberController.GetAll():", e);
            return Request.CreateErrorResponse(HttpStatusCode.NoContent, e);
        }

        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "No content");
    }

    //Add By Kittanop For get member with group name
    public HttpResponseMessage GetAllMembers()
    {
        try
        {
            IEnumerable<string> roles = Services.MemberService.GetAllRoles();
            IEnumerable<IMember> members = null;
            MemberModel model = new MemberModel();
            Dictionary<int, Employee> memberList = new Dictionary<int, Employee>();
            Employee m = null;
            Employee existsMember = null;
            //foreach (string r in roles)
            //{
            //    members = Services.MemberService.GetMembersByGroup(r);
            //    foreach(IMember im in members)
            //    {
            //        m = IntranetMember.From(im, Services.MediaService, r);
            //        if (memberList.ContainsKey(m.Id))
            //        {
            //            memberList.TryGetValue(m.Id, out existsMember);
            //            existsMember.TeamList.Add(r);
            //        }
            //        else {
            //            memberList.Add(m.Id, m);
            //        }
            //    }
            //}


            members = Services.MemberService.GetAllMembers();
            foreach (IMember im in members)
            {
                m = new Employee(im, Services.MediaService);
                if (memberList.ContainsKey(m.MemberId))
                {
                    memberList.TryGetValue(m.MemberId, out existsMember);
                    //existsMember.TeamList.Add(r);
                }
                else
                {
                    memberList.Add(m.MemberId, m);
                }
            }

            if (members != null)
            {
                model.Members = memberList.Values;

                return Request.CreateResponse(HttpStatusCode.OK, model, new JsonMediaTypeFormatter());
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<MemberController>("MemberController.GetAll():", e);
            return Request.CreateErrorResponse(HttpStatusCode.NoContent, e);
        }

        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "No content");
    }

    public HttpResponseMessage GetMembersByGroup(string groupName)
    {
        try
        {
            int totalRecords = 0;
            IEnumerable<IMember> members = Services.MemberService.GetMembersByGroup(groupName);

            if (members != null)
            {
                MemberModel model = new MemberModel();
                model.TotalRecords = totalRecords;
                model.Members = members.Select(x => new Employee(x, Services.MediaService));

                return Request.CreateResponse(HttpStatusCode.OK, model, new JsonMediaTypeFormatter());
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<MemberController>("MemberController.GetMembersByGroup():", e);
            return Request.CreateErrorResponse(HttpStatusCode.NoContent, e);
        }

        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "No content");
    }

    public HttpResponseMessage GetTeamList()
    {
        try
        {
            Dictionary<string, string[]> dic = new Dictionary<string, string[]>();

            var rootnode = MhubContentHelper.GetRootNode();
            dic.Add("branches", rootnode.GetPropertyValue<string[]>("branches"));
            dic.Add("divisions", rootnode.GetPropertyValue<string[]>("divisions"));

            return Request.CreateResponse(HttpStatusCode.OK, dic, new JsonMediaTypeFormatter());
        }
        catch (Exception e)
        {
            LogHelper.Error<MemberController>("MemberController.GetTeamList():", e);
            return Request.CreateErrorResponse(HttpStatusCode.NoContent, e);
        }

        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "No content");
    }
    [HttpGet]
    public HttpResponseMessage GetLoggedInMemberDetails()
    {
        try
        {
           var member = MhubMemberHelper.LoggedInMember;
            var membermodel = new MemberPropertiesModel();
            membermodel.username = member.Username;
            membermodel.Id = member.Id;
            membermodel.branch = member.GetValue<string>("branch");
            membermodel.location = member.GetValue<string>("location");
            membermodel.RootNode = member.GetValue<int>("hub");
            membermodel.roleLevel = member.GetValue<int>("roleLevel");


            return Request.CreateResponse(HttpStatusCode.OK, membermodel, new JsonMediaTypeFormatter());
        }
        catch (Exception e)
        {
            LogHelper.Error<MemberController>("MemberController.GetLoggedInMemberDetails():", e);
            return Request.CreateErrorResponse(HttpStatusCode.NoContent, e);
        }

      
    }

}