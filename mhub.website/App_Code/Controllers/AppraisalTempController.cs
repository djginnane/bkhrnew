﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.WebApi;
using System.Net.Http;
using System.Net;
using System.Net.Http.Formatting;
using Umbraco.Core;
using UmbracoWithMvc.Controllers;
using System.Web.Http;
using umbraco;
using Umbraco.Web;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Text;
using System.Net.Mime;
using System.Xml;
using System.IO;
using OfficeOpenXml;
using System.Data;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;



//using System.Web.Mvc;

/// <summary>
/// Summary description for AppraisalTempController
/// </summary>
public class AppraisalTempController : UmbracoApiController
{

    public HttpResponseMessage Get()
    {

        //At some point fix this to use one query to grab all questions and appraisal templates
        var db = ApplicationContext.Current.DatabaseContext.Database;

        var query = "select * from mhubAppraisalForms where RootNode =@0";
        var appraisalTable = db.Fetch<AppraisalForms>(query, MhubMemberHelper.LoggedInMember.GetValue<int>("hub"));
        var TResult = new List<AppraisalForms>();
        var AppQuest = new List<QuestionSet>();
        foreach (var entry in appraisalTable)
        {
            //  AppQuest = db.Fetch<QuestionSet>("Where QTemplate=@0 and RootNode=@1", entry.Id,Member.GetValue<int>("hub"));
            AppQuest = db.Fetch<QuestionSet>("Where QTemplate=@0 ", entry.Id);
            TResult.Add(new AppraisalForms()
            {


                Id = entry.Id,
                TemplateName = entry.TemplateName,
                AppQuest = AppQuest
                // start = entry.LeaveDay,
                //end = entry.LeaveDay
                // start = entry.LeaveStart,
                // end = entry.LeaveEnd




            });
        }




        return Request.CreateResponse(HttpStatusCode.OK, TResult, new JsonMediaTypeFormatter());
    }
    [HttpPost]
    public HttpResponseMessage GetDelete(eLearningModel tempId)
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select * from mhubAppraisalForms where Id=@0";
        var appraisalTable = db.Single<AppraisalForms>(query, tempId.key);
        db.Delete(appraisalTable);
        var query2 = "select * from mhubquestionSet where QTemplate=@0";
        var questionTable = db.Fetch<QuestionSet>(query2, tempId.key);
        foreach (var question in questionTable)
        {
            db.Delete(question);
        }

        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
    }

    [HttpPost]
    public HttpResponseMessage saveChanges(AppraisalForms questionChanges)
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        foreach (var q in questionChanges.AppQuest)
        {
            var query2 = "select * from mhubquestionSet where Id=@0";
            var questionTable = db.Single<QuestionSet>(query2, q.Id);
            questionTable.QWording = q.QWording;
            questionTable.QSection = q.QSection;
            db.Save(questionTable);

        }

        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
    }
    [HttpPost]
    public HttpResponseMessage DeleteQuestion(eLearningModel tempId)
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;

        var query2 = "select * from mhubquestionSet where Id=@0";
        var questionTable = db.Single<QuestionSet>(query2, tempId.key);
        db.Delete(questionTable);
        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
    }

    [HttpPost]
    public HttpResponseMessage StoreAppraisal(AppraisalForms data)
    //public HttpResponseMessage ApproveLeave()

    {

        var db = ApplicationContext.Current.DatabaseContext.Database;
        var AppForm = new AppraisalForms();
        AppForm.AppraisalQuestions = "Possibly Put Json Here";
        AppForm.TemplateName = data.TemplateName;
        AppForm.TemplateDescription = "the description";
        AppForm.CreatedDate = DateTime.UtcNow;
        AppForm.ModifiedDate = DateTime.UtcNow;
        AppForm.AppQuest = data.AppQuest;
        AppForm.RootNode = MhubMemberHelper.LoggedInMember.GetValue<int>("hub");
        var qResult = new QuestionSet();

        var result = db.Insert("mhubAppraisalForms", "Id", AppForm);
        foreach (var entry in data.AppQuest)
        {
            qResult = new QuestionSet();
            qResult.QWording = entry.QWording;
            qResult.QType = entry.QType;
            qResult.QTemplate = AppForm.Id;
            qResult.QSection = entry.QSection;
            qResult.QCalc = entry.QCalc;

            db.Insert("mhubQuestionSet", "Id", qResult);

        }


        return Request.CreateResponse(HttpStatusCode.OK, AppForm);
    }

    /// <summary>
    /// Test method for getting a list of Members as an array for querying. Can Delete
    /// </summary>
    [HttpGet]
    public HttpResponseMessage GetIn()
    {
        int[] array;
        array = new int[2];
        array[0] = 36;
        array[1] = 25;
        //  var member = ApplicationContext.Current.Services.MemberService.GetById(1527);
        //  var mList = MhubMemberHelper.APIGetAll(member);
        //  var mIdList = MhubMemberHelper.GetarraylistI(mList);


        //  var mList = MhubMemberHelper.APIGetAll(member);
        //   var rootId = member.GetValue<int>("hub");
        //   var branch = member.GetValue<string>("branch");
        ///   var division = member.GetValue<string>("division");
        //  var roleLevel = member.GetValue<int>("roleLevel");

        //var members = ApplicationContext.Current.Services.MemberService.GetMembersByPropertyValue("hub", rootId);

        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select * from mhubleaverequest where memberid IN(@0)";
        //  var TResult = db.Fetch<LeaveRequest>(query, mIdList.ToArray());
        var mIdList2 = MhubMemberHelper.GetAllMemberList();
        var rLevel = MhubMemberHelper.getMemberProperty<int>("roleLevel");
        var memberService = ApplicationContext.Current.Services.MemberService;
        // member.SetValue("experience", 5);
        // memberService.Save(member);
        var testMember = MhubMemberHelper.LoggedInMember;
        // testMember.SetValue("assessmentLevek", 5);
        //memberService.Save(testMember);

        var parent = Umbraco.TypedContent(1523);

        int pID = parent.Parent.Id;
        var sibList = new List<int>();
        var contentSer = ApplicationContext.Current.Services.ContentTypeService;
        var siblings = contentSer.GetContentTypeChildren(1522);
        int childr = uQuery.GetNodesByType("trainingPage").Count();
        foreach (var children in parent.Children)
        {
            sibList.Add(children.Id);
        }

        //   var m = ApplicationContext.Current.Services.MemberService.GetById(1527);
        //   var RootHub = m.GetValue<int>("hub");
        //   var RootPage = Umbraco.TypedContent(RootHub);
        //   var TmanId = RootPage.GetProperty("trainingManager").Value;
        //   var tmanId = Convert.ToInt32(TmanId);
        //   var TrainingMan = ApplicationContext.Current.Services.MemberService.GetById(tmanId);

        //var TreqList = TrainingHelper.TrainingJoin(23);
        //var currentPage = Umbraco.TypedContent(2103).Parent.Parent.Name;
        // var mList2 = MhubMemberHelper.GetByDivision(branch, division);
        // var  userList = MhubMemberHelper.GetarraylistI(mList2);
        var userList = MhubMemberHelper.GetAllMemberList();
        // var parent1 = currentPage.Parent;
        // var parent2 = parent1.Parent.Id;
        //var bagus= ApplicationContext.Current.Services.MemberService.GetById(2807);
        //var memlist=MhubMemberHelper.APIGetAll2(bagus);
        //var  mIdList = MhubMemberHelper.GetarraylistI(memlist);


        return Request.CreateResponse(HttpStatusCode.OK, userList, new JsonMediaTypeFormatter());


    }

    [HttpGet]
    public HttpResponseMessage GetunLocked()
    {
        var userList = ApplicationContext.Current.Services.MemberService.GetMembersByPropertyValue("hub", 1057);
        var i = 0;
        foreach (var m in userList)
        {
            if (m.FailedPasswordAttempts != 0)
            {
                i++;
                //  m.IsLockedOut = false;
                m.FailedPasswordAttempts = 0;
                ApplicationContext.Current.Services.MemberService.Save(m);

            }
        }

        return Request.CreateResponse(HttpStatusCode.OK, i, new JsonMediaTypeFormatter());
    }

    [HttpGet]
    public HttpResponseMessage GetEmailReset()
    {
        var userList = ApplicationContext.Current.Services.MemberService.GetMembersByPropertyValue("hub", 1057);
        // var userList = ApplicationContext.Current.Services.MemberService.GetById(2526);
        var i = 0;

        var subject = "Reseting Team Hub Password";
        var body = "<html><body><p>Dear Team Hub Users,</p><p>For those having having trouble logging into the team hub portal, please reset your password by going to the login page  <a href='http://monroe.teamhub.info'>here</a> and clicking on the forgot password link underneath the login button for the portal. Then enter your Username followed by your email and click reset password.</p>Once reset you will recieve an email with a temporary password which can be used to login to the portal.<p> <p>Please also be aware that when logging into the portal you should use your username not your email followed by your password.</p><p>Once logged in, you will be redirected to a page where you can change the temporary password to your prefered private password.</p><p>Best Regards,<br/> Team Hub Support</p></body></html>";

        foreach (var m in userList)
        {
            var email = m.Email;
            AlertHelpers.emailAlert(body, email, subject);
        }

        return Request.CreateResponse(HttpStatusCode.OK, i, new JsonMediaTypeFormatter());
    }
    [HttpGet]
    static void TestEmail3()
    {


        System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
        mail.To.Add("dean@bakertillythailand.com");
        mail.From = new MailAddress("djginnane@gmail.com", "Dean", System.Text.Encoding.UTF8);
        mail.Subject = "This mail is send from asp.net application";
        mail.SubjectEncoding = System.Text.Encoding.UTF8;
        mail.Body = "This is Email Body Text";
        mail.BodyEncoding = System.Text.Encoding.UTF8;
        mail.IsBodyHtml = true;
        mail.Priority = MailPriority.High;
        SmtpClient client = new SmtpClient();
        client.Credentials = new System.Net.NetworkCredential("djginnane@gmail.com", "taylor129");
        client.Port = 587;
        client.Host = "smtp.gmail.com";
        client.EnableSsl = true;
        try
        {
            client.Send(mail);

        }
        catch (Exception ex)
        {
            Exception ex2 = ex;
            string errorMessage = string.Empty;
            while (ex2 != null)
            {
                errorMessage += ex2.ToString();
                ex2 = ex2.InnerException;
            }

        }

    }
    //[HttpGet]
    //public HttpResponseMessage CheckContent()
    //{
    //    var i = 1;


    //    var member1 = ApplicationContext.Current.Services.MemberService.GetById(2526);
    //    var rootHub = member1.GetValue<int>("hub");
    //    var members = Services.MemberService.GetMembersByPropertyValue("hub", rootHub);
    //    var mService = ApplicationContext.Current.Services;
    //    var db = ApplicationContext.Current.DatabaseContext.Database;
    //    foreach (var member in members)
    //    {

    //        var completedContent1 = member.GetValue<string>("contentCompleted");
    //        if (!String.IsNullOrWhiteSpace(completedContent1))
    //        {
    //            var completedContent = completedContent1.Split(',').ToList();



    //            var TrainingModules = MhubContentHelper.GetTotalCoursePages;
    //            foreach (var p in TrainingModules)
    //            {
    //                if (completedContent.Exists(x => x == p.Id.ToString()))
    //                {
    //                    //check that all content is completed
    //                    if (!MhubContentHelper.CheckCourseComplete(p, completedContent))
    //                    {
    //                        completedContent.RemoveAll(x => x == p.Id.ToString());
    //                    }
    //                }
    //            }
    //            var cList = String.Join(",", completedContent);
    //            //  var cList = completedContent.ToString();
    //            member.SetValue("contentCompleted", cList);
    //            mService.MemberService.Save(member);

    //        }
    //    }
    //    return Request.CreateResponse(HttpStatusCode.OK, "completed", new JsonMediaTypeFormatter());
    //}
    //[HttpGet]
    //public HttpResponseMessage BackupContent()
    //{
    //    var member1 = ApplicationContext.Current.Services.MemberService.GetById(2526);
    //    var rootHub = member1.GetValue<int>("hub");
    //    var members = Services.MemberService.GetMembersByPropertyValue("hub", rootHub);

    //    var db = ApplicationContext.Current.DatabaseContext.Database;
    //    foreach (var member in members)
    //    {
    //        var completedContent = member.GetValue<string>("contentCompletedBackup");
    //        //var cContent = completedContent.Split(',').ToList();
    //        //var completedContentBack = member.GetValue<string>("contentCompletedBackup");
    //        //var backedupContent = completedContent;
    //        //var  bc = backedupContent.ToList();
    //        //   member.SetValue("contentCompletedBackup", completedContent);
    //        member.SetValue("contentCompleted", completedContent);
    //        ApplicationContext.Current.Services.MemberService.Save(member);
    //    }
    //    return Request.CreateResponse(HttpStatusCode.OK, "Completed", new JsonMediaTypeFormatter());
    //}
    //static void TestEmail2()
    //{
    //    try
    //    {
    //        MailMessage mailMsg = new MailMessage();

    //        // To
    //        mailMsg.To.Add(new MailAddress("dean@bakertillythailand.com", "Dean"));

    //        // From
    //        mailMsg.From = new MailAddress("deang@redskydigital.com", "Red Sky");

    //        // Subject and multipart/alternative Body
    //        mailMsg.Subject = "subject";
    //        string text = "text body";
    //        string html = @"<p>html body</p>";
    //        mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
    //        mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

    //        // Init SmtpClient and send
    //        SmtpClient smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
    //        System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("deang@redskydigital.com", "DG00001@");
    //        smtpClient.Credentials = credentials;

    //        smtpClient.Send(mailMsg);
    //    }
    //    catch (Exception ex)
    //    {
    //        Console.WriteLine(ex.Message);
    //    }

    //}

    //static async Task Execute()
    //{
    // //   string apiKey = Environment.GetEnvironmentVariable("NAME_OF_THE_ENVIRONMENT_VARIABLE_FOR_YOUR_SENDGRID_KEY", EnvironmentVariableTarget.User);
    //    var apiKey = "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";
    //    dynamic sg = new SendGridAPIClient(apiKey);

    //    Email from = new Email("deang@redskydigital.com");
    //    string subject = "Sending with SendGrid is Fun";
    //    Email to = new Email("dean@bakertilythailand.com");
    //    Content content = new Content("text/plain", "and easy to do anywhere, even with C#");
    //    Mail mail = new Mail(from, subject, to, content);

    //    dynamic sendGridClient = new SendGridAPIClient(apiKey);
    //    // All three of these calls never return. The email message is sent, however.
    //    // dynamic response = sendGridClient.client.mail.send.post(requestBody: mail.Get());
    //    // dynamic response = await sendGridClient.client.mail.send.post(requestBody: mail.Get());
    //    dynamic response = await (sendGridClient.client.mail.send.post(requestBody: mail.Get())).ConfigureAwait(false);
    //   // return this.Ok();

    //}

    //[HttpGet]
    //public HttpResponseMessage TestEmail()
    //{
    //    //String apiKey = "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";
    //    ////  var myMessage = new SendGrid.SendGridAPIClient(apiKey, "https://api.sendgrid.com");
    //    //dynamic sg = new SendGridAPIClient(apiKey);

    //    var apiKey = "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";
    //    dynamic sg = new SendGridAPIClient(apiKey);

    //    Email from = new Email("deang@redskydigital.com");
    //    string subject = "Sending with SendGrid is Fun";
    //    Email to = new Email("dean@bakertilythailand.com");
    //    Content content = new Content("text/plain", "and easy to do anywhere, even with C#");
    //    Mail mail = new Mail(from, subject, to, content);
    //  //  dynamic response = sg.client.mail.send.post(requestBody: mail.Get());
    // //   dynamic sendGridClient = new SendGridAPIClient(apiKey);
    //    // All three of these calls never return. The email message is sent, however.
    //   // dynamic response = sendGridClient.client.mail.send.post(requestBody: mail.Get());
    //   // dynamic response = await sendGridClient.client.mail.send.post(requestBody: mail.Get());
    //  //  dynamic response = await(sendGridClient.client.mail.send.post(requestBody: mail.Get())).ConfigureAwait(false);

    //    // This line is never executed.
    //  //  return this.Ok();

    //    Execute().Wait();
    //    TestEmail3();


    //    return Request.CreateResponse(HttpStatusCode.OK, "sent", new JsonMediaTypeFormatter());
    //}


    /// <summary>
    /// test method for creating new hub
    /// </summary>
    [HttpGet]
    public HttpResponseMessage CreateHub()
    {
        /* var hubPages = new List<string>();
         var CS = ApplicationContext.Current.Services.ContentService;
         var DocumentToCopy = CS.GetById(1496);
         // CS.Copy(DocumentToCopy, 2008, true, 1527);
         var RootPage = Umbraco.TypedContent(2473);
         // var TemplateName = "";
         //var TemplateName = RootPage.GetProperty("template").Value;
         var TemplateId = RootPage.TemplateId;
         var DocType = RootPage.DocumentTypeId;
         foreach (var chil in RootPage.Children())
         {
             TemplateId = chil.TemplateId;
             DocType = chil.DocumentTypeId;
             hubPages.Add(chil.Name);
             hubPages.Add(TemplateId.ToString());
             hubPages.Add(DocType.ToString());

             if (chil.Children().Count() != 0)
             {
                 foreach (var cc in chil.Children())
                 {
                     hubPages.Add(cc.Name);
                 }
             }


         }*/

        var alMem = ApplicationContext.Current.Services.MemberService.GetAllMembers();
        var unameHub = new List<eLearningModel>();

        foreach (var m in alMem)
        {
            var temp = new eLearningModel();

            // temp.value = m.GetValue<int>("hub").ToString(); 
            if (m.GetValue("annualLeave") != null)
            {
                var aLeave = m.GetValue("annualLeave").ToString();

                temp.value = m.GetValue("annualLeave").ToString();
            }
            else
            {
                temp.value = "0";
            }
            temp.key = m.Username;
            unameHub.Add(temp);
        }
        return Request.CreateResponse(HttpStatusCode.OK, unameHub, new JsonMediaTypeFormatter());
    }
    //Sends email with calendar appointment

    [HttpGet]
    public HttpResponseMessage GetMailTest2()
    {
        //var now = DateTime.Now;
        //var later = now.AddHours(1);

       

        StringBuilder str = new StringBuilder();
        str.AppendLine("BEGIN:VCALENDAR");
        str.AppendLine("PRODID:-//Schedule a Meeting");
        str.AppendLine("VERSION:2.0");
        str.AppendLine("METHOD:REQUEST");
        str.AppendLine("BEGIN:VEVENT");
        str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", DateTime.Now.AddMinutes(+330)));
        str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
        str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", DateTime.Now.AddMinutes(+660)));
        str.AppendLine("LOCATION: " + "Somewhere");
        str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
        str.AppendLine(string.Format("DESCRIPTION:{0}", "Come to meeting"));
        str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", "come to meeting"));
        str.AppendLine(string.Format("SUMMARY:{0}", "Meeting now"));
        str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", "dean@bakertillythailand.com"));

        str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", "dean", "dean@bakertillythailand.com"));

        str.AppendLine("BEGIN:VALARM");
        str.AppendLine("TRIGGER:-PT15M");
        str.AppendLine("ACTION:DISPLAY");
        str.AppendLine("DESCRIPTION:Reminder");
        str.AppendLine("END:VALARM");
        str.AppendLine("END:VEVENT");
        str.AppendLine("END:VCALENDAR");


        MailMessage mailMsg = new MailMessage();

        // To
        mailMsg.To.Add(new MailAddress("dean@bakertillythailand.com"));

      

        // From
        mailMsg.From = new MailAddress("dean@bakertillythailand.com", "Baker Connect");

        // Subject and multipart/alternative Body
        mailMsg.Subject = "test";
        var text = "Test content";
        var html = "<h1>dede</h1>";

        if (!String.IsNullOrWhiteSpace(text))
        {
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
        }

        if (!String.IsNullOrWhiteSpace(html))
        {
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));
        }

        // Init SmtpClient and send
        SmtpClient smtpClient = new SmtpClient();// "smtp.sendgrid.net", Convert.ToInt32(587));
                                                 //System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("username@domain.com", "yourpassword");
                                                 //smtpClient.Credentials = credentials;
      
        mailMsg.Headers.Add("Content-class", "urn:content-classes:calendarmessage");

        System.Net.Mime.ContentType contype = new System.Net.Mime.ContentType("text/calendar");
        contype.Parameters.Add("method", "REQUEST");
        contype.Parameters.Add("name", "Meeting.ics");
        AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), contype);
        mailMsg.AlternateViews.Add(avCal);
        smtpClient.Send(mailMsg);


        return Request.CreateResponse(HttpStatusCode.OK, 1, new JsonMediaTypeFormatter());

    }
    [HttpGet]
    public HttpResponseMessage GetMailTest()
    {
        //var members = ApplicationContext.Current.Services.MemberService.GetAllMembers();
        //var dateList = new List<DateTime>();
        //var dateList2 = new List<DateTime>();
        //foreach (var m in members)
        //{
        //  var currentBD= m.GetValue<DateTime>("birthday");
        //    if (currentBD.Year>1900)
        //    {
        //        DateTime newBD = currentBD.AddDays(1);
        //        dateList.Add(currentBD);
        //        dateList2.Add(newBD);
        //           m.SetValue("birthday", newBD);
        //         // Services.MemberService.Save(m);
        //    }
        //}
        var member = ApplicationContext.Services.MemberService.GetById(2825);
        //   var lineManagers = MhubMemberHelper.LineMembers(member);
        //   var subStaff= MhubMemberHelper.GetarraylistI(lineManagers);
        //   var hlist= MhubMemberHelper.HubMembers.ToList().Where(m => m.IsApproved);
        // var member = MhubMemberHelper.LoggedInMember;
        // MhubMemberHelper.UpdateMembersCache(member);
        //var AndrewList= MhubMemberHelper.HubMembers.ToList().Where(m => m.GetValue<int>("lineManager") == 2017);
        var stringList = new List<string>();
        var dateList = new List<string>();
        // foreach (var m in AndrewList)
        // {
        //     stringList.Add(m.Id.ToString());
        // }
        // string iDate = "02/24/2017";
        //  DateTime oDate = Convert.ToDateTime(iDate);
        // DateTime CurrentDate = DateTime.Today;
        //  int daysDiffernce = oDate.Date.Subtract(CurrentDate.Date).Days;
        //var memberList = MhubMemberHelper.HubMembers.OrderBy(o => o.GetValue<DateTime>("companyStart"));
        //var mList = new List<eLearningModel>();
        //foreach (var m in memberList)
        //{
        //    var tempList = new eLearningModel();
        //    tempList.value = m.Id.ToString()+"-"+m.Name;
        //    tempList.key = m.GetValue<DateTime>("companyStart").ToString();
        //    mList.Add(tempList);
        //}
        //var appCheck = AppraisalHelpers.CheckStartDate(member);
        // var eSubject = "Appraisal Reminder";
        // var eMessage = "You have not yet actioned the appraisal request you were sent. Please fill out your appraisal form for this appraisal period immediately to ensure a speedy appraisal process. You can find this form in the MonroeHub under Monroe Administration / Employee Appraisal. </p><p>The Monroe appraisal process is as follows:</p><ul><li>Step 1:  the employee fills out his or her part of the appraisal, which will be sent to the direct supervisor</li><li>Step 2: the direct supervisor provides feedback</li><li>Step 3: The employee and direct supervisor sit down together to formalise the discussed appraisal actions</li></ul>";
        // var mesBody = AlertHelpers.emailBodyWarmest(eMessage, member);
        // AlertHelpers.emailAlert(mesBody, member.Email, eSubject);

        //  var members = ApplicationContext.Current.Services.MemberService.GetAllMembers();
        ////  var memList = members.ToList().Where(m => m.GetValue<int>("appraisal") != 0 && m.GetValue<int>("appraisalSchedule") != 111 && m.GetValue<int>("appraisalSchedule") != 0);
        //  foreach (var m in members)
        //  {
        //      //function to check if appraisal needs to be created
        //      var AppraisalExists = AppraisalHelpers.AppraisalCheck(m);
        //      stringList.Add(AppraisalExists.ToString());
        //  }
      //  var userList = MhubMemberHelper.APIGetAll(member);
        //   var Active = MhubMemberHelper.userIsActive(member);
        //var singleUser = new SelectListItem();
        //var UserOptions = new List<SelectListItem>();
        //if (userList != null)
        //{
        //    foreach (var u in userList)
        //    {
        //        singleUser = new SelectListItem();
        //        singleUser.Text = u.Username;
        //        singleUser.Value = u.Id.ToString();

        //        UserOptions.Add(singleUser);
        //    }

        //}
        var tempList = new List<eLearningModel>();
        var members = ApplicationContext.Current.Services.MemberService.GetMembersByMemberType("Staff");
        foreach (var m in members)
        {
            if (m.GetValue<int>("lineManager") == 3837)
            {
                tempList.Add(new eLearningModel() { key = m.Name, value = m.GetValue<decimal>("holidayLeave").ToString() });
            }
        }

      //  var memberList = MhubMemberHelper.APIGetLineMembers();
        return Request.CreateResponse(HttpStatusCode.OK, tempList, new JsonMediaTypeFormatter());
    }
    public HttpResponseMessage GetCache()
    {
        var cache = ApplicationContext.Current.ApplicationCache.RuntimeCache;
        IEnumerable<XmlNode> nodeList = null;
        var rssFeedUrl = "https://www.monroeconsulting.com/Modules/Blog/public/publicblogrss.ashx?oid=146";
        //Get the XML from remote URL
        XmlDocument xml = new XmlDocument();

        //URL currently hardcoded - but you could use a macro param to pass in URL
        xml.Load("" + rssFeedUrl);

        //Select the nodes we want to loop through
        //XmlNodeList nodes = xml.SelectNodes("//item[position() <= 3]");
        XmlNodeList nodes = xml.SelectNodes("//item");

        nodeList = nodes.Cast<XmlNode>().Skip(0).Take(3);

        // load the data in the cache so that it times out every 2 days.
        cache.InsertCacheItem("rssFeed", () => { return nodeList; }, timeout: TimeSpan.FromDays(1));

        return Request.CreateResponse(HttpStatusCode.OK, nodeList, new JsonMediaTypeFormatter());
    }
    [HttpGet]
    public HttpResponseMessage RemoveDuplicateAppraisals()
    {
        var memberList = new List<int>();
        ////memberList.Add(2877);
        ////memberList.Add(3001);
        ////memberList.Add(3005);
        ////memberList.Add(3153);
        ////memberList.Add(3204);
        ////memberList.Add(3208);
        ////memberList.Add(3212);
        ////memberList.Add(3213);
        ////memberList.Add(2831);
        ////memberList.Add(2863);
        ////memberList.Add(2865);
        ////memberList.Add(2868);
        ////memberList.Add(2871);
        ////memberList.Add(2875);
        ////memberList.Add(3318);
        ////memberList.Add(2831);
        ////memberList.Add(2863);
        ////memberList.Add(2865);
        ////memberList.Add(2868);
        ////memberList.Add(2871);
        ////memberList.Add(2875);
        ////memberList.Add(2877);
        ////memberList.Add(3001);
        ////memberList.Add(3005);
        ////memberList.Add(3153);
        ////memberList.Add(3204);
        ////memberList.Add(3208);
        ////memberList.Add(3212);
        ////memberList.Add(3213);
        ////memberList.Add(3318);
        ////memberList.Add(3332);
        ////memberList.Add(3367);
        ////memberList.Add(3147);
        ////memberList.Add(3147);
        memberList.Add(3316);
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select * from[mhubMemberAppraisal] where AppStatus = 0 and member_Id in (3316) and AppraisalSchedule = 313  order by  Member_Id, ScheduleAlias";
        var appraisalTable = db.Fetch<UmbracoWithMvc.Entities.MemberAppraisal>(query, memberList.ToArray());

        foreach (var app in appraisalTable)
        {
            db.Delete(app);
        }
        return Request.CreateResponse(HttpStatusCode.OK, memberList.ToString(), new JsonMediaTypeFormatter());
    }
    [HttpGet]
    public HttpResponseMessage ExcelTest()
    {
        string testVVar = "Works";
        var dirPath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/TEMP/FileUploads/"));
        var path = dirPath + "THSP Model 11282018.xlsx";
        //   FileInfo file = new FileInfo(@"C:\Users\Dean\Source\Repos\bkhrnew2\mhub.website\App_Data\TEMP\FileUploads\Book1.xlsx");
        FileInfo file = new FileInfo(path);
        if (file.Exists)
        {
            using (ExcelPackage excelPackage = new ExcelPackage(file))
            {
                ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                //  ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets["Assumption & Result"];
             //   excelWorksheet.Cells[1, 1].Value = 44;
              //  excelWorksheet.Cells[1, 2].Value = 1007;
                excelWorksheet.Cells[10, 11].Value = 3000000000;
               // excelWorksheet.Cells[14, 4].Style.Numberformat.Format = "#0.00%";

                excelPackage.Save();
            }
            testVVar = "ReallyWorks";
        }
        return Request.CreateResponse(HttpStatusCode.OK, testVVar, new JsonMediaTypeFormatter());
    }
    [HttpGet]
    public HttpResponseMessage ReadExcelTest()
    {
        var texty="";
        string testVVar = "Works";
        var dirPath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/TEMP/FileUploads/"));
        var path = dirPath + "Book2.xlsx";
        //   FileInfo file = new FileInfo(@"C:\Users\Dean\Source\Repos\bkhrnew2\mhub.website\App_Data\TEMP\FileUploads\Book1.xlsx");
        FileInfo file = new FileInfo(path);
        //if (file.Exists)
        //{
        //    using (ExcelPackage excelPackage = new ExcelPackage(file))
        //    {
        //        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
        //        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
        //        testVVar = excelWorksheet.Cells[1, 1].Value.ToString(); 
               

        //        excelPackage.Save();
        //    }
         
        //}
        var hello = new List<string>();

        // Open the document for editing.
        using (SpreadsheetDocument spreadsheetDocument =
            SpreadsheetDocument.Open(path, false))
        {
            testVVar = "opened";
            // Code removed here.
           
            WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
            WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
            OpenXmlReader reader = OpenXmlReader.Create(worksheetPart);
            string text;
            while (reader.Read())
            {
                if (reader.ElementType == typeof(CellValue))
                {
                    text = reader.GetText();
                    Console.Write(text + " ");
                    hello.Add(text);
                }
            }

            //SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
            
            //foreach (Row r in sheetData.Elements<Row>())
            //{
            //    foreach (Cell c in r.Elements<Cell>())
            //    {
                  
            //        hello.Add(c.CellValue.Text);
                

            //    }
            //}
       
    }

        return Request.CreateResponse(HttpStatusCode.OK, hello, new JsonMediaTypeFormatter());
    }

    [HttpGet]
    public HttpResponseMessage ReadExcelTest2()
    {
        var hello = new List<string>();
        var dirPath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/TEMP/FileUploads/"));
        var path = dirPath + "KBANK_Valuation.xlsx";
        //   FileInfo file = new FileInfo(@"C:\Users\Dean\Source\Repos\bkhrnew2\mhub.website\App_Data\TEMP\FileUploads\Book1.xlsx");
        FileInfo file = new FileInfo(path);
        if (file.Exists)
        {
            using (ExcelPackage excelPackage = new ExcelPackage(file))
            {
                //get the first worksheet in the workbook
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets["Assumptions and Results"];
                int colCount = worksheet.Dimension.End.Column;  //get Column Count
                int rowCount = worksheet.Dimension.End.Row;     //get row count
                //for (int row = 1; row <= rowCount; row++)
                //{
                //    for (int col = 1; col <= colCount; col++)
                //    {
                //        Console.WriteLine(" Row:" + row + " column:" + col + " Value:" + worksheet.Cells[row, col].Value.ToString().Trim());
                //        hello.Add(worksheet.Cells[row, col].Value.ToString());
                //    }
                //}
              var result= worksheet.Cells[13,9].Value;
                var dec = Convert.ToDecimal(result);
                hello.Add(result.ToString());
            }
        }
        return Request.CreateResponse(HttpStatusCode.OK, hello, new JsonMediaTypeFormatter());
    }
    [HttpGet]
    public HttpResponseMessage calExcelTest2()
    {
        var dirPath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/TEMP/FileUploads/"));
        var path = dirPath + "THSP.xlsx";
        using (var package = new ExcelPackage(new FileInfo(path)))
    {
            // calculate all formulas in the workbook
         //   package.Workbook.Calculate();
            // calculate one worksheet
            package.Workbook.Worksheets["Assumption & Result"].Calculate();
            // calculate a range
            //  package.Workbook.Worksheets["Assumptions and Results"].Cells["I11"].Calculate();
            package.Save();
        }
        return Request.CreateResponse(HttpStatusCode.OK, "hello", new JsonMediaTypeFormatter());
    }
    public AppraisalTempController()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}