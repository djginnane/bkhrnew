﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using umbraco;
using Umbraco.Core;
using Umbraco.Web.WebApi;

/// <summary>
/// Summary description for CashFlowApiController
/// </summary>
public class CashFlowApiController : UmbracoApiController
{
    [HttpGet]
    public HttpResponseMessage GetSuppliers()
    {
        var suppliers = new List<Supplier>();
        var repo = new CashFlowRepository();
        suppliers = repo.GetSupplierList("Popcorn");
        return Request.CreateResponse(HttpStatusCode.OK, suppliers, new JsonMediaTypeFormatter());
    }

    [HttpGet]
    public HttpResponseMessage GetPaymentModel()
    {
        var pay = new CFPayment();
        return Request.CreateResponse(HttpStatusCode.OK, pay, new JsonMediaTypeFormatter());
    }
    [HttpPost]
    public HttpResponseMessage SubmitPayment(CFPayment PaymentForm)
    {
        var repo = new CashFlowRepository();
        PaymentForm.Job_Name = "Popcorn";
        PaymentForm.Job_Id = 1;

        //Add user info
        var member = MhubMemberHelper.LoggedInMember;
        PaymentForm.Created_By = member.Id;
        PaymentForm.Date_Created = DateTime.UtcNow;
        PaymentForm.Date_Last_edit = DateTime.UtcNow;
        PaymentForm.Last_Edit_By = member.Id;


        ///create new supplier
        PaymentForm = CashFlowHelpers.HandleSuppliers(PaymentForm);
        if (PaymentForm.Estimated_Amount==true)
        {
            PaymentForm.Estimate_Table = 1;
        }
        var payment = repo.AddPayment(PaymentForm);

        return Request.CreateResponse(HttpStatusCode.OK, new CFPayment(), new JsonMediaTypeFormatter());
    }
    [HttpGet]
    public HttpResponseMessage CheckInvoice(string Invoice)
    {
        var repo = new CashFlowRepository();
        var Invoices = repo.CheckInvoice(Invoice, "Popcorn");
        if (Invoices == null)
        {
            Invoices = new List<CFPayment>();
        }
        return Request.CreateResponse(HttpStatusCode.OK, Invoices, new JsonMediaTypeFormatter());
    }
    /// <summary>
    /// Gets all transactions from the PRTable
    /// </summary>
    /// <param name="Comp_Name"></param>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage PRTable(string Comp_Name = "Popcorn")
    {
        var repo = new CashFlowRepository();
        var prList = repo.GetPRList(Comp_Name);

        return Request.CreateResponse(HttpStatusCode.OK, prList, new JsonMediaTypeFormatter());
    }

    /// <summary>
    /// Gets all transactions entered
    /// </summary>
    /// <param name="Comp_Name"></param>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage AllTable(string Comp_Name = "Popcorn")
    {
        var repo = new CashFlowRepository();
        var allList = repo.GetAll(Comp_Name);

        return Request.CreateResponse(HttpStatusCode.OK, allList, new JsonMediaTypeFormatter());
    }
    [HttpGet]
    public HttpResponseMessage EstimateTable(string Comp_Name = "Popcorn")
    {
        var repo = new CashFlowRepository();
        var esList = repo.GetEstimateList(Comp_Name);

        return Request.CreateResponse(HttpStatusCode.OK, esList, new JsonMediaTypeFormatter());
    }
    [HttpGet]
    public HttpResponseMessage AdvanceList(string Comp_Name = "Popcorn")
    {
        var repo = new CashFlowRepository();
        var esList = repo.GetAdvanceList(Comp_Name);

        return Request.CreateResponse(HttpStatusCode.OK, esList, new JsonMediaTypeFormatter());
    }

    [HttpGet]
    public HttpResponseMessage CompleteAdvanced(int ID, string Comp_Name = "Popcorn")
    {
        var repo = new CashFlowRepository();
        var paySingle = repo.UpdateAdvanced(ID, Comp_Name);
        paySingle.Advance_Completed = true;
        var pay = repo.Payment(paySingle);
        var adList = repo.GetAdvanceList(Comp_Name);

        return Request.CreateResponse(HttpStatusCode.OK, adList, new JsonMediaTypeFormatter());
    }
    [HttpGet]
    public HttpResponseMessage PaymentById(int ID, string Comp_Name = "Popcorn")
    {
        var repo = new CashFlowRepository();
        var paySingle = repo.UpdateAdvanced(ID, Comp_Name);

        return Request.CreateResponse(HttpStatusCode.OK, paySingle, new JsonMediaTypeFormatter());
    }

    [HttpPost]
    public HttpResponseMessage UpdatePayment(CFPayment PaymentForm)
    {
        var member = MhubMemberHelper.LoggedInMember;
        var repo = new CashFlowRepository();
        PaymentForm.Estimated_Amount = false;
        PaymentForm.Date_Actual_Payment = DateTime.UtcNow;
        PaymentForm.Date_Last_edit = DateTime.UtcNow;
        PaymentForm.Last_Edit_By = member.Id;
        // PaymentForm.Variance_Difference = 1m;
        var paySingle = repo.Payment(PaymentForm);

        return Request.CreateResponse(HttpStatusCode.OK, paySingle, new JsonMediaTypeFormatter());
    }

    [HttpPost]
    public HttpResponseMessage CompleteAdvanced(CFPayment PaymentForm)
    {
        var repo = new CashFlowRepository();
        PaymentForm.Advance_Completed = true;
        var paySingle = repo.Payment(PaymentForm);

        return Request.CreateResponse(HttpStatusCode.OK, paySingle, new JsonMediaTypeFormatter());
    }

    /// <summary>
    /// Gets the groups for the drop down menu on payment form
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage GetGroups()
    {
        var repo = new CashFlowRepository();
        var payGroups = repo.GetGroups();

        return Request.CreateResponse(HttpStatusCode.OK, payGroups, new JsonMediaTypeFormatter());
    }

    /// <summary>
    /// gets the sub groups for the drop down menu
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>

    [HttpGet]
    public HttpResponseMessage GetSubGroups(int Id)
    {
        var repo = new CashFlowRepository();
        var payGroups = repo.GetSubGroups(Id);

        return Request.CreateResponse(HttpStatusCode.OK, payGroups, new JsonMediaTypeFormatter());
    }
    /// <summary>
    /// Retrieves the Net amount totals for the summary letter, store estimate total in Actual Amount result 1
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage GetPaymentSummary(int Letter_No, int Month, int Year)
    {
        var repo = new CashFlowRepository();
      
        var payGroups = repo.GetSummary(Letter_No, Month);
        var EstimateTotal = repo.GetEstimateTotal(Letter_No, Month);

        payGroups[0].Actual_Amount = EstimateTotal.NetAmount_Total;
        payGroups[0].Version2 = CashFlowHelpers.CompareToBudgetTotals(Letter_No, Month, Year);
        var FinalGroups = CashFlowHelpers.AddpaymentsToGroups(payGroups, Letter_No, Month, Year);
        foreach (var fg in FinalGroups)
        {
            var StringList = !String.IsNullOrWhiteSpace(fg.Supplier_Title)?CashFlowHelpers.splitTitle(fg.Supplier_Title): new List<string>();
            fg.Supplier_Title_First = StringList.Count > 0 ? StringList[0] : "";
            fg.Supplier_Title_Last = StringList.Count > 1 ? StringList[1] : "";
        }
        return Request.CreateResponse(HttpStatusCode.OK, FinalGroups, new JsonMediaTypeFormatter());
    }
    /// <summary>
    /// Gets suppliers old debts from the old debt table / Id is supplier code
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage OldSupplier(int Id)
    {
        var repo = new CashFlowRepository();
        var Invoices = repo.GetOldDebt(Id);
        return Request.CreateResponse(HttpStatusCode.OK, Invoices, new JsonMediaTypeFormatter());
    }

    [HttpGet]
    public HttpResponseMessage OldSupplierTable()
    {
        var repo = new CashFlowRepository();
        var Invoices = repo.OldDebtTable();
        return Request.CreateResponse(HttpStatusCode.OK, Invoices, new JsonMediaTypeFormatter());
    }
    [HttpGet]
    public HttpResponseMessage GetBudgetModel()
    {
        var bud = new BudgetTotals();
        return Request.CreateResponse(HttpStatusCode.OK, bud, new JsonMediaTypeFormatter());
    }
    [HttpPost]
    public HttpResponseMessage SubmitBudget(List<BudgetTotals> Budgets)
    {
        var member = MhubMemberHelper.LoggedInMember;
        var repo = new CashFlowRepository();
        // if grouping not created yet add a template of the groups
        var GroupExisting = repo.checkExistingBudgetGroups(Budgets[0].Payment_Month, Budgets[0].Payment_Year);
        var gCount= GroupExisting.Count;
        var uniqueID = new Guid();
        var existing = repo.getExistingBudget( Budgets[0].Payment_Month, Budgets[0].Payment_Year);

        int i = 0;
        var dateTemp = DateTime.UtcNow;

        if (existing.Count == 0)

        {
            foreach (var b in Budgets)
            {
                b.Date_Created = dateTemp;
                b.Job_Name = "popcorn";
                repo.AddBudget(b);
                if (gCount==0)
                {
                    var groupings = new BudgetGroupings();
                    groupings.Date_Created = dateTemp;
                    groupings.Created_By = member.Id;
                    groupings.Payment_Month = b.Payment_Month;
                    groupings.Payment_Year = b.Payment_Year;
                    groupings.Group_Id = b.Group_Id;
                    groupings.Budget_ID = uniqueID;
                    repo.AddBudgetGroup(groupings);

                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, 2, new JsonMediaTypeFormatter());
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, 1, new JsonMediaTypeFormatter());
        }
      
    }

    [HttpPost]
    public HttpResponseMessage UpdateBudget(List<BudgetTotals> Budgets)
    {
        var repo = new CashFlowRepository();
       var Date_Created = DateTime.UtcNow;

        foreach (var bud in Budgets)
        {
           if (bud.Delete_Group)
            {
                if (bud.Id > 0) { CashFlowHelpers.RemoveBudgetGroup(bud); }
               
               
                
            }
            else
            {
                if (bud.Id > 0)
                {
                    Date_Created = bud.Date_Created;
                    repo.SaveBudget(bud);
                }
                else
                {
                    bud.Job_Name = "popcorn";
                    bud.Date_Created = Date_Created;
                    repo.AddBudget(bud);
                    /// TODO: Add function to add to all groups within month and year, as well as group lists
                }
            }
        }

        return Request.CreateResponse(HttpStatusCode.OK, 2, new JsonMediaTypeFormatter());
    }

    [HttpGet]
    public HttpResponseMessage GetExistingBudget()
    {
        var bud = new BudgetTotals();
        var repo = new CashFlowRepository();
        var Allexisting = repo.getAllExistingBudget();
        var ExistinginGroups = CashFlowHelpers.OrganiseBudget(Allexisting);
        return Request.CreateResponse(HttpStatusCode.OK, ExistinginGroups, new JsonMediaTypeFormatter());
    }
    /// <summary>
    /// get grouped budgets witht totels
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage ReviewBudgets()
    {
        var bud = new BudgetTotals();
        var repo = new CashFlowRepository();
        var Allexisting = repo.BudgetGrouped();
       // var ExistinginGroups = CashFlowHelpers.OrganiseBudget(Allexisting);
        return Request.CreateResponse(HttpStatusCode.OK, Allexisting, new JsonMediaTypeFormatter());
    }

    /// <summary>
    /// Get based on single budget
    /// </summary>
    /// <param name="CreatedDate"></param>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage GetIndivdualBudget(int Payment_Letter, int Payment_Month, int Payment_Year, DateTime CreatedDate )
    {
        var repo = new CashFlowRepository();
        var Allexisting = repo.BudgetIndiv(Payment_Letter, Payment_Month, Payment_Year, CreatedDate);
        return Request.CreateResponse(HttpStatusCode.OK, Allexisting, new JsonMediaTypeFormatter());
    }
    [HttpGet]
    public HttpResponseMessage BudgetSummary()
    {
        var repo = new CashFlowRepository();
        var Allexisting = repo.BudgetSummary();
        return Request.CreateResponse(HttpStatusCode.OK, Allexisting, new JsonMediaTypeFormatter());
    }


    /// <summary>
    /// loads the new budget model based on existing groups
    /// </summary>
    /// <param name="Payment_Month"></param>
    /// <param name="Payment_Year"></param>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage GetNewBudget(int Payment_Letter, int Payment_Month, int Payment_Year)
    {
        var repo = new CashFlowRepository();
        var groups = repo.checkExistingBudgetGroups(Payment_Month, Payment_Year);
        var existing = repo.getExistingBudget( Payment_Month, Payment_Year);
        var BudgetList = new List<BudgetTotals>();
        if (existing.Count>0)
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, 1, new JsonMediaTypeFormatter());
        }
        else
        { 
       
            foreach (var g in groups)
            {
                var bt = new BudgetTotals();
                bt.Group_Name = g.Group_Name;
                bt.Group_Id = g.Group_Id;
                bt.Payment_Month = g.Payment_Month;
                bt.Payment_Year = g.Payment_Year;
                bt.Payment_Letter = Payment_Letter;
                BudgetList.Add(bt);
            }
        }
        return Request.CreateResponse(HttpStatusCode.OK, BudgetList, new JsonMediaTypeFormatter());
    }
    public CashFlowApiController()
    {
        //
        // TODO: Add constructor logic here
        //
    }

  
}