﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;

using Umbraco.Core.Persistence;

/// <summary>
/// Summary description for ManageLeave
/// </summary>
public class ManageLeaveController : Controller
{
    public ActionResult Index()
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;

        // var query = "select * from mhubLeaveRequest where memberId = @0";
        var query = " select MemberId, count(LeaveDay) as ApprovedBy from mhubLeaveRequest  group by MemberId, createdAt";


        var leaveTable = db.Fetch<LeaveRequest>(query, 2);

        //leaveTable.ForEach(obj =>
        //{
        //    obj.HalfDay = true;
        //});

        ViewBag.qResult = leaveTable;
        return View();
    }
}