﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using umbraco;
using Umbraco.Core;
using Umbraco.Web.WebApi;
using System.Web.Mvc;
using System.Linq;

/// <summary>
/// Summary description for StationaryApiController
/// </summary>
public class StationaryApiController : UmbracoApiController
{
    
    public HttpResponseMessage Get()
    {
       
        var StRepo = new StationaryRepository();
        var StItem = StRepo.GetAll();
      
        return Request.CreateResponse(HttpStatusCode.OK, StItem, new JsonMediaTypeFormatter());

    }

    /// <summary>
    ///  To get Items based on category selected
    /// </summary>
    /// <param name="Cat_Id"></param>
    [System.Web.Http.HttpGet]
    public HttpResponseMessage GetItemsByCatergory(int Cat_Id)
    {

        var StRepo = new StationaryRepository();
        var StItem = StRepo.GetItemByCategory(Cat_Id);
        var ItemList = new List<SelectListItem>();
        
      
        foreach (var it in StItem)
        {
            var singleUser = new SelectListItem();
            singleUser.Text = it.Description;
            singleUser.Value = it.Item_Id.ToString();

            ItemList.Add(singleUser);
        }

        return Request.CreateResponse(HttpStatusCode.OK, ItemList, new JsonMediaTypeFormatter());

    }

    [System.Web.Http.HttpGet]
    public HttpResponseMessage AddCategory(string Category)
    {
        //get the database
        var StRepo = new StationaryRepository();
        var Station_Cat = new Stationary();
        Station_Cat.Category_Name = Category;
        var StItem = StRepo.AddCategory(Station_Cat);
       
        return Request.CreateResponse(HttpStatusCode.OK, StItem, new JsonMediaTypeFormatter());

    }
    [System.Web.Http.HttpGet]
    public HttpResponseMessage AddItem(int Category_Id, string Item_Description, string Item_Type, bool IsAvailable = true)
    {
        //get the database
        var StRepo = new StationaryRepository();
        var Station_Cat = new StationaryItem();
        Station_Cat.Category_Id= Category_Id;
        Station_Cat.Description = Item_Description;
        Station_Cat.IsAvailable = IsAvailable;
        Station_Cat.Type = Item_Type;
        Station_Cat.Last_Update = DateTime.UtcNow;
        Station_Cat.IsActive = true;
       // Station_Cat.Quantity = Quantity;

       var StItem = StRepo.AddStationaryItem(Station_Cat);
        StItem.Category_Name = StRepo.GetCategoryName(StItem.Category_Id);

        return Request.CreateResponse(HttpStatusCode.OK, StItem, new JsonMediaTypeFormatter());

    }

    //make item active inactive

    [System.Web.Http.HttpGet]
    public HttpResponseMessage DisableStationaryItem(int Item_Id)
    {
        //retrieve stationary item, change active status and save
        var StRepo = new StationaryRepository();
        var Station_Cat = new StationaryItem();
        Station_Cat = StRepo.GetItem(Item_Id);
        Station_Cat.IsActive = Station_Cat.IsActive ? false : true;
        Station_Cat.Last_Update = DateTime.UtcNow;
        var UpdatedItem=StRepo.UpdateStationaryItem(Station_Cat);

        return Request.CreateResponse(HttpStatusCode.OK, UpdatedItem, new JsonMediaTypeFormatter());

    }
    [System.Web.Http.HttpPost]
    public HttpResponseMessage UpdateItem(StationaryItem Item)
    {
        //retrieve stationary item, change active status and save
        var StRepo = new StationaryRepository();
       Item.Last_Update = DateTime.UtcNow;
        var UpdatedItem = StRepo.UpdateStationaryItem(Item);

        return Request.CreateResponse(HttpStatusCode.OK, Item, new JsonMediaTypeFormatter());

    }
    /// <summary>
    /// Update Stationary Request from Stationary manager
    /// </summary>
    /// <param name="Item"></param>
    /// <returns></returns>
    [System.Web.Http.HttpPost]
    public HttpResponseMessage UpdateRequest(StationaryRequisition Item)
    {
        //retrieve stationary item, change active status and save

        var member = ApplicationContext.Services.MemberService.GetById(Item.User_Id);
        var StRepo = new StationaryRepository();
        var stManager = DataHelpers.GetStationaryManager;
        if (Item.Status == 1)
        {
            Item.Date_Approved = DateTime.UtcNow;
            Item.Approved_By = MhubMemberHelper.LoggedInMember.Id;

            /// send email to either pick up stationary, or wait for it to be delivered
            if (Item.PickUp == true)
            {
                var Subject = "Your stationary request has been approved";
                var Email = "<html><body> <p>Dear " + member.Name + ",</p><p>Your stationary request for " + Item.Category_Name + " " + Item.Item_Name + " has been approved. Please visist " + stManager.Name + " to pick up your request <table><tr><td> Category Name: </td><td>" + Item.Category_Name + "</td></tr><tr><td> Item: </td><td>" + Item.Item_Name + " </td></tr><tr><td> Quantity: </td><td>" + Item.Quantity_Ordered + "</td></tr><tr><td>Comments:</td><td>" + Item.AdminComments + "</td></tr></table></p>   <p>Best Regards, <br /> HR Support Team</p>   </body></html>";
                EmailHelper.SendEmail(Subject, member.Email, "", Email);
            }
            else
            {
                var Subject = "Your stationary request has been approved";
                var Email = "<html><body> <p>Dear " + member.Name + ",</p><p>Your stationary request for " + Item.Category_Name + " " + Item.Item_Name + " has been approved. Your stationary items should be delivered to you within 24 hours <table><tr><td> Category Name: </td><td>" + Item.Category_Name + "</td></tr><tr><td> Item: </td><td>" + Item.Item_Name + " </td></tr><tr><td> Quantity: </td><td>" + Item.Quantity_Ordered + "</td></tr><tr><td>Comments:</td><td>" + Item.AdminComments + "</td></tr></table></p>   <p>Best Regards, <br /> HR Support Team</p>   </body></html>";
                EmailHelper.SendEmail(Subject, member.Email, "", Email);
            }
        }
        else if (Item.Status == 2)
        {
            var Subject = "Your stationary request has been rejected";
            var Email = "<html><body> <p>Dear " + member.Name + ",</p><p>Your stationary request for " + Item.Category_Name + " " + Item.Item_Name + " has been not been approved. Please see the reason in the comments section <table><tr><td> Category Name: </td><td>" + Item.Category_Name + "</td></tr><tr><td> Item: </td><td>" + Item.Item_Name + " </td></tr><tr><td> Quantity: </td><td>" + Item.Quantity_Ordered + "</td></tr><tr><td>Comments:</td><td>" + Item.AdminComments + "</td></tr></table></p>   <p>Best Regards, <br /> HR Support Team</p>   </body></html>";
            EmailHelper.SendEmail(Subject, member.Email, "", Email);
        }
       
        var UpdatedItem = StRepo.UpdateStationaryRequest(Item);

        return Request.CreateResponse(HttpStatusCode.OK, Item, new JsonMediaTypeFormatter());

    }




    //returns list of Station Item Categories and ID's
    [System.Web.Http.HttpGet]
    public HttpResponseMessage GetCategories()
    {
       
        var StRepo = new StationaryRepository();
        var StItem = StRepo.GetAllCategories();
      
        return Request.CreateResponse(HttpStatusCode.OK, StItem, new JsonMediaTypeFormatter());

    }
    /// <summary>
    /// returns list of unproccessed stationary requests
    /// </summary>
    /// <returns></returns>
    [System.Web.Http.HttpGet]
    public HttpResponseMessage GetStationeryRequestsPending()
    {
        var StRepo = new StationaryRepository();
        var StReq = StRepo.GetStationeryRequestsPending();
        return Request.CreateResponse(HttpStatusCode.OK, StReq, new JsonMediaTypeFormatter());
    }
    [System.Web.Http.HttpGet]
    public HttpResponseMessage GetAllStationeryRequests()
    {
        var StRepo = new StationaryRepository();
        var StReq = StRepo.GetStationeryRequests(1);
        return Request.CreateResponse(HttpStatusCode.OK, StReq, new JsonMediaTypeFormatter());
    }
    [System.Web.Http.HttpGet]
    public HttpResponseMessage GetReport()
    {
        var StRepo = new StationaryRepository();
        var StReq = StRepo.ReportTotals();
        return Request.CreateResponse(HttpStatusCode.OK, StReq, new JsonMediaTypeFormatter());
    }

    [System.Web.Http.HttpPost]
    public HttpResponseMessage GetReportByVariables(ReportVariables statReport)
    {
        var memberIds = new List<int>().ToArray();
        if (statReport.Division == null)
        {
            statReport.Division = new string[0];
            var MemberList1 = MhubMemberHelper.HubMembersAll.ToList();
            memberIds = MhubMemberHelper.GetarraylistI(MemberList1);
        }
        else if (statReport.Division.Length != 0)
        {
            var MemberList = statReport.Division.Contains("")? MhubMemberHelper.HubMembersAll.ToList() : MhubMemberHelper.HubMembersAll.ToList().Where(m => statReport.Division.Contains(m.GetValue<string>("division")));//.Where(m => statReport.Division.Any(a => m.GetValue<string>("division").Contains(a)));
            memberIds = MhubMemberHelper.GetarraylistI(MemberList);
        }
        var StRepo = new StationaryRepository();
        var StReq = StRepo.ReportTotals(statReport.startRange, statReport.endRange,memberIds);
        return Request.CreateResponse(HttpStatusCode.OK, StReq, new JsonMediaTypeFormatter());
    }
    public StationaryApiController()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}