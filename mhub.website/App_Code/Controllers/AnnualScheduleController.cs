﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Web.WebApi;
using System.Net.Http;
using System.Net;
using System.Net.Http.Formatting;

/// <summary>
/// All tasks that only need to be checked yearly
/// </summary>
public class AnnualScheduleController : UmbracoApiController
{
    /// <summary>
    /// Class may not be neccesary - It's for any scheduled tasks that would need to only be run once a year.
    /// </summary>
    /// <returns></returns>
    public HttpResponseMessage Get()
    {
        return Request.CreateResponse(HttpStatusCode.OK, 0, new JsonMediaTypeFormatter());
    }


    public AnnualScheduleController()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}