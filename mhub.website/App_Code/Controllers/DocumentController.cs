﻿using System.Collections.Generic;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using umbraco.cms.businesslogic.web;
using Umbraco.Web.WebApi;

public class DocumentController : UmbracoApiController//ApiController
{
    // GET umbraco/api/document/get/1060
    public HttpResponseMessage Get(int id)
    {
        Document d = new Document(id);
        var dictionary = new Dictionary<string, object>();
        d.GenericProperties.ForEach(p => dictionary[p.PropertyType.Alias] = p.Value);
        return Request.CreateResponse(HttpStatusCode.OK, dictionary, new JsonMediaTypeFormatter());
    }
}
