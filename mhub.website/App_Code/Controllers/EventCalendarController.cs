﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Script.Serialization;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web.WebApi;
using System.Linq;

public class EventCalendarController : UmbracoApiController
{
      
    public HttpResponseMessage Get()
    {
        var result = new List<CalendarEvent>();

        try
        {
            var entries = Services.ContentService.GetChildren(1347);
            var limitFromDate = DateTime.UtcNow.AddMonths(-2);
            
            foreach (var entry in entries)
            {
                var starts = entry.GetValue<DateTime>("starts");
				if (starts < limitFromDate || entry.Status != ContentStatus.Published)
					continue;

				result.Add(new CalendarEvent()
                {
                    Id = entry.Id,
                    Title = entry.Name,
                    Subtitle = entry.GetValue<string>("subtitle"),
                    StartsAt = starts,
                    StartsAtMonth = starts.ToString("MMM", CultureInfo.InvariantCulture),
                    StartsAtDay = starts.Day,
                    Url = Umbraco.NiceUrl(entry.Id),
                    EndsAt = entry.GetValue<DateTime?>("ends")
                });
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<EventCalendarController>("EventCalendarController.Get():", e);
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Sorry, but an error just occured. We are notified and will look into it.");
        }

        return Request.CreateResponse(HttpStatusCode.OK, result, new JsonMediaTypeFormatter());
    }

    public HttpResponseMessage GetDatesToRemember()
    {
        var result = new List<CalendarEvent>();

        try
        {
            var entries = Services.ContentService.GetChildren(1347);
            var limitFromDate = DateTime.UtcNow;

            foreach (var entry in entries)
            {
                var starts = entry.GetValue<DateTime>("starts");
				if (starts < limitFromDate || entry.Status != ContentStatus.Published)
					continue;

                DateTime Dfix = new DateTime(2016, 01, 10);

                result.Add(new CalendarEvent()
                {
                    Id = entry.Id,
                    Title = entry.Name,
                    StartsAt = Dfix, //starts,
                    StartsAtMonth = starts.ToString("MMM", CultureInfo.InvariantCulture), 
                    StartsAtDay = starts.Day,
                    EndsAt = entry.GetValue<DateTime?>("ends")
                });
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<EventCalendarController>("EventCalendarController.GetDatesToRemember():", e);
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Sorry, but an error just occured. We are notified and will look into it.");
        }

        return Request.CreateResponse(HttpStatusCode.OK, result, new JsonMediaTypeFormatter());
    }

    public HttpResponseMessage GetEventDetail(string fulldate = "")
    {
        var result = new List<CalendarEvent>();
        var companyCalendarNode = umbraco.NodeExtensions.GetDescendantNodesByType(new umbraco.NodeFactory.Node(-1), "Calendar").First();

        try
        {
            var entries = Services.ContentService.GetChildren(companyCalendarNode.Id).Where(x => x.GetValue<DateTime>("starts").ToString("yyyy-MM-dd").Equals(fulldate));  
            var limitFromDate = DateTime.UtcNow;
               
            foreach (var entry in entries)
            {
                var starts = entry.GetValue<DateTime>("starts");
                if (starts < limitFromDate || entry.Status != ContentStatus.Published)
                    continue;

                result.Add(new CalendarEvent()
                {
                    Id = entry.Id,
                    Title = entry.Name,
                    StartsAt = starts,
                    StartsAtMonth = starts.ToString("MMM", CultureInfo.InvariantCulture),
                    StartsAtDay = starts.Day,
                    EndsAt = entry.GetValue<DateTime?>("ends"),
                    Subtitle = entry.GetValue<String>("subtitle")
                });

            } 

        }
        catch (Exception e)
        {
            LogHelper.Error<EventCalendarController>("EventCalendarController.GetDatesToRemember():", e);
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Sorry, but an error just occured. We are notified and will look into it.");
        }

        return Request.CreateResponse(HttpStatusCode.OK, result, new JsonMediaTypeFormatter());
    }
     
    public HttpResponseMessage GetDatesEvent(string dateYM = "")
    {
        Dictionary<string, CalendarEventData> calendarData = new Dictionary<string, CalendarEventData>();
        try
        {
            var companyCalendarNode = umbraco.NodeExtensions.GetDescendantNodesByType(new umbraco.NodeFactory.Node(-1), "Calendar").First(); 
            //var entries = Services.ContentService.GetChildren(companyCalendarNode.Id).Where(x => x.GetValue<DateTime>("starts").ToString("yyyy-MM").Equals(dateYM));
            var entries = Services.ContentService.GetChildren(companyCalendarNode.Id);

            var limitFromDate = DateTime.UtcNow;

            foreach (var entry in entries)
            {
                var starts = entry.GetValue<DateTime>("starts");
                if (starts < limitFromDate || entry.Status != ContentStatus.Published)
                    continue;

                if (calendarData.ContainsKey(starts.ToString("yyyy-MM-dd")))
                {
                    calendarData[starts.ToString("yyyy-MM-dd")].number += 1;
                }
                else {
                    calendarData.Add(starts.ToString("yyyy-MM-dd"), new CalendarEventData { cssClass = "active special", number = 1 });
                }
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<EventCalendarController>("EventCalendarController.GetDatesToRemember():", e);
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Sorry, but an error just occured. We are notified and will look into it.");
        }

        return Request.CreateResponse(HttpStatusCode.OK, calendarData, new JsonMediaTypeFormatter());
    }
}
 
 
