﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;


    public class ContactFormController : SurfaceController
    {
        [HttpPost]
        public ActionResult Submit(ContactFormViewModel model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            /// Work with form data here
            ViewBag.Name = model.Name;
            TempData["test"] = model.Name;


            return RedirectToCurrentUmbracoPage();
        }
    }
