﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Web.WebApi;

/// <summary>
/// Summary description for HolidayController
/// </summary>
public class HolidayController : UmbracoApiController
{

    public HttpResponseMessage GetHolidays()
    {
        var member = MhubMemberHelper.LoggedInMember;
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select * from mhubPublicHolidays where HubRoot=@0 order by HoliDayDate";

        var holTable = db.Fetch<PublicHolidays>(query, member.GetValue<int>("hub"));
        return Request.CreateResponse(HttpStatusCode.OK, holTable, new JsonMediaTypeFormatter());
    }
    
    [HttpPost]
    public HttpResponseMessage AddHoliday(PublicHolidays data)
 
    {
// public static List<string> items = new List<string>();   
  //pulic static list<PublicHolidays> result= new list<PublicHolidays>();
 // var  result=data;
   /* var result= new PublicHolidays{
    
   Description=data.Description;
    HoliDayDate=data.HoliDayDate;
    Location=data.Location;
    };
    */
    
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var member = MhubMemberHelper.LoggedInMember;
        var publicHolidays = new PublicHolidays
      
            {
           
               Description=data.Description,
    HoliDayDate=data.HoliDayDate,
    Location=data.Location,
    HubRoot=member.GetValue<int>("hub"),




            };
           // var query="insert into mhubPublicHolidays (HoliDayDate,Location,Description) values (@0,@1,@2)";
          //  var holTable = db.RawQuery<PublicHolidays>(query,data.HoliDayDate,data.Location,data.Description);
             var result = db.Insert(publicHolidays);
   // var publicSuccess= db.Insert(publicHolidays);
     return Request.CreateResponse(HttpStatusCode.OK,data.HoliDayDate);
    }

    public HttpResponseMessage RemoveHoliday(PublicHolidays data)

    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        Console.WriteLine("Hello");
        if (data.Id==0)
            {
            var query = "select * from mhubPublicHolidays where HoliDayDate=@0 and Location=@1 and Description=@2";

            var holTable = db.Fetch<PublicHolidays>(query, data.HoliDayDate, data.Location, data.Description);
            if (holTable.Count!=0)
            {   foreach (var ht in holTable)
                {
                    var result = db.Delete(ht);
                }
            }
            else
            {
                data.Id = 2;
            }
        }
        else
        {
            var result = db.Delete(data);
        }
       // var result=db.Delete(data);

        return Request.CreateResponse(HttpStatusCode.OK, data.Id);
    }


    public HolidayController()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}