﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using System.Web.Security;
using Umbraco.Core;

/// <summary>
/// Summary description for TravelRequest
/// </summary>
public class TravelRequestSurfaceController : SurfaceController
{
    [HttpPost]
    public ActionResult TravelRequest(UmbracoWithMvc.Entities.TravelRequest model)
    {

        if (!ModelState.IsValid)
        {
            ViewBag.Message = "Form was was not submitted Successfully, Please review";
            return CurrentUmbracoPage();
        }

        TempData["tempval"] = "Submitted Successfully";
        model.member_id = MhubMemberHelper.LoggedInMember.Id;

        var db = ApplicationContext.Current.DatabaseContext.Database;
        db.Insert(model);

        //send travel request to line manager
        var mManager_Id = MhubMemberHelper.LoggedInMember.GetValue<int>("lineManager");
        var mManager = ApplicationContext.Current.Services.MemberService.GetById(mManager_Id);
        //Message in html
        var eMessage = "A travel request has been submitted by " +
                                        MhubMemberHelper.LoggedInMember.Name + " with Username" +
                                        MhubMemberHelper.LoggedInMember.Username + " See the details of request below <br><br> Travel Date:" +
                                        model.TravelDate + " Preffered Time " +
                                        model.ArrivalTime + "<br> Return Date" +
                                        model.ReturnDate + " Preffered Departure Time " +
                                        model.DepartureTime + "<br><br>Rail card needed:" +
                                        model.RailcardNeed + " Hotel needed:" +
                                        model.HotelNeed + "<br><br>Hotel Name: " +
                                        model.HotelName + " Prefered Hotel location:" +
                                        model.HotelLocation + "<br><br> Other Notes" +
                                        model.OtherNotes;
        var eSubject = "A travel request has been submitted";
        if (mManager!=null)
        {
            var mEmail = mManager.Email;
            //Funtion to send email
            AlertHelpers.emailAlert(eMessage, mEmail, eSubject);
        }

        return RedirectToCurrentUmbracoPage();
    }
}