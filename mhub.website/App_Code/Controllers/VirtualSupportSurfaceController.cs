﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using System.Web.Security;
using Umbraco.Core;

/// <summary>
/// Summary description for VirtualSupportSurfaceController
/// </summary>
public class VirtualSupportSurfaceController : SurfaceController
{

    [HttpPost]
    public ActionResult VirtualRequest(UmbracoWithMvc.Entities.VirtualSupport model)
    {

        if (!ModelState.IsValid)
        {
            ViewBag.Message = "Form was was not submitted Successfully, Please review";
            return CurrentUmbracoPage();
        }

        TempData["tempval"] = "Submitted Successfully";
        var RootHub = MhubMemberHelper.LoggedInMember.GetValue<int>("hub");
        var RootPage = Umbraco.TypedContent(RootHub);
        var TmanId = RootPage.GetProperty("trainingManager").Value;
        var tmanId = Convert.ToInt32(TmanId);
        var TrainingMan = ApplicationContext.Current.Services.MemberService.GetById(tmanId);

        model.Member_Id = MhubMemberHelper.LoggedInMember.Id;

        var db = ApplicationContext.Current.DatabaseContext.Database;
        db.Insert(model);

        //send travel request to line manager
        var mManager_Id = MhubMemberHelper.LoggedInMember.GetValue<int>("lineManager");
        var mManager = ApplicationContext.Current.Services.MemberService.GetById(mManager_Id);
        //Message in html
        // var eMessage = "";
        var eMessage = "<html><body><p>Please respond the users support request within the next 24 hours:</p><table><tr><td>Name:</td><td>" +
                                                    MhubMemberHelper.LoggedInMember.Name + "</td></tr><tr><td>Email:</td><td>" +
                                                    MhubMemberHelper.LoggedInMember.Email + "</td></tr><tr><td>Branch:</td><td>" +
                                                    MhubMemberHelper.LoggedInMember.GetValue<string>("Branch") + "</td></tr><tr><td>Support Needed:</td><td>" +
                                                    model.SupportNeeded + "</td></tr></table>";
        var eSubject = "Training support request";
        if (mManager != null)
        {
            var mEmail = mManager.Email;
            //Funtion to send email
            AlertHelpers.emailAlert(TrainingMan.Email, eMessage, eSubject);
        }

        return RedirectToCurrentUmbracoPage();
    }
}