﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.WebApi;

public class ProfileController : UmbracoApiController
{
    private IMediaService _mediaService;
    private SocialRepository _socialRepository;

    public ProfileController()
    {
        _mediaService = Services.MediaService;
        _socialRepository = new SocialRepository();
    }

    public HttpResponseMessage Get()
    {
        try
        {
            int currentMemberId = Members.GetCurrentMemberId();
            IMember currentMember = Services.MemberService.GetById(currentMemberId);

            if (currentMember == null)
            {
                return Request.CreateUserNoAccessResponse();
            }

            IEnumerable<SocialMessage> messages = _socialRepository.GetMessagesByMember(currentMemberId);

            if (currentMember != null && messages != null)
            {
                ProfileModel model = new ProfileModel();
                model.Profile = new Employee(currentMember, Services.MediaService);
                model.Messages = messages.Select(msg => IntranetMessage.From(msg));

                return Request.CreateResponse(HttpStatusCode.OK, model, new JsonMediaTypeFormatter());
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<ProfileController>("ProfileController.Get():", e);
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Sorry, but an error just occured. We are notified and will look into it.");
        }

        return Request.CreateUserNoAccessResponse();
    }

    [HttpPost]
    public async Task<HttpResponseMessage> PhotoUpload()
    {
        try
        {
            int currentMemberId = Members.GetCurrentMemberId();
            IMember currentMember = Services.MemberService.GetById(currentMemberId);

            if (currentMember == null)
            {
                return Request.CreateUserNoAccessResponse();
            }

            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);

            var image = provider.Contents.FirstOrDefault(x => x.Headers.ContentType != null &&
                                                            x.Headers.ContentType.MediaType.Contains("image"));

            if (image != null)
            {
                string extension = ".jpg";

                var filenameFromHeader = image.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                if (!string.IsNullOrEmpty(filenameFromHeader) && Path.HasExtension(filenameFromHeader))
                {
                    extension = Path.GetExtension(filenameFromHeader);
                }

                var filename = Convert.ToBase64String(Guid.NewGuid().ToByteArray())
                                    .Replace("/", "_")
                                    .Replace("+", "-")
                                    .Substring(0, 22) + extension;

                var stream = await image.ReadAsStreamAsync();
                var media = _mediaService.CreateMedia(filename, 1281, "Image");
                media.SetValue("umbracoFile", filename, stream);
                _mediaService.Save(media);

                var response = new PhotoUpload
                {
                    Id = media.Id,
                    Name = media.Name,
                    ParentId = media.ParentId,
                    Path = media.GetValue("umbracoFile")
                };

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<ProfileController>("ProfileController.PhotoUpload():", e);
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
        }

        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An error occured during upload");
    }

    [HttpPost]
    public async Task<HttpResponseMessage> PhotoSave(PhotoUpload photo)
    {
        try
        {
            int currentMemberId = Members.GetCurrentMemberId();
            IMember currentMember = Services.MemberService.GetById(currentMemberId);

            if (currentMember == null)
            {
                return Request.CreateUserNoAccessResponse();
            }

            if (currentMember.HasProperty("picture"))
            {
                currentMember.SetValue("picture", photo.Id);
                Services.MemberService.Save(currentMember);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<ProfileController>("ProfileController.PhotoSave():", e);
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
        }

        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Could not save photo to profile");
    }

    [HttpPost]
    public HttpResponseMessage Save(ProfilePostData profile)
    {
        try
        {
            int currentMemberId = Members.GetCurrentMemberId();
            IMember currentMember = Services.MemberService.GetById(currentMemberId);

            if (currentMember == null)
            {
                return Request.CreateUserNoAccessResponse();
            }

            if (ModelState.IsValid)
            {
                currentMember.Email = profile.Email;
                currentMember.Name = profile.Name;

                Services.MemberService.Save(currentMember);
                return Request.CreateResponse(HttpStatusCode.OK, "Profile successfully updated");
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<ProfileController>("ProfileController.Save():", e);
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Sorry, but an error just occured. We are notified and will look into it.");
        }

        return Request.CreateValidationErrorResponse(ModelState.Values);
    }
}