﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.WebApi;
using System.Net.Http;
using System.Net;
using System.Net.Http.Formatting;
using Umbraco.Core;
using UmbracoWithMvc.Controllers;
using System.Web.Http;
using umbraco.cms.businesslogic.web;
using UmbracoWithMvc.Entities;
using Umbraco.Core.Models;

/// <summary>
/// Summary description for MhubScheduleController for Scheduled tasks that should be run daily/weekly
/// </summary>
public class MhubScheduleController : UmbracoApiController
{

    public HttpResponseMessage Get()
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "";
        var query2 = "";
        var eMessage = "";
        var MemberApp = new MemberAppraisal();
        //    MemberApp.TemplateId = 5;
        //   MemberApp.CreatedDate = DateTime.Today;
        //    MemberApp.TemplateName = "Default";
        //    MemberApp.AppraisalSchedule = appSched;
        //    MemberApp.Member_Id = 1171;
        var AppQuest = new List<AppraisalAnswer>();
        bool AppraisalExists = false;
        var members = ApplicationContext.Current.Services.MemberService.GetAllMembers();
        var memList = members.ToList().Where(m => m.GetValue<int>("appraisal") != 0 && m.GetValue<int>("appraisalSchedule") != 111 && m.GetValue<int>("appraisalSchedule") != 0);

        foreach (var m in memList)
        {
            //function to check if appraisal needs to be created
            AppraisalExists = AppraisalHelpers.AppraisalCheck(m);
            var carStartCheck = AppraisalHelpers.CheckStartDate(m);

            //this gets the appraisal template and questions set and creates appraisal for individual user.
            if (!AppraisalExists && carStartCheck==true && (MhubMemberHelper.userIsActive(m)))
            {

                query2 = "select * from mhubQuestionSet where QTemplate=@0";
                query = "select * from mhubAppraisalForms where id=@0 ";
                var appraisalTable = db.SingleOrDefault<AppraisalForms>(query, m.GetValue<int>("appraisal"));
                var questSet = db.Fetch<QuestionSet>(query2, m.GetValue<int>("appraisal"));
                var currentYear = DateTime.Today.Year;
                // var appSched = m.GetValue<int>("appraisalSchedule");
                if (appraisalTable != null)
                {
                    MemberApp.ScheduleAlias = AppraisalHelpers.AppraisalScAlias(m.GetValue<int>("appraisalSchedule"));
                    MemberApp.TemplateId = appraisalTable.Id;
                    MemberApp.CreatedDate = DateTime.Today;
                    MemberApp.TemplateName = appraisalTable.TemplateName; //appraisalTable.TemplateName;
                    MemberApp.AppraisalSchedule = m.GetValue<int>("appraisalSchedule");
                    MemberApp.Member_Id = m.Id;
               
              //  if (MemberApp.AppraisalSchedule != 114 && currentYear==2016)
                //{
                    db.Insert("mhubMemberAppraisal", "Id", MemberApp);

                    var questSets = new AppraisalAnswer();
                    foreach (QuestionSet question in questSet)
                    {
                        questSets.QTemplate = MemberApp.Id;
                        questSets.QWording = question.QWording;
                        questSets.QType = question.QType;
                        questSets.QSection = question.QSection;
                        questSets.QCalc = question.QCalc;
                        db.Insert(questSets);
                    }
                    var eSubject = "New Appraisal Available";

                    var mesBody = "Please complete your appraisal form for this appraisal period. You can find this form in the MonroeHub under Monroe Administration / Employee Appraisal.</p> <p>The Monroe appraisal process is as follows:</p><ul><li>Step 1:  the employee fills out his or her part of the appraisal, which will be sent to the direct supervisor</li><li>Step 2: the direct supervisor provides feedback</li><li>Step 3: The employee and direct supervisor sit down together to formalise the discussed appraisal actions</li></ul>";
                    eMessage = AlertHelpers.emailBodyWarmest(mesBody, m);
                    var uEmail = m.Email;
                   // uEmail = "dean@bakertillythailand.com";
                    AlertHelpers.emailAlert(eMessage, uEmail, eSubject);
              //  }
                }

            }
        }


        return Request.CreateResponse(HttpStatusCode.OK, eMessage, new JsonMediaTypeFormatter());

    }
    [HttpGet]
    public HttpResponseMessage trainingReminder()
    {
        int totalRecords;
        var db = ApplicationContext.DatabaseContext.Database;
        var members = ApplicationContext.Current.Services.MemberService.GetAll(0, int.MaxValue, out totalRecords);
        //  var RootPage1 = Umbraco.TypedContent(1057);
        //var RemindFreq1 = RootPage1.GetProperty("trainingReminderFrequency").Value.ToString();
        foreach (var member in members)
        {
            var lastTraining = member.GetValue<DateTime>("trainingDate");
            var hubroot = member.GetValue<int>("hub");
            var RootPage = Umbraco.TypedContent(hubroot);
            if (RootPage != null)
            {
                var RemindFreq = RootPage.GetProperty("trainingReminderFrequency").Value.ToString();
                RemindFreq = RemindFreq.ToString();
                int RemindFreqInt = 365;
                DateTime CurrentDate = DateTime.Today;

                int daysDifference = CurrentDate.Date.Subtract(lastTraining.Date).Days;
                bool res = int.TryParse(RemindFreq, out RemindFreqInt);
                if (daysDifference >= RemindFreqInt)
                {
                    if (!AlertHelpers.alertExist("training", member.Id, RemindFreqInt))
                    {
                        var eMessage = "Please complete a training module, you're training is not up to date";
                        var mesBody = AlertHelpers.emailBody(eMessage, member);
                        var eSubject = "Training needs to be completed";
                        AlertHelpers.emailAlert(mesBody, member.Email, eSubject);
                        AlertHelpers.createAlert("training", member.Id, eSubject, "Training Reminder");
                    }

                }
            }




        }
        return Request.CreateResponse(HttpStatusCode.OK, totalRecords, new JsonMediaTypeFormatter());
    }
    [HttpGet]
    public HttpResponseMessage leaveReminder()
    {
        var db = ApplicationContext.DatabaseContext.Database;
        var leaveReminder = 5;
        // query gets all leaverequests up to a month over current date that are not approved
        var query = " select memberId,LeaveStart, CreatedAt, LeaveType, LeaveEnd,cmsPropertyData.dataInt LineManager, cmsMember.Email as LineEmail from mhubLeaveRequest, cmspropertyData, cmsMember where cmsPropertyData.contentNodeId = mhubLeaveRequest.MemberId and cmsPropertyData.dataInt = cmsMember.NodeId and ApprovedLeave = 0 and DateDiff(day, LeaveStart, getDate())< 30 and DateDiff(day, CreatedAt, getDate())> @0 and cmsPropertyData.propertytypeid = 80  group by memberId,leaveStart,leaveEnd, CreatedAt, LeaveType,cmsPropertyData.dataInt, cmsMember.Email ";
        var leaveRequest = db.Fetch<LeaveRequest>(query, leaveReminder);
        foreach (var lRequest in leaveRequest)
        {
            if (!AlertHelpers.alertExist("leaveRequest", lRequest.LineManager, leaveReminder))
            {

                var member = ApplicationContext.Current.Services.MemberService.GetById(lRequest.MemberId);
                var eSubject = "Leave Request is awaiting approval";
                var mesBody = "<p>A  holiday  request submitted  over 5  days  ago  is  still  pending  your  approval.  Please  action  this  request immediately</p><p><table><tr><td>Leave type:</td><td>" + lRequest.LeaveType + "</td></tr><tr><td>Name:</td><td>" + member.Name + "</td></tr><tr><td>Email:</td><td>" + member.Email + "</td></tr><tr><td>Date:</td><td>" + lRequest.LeaveStart.ToString("MMMM dd, yyyy") + " to " + lRequest.LeaveEnd.ToString("MMMM dd, yyyy") + "</td></tr></table></p>";
                var eMessage = AlertHelpers.emailBody(mesBody, ApplicationContext.Current.Services.MemberService.GetById(lRequest.LineManager));
               
                AlertHelpers.emailAlert(mesBody, lRequest.LineEmail, eSubject);
              //  AlertHelpers.emailAlert(mesBody, "dean@bakertillythailand.com", eSubject);
                AlertHelpers.createAlert("leaveRequest", lRequest.LineManager, eSubject, "Leave Request is awaiting approval");
            }
        }

        return Request.CreateResponse(HttpStatusCode.OK, leaveReminder, new JsonMediaTypeFormatter());
    }

    [HttpGet]
    public HttpResponseMessage appraisalReminder()
    {

        var db = ApplicationContext.DatabaseContext.Database;
        var query = "select *, DateDiff(day,CreatedDate,getDate()) as daysSince from mhubmemberAppraisal where DateDiff(day,CreatedDate,getDate())<90 and AppStatus in (0,1) order by Member_id, CreatedDate desc";
        var appraisals = db.Fetch<MemberAppraisal>(query, 2);
        int totalRecords = appraisals.Count();
        var userId = 0;
        // var members = ApplicationContext.Current.Services.MemberService.GetAll(0, int.MaxValue, out totalRecords);
        // var alertCheck = AlertHelpers.alertExist2("appraisal", 1527, 14);
        foreach (var appraisal in appraisals)
        {

            var member = ApplicationContext.Current.Services.MemberService.GetById(appraisal.Member_Id);
            IPublishedContent RootPage = null;
          
            if (member!=null)
            {
                if ((MhubMemberHelper.userIsActive(member)))
                {
                    RootPage = Umbraco.TypedContent(member.GetValue<int>("hub"));
                }
            }

            if (RootPage != null && userId!=appraisal.Member_Id)
            {
                var RemindFreq = RootPage.GetProperty("appraisalReminder").Value.ToString();
                var ManRemindFreq = RootPage.GetProperty("apraisal").Value.ToString();
                int RemindFreqInt = 365;
                int ManRemindFreqInt = 365;
                bool res = int.TryParse(RemindFreq, out RemindFreqInt);
                bool reMan = int.TryParse(RemindFreq, out ManRemindFreqInt);
                var daysSinceCreation = appraisal.daysSince;
                var lineMan = ApplicationContext.Current.Services.MemberService.GetById(member.GetValue<int>("lineManager"));

                if (daysSinceCreation > RemindFreqInt && !AlertHelpers.alertExist("appraisal", member.Id, RemindFreqInt) && (appraisal.AppStatus == 0 || appraisal.AppStatus == 1))
                {
                    var eMessage = "You have not yet actioned the appraisal request you were sent. Please fill out your appraisal form for this appraisal period immediately to ensure a speedy appraisal process. You can find this form in the MonroeHub under Monroe Administration / Employee Appraisal. </p><p>The Monroe appraisal process is as follows:</p><ul><li>Step 1:  the employee fills out his or her part of the appraisal, which will be sent to the direct supervisor</li><li>Step 2: the direct supervisor provides feedback</li><li>Step 3: The employee and direct supervisor sit down together to formalise the discussed appraisal actions</li></ul>";
                    var mesBody = AlertHelpers.emailBodyWarmest(eMessage, member);
                    var eSubject = " Appraisal Reminder";
                    if (reMan && ManRemindFreqInt < daysSinceCreation)

                    {
                        AlertHelpers.emailAlert(mesBody, member.Email, eSubject);
                        AlertHelpers.createAlert("appraisal", member.Id, eSubject, "Appraisal Reminder", appraisal.Id);
                    }
                    else
                    {


                        AlertHelpers.emailAlert(mesBody, member.Email, eSubject, new string[1] { lineMan.Email });
                        AlertHelpers.createAlert("appraisal", member.Id, eSubject, "Appraisal Reminder ccMan", appraisal.Id);
                    }
                }
                else if (appraisal.daysSinceMod > RemindFreqInt && !AlertHelpers.alertExist("appraisal", member.Id, RemindFreqInt) && (appraisal.AppStatus == 2 || appraisal.AppStatus == 3))
                {
                    var eMessage = "You  have  not  actioned  the  appraisal  request  you  were  sent.  Please  ensure  you  do  this  immediately  to ensure a speedy appraisal process.";
                    var mesBody = AlertHelpers.emailBodyWarmest(eMessage, lineMan);
                    var eSubject = "Appraisal Reminder";
                    AlertHelpers.emailAlert(mesBody, lineMan.Email, eSubject);
                    AlertHelpers.createAlert("appraisal", member.Id, eSubject, "Appraisal Reminder Manager", appraisal.Id);
                }

                userId = appraisal.Member_Id;

            }

        }
        return Request.CreateResponse(HttpStatusCode.OK, totalRecords, new JsonMediaTypeFormatter());
    }

    //TODO: check if need to check that training for a module is complete but assesment is not.
    [HttpGet]
    public HttpResponseMessage assessmentReminder()
    {
        int totalRecords;
        var db = ApplicationContext.DatabaseContext.Database;
        var mem = ApplicationContext.Current.Services.MemberService.GetAll(0, int.MaxValue, out totalRecords);
        var members = mem.ToList().Where(m => m.GetValue<int>("roleLevel") == 152 || m.GetValue<int>("roleLevel") == 153);
        foreach (var member in members)
        {

            var hubroot = member.GetValue<int>("hub");
            var RootPage = Umbraco.TypedContent(hubroot);
            if (RootPage != null && (MhubMemberHelper.userIsActive(member)))
            {
                var RemindFreq = RootPage.GetProperty("assessmentReminderFrequency").Value.ToString();
                int RemindFreqInt = 365;
                bool res = int.TryParse(RemindFreq, out RemindFreqInt);

                var query = "select * from AssessmentAnswers where DateDiff(day,CompletedAt, getDate()) >@0 and memberId=@1";
                var AssessResult = db.Fetch<AssessmentAnswerSet>(query, RemindFreqInt, member.Id);
                if (AssessResult.Count != 0)
                {
                    if (!AlertHelpers.alertExist("assessment", member.Id, RemindFreqInt))
                    {
                        var eMessage = "Please complete an assessment, your training is not up to date";
                        var mesBody = AlertHelpers.emailBody(eMessage, member);
                        var eSubject = "Assessment Reminder";
                        AlertHelpers.emailAlert(mesBody, member.Email, eSubject);
                        AlertHelpers.createAlert("assessment", member.Id, eSubject, "Assessment Reminder");
                    }
                    //send email alert
                }
            }

        }
        return Request.CreateResponse(HttpStatusCode.OK, totalRecords, new JsonMediaTypeFormatter());
    }

    //   when this is run will send birthday and anniversary notifications (7 days out, and on actual day)
    [HttpGet]
    public HttpResponseMessage birthdayNotifications()
    {
        int totalRecords;
        var members = ApplicationContext.Current.Services.MemberService.GetAll(0, int.MaxValue, out totalRecords);
        foreach (var member in members)
        {
            if (MhubMemberHelper.userIsActive(member))
            {
                birthdayEmail(member);
                anniversaryEmail(member);
            }

        }
      //  assessmentReminder();
        return Request.CreateResponse(HttpStatusCode.OK, totalRecords, new JsonMediaTypeFormatter());
    }
    /// <summary>
    ///  checks and sends out message to manage if its the users birthday or if its coming soon
    /// </summary>
    private void birthdayEmail(IMember member)
    {

        DateTime CurrentDate = DateTime.Today;
        var StringCurrent = CurrentDate.ToShortDateString();
        var CurrentMonth = CurrentDate.Month;
        var CurrentDay = CurrentDate.Day;
        var MemberId = 0;
        string lineEmail = "";
        string lineName = "";
        string eMessage = "";
        string eSubject = "";
        if (member.GetValue("birthday") != null)
        {
            DateTime mBD = member.GetValue<DateTime>("birthday");
            var mBdMonth = mBD.Month;
            var mBdDay = mBD.Day;

            var compareDate = new DateTime(CurrentDate.Year, mBD.Month, mBD.Day);
            int daysDifference = compareDate.Date.Subtract(CurrentDate.Date).Days;
            var lineMId = member.GetValue<int>("lineManager");
            MemberId = member.Id;
            var lineManager = ApplicationContext.Current.Services.MemberService.GetById(lineMId);
            if (CurrentMonth == mBdMonth && CurrentDay == mBdDay)
            {

                if (lineManager != null)
                {
                    lineEmail = lineManager.Email;
                    lineName = lineManager.Name;
                    eSubject = "Today is the birthday of " + member.Name + "";
                    eMessage = "Hello " + lineName + ", Please be aware that today is the birthday of " + member.Name;
                    AlertHelpers.emailAlert(eMessage, lineEmail, eSubject);
                }
            }
            else if (daysDifference <= 7 && daysDifference > 0)
            {   //checks to see if an alert has been sent already true means exists
                bool checkAlert = AlertHelpers.alertExist("birthday", member.Id, 7);
                if (!checkAlert && lineManager!=null)
                {
                    lineEmail = lineManager.Email;
                    lineName = lineManager.Name;
                    eSubject = daysDifference + " days until the birthday of " + member.Name;
                    eMessage = "Hello " + lineName + ", Please be aware that " + member.Name + "s' birthday is coming up on the " + mBdDay + "th of " + mBD.ToString("MMM");

                    AlertHelpers.emailAlert(eMessage, lineEmail, eSubject);
                    AlertHelpers.createAlert("birthday", member.Id, eSubject, "Work Aniverasy");
                }
            }

        }
    }

    private void anniversaryEmail(IMember member)
    {
        DateTime CurrentDate = DateTime.Today;
        var StringCurrent = CurrentDate.ToShortDateString();
        var CurrentMonth = CurrentDate.Month;
        var CurrentDay = CurrentDate.Day;
        var CompStart = member.GetValue<DateTime>("companyStart");
        // var daysDifference = 8;
        if (CompStart != null)
        {
            var YearsWorked = (CurrentDate.Date.Subtract(CompStart.Date).Days) / 365;
            var lineMId = member.GetValue<int>("lineManager");
            var lineManager = ApplicationContext.Current.Services.MemberService.GetById(lineMId);
            var CompStartMonth = CompStart.Month;
            var CompStartDay = CompStart.Day;

            var compareDate = new DateTime(CurrentDate.Year, CompStartMonth, CompStartDay);
            int daysDiffernce = compareDate.Date.Subtract(CurrentDate.Date).Days;
            if (CurrentMonth == CompStartMonth && CurrentDay == CompStartDay && lineManager != null)
            {
                var lineEmail = lineManager.Email;
                var lineName = lineManager.Name;
                var eSubject = "Today is the work anniversary of " + member.Name + "";
                var eMessage = "Hello " + lineName + ", Please be aware that today is the " + YearsWorked + " year anniversary of " + member.Name + " working at the company";
                AlertHelpers.emailAlert(eMessage, lineEmail, eSubject);
                var memberService = ApplicationContext.Current.Services.MemberService;
                var experience = member.GetValue<int>("experience");
                member.SetValue("experience", experience + 1);
                memberService.Save(member);
            }
            else if (daysDiffernce < 7 && daysDiffernce>0)
            {
                bool checkAlert = AlertHelpers.alertExist("anniversary", member.Id, 7);

                if (!checkAlert && lineManager != null)
                {
                    var lineEmail = lineManager.Email;
                    var lineName = lineManager.Name;
                    var eSubject = daysDiffernce + " days until the work anniversary of " + member.Name;
                    var eMessage = "Hello " + lineName + ", Please be aware that the " + YearsWorked + " year anniversary of " + member.Name + " is coming up on the " + CompStartDay + "th of " + CompStart.ToString("MMM");

                    AlertHelpers.emailAlert(eMessage, lineEmail, eSubject);
                    AlertHelpers.createAlert("anniversary", member.Id, eSubject, "Work Aniverasy");
                }

            }
        }
    }
    public MhubScheduleController()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}