﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using umbraco;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web.WebApi;

public class NewsController : UmbracoApiController
{
    public HttpResponseMessage Get()
    {
        var result = new List<NewsItem>();

        try
        {
            var entries = Services.ContentService.GetChildren(1087);
            var limitFromDate = DateTime.UtcNow;
            
            foreach (var entry in entries)
            {
                if (entry.Status != ContentStatus.Published)
                {
                    continue;
                }

                var date = entry.CreateDate;
                if (entry.ReleaseDate.HasValue)
                {
                    date = entry.ReleaseDate.Value;
                }

                var pictureId = entry.GetValue<int>("image");
                var media = Services.MediaService.GetById(pictureId);
                string imageUrl = null;
                    
                if (media != null)
                {
                    imageUrl = media.GetValue<string>("umbracoFile");
                }

				result.Add(new NewsItem()
				{
					Id = entry.Id,
					Title = entry.Name,
					PublishedAt = date,
					PublishedAtMonth = date.ToString("MMM", CultureInfo.InvariantCulture),
					PublishedAtDay = date.Day,
					Url = Umbraco.NiceUrl(entry.Id),
                    Text = entry.GetValue<string>("text"),
                    Image = imageUrl
				});               
                
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<NewsController>("NewsController.Get():", e);
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Sorry, but an error just occured. We are notified and will look into it.");
        }

        return Request.CreateResponse(HttpStatusCode.OK, result, new JsonMediaTypeFormatter());
    }

    public HttpResponseMessage GetNewsLanding()
    {
        var newsLanding = uQuery.GetNodesByType("NewsFolder").First();
        if(newsLanding != null)
        {
            //Find Image Url
            var featureImageId = newsLanding.GetProperty("featureImage");
            if(featureImageId != null && !string.IsNullOrEmpty(featureImageId.Value))
            {
                var media = Services.MediaService.GetById(Convert.ToInt16(featureImageId.Value));
                string imageUrl = null;

                if (media != null)
                {
                    imageUrl = media.GetValue<string>("umbracoFile");
                }

                var newsFolder = new NewsFolder
                {
                    NodeName = newsLanding.Name,
                    FeatureImage = imageUrl
                };
                return Request.CreateResponse(HttpStatusCode.OK, newsFolder, new JsonMediaTypeFormatter());
            }
        }
        return Request.CreateResponse(HttpStatusCode.NotFound);
    }
}
