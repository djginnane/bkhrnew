﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.WebApi;
using System.Net.Http;
using System.Net;
using System.Net.Http.Formatting;
using Umbraco.Core;
using UmbracoWithMvc.Controllers;
using UmbracoWithMvc.Entities;
using System.Web.Http;
using System.Web.Security;
using Umbraco.Core.Models;

/// <summary>
/// Summary description for AssessmentReport
/// </summary>

public class AssessmentReportController : UmbracoApiController
{

    public HttpResponseMessage Get()
    {

        var userList = MhubMemberHelper.GetAllMemberList();
        // int[] userList = new int[2];
        var db = ApplicationContext.Current.DatabaseContext.Database;
        //gets Assessments part of report
        var assessList = new List<UserResults>();
       // var query = "select assessmentId as Assessment from AssessmentAnswers where MemberId in (@0) group by assessmentId order by assessmentId";

        //if (userList.Count() == 0)
        //{
        //    query = "select assessmentId as Assessment from AssessmentAnswers where MemberId =(@0) group by assessmentId order by assessmentId";

        //}
        //   var Assessments = db.Fetch<UserResults>(query, userList);
        var assIds = new List<int>();
        var Assessments = MhubContentHelper.GetAssessmentPages;
        foreach (var a in Assessments)
        {
            var uR = new UserResults();
            uR.AssessmentName = a.Name;
            uR.Assessment = a.Id;
            assIds.Add(a.Id);
            assessList.Add(uR);


            //if (Umbraco.TypedContent(a.Assessment) == null)
            //{
            //    if (Services.ContentService.GetById(a.Assessment) != null)
            //    {
            //        a.AssessmentName = Services.ContentService.GetById(a.Assessment).Name;
            //    }
            //    continue;
            //}
            
            //else
            //{
            //    a.AssessmentName = Umbraco.TypedContent(a.Assessment).Name;
            //}




        }
        int[] AssessIds = assIds.ToArray();
        if (AssessIds.Count() != 0)
        {

            //gets User part of report
            var query2 = "select memberId, AssessmentId as Assessment, Grade, Score, Course from AssessmentAnswers where AssessmentId in (@0) and memberId in(@1) order by MemberId,assessmentId,createdAt";
            var uAssessments = db.Fetch<UserResults>(query2, AssessIds, userList);
            foreach (var uid in userList)
            {
                if (!uAssessments.Exists(x => x.memberId == uid))
                {
                    var emptyUser = new UserResults();
                    emptyUser.memberId = uid;
                    emptyUser.Assessment = 11121212;
                    emptyUser.Grade = "fail";
                    emptyUser.Score = 0;
                    uAssessments.Add(emptyUser);
                }

                    }
            var userInfo = MhubContentHelper.createAssessmentReport(assessList, uAssessments);
            return Request.CreateResponse(HttpStatusCode.OK, userInfo, new JsonMediaTypeFormatter());

        }
        else
        {
            string empty = null;
            return Request.CreateResponse(HttpStatusCode.OK, empty, new JsonMediaTypeFormatter());
        }


    }

    [HttpPost]
    public HttpResponseMessage GetByVariables(ReportVariables assReport)
    {
        bool posCheck = false;
        if (assReport.CompanyPositions==null)
        {
            assReport.CompanyPositions = new string[0];
        }
        if (assReport.Division == null)
        {
            assReport.Division = new string[0];
        }
        if (assReport.CompanyPositions.Length==1)
        {
            if (string.IsNullOrWhiteSpace(assReport.CompanyPositions[0]))
            {
                posCheck = true;
            }

        }
        var Division = "";
        int[] userList;
        if (assReport.Branch == "" && assReport.Division.Length==0)
        {
            if ((assReport.CompanyPositions.Length != 0) && !posCheck)
            {
                
                var hubList = MhubMemberHelper.APIGetAll(MhubMemberHelper.LoggedInMember);
                var FilteredHubList = hubList.Where(m => assReport.CompanyPositions.Contains(m.GetValue<string>("position")));
                //  var FilteredHubList = hubList.Where(m => m.GetValue<string>("position") == assReport.CompanyPositions);
                userList = MhubMemberHelper.GetarraylistI(FilteredHubList);
            
            }
            else { userList = MhubMemberHelper.GetAllMemberList(); }
           
        }
        else

        {
            var mList = MhubMemberHelper.GetByDivision(assReport.Branch, Division);
            if ((assReport.CompanyPositions.Length != 0))
            {
                if (!string.IsNullOrWhiteSpace(assReport.CompanyPositions[0]))
                {
                    mList = mList.Where(m => assReport.CompanyPositions.Contains(m.GetValue<string>("position")));
                }
            }
            if (assReport.Division.Length != 0)
            {
                if (!string.IsNullOrWhiteSpace(assReport.Division[0]))
                {
                    mList = mList.Where(m => assReport.Division.Contains(m.GetValue<string>("division")));
                }
            }

            userList = MhubMemberHelper.GetarraylistI(mList);
        }
        var assessList = new List<UserResults>();
        var assIds = new List<int>();
        var Assessments = MhubContentHelper.GetAssessmentPages;
        foreach (var a in Assessments)
        {
            if (!string.IsNullOrEmpty(assReport.CourseType))
            {
                if (a.Parent.Parent.Name== assReport.CourseType)
                {
                    var uR = new UserResults();
                    uR.AssessmentName = a.Name;
                    uR.Assessment = a.Id;
                    assIds.Add(a.Id);
                    assessList.Add(uR);
                }
            }
          
            else
            {
                var uR = new UserResults();
                uR.AssessmentName = a.Name;
                uR.Assessment = a.Id;
                assIds.Add(a.Id);
                assessList.Add(uR);
            }
        }
        int[] AssessIds = assIds.ToArray();

        var db = ApplicationContext.Current.DatabaseContext.Database;
        //gets Assessments part of report
       // var query = "select assessmentId as Assessment from AssessmentAnswers where MemberId in (@0) and Course Like @1 and createdAt>=@2 and createdAt<=@3  group by assessmentId order by assessmentId";
        //var Assessments = db.Fetch<UserResults>(query, userList.ToArray(), "%" + assReport.CourseType + "%", assReport.startRange, assReport.endRange);
       // int[] AssessIds = Assessments.AsEnumerable().Select(r => (int)r.Assessment).ToArray();
        //foreach (var a in Assessments)
        //{
        //    if (Umbraco.TypedContent(a.Id) == null)
        //    {
        //        continue;
        //    }
        //    else
        //    {
        //        a.Name = Umbraco.TypedContent(a.Id).Name;
        //    }


        //}
        if (AssessIds.Count() != 0)
        {
            //gets User part of report
            var query2 = "select memberId, AssessmentId as Assessment, Grade, Score, Course from AssessmentAnswers where AssessmentId in (@0) and memberId!=0 and memberId in (@1) and createdAt>=@2 and createdAt<=@3 order by MemberId,assessmentId,createdAt";
            var uAssessments = db.Fetch<UserResults>(query2, AssessIds, userList.ToArray(), assReport.startRange, assReport.endRange);
            foreach (var uid in userList)
            {
                if (!uAssessments.Exists(x => x.memberId == uid))
                {
                    var emptyUser = new UserResults();
                    emptyUser.memberId = uid;
                    emptyUser.Assessment = 11121212;
                    emptyUser.Grade = "fail";
                    emptyUser.Score = 0;
                    uAssessments.Add(emptyUser);
                }

            }
            var userInfo = MhubContentHelper.createAssessmentReport(assessList, uAssessments);
          
            return Request.CreateResponse(HttpStatusCode.OK, userInfo, new JsonMediaTypeFormatter());

        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.OK, AssessIds, new JsonMediaTypeFormatter());
        }
    }


    [HttpGet]
    public HttpResponseMessage GetAssessment()
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select * from AssessmentAnswers where AssessmentId>2121 and memberid=2913 and grade!='fail' order by memberid";
        var uAssess = db.Fetch<AssessmentAnswerSet>(query, 2);
        var assessList = new List<int>();
        var userid = 0;
        string completedList = "";
        var mm = ApplicationContext.Current.Services.MemberService.GetById(2913);
        string compCon1 = mm.GetValue<string>("contentCompleted");
        string compCon = "";
        string testString = "Hello";
        int users = 0;
        var Combined = new List<eLearningModel>();
        var eList = new eLearningModel();
        foreach (var ua in uAssess)

        {
            var module = Umbraco.TypedContent(ua.AssessmentId).Parent;
                   assessList.Add(module.Id);
                   foreach (var kids in module.Children)

                   {
                       assessList.Add(kids.Id);
                  }

            //if (userid != ua.MemberId)
            //{
            //    if (userid != 0)
            //    {
            //        var member = ApplicationContext.Current.Services.MemberService.GetById(userid);
            //        compCon = member.GetValue<string>("contentCompleted");
            //        completedList = compCon;
            //        foreach (var aL in assessList )
            //        {
            //            completedList = completedList + "," + aL;
            //        }
            //        eList = new eLearningModel();
            //        eList.key = userid.ToString();
            //        eList.value = compCon;
            //        Combined.Add(eList);
            //        users = users + 1;
            //       // member.SetValue("contentCompleted", completedList);
            //       // ApplicationContext.Current.Services.MemberService.Save(member);
            //    }

            //    assessList = new List<int>();
            //    if (Umbraco.TypedContent(ua.AssessmentId) == null)
            //    {
            //        continue;
            //    }
            //    else
            //    {
            //        var module = Umbraco.TypedContent(ua.AssessmentId).Parent;
            //        assessList.Add(module.Id);
            //        foreach (var kids in module.Children)

            //        {
            //            assessList.Add(kids.Id);
            //        }
            //    }
            //   // assessList.Add(ua.AssessmentId);
            //}
            //else
            //{
            //    if (Umbraco.TypedContent(ua.AssessmentId) == null)
            //    {
            //        continue;
            //    }
            //    else
            //    {
            //        var module = Umbraco.TypedContent(ua.AssessmentId).Parent;
            //        assessList.Add(module.Id);
            //        foreach (var kids in module.Children)

            //        {
            //            assessList.Add(kids.Id);
            //        }
            //    }

            //}
            //userid = ua.MemberId;
        }
        var member = ApplicationContext.Current.Services.MemberService.GetById(2913);
        var completeContent = "";
       int i= 0;
        foreach(var s in assessList)
        {
            if (i == 0)
            { completeContent = s.ToString(); }
            else
            {
                completeContent = completeContent + "," + s.ToString();
            }
            i++;
        }
         member.SetValue("contentCompleted", completeContent);
        ApplicationContext.Current.Services.MemberService.Save(member);

        return Request.CreateResponse(HttpStatusCode.OK, completeContent, new JsonMediaTypeFormatter());
    }
}