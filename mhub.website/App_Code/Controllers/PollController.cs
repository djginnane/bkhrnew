﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using umbraco;
using Umbraco.Core.Logging;
using Umbraco.Web.WebApi;
using Umbraco.Core.Models;
using System.Net.Http.Formatting;

/// <summary>
/// Summary description for PollController
/// </summary>
public class PollController : UmbracoApiController
{
    public PollController()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public HttpResponseMessage GetPoll()
    {
        try
        {
            var pollsNode = uQuery.GetNodesByType("Poll").First();

            //var pollQuestions = NodeExtensions.GetDescendantNodesByType(pollsNode, "pollQuestion");
            List<PollQuestion> pollQuestionResults = new List<PollQuestion>();
            if (pollsNode != null)
            {
                var pollQuestionList = Services.ContentService.GetChildren(pollsNode.Id).Where(x => x.Status == ContentStatus.Published);

                if (pollQuestionList != null)
                {
                    foreach (IContent q in pollQuestionList)
                    {
                        pollQuestionResults.Add(PollQuestion.From(q, q.Descendants()));
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, pollQuestionResults, new JsonMediaTypeFormatter());
                }
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<MemberController>("MemberController.GetAll():", e);
            return Request.CreateErrorResponse(HttpStatusCode.NoContent, e);
        }

        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "No content");

    }

    public HttpResponseMessage UpdatePollVotes(int ansId)
    {
        try
        {
            var ansNode = Services.ContentService.GetById(ansId);
            int votesCnt = ansNode.GetValue<int>("votes") + 1;
            ansNode.SetValue("votes", votesCnt);
            Services.ContentService.SaveAndPublishWithStatus(ansNode);

            IEnumerable<PollResults> res = GetPollResults(ansId);
            if (res.Count() > 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res, new JsonMediaTypeFormatter());
            }
        }
        catch (Exception e)
        {
            LogHelper.Error<MemberController>("MemberController.GetAll():", e);
            return Request.CreateErrorResponse(HttpStatusCode.NoContent, e);
        }

        return Request.CreateErrorResponse(HttpStatusCode.NoContent, "No content");
    }


    public IEnumerable<PollResults> GetPollResults(int ansId)
    {

        try
        {
            var pollQuestion = Services.ContentService.GetParent(Services.ContentService.GetById(ansId));
            //IEnumerable<PollResults> res = pollQuestion.Children().Select(x => PollResults.From(pollQuestion, x));

            double totalVotes = pollQuestion.Children().AsEnumerable().Sum(x => x.GetValue<double>("votes"));

            IEnumerable<PollResults> res = from a in pollQuestion.Children().AsEnumerable()
                                           select new PollResults
                                           {
                                               PollId = a.ParentId,
                                               PollAnswerId = a.Id,
                                               PollAnswerText = a.GetValue<string>("answer"),
                                               Votes = a.GetValue<int>("votes"),
                                               percent = Math.Round((a.GetValue<double>("votes") / totalVotes) * 100, 2)
                                           };
            return res;
        }
        catch (Exception e)
        {
            LogHelper.Error<MemberController>("MemberController.GetPollResults():", e);
        }

        return null;

    }


}
