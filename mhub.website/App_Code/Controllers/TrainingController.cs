﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Web.WebApi;
using UmbracoWithMvc.Entities;

/// <summary>
/// Summary description for TrainingController
/// </summary>
public class TrainingController : UmbracoApiController
{

    public HttpResponseMessage Get()
    {//ToDo: get all training based on hubroot
        DateTime curDate = DateTime.UtcNow;
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var trainList = new List<TrainingDays>();
        // add  group to query, ie. 'monroe group'
        trainList = db.Fetch<TrainingDays>("Select * FROM mhubTrainingDays where DATEPART(year,[start])>=DATEPART(year,getDate())-1", curDate);
        foreach (var tDay in trainList)

        {
            tDay.tRequests = TrainingHelper.TrainingJoin(tDay.Id);
            tDay.emailList = TrainingHelper.EmailString(tDay.tRequests);
        }
        return Request.CreateResponse(HttpStatusCode.OK, trainList, new JsonMediaTypeFormatter());
    }

    [HttpGet]
    public HttpResponseMessage GetTraining()
    {
        DateTime curDate = DateTime.UtcNow;
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var trainList = new List<trainingRequest>();
        // add  group to query, ie. 'monroe group'
        trainList = db.Fetch<trainingRequest>("Select mhubtrainingrequest.Id, mhubtrainingrequest.[start], mhubtrainingrequest.[end],trainer_id, mhubtrainingrequest.title,mhubtrainingrequest.member_id,mhubtrainingrequest.member_group, trainingDay_id, registerDate,mhubtrainingrequest.rootHub,trainingApproved, mhubtrainingdays.comment, mhubtrainingdays.title as trainingName, Email FROM mhubtrainingRequest left join  mhubtrainingdays on mhubtrainingRequest.trainingDay_id=mhubtrainingdays.Id join cmsMember on cmsMember.NodeId=mhubtrainingRequest.member_id and mhubtrainingrequest.start>=@0 and mhubtrainingrequest.member_id=@1",
                                                curDate,
                                                MhubMemberHelper.LoggedInMember.Id);
        return Request.CreateResponse(HttpStatusCode.OK, trainList, new JsonMediaTypeFormatter());
    }

    [HttpPost]
    public HttpResponseMessage TrainingEmail(TrainingDays emailStuff)
    {
       var trainerEmail= TrainingHelper.GetTrainer().Email;
      //  string[] tEmails = new string[100];
        int i = 0;
        var mailList = new List<string>();
        string splitEmails = "";
        if (emailStuff.emailList != null)
        {

           //var tEmails = emailStuff.emailList.ToString().ToArray();
            foreach (var mail in emailStuff.emailList)
            {
                //    tEmails[i] = em.ToString();
                //    i++;
                if ((mail==',') || mail==';')
                {
                    mailList.Add(splitEmails);
                    splitEmails = "";
                }
                else { splitEmails = splitEmails + mail; }
               
                
            }
            if (splitEmails != "")
            {
                mailList.Add(splitEmails);
            }
        }
        AlertHelpers.emailAlert(emailStuff.comment, trainerEmail, emailStuff.title,  mailList.ToArray());
        return Request.CreateResponse(HttpStatusCode.OK, 1, new JsonMediaTypeFormatter());
    }

    [HttpGet]
    public HttpResponseMessage GetTrainingRequest()
    {
        var rootHub = MhubMemberHelper.LoggedInMember.GetValue<int>("hub");
        DateTime curDate = DateTime.UtcNow;
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var trainList = new List<trainingRequest>();
        // add  group to query, ie. 'monroe group'
        trainList = db.Fetch<trainingRequest>("Select mhubtrainingrequest.Id,mhubtrainingrequest.comments, mhubtrainingrequest.[start], mhubtrainingrequest.[end],trainer_id, mhubtrainingrequest.title,mhubtrainingrequest.member_id,mhubtrainingrequest.member_group, trainingDay_id, registerDate,mhubtrainingrequest.rootHub,trainingApproved, mhubtrainingdays.comment, mhubtrainingdays.title as trainingName, Email FROM mhubtrainingRequest left join  mhubtrainingdays on mhubtrainingRequest.trainingDay_id=mhubtrainingdays.Id join cmsMember on cmsMember.NodeId=mhubtrainingRequest.member_id and mhubtrainingrequest.start>=@0 and mhubtrainingrequest.rootHub=@1", curDate, rootHub);
        return Request.CreateResponse(HttpStatusCode.OK, trainList, new JsonMediaTypeFormatter());
    }

    [HttpPost]
    public HttpResponseMessage RemoveTraining(TrainingDays data)
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var a = db.SingleOrDefault<TrainingDays>("SELECT * FROM mhubTrainingDays WHERE Id=@0 ", data.Id);
        var trainingList = new List<trainingRequest>();

        if (a != null)
        {
             trainingList = db.Fetch<trainingRequest>("SELECT * FROM mhubTrainingRequest WHERE trainingDay_Id =@0 ",a.Id);
           foreach(var tl in trainingList)
            {
                db.Delete(tl);
            }

            db.Delete("mhubTrainingDays", "Id", a);
        }

        return Request.CreateResponse(HttpStatusCode.OK, a.Id);
    }


    [HttpPost]
    public HttpResponseMessage RemoveTrainingRequest(trainingRequest data)
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var a = db.SingleOrDefault<trainingRequest>("SELECT * FROM mhubTrainingRequest WHERE Id=@0 ", data.Id);
        
        if (a != null)
        {


            db.Delete("mhubTrainingRequest", "Id", a);
        }

        return Request.CreateResponse(HttpStatusCode.OK, a.Id);
    }

    [HttpPost]
    public HttpResponseMessage SaveTraining(TrainingModel data)
    {
        var root = MhubMemberHelper.LoggedInMember.GetValue<int>("hub");

        //get member id and group
        var mem = MhubMemberHelper.LoggedInMember;
        var mid = mem.Id;
        var mGroup = "Monroe";
        int testId = 0;
        var db = ApplicationContext.Current.DatabaseContext.Database;

        var trainingList = new List<TrainingDays>();
        var trainingDay = new TrainingDays();
        if (data != null)
        {
            foreach (var tDays in data.tDays)
            {
                trainingDay = new TrainingDays();

                testId = tDays.Id;

                if (tDays.Id != 0)
                {
                    trainingDay = db.SingleOrDefault<TrainingDays>("SELECT * FROM mhubTrainingDays WHERE id=@0", tDays.Id);
                    var requestList = db.Fetch<eLearningModel>("select distinct(email) as [key],title as [value] from mhubtrainingrequest, cmsMember where trainingDay_id=@0 and member_id=cmsmember.nodeId", tDays.Id);

                    if (trainingDay.trainingStatus != tDays.trainingStatus && tDays.trainingStatus == 1)
                    {
                        foreach (var e in requestList)
                        {
                            //var mem= ApplicationContext.Services.MemberService.GetById()
                           
                            var msgBody = "<p>Dear " + e.value + ", </p><p>The training day " + tDays.title + " has been confirmed for the date of " + tDays.start.ToString() + " and ending on " + tDays.end.ToString() + "</p> <p>Regards,<br> Baker Tilly Management Team</p>";
                            var sendEmail = e.key;
                            AlertHelpers.emailAlert(msgBody, sendEmail, "Training Day Has Been Confirmed");
                        }

                    }

                    if (tDays.start != trainingDay.start)
                    {
                        foreach (var e in requestList)
                        {
                          EmailHelper.SendAppointment2(tDays.title, tDays.Description, tDays.title, tDays.start, tDays.end, e.key, trainingDay.appointment_id, tDays.comment);

                        }
                    }
                    trainingDay.title = tDays.title;
                    trainingDay.Description = tDays.Description;
                    trainingDay.CourseType = tDays.CourseType;
                    trainingDay.trainingStatus = tDays.trainingStatus;
                    trainingDay.start = tDays.start;
                    trainingDay.end= tDays.end;
                    trainingDay.member_id = mid;
                    trainingDay.member_group = mGroup;

                    db.Save(trainingDay);
                    trainingList.Add(trainingDay);
                }
                else
                {


                    trainingDay.title = tDays.title;
                    trainingDay.trainingStatus = tDays.trainingStatus;
                    trainingDay.start = tDays.start;
                    trainingDay.end = tDays.end;
                    trainingDay.member_id = mid;
                    trainingDay.member_group = mGroup;
                    trainingDay.CreatedAt = DateTime.UtcNow;
                    trainingDay.Description = tDays.Description;
                    trainingDay.CourseType = tDays.CourseType;
                    trainingDay.Branch = tDays.Branch;
                    trainingDay.startTime = tDays.startTime;
                    trainingDay.endTime = tDays.endTime;
                    trainingDay.Duration = tDays.Duration;
                    trainingDay.LoginName = MhubMemberHelper.LoggedInMember.Name;
                    trainingDay.rootHub = root;
                    trainingDay.comment = tDays.comment;
                    trainingDay.allDay = tDays.allDay;
                    trainingDay.appointment_id = Guid.NewGuid();

                    EmailHelper.SendAppointment2("Created New Training", trainingDay.Description, trainingDay.title, trainingDay.start, trainingDay.end, mem.Email, trainingDay.appointment_id, trainingDay.comment);


                    db.Insert("mhubTrainingDays", "Id", trainingDay);
                    trainingList.Add(trainingDay);
                    //TODO: Send alert to all users in root or just branch depending on training type

                }
            }
        }

        return Request.CreateResponse(HttpStatusCode.OK, trainingList, new JsonMediaTypeFormatter());
    }

    [HttpPost]
    public HttpResponseMessage AddTrainingRequest(trainingRequest data)
    {
        //TODO:Add email alert
        int Request_Id = 0;
        var db = ApplicationContext.Current.DatabaseContext.Database;
        data.registerDate = DateTime.UtcNow;
        db.Insert("mhubtrainingRequest", "Id", data);
        Request_Id = data.Id;
        var member = MhubMemberHelper.LoggedInMember;
        var rootHub = 0;
        string[] cc = new string[1];
        if (member!=null)
        {
            rootHub = member.GetValue<int>("hub");
            var rootPage = MhubContentHelper.HubRootFromId(rootHub);
            var TrainerId = rootPage.GetProperty("trainingManager").Value.ToString();
           
                var Trainer = ApplicationContext.Services.MemberService.GetById(Int32.Parse(TrainerId));
            if (Trainer != null)
            {
                var msgBody = "";
                if (data.Name=="New")
                {
                    var lineM = ApplicationContext.Services.MemberService.GetById(member.GetValue<int>("lineManager"));
                    msgBody = "The user " + member.Name + " has enrolled in the training "+ data.TrainingName +" for the date " + data.start.ToString();
                   var msgBody2 = "The user " + member.Name + " has enrolled in the training " + data.TrainingName + " for the date " + data.start.ToString() +". Please log in to approve their request.";
                    msgBody2 = AlertHelpers.emailBody(msgBody2, lineM);
                    //    AlertHelpers.emailAlert(msgBody2, lineM.Email, "Approve Training Request");

                    EmailHelper.SendAppointment2("Enrolled in training",data.TrainingName, data.title,data.start, data.end, member.Email,data.appointment_id, data.comments);

                }
                else { 
                msgBody = "A training request has been submitted by " + member.Name + " for the date " + data.start.ToString();
                }
                msgBody = AlertHelpers.emailBody(msgBody, Trainer);
                AlertHelpers.emailAlert(msgBody, Trainer.Email, "Training Request");
            }
        }
        return Request.CreateResponse(HttpStatusCode.OK, Request_Id, new JsonMediaTypeFormatter());
    }



    [HttpPost]
    public HttpResponseMessage ApproveTraining(eLearningModel data)
    {

        var Request_Id = data.value;
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select * from mhubtrainingrequest where id=@0";
        var trainingPending = db.Single<trainingRequest>(query, Request_Id);
        var member = ApplicationContext.Services.MemberService.GetById(trainingPending.member_id);
        var msgBody = "";
        var subject = "";
        if (data.key == "app")
        {
            trainingPending.trainingApproved = 1;
            db.Save(trainingPending);
             msgBody = "Your training request for date of  " + trainingPending.start.ToString() + " has been approved";
            subject = "Training Request Approved";
        }
        else if (data.key == "rej")
        {
            trainingPending.trainingApproved = 2;
            db.Save(trainingPending);
             msgBody = "Your training request for date of  " + trainingPending.start.ToString() + " has been Rejected";
            subject = "Training Request Rejected";
        }
        //TODO: Add email alert
       // var cHelper = new MhubContentHelper();
      
        string[] ccList = new string[1];
        ccList[0] = TrainingHelper.GetTrainer().Email;
      
        msgBody = AlertHelpers.emailBody(msgBody, member);
        AlertHelpers.emailAlert(msgBody, member.Email, subject,ccList);
        //int Request_Id = 0;
        //var db = ApplicationContext.Current.DatabaseContext.Database;
        //data.registerDate = DateTime.UtcNow;
        //db.Insert("mhubtrainingRequest", "Id", data);
        //Request_Id = data.Id;
        return Request.CreateResponse(HttpStatusCode.OK, Request_Id, new JsonMediaTypeFormatter());
    }
}