﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Umbraco.Web.WebApi;
using Umbraco.Web.Editors;
using Umbraco.Core.Persistence;
using UmbracoWithMvc.Controllers;

/// <summary>
/// Summary description for AppraisalApiController
/// </summary>
/// 
namespace My.Controllers
{
    [Umbraco.Web.Mvc.PluginController("My")]
    public class AppraisalApiController : UmbracoAuthorizedJsonController
    {

        public IEnumerable<AppraisalForms> GetAll()
        {
            //get the database
            var db = UmbracoContext.Application.DatabaseContext.Database;
            //build a query to select everything the people table
            var query = new Sql().Select("*").From("mhubAppraisalForms");
            //fetch data from DB with the query and map to Person object
            return db.Fetch<AppraisalForms>(query);

        }
        public AppraisalApiController()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}