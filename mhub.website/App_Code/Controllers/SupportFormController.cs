﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

/// <summary>
/// Summary description for SupportFormController
/// </summary>
public class SupportFormController : Umbraco.Web.Mvc.SurfaceController
{
    [HttpPost]
    public ActionResult Submit(SupportFormViewModel model)
    {
        if (!ModelState.IsValid)
            return CurrentUmbracoPage();

        /// Work with form data here
        ///

        var message = String.Format("<h2>Support request from {0}</h2><h4>Username: {1}</h4><h4>email: {2}<h4><div>{3}</div>",
                            Request.UrlReferrer.Host,
                            MhubMemberHelper.LoggedInMember.Username,
                            MhubMemberHelper.LoggedInMember.Email,
                            model.Message);

        EmailHelper.SendEmail(String.Format("Support request on the Baker Tilly Connect Network - {0}", Request.UrlReferrer.Host),
                                            "dean@bakertillythailand.com",
                                            null,
                                            message
                                            );

        model.MessageSent = true;

        return RedirectToCurrentUmbracoPage();
    }
}

public class SupportFormViewModel
{
    public string Message { get; set; }

    public bool MessageSent { get; set; }
}