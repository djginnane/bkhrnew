﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Core;
using Umbraco.Web.Mvc;
using Umbraco.Web;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for TrainingAppraisalSurfaceController
/// </summary>
public class TrainingAppraisalSurfaceController : SurfaceController
{
    [HttpPost]
    public ActionResult TrainingAppraisal(UmbracoWithMvc.Entities.TrainingAppraisal model)
    {
        if (!ModelState.IsValid)
        {
            ViewBag.Message = "Form was was not submitted Successfully, Please review";
            return CurrentUmbracoPage();
        }
        var member = MhubMemberHelper.LoggedInMember;
        //  var root = member.GetValue<int>("hub");
        var root = Umbraco.TypedContentAtRoot().First();
        var trainerId = root.GetPropertyValue<int>("trainingmanager");
        if (trainerId!=0)
        {
            var emailBody = "<p>A Training Appraisal has been submitted by "+member.Name+"</p>";
            var trainerEmail = ApplicationContext.Services.MemberService.GetById(trainerId).Email;
           // EmailHelper.SendEmail("TrainingAppraisal",trainerEmail,emailBody);
        }
      
       // TempData["tempvalErr"] = "There was an error submitting the training appraisal";
        TempData["tempvalSucc"] = "Training Appraisal Submitted Successfully";

        return RedirectToCurrentUmbracoPage();
    }
    public TrainingAppraisalSurfaceController()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}