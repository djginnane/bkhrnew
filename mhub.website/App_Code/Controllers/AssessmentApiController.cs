﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using umbraco;
using Umbraco.Core;
using Umbraco.Web.WebApi;

/// <summary>
/// Summary description for AssessmentController
/// </summary>
public class AssessmentController : UmbracoApiController
{
    AssessmentRepository repository = new AssessmentRepository();

    [HttpPost]
    public HttpResponseMessage Submit(AssessmentAnswerSet answerSet)
    {
        //try
        //{
  
        answerSet.MemberId = MhubMemberHelper.LoggedInMember.Id;
        answerSet.Course = Umbraco.TypedContent(answerSet.AssessmentId).Parent.Parent.Name;
        repository.Save(answerSet);

        if (answerSet.Grade != "Fail")
        {
            MhubContentHelper.MarkCurrentTrainingComplete(answerSet.AssessmentId);
            MhubContentHelper.MarkCurrentTrainingCourseComplete(answerSet.AssessmentId);
        }

        return Request.CreateResponse(HttpStatusCode.OK, "success", new JsonMediaTypeFormatter());
        //}
        //finally
        //{
        //    // To run the program in Visual Studio, type CTRL+F5. Then 
        //    // click Cancel in the error dialog.
        //    var currentSession = HttpContext.Current.Session;
        //    currentSession["LoggedInMemberName"] = ApplicationContext.Services.MemberService.GetById(MhubMemberHelper.LoggedInMember.Id);
        //   // AlertHelpers.createAlert("After assessment", 323213,"this ran ", "test of finally");
        //}
        //catch (Exception ex)
        //{
        //    return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message, new JsonMediaTypeFormatter());
        //}
    }

    public HttpResponseMessage GetAssessmentList()
    {
        Dictionary<int, string> dic = new Dictionary<int, string>();
        var training = uQuery.GetNodesByType("assessment");
        foreach (var item in training)
        {
            dic.Add(item.Id, item.Name);
        }
        return Request.CreateResponse(HttpStatusCode.OK, dic, new JsonMediaTypeFormatter());
    }

    public HttpResponseMessage GetAllResults()
    {

        var assessments = repository.GetAll();
        return Request.CreateResponse(HttpStatusCode.OK, assessments, new JsonMediaTypeFormatter());
    }

    public HttpResponseMessage GetResultsByAssessment(int id)
    {
        var assessments = repository.GetByAssessment(id);
        return Request.CreateResponse(HttpStatusCode.OK, assessments, new JsonMediaTypeFormatter());
    }

    [HttpGet]
    public HttpResponseMessage MarkTrainingComplete(string id)
    {
        //try
        //{
            MhubContentHelper.MarkCurrentTrainingComplete(int.Parse(id));
            MhubContentHelper.MarkCurrentTrainingCourseComplete(int.Parse(id));

            return Request.CreateResponse(HttpStatusCode.OK, true, new JsonMediaTypeFormatter());
        //}
        //finally
        //{
          
        //    var currentSession = HttpContext.Current.Session;
        //    currentSession["LoggedInMemberName"] = ApplicationContext.Services.MemberService.GetById(MhubMemberHelper.LoggedInMember.Id);
            
        //}
    }

    [HttpGet]
    public HttpResponseMessage MarkKnowledgeComplete(string id)
    {
        var CompletedList=MhubContentHelper.GetCompletedKnowledge();
        if (!CompletedList.Exists(x => x == id.ToString()))
        {
            MhubContentHelper.MarkCurrentKnowledgeComplete(int.Parse(id));
        }
      //  MhubContentHelper.MarkCurrentTrainingCourseComplete(int.Parse(id));

        return Request.CreateResponse(HttpStatusCode.OK, true, new JsonMediaTypeFormatter());
    }

    [HttpGet]
    public HttpResponseMessage RecordPoll(string pollNUmber,string pollName)
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var member = MhubMemberHelper.LoggedInMember;
        //  MhubContentHelper.MarkCurrentTrainingCourseComplete(int.Parse(id));
        var pAns = new PollsAnswers();

        pAns.PollAnswer = pollNUmber;
        pAns.Poll_Id = 1;
        pAns.User_Id = member.Id;
        pAns.User_Name = member.Name;
        pAns.Poll_Name = pollName;
        db.Save(pAns);



        return Request.CreateResponse(HttpStatusCode.OK, true, new JsonMediaTypeFormatter());
    }

    [HttpGet]
    public HttpResponseMessage LatestPoll()
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var pName = DataHelpers.GetPollName;
        var rp = new PollRepository();
        var pollResults = rp.GetLatestPollResults(pName);
        var AllInfo = new Poll_Info();
        //  MhubContentHelper.MarkCurrentTrainingCourseComplete(int.Parse(id));
        var pAns = new PollsAnswers();
        AllInfo.Poll_Results = pollResults;
        AllInfo.Poll_Name = pName;

      



        return Request.CreateResponse(HttpStatusCode.OK, AllInfo, new JsonMediaTypeFormatter());
    }
    [HttpGet]
    public HttpResponseMessage timeChoice(string timeSel)
    {
        var member = MhubMemberHelper.LoggedInMember;
        var memberService = ApplicationContext.Current.Services.MemberService;
       
        member.SetValue("startingTime", timeSel);
        memberService.Save(member);






        return Request.CreateResponse(HttpStatusCode.OK, 1, new JsonMediaTypeFormatter());
    }
}