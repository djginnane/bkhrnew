﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Security;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Web.WebApi;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web.PublishedContentModels;
using System.Web;

/// <summary>
/// Summary description for LeaveManagerController
/// </summary>
public class LeaveManagerController : UmbracoApiController
{
    /// <summary>
    ///get current leave requests
    ///adjust hardcoded username once in main repo
    /// </summary>
    public HttpResponseMessage Get()
    {
        
        var DaysUsed = 0.00m;
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var leaveTable = new List<LeaveRequest>();
        //  var username = Membership.GetUser().UserName.ToString();
        var memberLogged = MhubMemberHelper.LoggedInMember;//ApplicationContext.Current.Services.MemberService.GetByUsername(username);
        var hubRoot = memberLogged.GetValue<int>("hub");
        var mList = MhubMemberHelper.APIGetAll2(memberLogged);


      DateTime nowTime = DateTime.Today;
        nowTime = nowTime.AddMonths(-1);
        var curYear = nowTime.Year.ToString();
        var now=curYear+"-01-01 00:00:00.000";
        DateTime retrieveDate= DateTime.Today;
         retrieveDate = retrieveDate.AddMonths(-6);


        // retrieves a list of all leave days taken in a year-- can add to cache in future
        var LeaveListQuery = "select MemberId, LeaveDay, LeaveStart, LeaveEnd, HalfDay, LeaveCounted, ApprovedLeave from mhubLeaveRequest where  memberId in(@0) and hubRoot=@1 and LeaveStart>=@2  order by MemberId, LeaveDay";
        var leaveList = new List<LeaveRequest>();
        if (mList == null)
        {
            var query = "select ApprovedLeave, CreatedAt,MemberId,LoginName, LeaveType,Comment, count(LeaveDay) as TotalDays, LeaveStart, LeaveEnd  from mhubLeaveRequest, cmsMember where memberId=NodeId  and memberId in(@0) group by MemberId, createdAt, LeaveStart, LeaveEnd, LeaveType, LoginName, ApprovedLeave, Comment";
            leaveTable = db.Fetch<LeaveRequest>(query, MhubMemberHelper.LoggedInMember.Id);
        }
        else
        {
            var mIdList = MhubMemberHelper.GetarraylistI(mList);
         //   var mIdList = MhubMemberHelper.GetAllMemberList();
            var query = "select ApprovedLeave, CreatedAt,MemberId,LoginName, LeaveType,Comment, count(LeaveDay) as daysMinusHalf,sum(case  when halfDay='true' then 1 end) as totalHalfDays, LeaveStart, LeaveEnd  from mhubLeaveRequest, cmsMember where memberId=NodeId  and memberId in(@0) and LeaveStart>=@1 group by MemberId, createdAt, LeaveStart, LeaveEnd, LeaveType, LoginName, ApprovedLeave, Comment";
            leaveTable = db.Fetch<LeaveRequest>(query, mIdList.ToArray(), retrieveDate);
            leaveList = db.Fetch<LeaveRequest>(LeaveListQuery, mIdList.ToArray(), hubRoot, now);
        }

      //  var memList = MhubMemberHelper.HubMembers.ToList();
        var memList = MhubMemberHelper.GetAll().ToList();
        
        var result = new List<LeaveRequest>();
        var PublicHol = LeaveHelpers.GrabPublicHolidays(memberLogged.Id);
        foreach (var entry in leaveTable)
        {/* The first part gets all the leave requests from the year to calulate the leave used */
            var member = memList.Find(x => x.Id == entry.MemberId);//ApplicationContext.Current.Services.MemberService.GetById(entry.MemberId);
            var remainingList = leaveList.FindAll(x => x.MemberId == entry.MemberId && x.LeaveCounted == 1 && x.ApprovedLeave == 1 && x.LeaveDay.Year == DateTime.UtcNow.Year);
            var usedTotel = LeaveHelpers.filterAllRequests(remainingList, PublicHol);
            var LeaveAvail = MhubMemberHelper.AvailableLeaveDec(member);

            /* The Second part gets the current leave request from the leavelist and calculates days used*/
            var dUsed = leaveList.FindAll(x => x.MemberId == entry.MemberId && x.LeaveStart == entry.LeaveStart && x.LeaveEnd == entry.LeaveEnd);
            var dUsedTot = LeaveHelpers.filterAllRequests(dUsed, PublicHol);
            var leaveCounted = LeaveHelpers.filterHolidays(entry.LeaveStart, entry.LeaveEnd, PublicHol);
            var CarryOverToAdd = LeaveHelpers.CarryOverLeaveWithList(member, remainingList, PublicHol);//LeaveHelpers.CarryOverLeaveToAdd(member);
            LeaveAvail=LeaveAvail+CarryOverToAdd;
            result.Add(new LeaveRequest()
            {
                MemberId = entry.MemberId,
                LeaveStart = entry.LeaveStart,
                LeaveEnd = entry.LeaveEnd,
                LeaveType = entry.LeaveType,
                daysMinusHalf = dUsedTot,//leaveCounted - entry.TotalHalfDays * 0.5m,// entry.daysMinusHalf - entry.TotalHalfDays * 0.5m,
                LoginName = entry.LoginName,
                Name = member.Name,
                ApprovedLeave = entry.ApprovedLeave,
                CreatedAt = entry.CreatedAt,
                TotalDays = LeaveAvail - usedTotel,//need to minus from available leave
                HolidaysCurrYear = LeaveAvail,//+CarryOverToAdd ,
                Comment = entry.Comment
            });

        }

        return Request.CreateResponse(HttpStatusCode.OK, result, new JsonMediaTypeFormatter());

    }
    /*
    Gets all the leave days for all employees under the manager,
    then sends to the calender to display all leave days on the calendar
    */
    public HttpResponseMessage GetCalendarDetails()
    {
        DateTime nowTime = DateTime.Today;
        nowTime = nowTime.AddMonths(-1);
        var curYear = nowTime.Year.ToString();
        var now = curYear + "-01-01 00:00:00.000";
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var mem = MhubMemberHelper.LoggedInMember;

        var mIdList = MhubMemberHelper.GetAllMemberList();
        //var query = "Select leaveday, leavestart, ApprovedLeave, leaveend, MemberId,cmsmember.loginname  from mhubLeaveRequest, cmsmember where mhubleaverequest.memberid=cmsmember.nodeid and  (ApprovedLeave=1 or ApprovedLeave=0) and mhubleaverequest.memberid in (@0) and LeaveStart>='2017-01-20 00:00:00.000'";

        //if (mIdList.Count() == 0)
        //{
        //    query = "Select leaveday, leavestart, ApprovedLeave, leaveend, MemberId,cmsmember.loginname  from mhubLeaveRequest, cmsmember where mhubleaverequest.memberid=cmsmember.nodeid and  (ApprovedLeave=1 or ApprovedLeave=0) and mhubleaverequest.memberid =9999";

        //}
        // System.Diagnostics.Debug.WriteLine("Does a loop");
        var leaveRepo = new LeaveRepository();
        var leaveTable = leaveRepo.CalenderEvents(mIdList);// db.Fetch<LeaveRequest>(query, mIdList);
        var result = new List<calEvent>();
        var rootId = mem.GetValue<int>("hub");
        var requestCache = ApplicationContext.Current.ApplicationCache.RuntimeCache;
        var cachedMemList = (IEnumerable<IMember>)requestCache.GetCacheItem("hubMembers" + rootId.ToString());
        var memList = cachedMemList.ToList();
        //    string [,] cEvent= new  string[9,5];
        foreach (var entry in leaveTable)
        {
            var member = memList.Find(x => x.Id == entry.MemberId);//ApplicationContext.Current.Services.MemberService.GetById(entry.MemberId);
            var mName = member.Name;
            System.Diagnostics.Debug.WriteLine("Message in here");
            //      cEvent[1,1] = "58";
            result.Add(new calEvent()
            {
                id = entry.MemberId,
                title = mName,
                ApprovedLeave = entry.ApprovedLeave,
                start = entry.LeaveDay,
                end = entry.LeaveDay,
                allDay = true





            });

        }

        return Request.CreateResponse(HttpStatusCode.OK, result, new JsonMediaTypeFormatter());

    }

    [HttpPost]
    public HttpResponseMessage ApproveLeave(calEvent data)
    //public HttpResponseMessage ApproveLeave()

    {


        var db = ApplicationContext.Current.DatabaseContext.Database;

        DateTime CreatedAt = data.CreatedAt;
        int Approved = data.ApprovedLeave;
        int membId = data.id;

        //this code grabs leaveRequest days then approves/rejects each day. Could be done simpler by replacing code with a simple update query
        var query = " select * from mhubLeaveRequest where CreatedAt=@0 and MemberId=@1";
        var leaveTable = db.Fetch<LeaveRequest>(query, CreatedAt, membId);
        var manager = MhubMemberHelper.LoggedInMember;
        int i = 0;
        var result = new LeaveRequest();
        var leaveStart = new DateTime();
        var leaveEnd = new DateTime();

        foreach (var entry in leaveTable)
        {
            result = entry;
            result.ApprovedLeave = Approved;
            result.CreatedAt = entry.CreatedAt;
            result.ApprovedBy = manager.Id;
            leaveEnd = entry.LeaveEnd;
            leaveStart = entry.LeaveStart;
            db.Save(result);
            i++;
        }
        var member = ApplicationContext.Current.Services.MemberService.GetById(data.id);
        var eMail = member.Email;
        var eSubject = "Your leave request has been approved";
        var eMessage = "Hello " + member.Name + ", Your leave request for the dates " + leaveStart.ToString("MMMM dd, yyyy") + " Until " + leaveEnd.ToString("MMMM dd, yyyy") + " have been approved by your manager";

        if (Approved==2)
        {
            eMessage =  "Hello " + member.Name + ", Your leave request for the dates " + leaveStart.ToString("MMMM dd, yyyy") + " Until " + leaveEnd.ToString("MMMM dd, yyyy") + " have been rejected by your manager, please consult with them if you are unsure of the reason";

        }
        AlertHelpers.emailAlert(eMessage, eMail, eSubject);

        return Request.CreateResponse(HttpStatusCode.OK, i);

    }

    // Cancel Leave Function
    [HttpPost]
    public HttpResponseMessage CancelLeave(LeaveRequest data)
    //public HttpResponseMessage ApproveLeave()

    {
        var member = MhubMemberHelper.LoggedInMember;
        var i = 1;
        if (member.Id == data.MemberId)
        {

            var query = "select * from mhubLeaveRequest where MemberId=@0  and LeaveEnd=@1 and LeaveStart=@2";
            var db = ApplicationContext.Current.DatabaseContext.Database;
            var leaveTable = db.Fetch<LeaveRequest>(query, data.MemberId, data.LeaveEnd, data.LeaveStart);
            // i = data.LeaveStart;
            foreach (var lr in leaveTable)
            {
                if (lr.ApprovedLeave == 0)
                {
                    db.Delete(lr);
                }
                else
                {
                    lr.ApprovedLeave = 2;
                    db.Save(lr);
                }
            }
            i = leaveTable.Count();
        }
        return Request.CreateResponse(HttpStatusCode.OK, i);

    }


    public LeaveManagerController()
    {


        //
        // TODO: Add constructor logic here
        //
    }
}