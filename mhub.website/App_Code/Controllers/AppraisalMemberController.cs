﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.WebApi;
using System.Net.Http;
using System.Net;
using System.Net.Http.Formatting;
using Umbraco.Core;
using UmbracoWithMvc.Controllers;
using UmbracoWithMvc.Entities;
using System.Web.Http;
using System.Web.Security;

/// <summary>
/// Summary description for AppraisalMemberController
/// </summary>
public class AppraisalMemberController : UmbracoApiController
{
    public HttpResponseMessage Get()
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select * from mhubMemberAppraisal where member_Id=@0 order by CreatedDate Desc";
        var appraisalTable = db.Fetch<MemberAppraisal>(query, MhubMemberHelper.LoggedInMember.Id);
        var TResult = new List<MemberAppraisal>();
        var AppQuest = new List<AppraisalAnswer>();
        foreach (var entry in appraisalTable)
        {
            AppQuest = db.Fetch<AppraisalAnswer>("where QTemplate=@0 ORDER BY QSection", entry.Id);
            TResult.Add(new MemberAppraisal()
            {
                Id = entry.Id,
                TemplateId = entry.TemplateId,
                TemplateName = entry.TemplateName,
                AppQuest = AppQuest,
                CreatedDate = entry.CreatedDate,
                LastModified = entry.LastModified,
                AppraisalSchedule = entry.AppraisalSchedule,
                ScheduleAlias = entry.ScheduleAlias,
                Member_Id = entry.Member_Id,
                AppStatus = entry.AppStatus

            });
        }
        return Request.CreateResponse(HttpStatusCode.OK, TResult, new JsonMediaTypeFormatter());
    }


    [HttpPost]
    public HttpResponseMessage AppraisalmUser(List<dynamic> data)
    {

        int TUser = 0;
        foreach (var us in data)
        {
            TUser = us.userId;
        }
        var m = ApplicationContext.Current.Services.MemberService.GetById(TUser);

        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select * from mhubMemberAppraisal where member_Id=@0 ";

        var appraisalTable = db.Fetch<MemberAppraisal>(query, TUser);
        var TResult = new List<MemberAppraisal>();
        var AppQuest = new List<AppraisalAnswer>();
        var AppQuest2 = new List<AppraisalAnswer>();
        var cal1 = "";
        Boolean calbool1 = false;
        var cal2 = "";
        Boolean calbool2 = false;
        foreach (var entry in appraisalTable)
        {
            AppQuest2 = new List<AppraisalAnswer>();
            AppQuest = db.Fetch<AppraisalAnswer>("where QTemplate=@0 ORDER BY QSection", entry.Id);

            // read all questions into a new list and add a new question if a calculation is neccesary
            foreach (var Quest in AppQuest)
            {
                if (Quest.QCalc == 1)
                {
                    cal1 = Quest.QAnswer;
                    calbool1 = true;
                }

                else if (Quest.QCalc == 2)
                {
                    cal2 = Quest.QAnswer;
                    calbool2 = true;
                }


                AppQuest2.Add(Quest);
                if (calbool2 && calbool1)
                {
                    calbool2 = false;
                    calbool1 = false;
                    int m1;
                    Int32.TryParse(cal1, out m1);
                    int m2;
                    Int32.TryParse(cal2, out m2);
                    int result = m1 - m2;

                    AppQuest2.Add(new AppraisalAnswer()
                    {

                        QAnswer = result.ToString(),
                        QWording = "Result Target",
                        QSection = 1,
                        QTemplate = Quest.QTemplate,
                        QType = "number"


                    }
                    );

                }
            }



            TResult.Add(new MemberAppraisal()
            {
                Id = entry.Id,
                TemplateId = entry.TemplateId,
                TemplateName = entry.TemplateName,
                AppQuest = AppQuest2,
                CreatedDate = entry.CreatedDate,
                LastModified = entry.LastModified,
                AppraisalSchedule = entry.AppraisalSchedule,
                ScheduleAlias = entry.ScheduleAlias,
                Member_Id = entry.Member_Id,
                AppStatus = entry.AppStatus

            });
        }
        var userList = new UserwAppraisal();
        userList.Member_Id = TUser;
        userList.FirstName = m.Name;
        userList.UserName = m.Username;
        userList.mAppraisals = TResult;


        return Request.CreateResponse(HttpStatusCode.OK, userList, new JsonMediaTypeFormatter());
    }



    [HttpPost]
    public HttpResponseMessage SaveAppraisal(List<AppraisalAnswer> data)
    //public HttpResponseMessage ApproveLeave()

    {
       
        int qId = 1;
        int app_id = 0;
        var db = ApplicationContext.Current.DatabaseContext.Database;
        foreach (AppraisalAnswer question in data)
        {
            qId = question.Id;
            app_id = question.QTemplate;
            if (question.QWording != "ResultTarget")
            {
                db.Save(question);
            }
        }
        var mApprais = new MemberAppraisal();
        mApprais = db.SingleOrDefault<MemberAppraisal>("where Id=@0", app_id);
        if (mApprais != null)
        {
            if (mApprais.AppStatus != 3 && mApprais.AppStatus != 2)
            {
                mApprais.AppStatus = 1;
            }
            db.Save(mApprais);
        }

        return Request.CreateResponse(HttpStatusCode.OK, qId);
    }


    [HttpPost]
    public HttpResponseMessage SubmitAppraisal(List<AppraisalAnswer> data)
    {
        int qId = 0;
        var db = ApplicationContext.Current.DatabaseContext.Database;
        foreach (var question in data)
        {
            if (question.QWording != "ResultTarget")
            {
                qId = question.QTemplate;
                db.Save(question);
            }
        }
        var LoggedinName = MhubMemberHelper.LoggedInMember;
        var mApprais = new MemberAppraisal();
        mApprais = db.SingleOrDefault<MemberAppraisal>("where Id=@0", qId);
        //get line manager
        var member = ApplicationContext.Current.Services.MemberService.GetById(mApprais.Member_Id);
        var mManager_Id = member.GetValue<int>("lineManager");
        var mManager = ApplicationContext.Current.Services.MemberService.GetById(mManager_Id);

        var email = "";
        var mesBody = "";
        var eMessage = "";
        var eSubject = "";
        if (mApprais.AppStatus == 0 || mApprais.AppStatus == 1)
        {
            mApprais.AppStatus = 2;
            mesBody = member.Name + " has completed the first stage of their online appraisal (link to the appraisal) and you are required to complete your online appraisal of them. You have three days to submit your part of the appraisal at which point a meeting will be arranged to finalise the appraisal of" + member.Name;
            eMessage = AlertHelpers.emailBody(mesBody, mManager);
            eSubject = mApprais.TemplateName + " Completed";
            email = mManager.Email;
        }
        else if ((mApprais.AppStatus == 2) && (LoggedinName.Id != member.Id))
        {
            mApprais.AppStatus = 3;
            mesBody = " Please ensure an appraisal meeting is booked between " + member.Username + " and " + mManager.Username + " to finalise this periods appraisal.";
            eMessage = AlertHelpers.emailBodyWarmest(mesBody, mManager);
            eSubject = "Please arrange an appraisal meeting";
            email = mManager.Email;
            AlertHelpers.emailAlert(eMessage, mManager.Email, eSubject, new string[1] { member.Email });
        }
        else if ((mApprais.AppStatus == 3) && (LoggedinName.Id != member.Id))
        {
            mApprais.AppStatus = 4;
        }
        db.Save(mApprais);


        if (mManager != null)
        {
            var mEmail = mManager.Email;
            // var mEmail = "wayne@redskydigital.com";
            eMessage = "An appraisal has been completed by " + member.Username + ", Please review.";
            eSubject = "New Appraisal Submission";
            //Function to send email
            AlertHelpers.emailAlert(eMessage, mEmail, eSubject);
        }


        return Request.CreateResponse(HttpStatusCode.OK, mApprais, new JsonMediaTypeFormatter());
    }

    public HttpResponseMessage Completed(List<AppraisalAnswer> data)
    //public HttpResponseMessage ApproveLeave()

    {
        int qId = 0;
        var db = ApplicationContext.Current.DatabaseContext.Database;
        foreach (var question in data)
        {
            qId = question.QTemplate;

        }

        var mApprais = new MemberAppraisal();
        mApprais = db.SingleOrDefault<MemberAppraisal>("where Id=@0", qId);


        mApprais.AppStatus = 4;

        db.Save(mApprais);

        return Request.CreateResponse(HttpStatusCode.OK, mApprais, new JsonMediaTypeFormatter());
    }

    //select all from members who belong to manager group fro showing appraisals
    [HttpGet]
    public HttpResponseMessage GetAllMembers()
    {
        /****** To get the line manager use query below, i think managers id is dataInt, Member id is just Id
         *
         * select * from cmsPropertyData where propertytypeid=80
         *
         * *****/

        // real member list for production

        var member = MhubMemberHelper.LoggedInMember;

        var lineManagers = MhubMemberHelper.APIGetLineMembers();
        var MemberList = MhubMemberHelper.GetarraylistI(lineManagers);

        // memberlist for testing
        /*

               var member = ApplicationContext.Services.MemberService.GetById(2808);
            var lineManagers = MhubMemberHelper.LineMembers(member);
            var MemberList = MhubMemberHelper.GetarraylistI(lineManagers);
          */



        /* Use Umbraco Service to grab all members that have a give property  */
        //  //  var allUsers= ApplicationContext.Current.Services.MemberService.GetMembersByGroup(member_group);
        var db = ApplicationContext.Current.DatabaseContext.Database;

        //Need take out hard coded group id once this is sorted. and add in line manager
        // var query = "select Member_Id, Email, AppStatus, mhubMemberAppraisal.Id, TemplateId, [text] as FName, CreatedDate, LastModified from cmsMember,cmsMember2memberGroup,mhubMemberAppraisal,umbracoNode where cmsMember2MemberGroup.Member=cmsMember.nodeId and cmsMember2MemberGroup.MemberGroup=@0 and cmsMember.nodeId=mhubMemberAppraisal.Member_Id and umbracoNode.Id=cmsMember.nodeId order by nodeId, AppStatus ";
        var query = "select Member_Id, Email, AppStatus, mhubMemberAppraisal.Id, TemplateId,  CreatedDate,[text] as fName,ScheduleAlias,  LastModified from mhubMemberAppraisal,cmsMember,umbraconode where Member_Id=cmsmember.nodeId and umbraconode.Id=cmsMember.nodeId and Member_Id in (@0) order by fName, CreatedDate desc";
        var mApprais = db.Fetch<MemberAppraisal>(query, MemberList);
        var UserAppraisal = new List<MemberAppModel>();
        var AppraisalList = new List<MemberAppraisal>();
        var UserApp = new MemberAppModel();
        var UserId = 0;
        foreach (var m in mApprais)
        {
            if (m.Member_Id != UserId)
            {
                if (UserId != 0)
                {
                    UserApp.MemberApps = AppraisalList;
                    UserAppraisal.Add(UserApp);
                }

                UserId = m.Member_Id;
                AppraisalList = new List<MemberAppraisal>();
                UserApp = new MemberAppModel();
                UserApp.member_id = m.Member_Id;
                UserApp.membername = m.FName;
                UserApp.username = m.UserName;
                UserApp.App_Status = m.AppStatus;

                AppraisalList.Add(new MemberAppraisal()
                {
                    Id = m.Id,
                    TemplateId = m.TemplateId,
                    TemplateName = m.TemplateName,
                    CreatedDate = m.CreatedDate,
                    LastModified = m.LastModified,
                    AppraisalSchedule = m.AppraisalSchedule,
                    ScheduleAlias = m.ScheduleAlias,
                    Member_Id = m.Member_Id,
                    AppStatus = m.AppStatus

                });

            }
            else
            {
                if (m.AppStatus == 2 || m.AppStatus == 3 || m.AppStatus == 4)
                    if (UserApp.App_Status == 0 || UserApp.App_Status == 1)
                    {
                        UserApp.App_Status = m.AppStatus;
                        AppraisalList.Insert(0, new MemberAppraisal()
                        {
                            Id = m.Id,
                            TemplateId = m.TemplateId,
                            TemplateName = m.TemplateName,

                            CreatedDate = m.CreatedDate,
                            LastModified = m.LastModified,
                            AppraisalSchedule = m.AppraisalSchedule,
                            Member_Id = m.Member_Id,
                            AppStatus = m.AppStatus

                        });
                    }
                    else
                    {
                        AppraisalList.Add(new MemberAppraisal()
                        {
                            Id = m.Id,
                            TemplateId = m.TemplateId,
                            TemplateName = m.TemplateName,

                            CreatedDate = m.CreatedDate,
                            LastModified = m.LastModified,
                            AppraisalSchedule = m.AppraisalSchedule,
                            Member_Id = m.Member_Id,
                            AppStatus = m.AppStatus

                        });
                    }
            }

        }





        UserApp.MemberApps = AppraisalList;
        UserAppraisal.Add(UserApp);

        // AppraisalList = new List<MemberAppraisal>();


        return Request.CreateResponse(HttpStatusCode.OK, UserAppraisal, new JsonMediaTypeFormatter());
    }


    [HttpGet]
    public HttpResponseMessage GetAppraisalReport()
    {
        var currentDate = DateTime.Today;
        var CurrentYear = currentDate.Year;
        var startDate = new DateTime(CurrentYear, 1, 1);
        var endDate = new DateTime(CurrentYear + 1, 1, 1);

        var AppraisalReport = AppraisalHelpers.AppraisalReportRange(startDate, endDate);
        return Request.CreateResponse(HttpStatusCode.OK, AppraisalReport, new JsonMediaTypeFormatter());
    }
    [HttpPost]
    public HttpResponseMessage GetAppraisalReportRange(LeaveRangeDates data)
    {


        var AppraisalReport = AppraisalHelpers.AppraisalReportRange(data.StartDate, data.EndDate);
        return Request.CreateResponse(HttpStatusCode.OK, AppraisalReport, new JsonMediaTypeFormatter());
    }


    public AppraisalMemberController()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}