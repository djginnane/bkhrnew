﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using umbraco.cms.businesslogic.member;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco.Core;
using Umbraco.Web.PublishedContentModels;
using System.ComponentModel.DataAnnotations;

/// <summary>
/// Summary description for mHubLoginController handle login
/// </summary>

namespace Login.Controllers
{
    public class mHubAuthSurfaceController : SurfaceController
    {
        [HttpPost]
        public ActionResult HandleLogin([Bind(Prefix = "loginModel")]MhubLoginModel model)
        {
            if (ModelState.IsValid == false)
            {
                return CurrentUmbracoPage();
            }
           var y = 1;
          var  x = 25/89;
            model.Password = model.Password.TrimStart().TrimEnd();
          //  model.Password = model.Password.TrimStart();

            string username = model.Username;
            if (model.Username.Contains("@"))
            {

                ModelState.AddModelError("loginModel", "Make sure you use your Username not your email.");

                //TODO - Get username from email.
                //Staff m = (Staff)Members.GetByEmail(username);
                //if (m != null)
                //{
                //    //TODO - Get username from email.
                //    //username = m.;
                //}
            }

            if (Members.Login(username, model.Password) == false)
            {
                // User NOT Authenticated
                //don't add a field level error, just model level
                var member = ApplicationContext.Services.MemberService.GetByUsername(username);
                if (member == null)
                {
                    ModelState.AddModelError("loginModel", "Invalid username or password");
                }
                else if (member.IsLockedOut)
                {

                    ModelState.AddModelError("loginModel", "Your account has been locked out due to too many failed attempts, please reset your password using the forgot password link below.");
                }
                else if (!member.IsApproved)
                {

                    ModelState.AddModelError("loginModel", "Your account has been disabled.");
                }
                else
                {
                    ModelState.AddModelError("loginModel", "Invalid username or password");
                }
                return CurrentUmbracoPage();
            }
            else
            {
                //var member = ApplicationContext.Services.MemberService.GetByUsername(username);

                // User IS Authenticated
                FormsAuthentication.SetAuthCookie(model.Username, model.RememberMe);
                TempData["LoginSuccess"] = true;
                var currentSession = Session;
                var loggedMember = ApplicationContext.Current.Services.MemberService.GetByUsername(model.Username);
                currentSession["MemberProfile"] = loggedMember;
                if (model.RedirectUrl.IsNullOrWhiteSpace() == false)
                {
                    return Redirect(model.RedirectUrl);
                }
                else
                {
                    var referer = Request.UrlReferrer;
                    var url = (referer.AbsoluteUri.Contains("logout")) ? "/" : referer.AbsoluteUri;
                    return Redirect(url);
                }
            }


            //   var uName= Membership.GetUser().UserName;

            //if there is a specified path to redirect to then use it
            //if (Members.IsLoggedIn())
            //{
            //    var member = MhubMemberHelper.LoggedInMember;

            //    if (member.GetValue<int>("passwordChecker") == 1)

            //    {
            //        System.Console.WriteLine("test");
            //        return Redirect("/Account/");
            //    }
            //}


        }

        [HttpGet]
        public ActionResult MemberLogout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return Redirect("/");
        }
    }

    public class MhubLoginModel : LoginModel
    {
        [Display(Name = "Remember me")]
        public bool RememberMe { get; set; }
    }
}