﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Core;
using Umbraco.Web.Mvc;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// Summary description for StationarySurfaceController
/// </summary>
public class StationarySurfaceController : SurfaceController
{

    [HttpPost]
    public ActionResult CreateStationary(StationaryRequisition model)
    {
       // var db= ApplicationContext.Current.DatabaseContext.Database;
        var StationaryRepo = new StationaryRepository();
        if (!ModelState.IsValid)
        {
            ViewBag.Message = "Form was was not submitted Successfully, Please review";
            return CurrentUmbracoPage();
        }
        else
        {
            model.Date_Subumited = DateTime.UtcNow;
            model.Status = 0;
            var member = MhubMemberHelper.LoggedInMember;
            model.User_Id = member.Id;
            var stItem= StationaryRepo.GetItem(model.Stationary_Item_Id);

            StationaryRepo.AddRequest(model);
            TempData["tempval"] = "Your stationary request has been submitted and you can see the progess below";
            // send email
            var StationaryMan=DataHelpers.GetStationaryManager;
            var Subject = "A stationary request has been made";
            var Email = "<html><body> <p>Dear "+StationaryMan.Name + ",</p><p>A stationary request has been submitted, please see the details below. <table><tr><td> From: </td><td>"+member.Name+ "</td></tr><tr><td> Requested Item: </td><td>"+stItem.Category_Name+" - " + stItem.Description+ "</td></tr><tr><td> Quantity: </td><td>" + model.Quantity_Ordered + "</td></tr><tr><td>Comments:</td><td>"+model.Comments+"</td></tr><tr><td> Manage this request: </td><td><a href=\"http://connect.bakertillythailand.com/Administration/manage-stationary/\" >connect.bakertillythaialand.com</a></td></tr></table></p>      </body></html>";
            EmailHelper.SendEmail(Subject, StationaryMan.Email, "", Email);


        }
        return RedirectToCurrentUmbracoPage();
    }
    public StationarySurfaceController()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}