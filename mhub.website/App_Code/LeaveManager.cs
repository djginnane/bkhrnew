﻿using System;
using System.Collections.Generic;
using System.Web.Security;

using Umbraco.Core;

/// <summary>
/// Summary description for LeaveManager
/// </summary>
public class LeaveManager
{

    // get leave days assigned for the year - where is leave property stored?
    public static int AvailableLeave
    {

        get
        {
            var username = Membership.GetUser().UserName;
            var member = MhubMemberHelper.LoggedInMember;
            var leaveAmount = MhubMemberHelper.getMemberProperty<int>("holidayleave");

            return leaveAmount;
        }
    }

    public static List<LeaveRequest> PendingLeave
    {
        get
        {
            var member = MhubMemberHelper.LoggedInMember;
            var db = ApplicationContext.Current.DatabaseContext.Database;
            var query = "select leaveEnd, leaveStart,leaveType,ApprovedLeave, count(LeaveDay) as ApprovedBy, sum(case  when halfDay='true' then 1 end) as LeaveCounted from mhubLeaveRequest where MemberId=@0 group by createdAt, leaveEnd, leaveStart, leaveType, ApprovedLeave";

            var leaveTable = db.Fetch<LeaveRequest>(query, member.Id);
            DateTime LeaveStartDate;

            foreach (var leaveGp in leaveTable)
            {
                LeaveStartDate = leaveGp.LeaveStart;
            }

            return leaveTable;
        }
    }
    public static int LeaveUsed
    {



        //Find All leave days used for the current year
        // make sure memberid, getYear are not hardcoded in
        get
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;
            DateTime CurrentYear = DateTime.Today;
            var query = " select * from mhubLeaveRequest where memberId = @0 and ApprovedLeave=1 and Year(LeaveDay)=@1";
            int i = 0;

            var leaveTable = db.Fetch<LeaveRequest>(query, MhubMemberHelper.LoggedInMember.Id, CurrentYear.Year);

            foreach (var ld in leaveTable)

            {
                i++;

            }

            return i;
        }
    }



    public LeaveManager()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}