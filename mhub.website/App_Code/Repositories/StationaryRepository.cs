﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using umbraco.cms.businesslogic.member;
using Umbraco.Core.Services;

/// <summary>
/// Summary description for StationaryRepository
/// </summary>
public class StationaryRepository
{
    private readonly UmbracoDatabase db;

    public List<StationaryItem> GetAll()
    {
        var query = "select HRSStationaryItem.*, HRSstationary.Category_Name as Category_Name from HRSStationaryItem join HRSstationary on HRSstationary.Id=HRSStationaryItem.Category_Id  ";
        return db.Fetch<StationaryItem>(query,0);
    }
    public List<Stationary> GetAllCategories()
    {
        var query = "select * from HRSStationary order by Category_Name ";
        return db.Fetch<Stationary>(query, 0);
    }
    public string GetCategoryName(int Cat_Id)
    {
        var query = "select * from HRSStationary where Id=@0";
        var Cat = db.SingleOrDefault<Stationary>(query, Cat_Id);
        return Cat.Category_Name;
    }
    public Stationary AddCategory( Stationary Cat)
    {
        db.Insert(Cat);
        return Cat;
    }
    public StationaryItem UpdateStationaryItem(StationaryItem Cat)
    {
        db.Save(Cat);
        return Cat;
    }
    public StationaryRequisition UpdateStationaryRequest(StationaryRequisition Req)
    {
        db.Save(Req);
        return Req;
    }
    public StationaryItem GetItem(int Item_Id)
    {
        var query = "select HRSStationaryItem.*, HRSstationary.Category_Name as Category_Name from HRSStationaryItem join HRSstationary on HRSstationary.Id=HRSStationaryItem.Category_Id where HRSStationaryItem.Item_Id=@0 ";
        return db.SingleOrDefault<StationaryItem>(query, Item_Id);
    }

    public List<StationaryItem> GetItemByCategory(int Item_Id)
    {
        var query = "select HRSStationaryItem.*, HRSstationary.Category_Name as Category_Name from HRSStationaryItem join HRSstationary on HRSstationary.Id=HRSStationaryItem.Category_Id where HRSStationaryItem.Category_Id=@0 and IsAvailable='true' and IsActive='true' order by description asc ";
        return db.Fetch<StationaryItem>(query, Item_Id);
    }
    public StationaryItem AddStationaryItem(StationaryItem Item)
    {
        db.Insert(Item);
        return Item;
    }

    public List<StationaryRequisition> GetStationeryRequestsById(int member_Id)
    {
        var query = "select HRSStationaryRequisition.*, HRSstationary.Category_Name as Category_Name, HRSStationaryItem.Description as Item_Name from HRSStationaryRequisition join HRSstationary on HRSStationaryRequisition.Stationary_Category_Id=HRSStationary.Id join HRSStationaryItem on HRSStationaryRequisition.Stationary_Item_Id =HRSStationaryItem.Item_Id where User_id=@0 ";
        return db.Fetch<StationaryRequisition>(query, member_Id);
    }
    public List<StationaryRequisition> GetStationeryRequests(int member_Id)
    {
        var query = "select HRSStationaryRequisition.*, HRSstationary.Category_Name as Category_Name, HRSStationaryItem.Description as Item_Name from HRSStationaryRequisition join HRSstationary on HRSStationaryRequisition.Stationary_Category_Id=HRSStationary.Id join HRSStationaryItem on HRSStationaryRequisition.Stationary_Item_Id =HRSStationaryItem.Item_Id  ";
        return db.Fetch<StationaryRequisition>(query, 0);
    }
    public List<StationaryRequisition> GetStationeryRequestsPending()
    {
        var query = "select LoginName, HRSStationaryRequisition.*, HRSstationary.Category_Name as Category_Name, HRSStationaryItem.Description as Item_Name from HRSStationaryRequisition join HRSstationary on HRSStationaryRequisition.Stationary_Category_Id=HRSStationary.Id join HRSStationaryItem on HRSStationaryRequisition.Stationary_Item_Id =HRSStationaryItem.Item_Id join cmsMember on nodeid=HRSStationaryRequisition.User_Id where [Status]=0";
        return db.Fetch<StationaryRequisition>(query, 0);
    }
    public StationaryRequisition AddRequest(StationaryRequisition Item)
    {

        db.Insert(Item);
        return Item;
    }

    public List<StationaryRequisition> GetTotals()
    {
        var query = "select sum(case when HRSStationaryRequisition.[Status] = 0 then HRSStationaryRequisition.Quantity_Ordered End) as Pending_Requests , sum(case when HRSStationaryRequisition.[Status] = 1 then HRSStationaryRequisition.Quantity_Ordered End) as Approved, sum(case when HRSStationaryRequisition.[Status] in (0, 1, 2) then HRSStationaryRequisition.Quantity_Ordered End) as AllOrdered from HRSStationaryRequisition group by HRSStationaryRequisition.[User_Id] ";
        return db.Fetch<StationaryRequisition>(query, 0);
    }

    public List<StationaryReport> ReportTotals()
    {
        var query = "select Category_Name,[Description] as Item_Name, sum(Quantity_Ordered) as Stationary_Totals, Stationary_Category_Id, Stationary_Item_Id from HRSStationaryRequisition join HRSStationary on HRSStationary.Id=HRSStationaryRequisition.Stationary_Category_Id join HRSStationaryItem on HRSStationaryItem.Item_Id=HRSStationaryRequisition.Stationary_Item_Id and [Status]=1 group by Stationary_Category_Id, Stationary_Item_Id, Category_Name,[Description] order by Category_Name";
        return db.Fetch<StationaryReport>(query, 0);
    }
    public List<StationaryReport> ReportTotals(DateTime StartDate, DateTime EndDate, int[] MemberList)
    {
        var query = "";
        if (MemberList.Length!=0)
        {
             query = "select Category_Name,[Description] as Item_Name, sum(Quantity_Ordered) as Stationary_Totals, Stationary_Category_Id, Stationary_Item_Id from HRSStationaryRequisition join HRSStationary on HRSStationary.Id=HRSStationaryRequisition.Stationary_Category_Id join HRSStationaryItem on HRSStationaryItem.Item_Id=HRSStationaryRequisition.Stationary_Item_Id and [Status]=1 and Date_Subumited>@0 and Date_Subumited<@1 and User_Id in (@2) group by Stationary_Category_Id, Stationary_Item_Id, Category_Name,[Description] order by Category_Name";

        }
        else
        {
             query = "select Category_Name,[Description] as Item_Name, sum(Quantity_Ordered) as Stationary_Totals, Stationary_Category_Id, Stationary_Item_Id from HRSStationaryRequisition join HRSStationary on HRSStationary.Id=HRSStationaryRequisition.Stationary_Category_Id join HRSStationaryItem on HRSStationaryItem.Item_Id=HRSStationaryRequisition.Stationary_Item_Id and [Status]=1 and Date_Subumited>@0 and Date_Subumited<@1  group by Stationary_Category_Id, Stationary_Item_Id, Category_Name,[Description] order by Category_Name";

        }
        return db.Fetch<StationaryReport>(query, StartDate, EndDate, MemberList);
    }
    public StationaryTotals GetTotalsById(int member_Id)
    {
        var query = "select sum(case when HRSStationaryRequisition.[Status] = 0 then HRSStationaryRequisition.Quantity_Ordered End) as Pending_Requests , sum(case when HRSStationaryRequisition.[Status] = 1 then HRSStationaryRequisition.Quantity_Ordered End) as Approved, sum(case when HRSStationaryRequisition.[Status] in (0, 1, 2) then HRSStationaryRequisition.Quantity_Ordered End) as AllOrdered from HRSStationaryRequisition where User_Id=@0 group by HRSStationaryRequisition.[User_Id]  ";
        var Totals= db.SingleOrDefault<StationaryTotals>(query, member_Id);
        if (Totals==null)
        {
            Totals = new StationaryTotals();
        }
        return Totals;
    }
    public StationaryRepository()
    {
        //
        db = ApplicationContext.Current.DatabaseContext.Database;
        //
    }
}