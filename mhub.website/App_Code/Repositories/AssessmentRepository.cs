﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using umbraco.cms.businesslogic.member;
using Umbraco.Core.Services;

/// <summary>
/// Summary description for AssessmentRepository
/// </summary>
public class AssessmentRepository
{
    private readonly UmbracoDatabase db;
    public AssessmentRepository()
    {
        db = ApplicationContext.Current.DatabaseContext.Database;
    }

    public AssessmentAnswerSet GetById(int setId)
    {
        //var query = "select * from SocialMessage where Id = @0";
        return db.SingleOrDefault<AssessmentAnswerSet>(setId);
    }

    public IEnumerable<AssessmentAnswerSet> GetByMember(int mId)
    {
        return db.Fetch<AssessmentAnswerSet>("select * from AssessmentAnswers where MemberId = @0 order by CreatedAt desc", mId);
    }

    public IEnumerable<AssessmentAnswerSet> GetByAssessmentAndMember(int memberId, int assessmentId)
    {
        return db.Fetch<AssessmentAnswerSet>("select * from AssessmentAnswers where MemberId = @0 AND AssessmentId = @1 order by CreatedAt desc", memberId, assessmentId);
    }

    public IEnumerable<AssessmentAnswerSet> GetAll()
    {
        return db.Fetch<AssessmentAnswerSet>("select * from AssessmentAnswers");
    }

    public AssessmentAnswerSet Save(AssessmentAnswerSet set)
    {
        // if the user did not fail then mark the assessment as completed.
        if (set.Grade.ToLower() != "fail")
        {
            set.CompletedAt = DateTime.Now;
        }

        // this was too big for the database. if they want to review assessments in the future we will need to change the field in the DB
        set.Assessment = "";

        object result = db.Insert(set);


        var member = MhubMemberHelper.LoggedInMember;
        var assessments = GetByMember(member.Id);

        member.Properties["assessmentsCompleted"].Value = assessments.Count();

        int total = 0;
        assessments.ForEach(a => total += Int32.Parse(a.Score));
        int average = (assessments.Count() < 1) ? int.Parse(set.Score) : (total / assessments.Count());
        member.Properties["averageAssessmentScore"].Value = average;

        ApplicationContext.Current.Services.MemberService.Save(member);

        return set;
        //if ((decimal)result>0)
        //{
        //    return GetById(set.Id);
        //}

        return null;
    }

    internal object GetByAssessment(int id)
    {
        var query = new Sql().Select("*").From("AssessmentAnswers").Where("AssessmentId = @0", id); //.OrderBy("CreatedAt", "desc");

        return db.Fetch<AssessmentAnswerSet>(query);
    }
}