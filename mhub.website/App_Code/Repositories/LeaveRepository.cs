﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using umbraco.cms.businesslogic.member;
using Umbraco.Core.Services;

/// <summary>
/// Summary description for LeaveRepository
/// </summary>
public class LeaveRepository
{
    private readonly UmbracoDatabase db;
   
    public List<eLearningModel> UnpaidLeaveReport(DateTime StartMonth, DateTime EndMonth)
    {
       

       // return db.Fetch<eLearningModel>("select MemberId as [key], sum(case when leavetype='Leave without pay' then 1 else 0 end) as [value], [LoginName] from [dbo].[mhubLeaveRequest] join cmsmember on nodeid=MemberId where ApprovedLeave=1 and LeaveDay>@0 and LeaveDay<@1 group by MemberId, [LoginName]",StartMonth,EndMonth);
        return db.Fetch<eLearningModel>(" select cmsmember.LoginName, cmsmember.nodeId, sum(case when  LeaveCounted = 0 then 1 else 0 end) as [Total] from[dbo].[mhubLeaveRequest] join cmsmember on nodeid = mhubLeaveRequest.MemberId where ApprovedLeave = 1 and LeaveDay>@0 and LeaveDay< @1 group by LoginName, cmsmember.nodeid order by cmsmember.LoginName", StartMonth, EndMonth);

    }

    public List<LeaveRequest> CalenderEvents (int[] staffCount)

    {
        var datetime = DateTime.UtcNow.AddMonths(-1);
        var query = "Select leaveday, leavestart, ApprovedLeave, leaveend, MemberId,cmsmember.loginname  from mhubLeaveRequest, cmsmember where mhubleaverequest.memberid=cmsmember.nodeid and  (ApprovedLeave=1 or ApprovedLeave=0) and mhubleaverequest.memberid in (@0) and LeaveStart>=@1";

        if (staffCount.Count() == 0)
        {
            query = "Select leaveday, leavestart, ApprovedLeave, leaveend, MemberId,cmsmember.loginname  from mhubLeaveRequest, cmsmember where mhubleaverequest.memberid=cmsmember.nodeid and  (ApprovedLeave=1 or ApprovedLeave=0) and mhubleaverequest.memberid =9999";

        }
        return db.Fetch<LeaveRequest>(query, staffCount, datetime);

    }
    public LeaveRepository()
    {
         db = ApplicationContext.Current.DatabaseContext.Database;  // TODO: Add constructor logic here
        //
    }
}