﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using umbraco.cms.businesslogic.member;
using Umbraco.Core.Services;
/// <summary>
/// Summary description for PollRepository
/// </summary>
public class PollRepository
{
    private readonly UmbracoDatabase db;

    public List<Stationary> GetAllPollresults(string PollName)
    {
        var query = "select * from HRSStationary order by Category_Name ";
        return db.Fetch<Stationary>(query, 0);
    }

    public List<PollsAnswers> GetUserPolls(int User_Id, string PollName)
    {
        var query = "select * from HRSPollsAnswers where User_Id=@0 and Poll_Name=@1 ";
        var Cat = db.Fetch<PollsAnswers>(query, User_Id, PollName);
        return Cat;
    }

    public List<eLearningModel> GetLatestPollResults(string PollName)
    {
        
        var query = "SELECT  Pollanswer as [key],   sum(Poll_Id) as [value] FROM[dbo].[HRSPollsAnswers] where Poll_Name=@0 group by Pollanswer  ";
        var Cat = db.Fetch<eLearningModel>(query,  PollName);
        return Cat;
    }
    public PollRepository()
    {
        //
        db = ApplicationContext.Current.DatabaseContext.Database;
        //
    }
}