﻿using System;
using System.Collections.Generic;
using Umbraco.Core;
using Umbraco.Core.Persistence;

/// <summary>
/// Summary description for SocialRepository
/// </summary>
public class SocialRepository
{
    private readonly UmbracoDatabase _database;

    public SocialRepository()
    {
        _database = ApplicationContext.Current.DatabaseContext.Database;
    }

    public SocialMessage GetMessageById(Guid pMessageId)
    {
        var query = "select * from SocialMessage where Id = @0";
        return _database.FirstOrDefault<SocialMessage>(query, pMessageId.ToString());
    }

    public IEnumerable<SocialMessage> GetMessagesByMember(int pMemberId)
    {
        return _database.Fetch<SocialMessage>("select * from SocialMessage where MemberId = @0 order by CreatedAt desc", pMemberId);
    }

    public SocialMessage SaveMessage(SocialMessageType pType, int pMemberId, string pContent)
    {
        var socialMessage = new SocialMessage()
        {
            Type = pType,
            MemberId = pMemberId,
            Content = pContent
        };

        var result = (bool)_database.Insert(socialMessage);

        if (result)
        {
            return GetMessageById(socialMessage.Id);
        }

        return null;
    }
}