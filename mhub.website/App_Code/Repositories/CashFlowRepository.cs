﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using umbraco.cms.businesslogic.member;
using Umbraco.Core.Services;
/// <summary>
/// Summary description for CashFlowRepository
/// </summary>
public class CashFlowRepository
{
    private readonly UmbracoDatabase db;

    public CFPayment AddPayment(CFPayment Payment)
    {
        db.Insert(Payment);
        return Payment;
    }
    public CFPayment Payment(CFPayment Payment)
    {
        db.Save(Payment);
        return Payment;
    }
    public Supplier AddSupplier(Supplier Sup)
    {
        db.Save(Sup);
        return Sup;
    }
    public BudgetTotals AddBudget(BudgetTotals bud)
    {
        db.Insert(bud);
        return bud;
    }
    public BudgetTotals SaveBudget(BudgetTotals bud)
    {
        db.Save(bud);
        return bud;
    }
   public void RemoveBudgetTotal(BudgetTotals bud)
    {
        db.Delete(bud);
    }
    public BudgetGroupings AddBudgetGroup(BudgetGroupings budGroup)
    {
        db.Insert(budGroup);
        return budGroup;
    }


    public List<Supplier> GetSupplierList(string Comp)
    {
        var query = "select * from BKSupplier where Job_Name=@0";
        var Cat = db.Fetch<Supplier>(query, Comp);
        return Cat;
    }
    public List<CFPayment> CheckInvoice(string Invoice, string Comp)
    {
        var query = "select * from BKCFPayment where Invoice_Number=@0 and Job_Name=@1";
        var Supplier = 0;

        var Invoices = db.Fetch<CFPayment>(query, Invoice, Comp);
        foreach (var i in Invoices)
        {
            Supplier = i.Supplier_Id;
        }
        if (Supplier != 0)
        { 
        var query2 = "select * from BKCFPayment left join BKSupplier on Supplier_Id=BKSupplier.Id  where Supplier_Id=@0";

        var Invoices2 = db.Fetch<CFPayment>(query2, Supplier);
        return Invoices2;
    }
        return new List<CFPayment>();
    }
    public List<CFPayment> GetPRList(string Comp)
    {
        var query = "select * from BKCFPayment left join BKSupplier on Supplier_Id = BKSupplier.Id where pr_number!= '' and (Invoice_Number = '' or Invoice_Number is null) and BKCFPayment.Job_Name=@0";
        var PrList= db.Fetch<CFPayment>(query, Comp);
        return PrList;
    }
    public List<CFPayment> GetAdvanceList(string Comp)
    {
        var query = "  select BKCFPayment.*, BKSupplier.Supplier_Name as Supplier_Name  from BKCFPayment left join BKSupplier on Supplier_Id = BKSupplier.Id where Advanced=1 and Advance_Completed=0";
        var PrList = db.Fetch<CFPayment>(query, Comp);
        return PrList;
    }
    public List<CFPayment> GetEstimateList(string Comp)
    {
        var query = "  select BKCFPayment.*, BKSupplier.Supplier_Name as Supplier_Name  from BKCFPayment left join BKSupplier on Supplier_Id = BKSupplier.Id where Estimate_Table=1";
        var PrList = db.Fetch<CFPayment>(query, Comp);
        return PrList;
    }

    public CFPayment UpdateAdvanced(int ID, string Comp)
    {
        var query = "select * from BKCFPayment where id=@0 and Job_Name=@1";
        var Py = db.Single<CFPayment>(query,ID, Comp);
        return Py;
    }
    public Supplier CheckSupplierExists(Supplier sup, string Comp)
    {
        var query = "select * from BKSupplier where Supplier_Title=@0 and Supplier_Name=@1 and Job_Name=@2";
    var Py = db.Fetch<Supplier>(query, sup.Supplier_Title,sup.Supplier_Name, Comp);
        var newSup = new Supplier();
        foreach (var sp in Py)
        {
            newSup = sp;
        }
        return newSup;
    }

    public List<CFPayment> GetAll(string Comp)
    {
        var query = "  select BKCFPayment.*, BKSupplier.Supplier_Name as Supplier_Name, BKSupplier.Supplier_Title as Supplier_Title, BKSupplier.Supplier_Code, BKCFPaymentGroups.Group_Name as Group_Of_Expenses_Name,BKCFPaymentSubGroups.Group_Name as SubGroup_Of_Expenses_name  from BKCFPayment left join BKSupplier on Supplier_Id = BKSupplier.Id left join BKCFPaymentGroups on Group_Of_Expenses=BKCFPaymentGroups.Id left join BKCFPaymentSubGroups on SubGroup_Of_Expenses=BKCFPaymentSubGroups.Id";
        var PrList = db.Fetch<CFPayment>(query, Comp);
        return PrList;
    }
    public List<PaymentGroups> GetGroups(string Comp="Popcorn")
    {
        var query = "from BKCFPaymentGroups";
        var pGroups = db.Fetch<PaymentGroups>(query, Comp);
        return pGroups;
    }
    public List<PaymentSubGroups> GetSubGroups(int groupId, string Comp = "Popcorn")
    {
        var query = "from BKCFPaymentSubGroups where Parent_Group=@0";
        var pGroups = db.Fetch<PaymentSubGroups>(query, groupId, Comp);
        return pGroups;
    }
    /// <summary>
    /// Gets payments grouped for the summary
    /// </summary>
    /// <param name="LetterNo"></param>
    /// <param name="Month"></param>
    /// <param name="Comp"></param>
    /// <returns></returns>
    public List<CFPayment> GetSummary(int LetterNo, int Month, string Comp = "Popcorn")
    {
        var query = "Select Group_Of_Expenses,BKCFPaymentGroups.Group_Name as Group_Of_Expenses_Name , sum(Case WHEN Actual_Net_Amount>0 and Estimate_Table=1 and DATEPART(m, Date_Actual_Payment) =@1 THEN Actual_Net_Amount else NetAmount_Total end) as netAmount_Total from BKCFPayment join BKCFPaymentGroups on Group_Of_Expenses=BKCFPaymentGroups.Id  where Payment_letterNo=@0 and Payment_Month=@1 group by Group_Of_Expenses, BKCFPaymentGroups.Group_Name order by netAmount_Total desc";
        var pGroups = db.Fetch<CFPayment>(query, LetterNo, Month);
        return pGroups;
    }

    public List<CFPayment> GetSubGroups(int LetterNo, int Month, string Comp = "Popcorn")
    {
        var query = "Select SubGroup_Of_Expenses , Group_Name as SubGroup_Of_Expenses_Name, Max(Group_Of_Expenses) AS Group_Of_Expenses , sum(Case WHEN Actual_Net_Amount>0 and Estimate_Table=1 and DATEPART(m, Date_Actual_Payment) =@1 THEN Actual_Net_Amount else NetAmount_Total end) as netAmount_Total from BKCFPayment join BKCFPaymentGroups on BKCFPaymentGroups.Id=SubGroup_Of_Expenses where  Payment_letterNo=@0 and Payment_Month=@1 group by SubGroup_Of_Expenses,BKCFPaymentGroups.Group_Name  order by netAmount_Total desc";
        var pGroups = db.Fetch<CFPayment>(query, LetterNo, Month);
        return pGroups;
    }
    public CFPayment GetEstimateTotal(int LetterNo, int Month, string Comp = "Popcorn")
    {
        var query = "select Sum (Variance_Difference) as NetAmount_Total from [dbo].[BKCFPayment] where DatePart(MM,Date_Actual_Payment) = @1 and Payment_letterNo=@0 and Payment_Month!=@1";
        var pGroups = db.SingleOrDefault<CFPayment>(query, LetterNo, Month);
        return pGroups;
    }
    public List<CFPayment> GetPaymentList(int LetterNo, int Month,  int Year ,string Comp = "Popcorn")
    {
        var query = "  select BKCFPayment.*, BKSupplier.Supplier_Name as Supplier_Name,  BKSupplier.Supplier_Title as Supplier_Title from BKCFPayment left join BKSupplier on BKSupplier.Id=BKCFPayment.Supplier_Id where Payment_letterNo=@0 and Payment_Month=@1 and Payment_Year=@2";
        var pGroups = db.Fetch<CFPayment>(query, LetterNo, Month, Year);
        return pGroups;
    }

    /// <summary>
    /// get all old unpaid invoices from supplier
    /// </summary>
    public List<OldDebt> GetOldDebt(int Supplier_Code)
    {
        var query = "SELECT   [dbo].[BKCFOldDebt].[Amount] ,[Supllier_Name] ,[Invoice_No], [dbo].[BKCFOldDebt].supplier_Code,    sum (BKCFPayment.NetAmount_Total) as Amount_Remaining  FROM [dbo].[BKCFOldDebt] left join BKCFPayment on Invoice_No=Invoice_Number and BKCFPayment.Supplier_Code=BKCFOldDebt.Supplier_Code where BKCFOldDebt.Supplier_Code=@0 group by Invoice_No, [dbo].[BKCFOldDebt].Amount , Supllier_Name,  [dbo].[BKCFOldDebt].supplier_Code";
        var invoices= db.Fetch<OldDebt>(query, Supplier_Code);
        return invoices;

    }

    /// <summary>
    /// get all old unpaid invoices from supplier
    /// </summary>
    public List<OldDebt> OldDebtTable()
    {
        var query = "SELECT [dbo].[BKCFOldDebt].[Amount] ,[Supllier_Name] ,[dbo].[BKCFOldDebt].[Supplier_Code],[Invoice_No], [dbo].[BKCFOldDebt].supplier_Code,    STRING_Agg (format(BKCFPayment.AmountplusVat, '#,#'),'--') as [Payments], sum(BKCFPayment.AmountplusVat) as Amount_Remaining  FROM [dbo].[BKCFOldDebt] left join BKCFPayment on Invoice_No=Invoice_Number and BKCFPayment.Supplier_Code=BKCFOldDebt.Supplier_Code group by Invoice_No, [dbo].[BKCFOldDebt].Amount , Supllier_Name,  [dbo].[BKCFOldDebt].supplier_Code";
        var OldInvoices = db.Fetch<OldDebt>(query, 1);

       
       
        return OldInvoices;
    }
    public List<BudgetTotals> getExistingBudget( int Payment_Month, int Payment_Year)
    {
        var query = "select * from BKCFBudgetTotals where  payment_Month=@0 and Payment_Year=@1";
        var bt = db.Fetch<BudgetTotals>(query,Payment_Month, Payment_Year);
       


        return bt;
    }

    public List<BudgetTotals> checkExistingBudgetGroups(int Payment_Month, int Payment_Year)
    {
        var query = " select * from BKCFBudgetGroupings join  BKCFPaymentGroups on Group_Id=BKCFPaymentGroups.Id where payment_Month=@0 and Payment_Year=@1";
        var bt = db.Fetch<BudgetTotals>(query, Payment_Month, Payment_Year);



        return bt;
    }
    public List<BudgetTotals> getAllExistingBudget()
    {
        var query = "  select [BKCFBudgetTotals].*, Group_Name  FROM [dbo].[BKCFBudgetTotals] join BKCFPaymentGroups on [dbo].[BKCFBudgetTotals].Group_Id=BKCFPaymentGroups.Id order by Date_Created desc";
        var bt = db.Fetch<BudgetTotals>(query);



        return bt;
    }
    public List<BudgetTotals> BudgetGrouped()
    {
        var query = " BKCFBudgetTotals.Payment_Year, BKCFBudgetTotals.Payment_Month, sum(bkcfPayment.NetAmount_Total)  as Actual_Amount, sum(BKCFBudgetTotals.Version1) Version1, sum(BKCFBudgetTotals.Version2) Version2 FROM [dbo].[BKCFBudgetTotals] left join bkcfPayment on bkcfPayment.Payment_Month=BKCFBudgetTotals.Payment_Month and bkcfPayment.Payment_letterNo=BKCFBudgetTotals.Payment_Letter  group by [BKCFBudgetTotals].Payment_Month, [BKCFBudgetTotals].Payment_Year";
        var bt = db.Fetch<BudgetTotals>(query);



        return bt;
    }
    /// <summary>
    /// gets budget for review where all payment letters below the one selected are used when calculating the Actual Amount
    /// </summary>
    /// <param name="Payment_Letter"></param>
    /// <param name="Payment_Month"></param>
    /// <param name="Payment_Year"></param>
    /// <param name="Created_Date"></param>
    /// <returns></returns>
    public List<BudgetTotals> BudgetIndiv(int Payment_Letter, int Payment_Month, int Payment_Year,DateTime Created_Date)
    {
          var query = " select Group_Id, Group_Name, sum(bkcfPayment.NetAmount_Total) as Actual_Amount,   sum(Version1) as Version1, sum(Version2) as Version2 from BKCFBudgetTotals left join BKCFPayment on bkcfPayment.Payment_Month=BKCFBudgetTotals.Payment_Month and BKCFPayment.Payment_LetterNo<=@0 and bkcfPayment.Payment_Year=BKCFBudgetTotals.Payment_Year join BKCFPaymentGroups on BKCFPaymentGroups.Id=BKCFBudgetTotals.Group_Id where BKCFBudgetTotals.Payment_Month=@1   and BKCFBudgetTotals.Payment_year=@2 group by Group_Id, BKCFPaymentGroups.Group_Name";
      //  var query = " select Group_Id, Group_Name, sum(bkcfPayment.NetAmount_Total) as Actual_Amount,   sum(Version1) as Version1, sum(Version2) as Version2 from BKCFBudgetTotals left join BKCFPayment on bkcfPayment.Payment_Month=BKCFBudgetTotals.Payment_Month and bkcfPayment.Payment_letterNo=BKCFBudgetTotals.Payment_Letter join BKCFPaymentGroups on BKCFPaymentGroups.Id=BKCFBudgetTotals.Group_Id where BKCFBudgetTotals.Payment_Month=@1 and BKCFBudgetTotals.Payment_letter=@0 group by Group_Id, BKCFPaymentGroups.Group_Name";
        var bt = db.Fetch<BudgetTotals>(query, Payment_Letter, Payment_Month,Payment_Year, Created_Date);



        return bt;
    }
    /// <summary>
    /// Same as above, only it gets totals not seperate groups
    /// </summary>
    /// <param name="Payment_Letter"></param>
    /// <param name="Payment_Month"></param>
    /// <param name="Payment_Year"></param>
    /// <param name="Created_Date"></param>
    /// <returns></returns>
    public List<BudgetTotals> BudgetIndivTotal(int Payment_Letter, int Payment_Month, int Payment_Year, DateTime Created_Date)

    {
        var query = " select sum(bkcfPayment.NetAmount_Total) as Actual_Amount,   sum(Version1) as Version1, sum(Version2) as Version2 from BKCFBudgetTotals left join BKCFPayment on bkcfPayment.Payment_Month=BKCFBudgetTotals.Payment_Month and BKCFPayment.Payment_LetterNo<=@0 and bkcfPayment.Payment_Year=BKCFBudgetTotals.Payment_Year join BKCFPaymentGroups on BKCFPaymentGroups.Id=BKCFBudgetTotals.Group_Id where BKCFBudgetTotals.Payment_Month=@1   and BKCFBudgetTotals.Payment_year=@2 group by BKCFBudgetTotals.Payment_year, BKCFBudgetTotals.Payment_month ";
    //  var query = " select Group_Id, Group_Name, sum(bkcfPayment.NetAmount_Total) as Actual_Amount,   sum(Version1) as Version1, sum(Version2) as Version2 from BKCFBudgetTotals left join BKCFPayment on bkcfPayment.Payment_Month=BKCFBudgetTotals.Payment_Month and bkcfPayment.Payment_letterNo=BKCFBudgetTotals.Payment_Letter join BKCFPaymentGroups on BKCFPaymentGroups.Id=BKCFBudgetTotals.Group_Id where BKCFBudgetTotals.Payment_Month=@1 and BKCFBudgetTotals.Payment_letter=@0 group by Group_Id, BKCFPaymentGroups.Group_Name";
    var bt = db.Fetch<BudgetTotals>(query, Payment_Letter, Payment_Month, Payment_Year, Created_Date);



        return bt;
    }
/// <summary>
/// Summary of all budgets showing totals only, including actual
/// </summary>
/// <returns></returns>
public List<BudgetTotals> BudgetSummary()
    {
        var query = " select BKCFBudgetTotals.Payment_Month, BKCFBudgetTotals.Payment_Year,sum(bkcfPayment.NetAmount_Total) as Actual_Amount,  sum(Version1) as Version1, sum(Version2) as Version2 from BKCFBudgetTotals left join BKCFPayment on bkcfPayment.Payment_Month=BKCFBudgetTotals.Payment_Month and bkcfPayment.Payment_Year=BKCFBudgetTotals.Payment_Year group by BKCFBudgetTotals.Date_Created,  BKCFBudgetTotals.Payment_Month, BKCFBudgetTotals.Payment_Year";
        var bt = db.Fetch<BudgetTotals>(query, 1);
        return bt;
    }
    public CashFlowRepository()
    {
        db = ApplicationContext.Current.DatabaseContext.Database;
    }
}