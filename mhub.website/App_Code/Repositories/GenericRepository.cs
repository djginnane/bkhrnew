﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Persistence;

/// <summary>
/// Summary description for GenericRepository
/// </summary>
public class GenericRepository<T>
{
    private readonly UmbracoDatabase db;
    public GenericRepository()
    {
        db = ApplicationContext.Current.DatabaseContext.Database;
    }

    public T GetById(string id)
    {
       return db.FirstOrDefault<T>(id);
    }

    public bool Save(T obj)
    {
        return (bool)db.Insert(obj);
    }
}