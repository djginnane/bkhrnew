﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

/// <summary>
/// Summary description for MhubEventHandler
/// </summary>
public class MhubEventHandler : ApplicationEventHandler
{
    protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
    {
        Umbraco.Core.Services.ContentService.Published += ContentService_Published;
        MediaService.Saved += MediaService_Saved;
        MediaService.Created += MediaService_Created;
        CreateSection();

        //TODO - remove if not needed
        //RegisterStyles(BundleTable.Bundles);
        //RegisterJavascript(BundleTable.Bundles);
    }

    private void MediaService_Created(IMediaService sender, Umbraco.Core.Events.NewEventArgs<IMedia> e)
    {
        //throw new NotImplementedException();
    }

    private void MediaService_Saved(IMediaService sender, Umbraco.Core.Events.SaveEventArgs<IMedia> e)
    {
        //throw new NotImplementedException();
        e.SavedEntities.ForEach(m =>
        {
            LogHelper.Debug(this.GetType(), m.Name);
        });
    }

    void ContentService_Published(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<Umbraco.Core.Models.IContent> e)
    {
        // LogHelper will write to tracelog.txt
        e.PublishedEntities.ForEach(m =>
        {
            LogHelper.Debug(this.GetType(), m.Id.ToString());
        });
        LogHelper.Info(typeof(MhubEventHandler), "Something has been published");
    }

    void ContentService_Saved(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<Umbraco.Core.Models.IContent> e)
    {

    }

    void CreateSection()
    {
        ApplicationContext.Current.Services.SectionService.MakeNew("Assessment", "assessment", "");
        //ApplicationContext.Current.Services.ApplicationTreeService.MakeNew(true,1,"");
    }

    //private static void RegisterStyles(BundleCollection bundles)
    //{
    //    //Add all style files. These will be bundled and minified.
    //    //After everything has been bundled (which only happens when debug=false in the web.config) check if there are no errors in the bundled file. Not all files can be bundled.
    //    bundles.Add(new StyleBundle("~/bundle/styles.css").Include(
    //            "~/css/main.css"
    //        )
    //    );
    //}

    //private static void RegisterJavascript(BundleCollection bundles)
    //{
    //    //Add all javascript files. These will be bundled and minified.
    //    //After everything has been bundled (which only happens when debug=false in the web.config) check if there are no errors in the bundled file. Not all files can be bundled.
    //    bundles.Add(new ScriptBundle("~/bundle/javascript.js").Include(
    //            "~/umbraco/plugins/TrackMedia/Tracking.js",
    //            "~/scripts/jquery.validate.js",
    //            "~/scripts/slimmage.js",
    //            "~/scripts/functions.js"
    //        )
    //    );
    //}

}