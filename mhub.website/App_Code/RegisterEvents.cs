﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Persistence;

/// <summary>
/// Create custom tables
/// </summary>

public class RegisterEvents : ApplicationEventHandler
{
    protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
    {
        var ctx = applicationContext.DatabaseContext;
        var db = new DatabaseSchemaHelper(ctx.Database, applicationContext.ProfilingLogger.Logger, ctx.SqlSyntax);
        if (!db.TableExist("mhubLeaveRequest"))
        {
            db.CreateTable<LeaveRequest>(false);
        }
        if (!db.TableExist("mhubPublicHolidays"))
        {
            db.CreateTable<PublicHolidays>(false);
        }
        if (!db.TableExist("mhubAppraisalAnswer"))
        {
            db.CreateTable<UmbracoWithMvc.Entities.AppraisalAnswer>(false);
        }

        if (!db.TableExist("mhubMemberAppraisal"))
        {
            db.CreateTable<UmbracoWithMvc.Entities.MemberAppraisal>(false);
        }

        if (!db.TableExist("AssessmentAnswers"))
        {
            db.CreateTable<AssessmentAnswerSet>(false);
        }
        if (!db.TableExist("mhubTrainingDays"))
        {
            db.CreateTable<UmbracoWithMvc.Entities.TrainingDays>(false);
        }
        if (!db.TableExist("mhubtrainingRequest"))
        {
            db.CreateTable<UmbracoWithMvc.Entities.trainingRequest>(false);
        }
        if (!db.TableExist("mhubTravelRequest"))
        {
            db.CreateTable<UmbracoWithMvc.Entities.TravelRequest>(false);
        }
        if (!db.TableExist("mhubAlerts"))
        {
            db.CreateTable<UmbracoWithMvc.Entities.Alerts>(false);
        }
        if (!db.TableExist("mhubVirtualSupport"))
        {
            db.CreateTable<UmbracoWithMvc.Entities.VirtualSupport>(false);
        }
        if (!db.TableExist("mhubCountryList"))
        {
            db.CreateTable<CountryList>(false);
        }
        if (!db.TableExist("HRSStationaryRequisition"))
        {
            db.CreateTable<StationaryRequisition>(false);
        }
        if (!db.TableExist("HRSStationary"))
        {
            db.CreateTable<Stationary>(false);
        }
        if (!db.TableExist("HRSStationaryItem"))
        {
            db.CreateTable<StationaryItem>(false);
        }
        if (!db.TableExist("HRSPollsAnswers"))
        {
            db.CreateTable<PollsAnswers>(false);
        }
        if (!db.TableExist("BKCFPayment"))
        {
            db.CreateTable<CFPayment>(false);
        }
        if (!db.TableExist("BKSupplier"))
        {
            db.CreateTable<Supplier>(false);
        }
        if (!db.TableExist("BKCFPaymentGroups"))
        {
            db.CreateTable<PaymentGroups>(false);
        }
        if (!db.TableExist("BKCFPaymentSubGroups"))
        {
            db.CreateTable<PaymentSubGroups>(false);
        }
        if (!db.TableExist("BKCFOldDebt"))
        {
            db.CreateTable<OldDebt>(false);
        }
        if (!db.TableExist("BKCFBudgetTotals"))
        {
            db.CreateTable<BudgetTotals>(false);
        }
        if (!db.TableExist("BKCFBudgetGroupings"))
        {
            db.CreateTable<BudgetGroupings>(false);
        }
        ctx = null;
        db = null;
    }
}