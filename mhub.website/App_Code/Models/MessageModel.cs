﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Core.Media;
using System;
using Umbraco.Core.Services;

/// <summary>
/// Summary description for MemberModel
/// </summary>
public class MessageModel
{
    public IEnumerable<IntranetMessage> Messages { get; set; }

    public MessageModel()
    {
        Messages = new List<IntranetMessage>();
    }
}

