﻿using System;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

[TableName("SocialMessage")]
[PrimaryKey("Id", autoIncrement = false)]
public class SocialMessage
{
    [PrimaryKeyColumn(AutoIncrement = false)]
    public Guid Id { get; set; }

    public int MemberId { get; set; }

    public SocialMessageType Type { get; set; }

    public int TargetId { get; set; }

    [SpecialDbType(SpecialDbTypes.NTEXT)]
    public string Content { get; set; }

    public DateTime CreatedAt { get; set; }

    public SocialMessage()
    {
        Id = Guid.NewGuid();
        CreatedAt = DateTime.UtcNow;
    }

    internal static SocialMessage From(int pMemberId, SocialMessageType pType, int pTargetId, string pMessageContent)
    {
        return new SocialMessage()
        {
            MemberId = pMemberId,
            Type = pType,
            TargetId = pTargetId,
            Content = pMessageContent
        };
    }
}

public enum SocialMessageType
{
    Text = 1
}