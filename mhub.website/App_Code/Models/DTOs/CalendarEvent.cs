﻿using Newtonsoft.Json;
using System;

public class CalendarEvent
{
    [JsonProperty("id")]
    public int Id { get; set; }

    [JsonProperty("title")]
    public string Title { get; set; }

    [JsonProperty("start")]
    public DateTime StartsAt { get; set; }

    [JsonProperty("startMonth")]
    public string StartsAtMonth { get; internal set; }

    [JsonProperty("startDay")]
    public int StartsAtDay { get; internal set; }

    [JsonProperty("end")]
    public DateTime? EndsAt { get; set; }

	[JsonProperty("publishAfter")]
	public DateTime? PublishAfter { get; set; }

	[JsonProperty("publishBefore")]
	public DateTime? PublishBefore { get; set; }
    
	[JsonProperty("url")]
	public string Url { get; set; }

	[JsonProperty("subtitle")]
	public string Subtitle { get; set; }
        
}