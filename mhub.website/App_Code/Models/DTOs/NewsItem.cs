﻿using Newtonsoft.Json;
using System;

public class NewsItem
{
    [JsonProperty("id")]
    public int Id { get; set; }

    [JsonProperty("title")]
    public string Title { get; set; }

    [JsonProperty("publishedAt")]
    public DateTime PublishedAt { get; set; }

    [JsonProperty("publishedAtMonth")]
    public string PublishedAtMonth { get; internal set; }

    [JsonProperty("publishedAtDay")]
    public int PublishedAtDay { get; internal set; }

	[JsonProperty("url")]
	public string Url { get; set; }

	[JsonProperty("text")]
	public string Text { get; set; }    
    
	[JsonProperty("image")]
	public string Image { get; set; }
}