﻿using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using System.Collections.Generic;
using System.Linq;
using System;

/// <summary>
/// Summary description for PollQuestion
/// </summary>
public class PollQuestion
{
    public PollQuestion()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    [JsonProperty("pollQuestionId")]
    public int PollId { get; set; }

    [JsonProperty("pollQuestion")]
    public string PollQuestionText { get; set; }

    [JsonProperty("pollAnswer")]
    public IEnumerable<PollAnswer> PollAnswerList { get; set; }

    [JsonProperty("pollResults")]
    public IEnumerable<PollResults> PollResultsList { get; set; }

    public static PollQuestion From(IContent question, IEnumerable<IContent> answers)
    {
        double totalVotes = answers.AsEnumerable().Sum(x => x.GetValue<double>("votes"));

        var result = new PollQuestion
        {
            PollId = question.Id,
            PollQuestionText = question.GetValue<string>("question"),
            PollAnswerList = answers.Select(x => PollAnswer.From(x)),
            PollResultsList = from a in answers
                              select new PollResults
                              {
                                  PollId = a.ParentId,
                                  PollAnswerId = a.Id,
                                  PollAnswerText = a.GetValue<string>("answer"),
                                  Votes = a.GetValue<int>("votes"),
                                  percent = Math.Round((a.GetValue<double>("votes") / totalVotes) * 100, 2)
                              }
    };

        return result;
    }
}

public class PollAnswer
{
    public PollAnswer()
    {

    }

    [JsonProperty("pollAnswerID")]
    public int PollAnswerId { get; set; }

    [JsonProperty("pollAnswer")]
    public string PollAnswerText { get; set; }

    [JsonProperty("votes")]
    public int Votes { get; set; }

    public static PollAnswer From(IContent answer)
    {
        var result = new PollAnswer
        {
            PollAnswerId = answer.Id,
            PollAnswerText = answer.GetValue<string>("answer"),
            Votes = answer.GetValue<int>("votes")
        };
        return result;
    }

}

public class PollResults
{
    [JsonProperty("pollID")]
    public int PollId { get; set; }

    [JsonProperty("pollAnswerID")]
    public int PollAnswerId { get; set; }

    [JsonProperty("pollAnswer")]
    public string PollAnswerText { get; set; }

    [JsonProperty("votes")]
    public int Votes { get; set; }

    [JsonProperty("votesPercent")]
    public double percent { get; set; }


    public static PollResults From(IContent pollQuestion, IContent answer)
    {
        var result = new PollResults
        {
            PollId = pollQuestion.Id,
            PollAnswerId = answer.Id,
            PollAnswerText = answer.GetValue<string>("answer"),
            Votes = answer.GetValue<int>("votes") 
        };

        return result;

    }
}
