﻿using Newtonsoft.Json;
using System;

public class IntranetMessage
{
    [JsonProperty("messageId")]
    public Guid MessageId { get; set; }

    [JsonProperty("messageType")]
    public SocialMessageType MessageType { get; set; }

    [JsonProperty("messageContent")]
    public string MessageContent { get; set; }

    [JsonProperty("messageCreatedAt")]
    public DateTime MessageCreatedAt { get; set; }

    //[JsonProperty("memberId")]
    //public int MemberId { get; set; }

    //[JsonProperty("email")]
    //public string Email { get; set; }

    //[JsonProperty("name")]
    //public string Name { get; set; }

    //[JsonProperty("nickname")]
    //public string Nickname { get; set; }

    //[JsonProperty("photo")]
    //public string Photo { get; set; }

    public static IntranetMessage From(SocialMessage pMessage)
    {
        var result = new IntranetMessage()
        {
            MessageId = pMessage.Id,
            MessageType = pMessage.Type,
            MessageContent = pMessage.Content,
            MessageCreatedAt = pMessage.CreatedAt
            //MessageCreatorId = pContent.CreatorId,
            //MessageCreatorId = pContent.CreatorId,
            //MemberId = pMember.Id,
            //Email = pMember.Email,
            //Name = pMember.Name,
            //Nickname = pMember.GetValue<string>("nickname"),
        };

        return result;
    }
}