﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Media;
using Umbraco.Core.Services;
using UmbracoWithMvc.Entities;


/// <summary>
/// Summary description for MemberAppModel
/// </summary>
public class MemberAppModel
{
    public string membername { get; set; }
    public int member_id { get; set; }
    public string username { get; set; }
    public int App_Status { get; set; }

    public List<MemberAppraisal> MemberApps { get; set; }

    public MemberAppModel()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
    public class MemberPropertiesModel
    {
        public string branch { get; set; }
        public string location { get; set; }
        public string username { get; set; }
        public int Id { get; set; }
        public int roleLevel { get; set; }
    public int RootNode { get; set; }
    //public List<MemberAppraisal> MemberApps { get; set; }

    public MemberPropertiesModel()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }