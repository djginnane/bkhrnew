﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AppraisalReport
/// </summary>
public class AppraisalReport
{

   public int Member_Id { get; set; }
    public int AppraisalCount { get; set; }
    public int ManagerIncomplete { get; set; }
    public int StaffIncomplete { get; set; }
    public int TotalCompleted { get; set; }
    public string UserName { get; set; }
    public string Name { get; set; }
    public string ManagerName { get; set; }
    public int ManagerId { get; set; }
   
    public string AppraisalConcatsMan { get; set; }
    public string AppraisalConcatsStaff { get; set; }
    public string AppraisalConcatsCom { get; set; }
    public string AppraisalConcats { get; set; }
    public AppraisalReport()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}