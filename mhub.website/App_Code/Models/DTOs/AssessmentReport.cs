﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AssessmentReport
/// </summary>
public class AssessmentReport
{
   public List<userAssessment> uAssessment { get; set; }
    public List<AssessmentCollection> aCollection { get; set; }
    public AssessmentReport()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
public class userAssessment
{
    public string Username { get; set; }
    public int Id { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public string Branch { get; set; }
    public string Password{ get; set; }

    public List<UserResults> UserAssessments { get; set; }
}
public class UserResults
{
    public int memberId { get; set; }
    public int Assessment { get; set; }
    public string Grade { get; set; }
    public int Score { get; set; }
    public int Attempts { get; set; }
    public string AssessmentName { get; set; }


}
public class AssessmentCollection
{
    public int Id { get; set; }
    public string AssessmentName { get; set; }
    public string Merit { get; set; }
    public string Fail { get; set; }
    public string Distinction { get; set; }

}
public class ReportVariables
{
    public int Id { get; set; }
    public string Branch { get; set; }
    public string[] Division { get; set; }
    public DateTime startRange { get; set; }
    public DateTime endRange { get; set; }
    public string CourseType { get; set; }
    public string[] CompanyPositions { get; set; }
    public string[] CompanyPositionsList { get; set; }

}