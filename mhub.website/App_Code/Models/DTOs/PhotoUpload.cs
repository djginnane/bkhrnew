﻿
using Newtonsoft.Json;

public class PhotoUpload
{
    [JsonProperty("id")]
    public int Id { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("parentId")]
    public int ParentId { get; set; }

    [JsonProperty("path")]
    public object Path { get; set; }
}