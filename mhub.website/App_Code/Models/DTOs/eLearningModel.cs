﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Model for grabing eLearning content where key is the alias and value is url
/// </summary>
public class eLearningModel
{

    
        public string key { get; set; }
        public string value { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public eLearningModel()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
public class Poll_Info
{
    public List<eLearningModel> Poll_Results { get; set; }
    public string Poll_Name{ get; set; }
    public string Poll_Description { get; set; }
    public bool Poll_Hidden { get; set; }
}