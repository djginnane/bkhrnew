﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Core.Services;


public class Employee
{
    [JsonProperty("memberId")]
    public int MemberId { get; set; }

    [JsonProperty("parentId")]
    public int ParentId { get; set; }

    [JsonProperty("username")]
    public string Username { get; set; }

    [JsonProperty("email")]
    public string Email { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("phone")]
    public string Phone { get; private set; }

    [JsonProperty("photo")]
    public string Photo { get; set; }

    [JsonProperty("position")]
    public string Position { get; set; }

    [JsonProperty("lineManager")]
    public string LineManager { get; set; }

    [JsonProperty("role")]
    public string Role { get; set; }

    [JsonProperty("division")]
    public string Division { get; set; }

    [JsonProperty("branch")]
    public string Branch { get; set; }

    [JsonProperty("birthday")]
    public DateTime Birthday { get; set; }

    [JsonProperty("companyStart")]
    public DateTime CompanyStart { get; set; }

    [JsonProperty("careerStart")]
    public DateTime CareerStart { get; set; }

    [JsonProperty("annualLeave")]
    public IEnumerable<eLearningModel> AnnualLeave { get; set; }

    [JsonProperty("assessmentLevel")]
    public string AssessmentLevel { get; set; }
    [JsonProperty("startingTime")]
    public int StartingTime { get; set; }

    [JsonProperty("contentCompleted")]
    public string ContentCompleted { get; set; }

    [JsonProperty("assessmentsCompleted")]
    public string AssessmentsCompleted { get; set; }

    [JsonProperty("trainingCompleted")]
    public string TrainingCompleted { get; set; }

    [JsonProperty("appraisalSchedule")]
    public string AppraisalSchedule { get; set; }

    [JsonProperty("appraisal")]
    public string Appraisal { get; set; }
    [JsonProperty("nickname")]
    public string Nickname { get; set; }

    [JsonProperty("inActive")]
    public bool inActive { get; set; }

    public Employee()
    {

    }

    public Employee(IMember pMember)
    {
        MemberId = pMember.Id;
        ParentId = pMember.ParentId;
        Username = pMember.Username;
        Email = pMember.Email;
        Name = pMember.Name;
        Position = pMember.GetValue<string>("position");
        Role = pMember.GetValue<string>("roleLevel");
        LineManager = pMember.GetValue<string>("lineManager");
        Division = pMember.GetValue<string>("division");
        Branch = pMember.GetValue<string>("branch");
        Birthday = pMember.GetValue<DateTime>("birthday");
        CompanyStart = pMember.GetValue<DateTime>("companyStart");
        CareerStart = pMember.GetValue<DateTime>("careerStart");
        AssessmentLevel = pMember.GetValue<string>("assessmentLevek"); // Note typo on this property. Must be fixed in Umbraco
        ContentCompleted = pMember.GetValue<string>("contentCompleted");
        AssessmentsCompleted = pMember.GetValue<string>("assessmentsCompleted");
        TrainingCompleted = pMember.GetValue<string>("trainingCompleted");
        StartingTime = pMember.GetValue<int>("startingTime");
        inActive = pMember.IsApproved;
        Nickname= pMember.GetValue<string>("nickName");

        var annualLeave = pMember.GetValue<string>("annualLeave");
        if (annualLeave != null && annualLeave !="{}")
        {
            AnnualLeave = JsonConvert.DeserializeObject<IEnumerable<eLearningModel>>(annualLeave);
        }

        AppraisalSchedule = pMember.GetValue<string>("appraisalSchedule");
        Appraisal = pMember.GetValue<string>("appraisal");
    }

    public Employee(IMember pMember, IMediaService pMediaService): this(pMember)
    {
        //if (pMediaService != null)
        //{
        //    var pictureId = pMember.GetValue<int>("picture");

        //    if (pictureId > 0)
        //    {
        //        var media = pMediaService.GetById(pictureId);

        //        if (media != null)
        //        {
        //            Photo = media.GetValue<string>("umbracoFile");
        //        }
        //    }
        //}
    }

    public Employee(IMember pMember, IMediaService pMediaService, string teamName) : this(pMember, pMediaService)
    {
        //Team = teamName;
        //TeamList = new List<string> { teamName };
    }
}