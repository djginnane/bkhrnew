﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Class1
/// </summary>
public class ProfilePostData
{
    [Required]
    [DataType(DataType.EmailAddress)]
    [JsonProperty("email")]
    public string Email { get; set; }

    [Required]
    [JsonProperty("name")]
    public string Name { get; set; }
}