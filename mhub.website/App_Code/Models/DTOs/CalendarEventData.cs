﻿using Newtonsoft.Json;
using System;

/// <summary>
/// Summary description for CalendarEventData
/// </summary>
public class CalendarEventData
{
    public CalendarEventData()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    [JsonProperty("class")]
    public string cssClass { get; set; }

    [JsonProperty("number")]
    public int number { get; set; }


}

