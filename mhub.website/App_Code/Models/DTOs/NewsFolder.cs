﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NewsFolder
/// </summary>
public class NewsFolder
{
    public string NodeName { get; set; }
    public string FeatureImage { get; set; }
}