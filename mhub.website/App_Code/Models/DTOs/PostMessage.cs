﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

/// <summary>
/// User to post social message from website to WebAPI
/// </summary>
public class PostMessage
{
    [Required]
    [JsonProperty("message")]
    public string Message { get; set; }
}