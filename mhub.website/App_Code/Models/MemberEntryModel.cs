﻿using Newtonsoft.Json;
using System.Collections.Generic;
using UmbracoWithMvc.Controllers;

/// <summary>
/// Summary description for MemberEntryModel
/// </summary>
public class MemberEntryModel
{
    [JsonProperty("employee")]
    public Employee Employee { get; set; }

    [JsonProperty("members")]
    public IEnumerable<object> Members { get; set; }

    [JsonProperty("roles")]
    public IEnumerable<object> Roles { get; set; }

    [JsonProperty("positions")]
    public IEnumerable<string> Positions { get; set; }

    [JsonProperty("divisions")]
    public IEnumerable<string> Divisions { get; set; }

    [JsonProperty("branches")]
    public IEnumerable<string> Branches { get; set; }

    [JsonProperty("appraisals")]
    public IEnumerable<AppraisalForms> Appraisals { get; set; }

    [JsonProperty("appraisalSchedules")]
    public IEnumerable<object> AppraisalSchedules { get; set; }

    public MemberEntryModel()
    {
        Members = new List<object>();
        Roles = new List<object>();
        Positions = new List<string>();
        Divisions = new List<string>();
        Branches = new List<string>();
        AppraisalSchedules = new List<AppraisalForms>();
    }
}