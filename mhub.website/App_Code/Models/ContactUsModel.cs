using System.ComponentModel.DataAnnotations;
using System;
using Umbraco.Web.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;
using Umbraco.Core.Models;
using System.Globalization;

public class ContactUsModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Message { get; set; }

    }

    public class LeaveRequestStart {
      public LeaveRequestStart()
    {
        LeaveStart = DateTime.Today;
       LeaveEnd = DateTime.Today;
       // DateTime.Today;
    }
    [Required]
    [Display(Name = "Start Leave")]
    [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime LeaveStart { get; set; }
        [Required]
    [Display(Name = "End Leave")]
    public DateTime LeaveEnd { get; set; }
        [Required]
    [Display(Name = "LeaveType")]
    public string LeaveType { get; set; }
        [Required]
    [Display(Name = "Half Day")]
    public Boolean HalfDayStart { get; set; }
    [Display(Name = "Half Day")]
    public Boolean HalfDayEnd { get; set; }
    [Display(Name = "Leave Comments")]
    public string Comment { get; set; }
    [Display(Name = "Post as User")]
    public string user_id { get; set; }



}

public class KeyValue
{

    public string key { get; set; }
    public string value { get; set; }

    public KeyValue()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
public class LeaveDays
    {
       
    }