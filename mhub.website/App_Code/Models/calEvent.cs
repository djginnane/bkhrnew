﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Net;
using System.Web.Security;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Web.WebApi;

using Umbraco.Core.Models;

/// <summary>
/// Summary description for calEvent
/// </summary>
public class calEvent {
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public string LoginName { get; set; }
    public int id { get; set; }
    public string title { get; set; }
    public DateTime start { get; set; }
    public DateTime end { get; set; }
    public int ApprovedLeave { get; set; }
   
    public DateTime CreatedAt { get; set; }

    public bool allDay { get; set; }
    public calEvent()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}