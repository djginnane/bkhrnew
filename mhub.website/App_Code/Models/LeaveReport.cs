﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LeaveReport
/// </summary>
public class LeaveReport
{
    public int MemberId { get; set; }
    public string MemberName { get; set; }
    public decimal carryOverLeave { get; set; }
    public decimal substituteLeave { get; set; }

    public  List<LeaveCounts> lCounts { get; set; }

public LeaveReport()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
public class LeaveCounts
{
    public int MemberId { get; set; }

    public string LeaveType { get; set; }
    public decimal LeaveCounted { get; set; }
    public decimal TotalDays { get; set; }
    public bool HalfDay { get; set; }
}

public class LeaveRangeDates
{
    public DateTime StartDate{ get; set; }

    public DateTime EndDate { get; set; }
    public int MemberId { get; set; }

}

public class LeaveUserTotals
{
    public string username  { get; set; }
    public string fullname { get; set; }
    public int Memberid { get; set; }
    public decimal TotalDays { get; set; }
    public decimal Holidays { get; set; }
    public decimal TotalAnnual { get; set; }
    public decimal LeaveRemaining { get; set; }
    public decimal otherLeave { get; set; }
    public decimal SickLeave { get; set; }
    public decimal AvailableLeave { get; set; }
    public decimal ProRataLeave { get; set; }
    public decimal OtherUnPaid { get; set; }
    public decimal OtherPaid { get; set; }
    public List<LeaveRequest> leaveDays { get; set; }

}