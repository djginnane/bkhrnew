﻿using Newtonsoft.Json;
using System.Collections.Generic;

/// <summary>
/// Summary description for ProfileModel
/// </summary>
public class ProfileModel
{
    [JsonProperty("profile")]
    public Employee Profile { get; set; }

    [JsonProperty("messages")]
    public IEnumerable<IntranetMessage> Messages { get; set; }

    public ProfileModel()
    {
    }
}

