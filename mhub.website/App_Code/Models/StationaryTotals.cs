﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StationaryTotals
/// </summary>
public class StationaryTotals
{

    public int Pending_Requests { get; set; }
    public int Approved { get; set; }
    public int AllOrdered { get; set; }
    public StationaryTotals()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
public class StationaryReport
{

    public int Stationary_Totals { get; set; }
    public int Stationary_Category_Id{ get; set; }
    public int Stationary_Item_Id { get; set; }
    public string Category_Name { get; set; }
    public string Item_Name { get; set; }
    public StationaryReport()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}