﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Core.Media;
using System;
using Umbraco.Core.Services;

/// <summary>
/// Summary description for MemberModel
/// </summary>
public class MemberModel
{
    public int TotalRecords { get; set; }

    public IEnumerable<Employee> Members { get; set; }

    public MemberModel()
    {
        Members = new List<Employee>();
    }
}

