﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BudgetGroups
/// </summary>
public class BudgetGroups
{

    public int Payment_Month;
    public int Payment_Letter;
    public int Payment_Year;
    public DateTime Date_Added;
    public decimal Version1Total;
    public decimal Version2Total;
    public decimal Actual_Amount_Total;
    public List<BudgetTotals> Budget;
    public BudgetGroups()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}