﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core;
using Umbraco.Web;

/// <summary>
/// Summary description for CustomUmbracoApplication
/// </summary>
public class CustomUmbracoApplication : UmbracoApplication
    {

        protected override IBootManager GetBootManager()
        {
            return new CustomWebBootManager(this);
        }

        class CustomWebBootManager : WebBootManager
        {

            public CustomWebBootManager(UmbracoApplicationBase application) : base(application)
            {

            }

            public override IBootManager Complete(Action<ApplicationContext> afterComplete)
            {
                RouteTable.Routes.MapRoute(
                    "LeavePage",
                    "leave/leavepage",
                    new { controller = "ManageLeave", action = "Index"}

                    );
          /*  RouteTable.Routes.MapRoute(
                    "MemberAppraisal",
                    "appraisals/memberappraisal",
                    new { controller = "Appraisal", action = "Index" }

                    );
                    */

            return base.Complete(afterComplete);
            }
        }
        public CustomUmbracoApplication()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }