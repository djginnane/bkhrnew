﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using umbraco;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using System;
using System.Web;
using Umbraco.Web.WebApi;
using System.Net.Http;
using System.Net;
using System.Net.Http.Formatting;
using UmbracoWithMvc.Controllers;
using System.Web.Http;
using System.Xml;

/// <summary>
/// Summary description for MhubContentHelper
/// </summary>
public static class MhubContentHelper
{
    #region "utility"

    /// <summary>
    /// Converts eLearning dictionary item to list of elearning Model
    /// </summary>
    /// <param name="json"></param>
    /// <returns></returns>
    public static List<eLearningModel> ConvertJson(string json)
    {
        //TODO - change this to a more generic type.
        var eLearn = new List<eLearningModel>();
        eLearn = JsonConvert.DeserializeObject<List<eLearningModel>>(json);
        return eLearn;
    }

    public static IPublishedContent HubRoot
    {
        get
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            return umbracoHelper.TypedContentAtRoot().First();
        }
    }
    public static IPublishedContent HubRootFromId(int rootId)
    {

        var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
        var page = umbracoHelper.TypedContent(rootId);
        return page;

    }

    public static IPublishedContent GetRootNode()
    {
        var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
        var rootNode = umbracoHelper.TypedContentSingleAtXPath("//root");

        return rootNode;
    }

    #endregion

    public static int GetTotalTraining
    {
        get
        {
            // TODO - make sure that this only applies to one hub
            return uQuery.GetNodesByType("trainingPage").Count();
        }
    }

    public static List<string> GetTotalCourse
    {
        get
        {

            //return uQuery.GetNodesByType("trainingModule").Count();
            var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);

            var emptyStringArray = new List<string>();
            var memberRoot = MhubMemberHelper.LoggedInMember.GetValue<int>("hub");
            var parent = umbracoHelper.TypedContent(memberRoot);
            var pages = parent.DescendantsOrSelf().Where(x => x.DocumentTypeAlias == "trainingModule");

            foreach (var pg in pages)
            {
                emptyStringArray.Add(pg.Id.ToString());
            }

            //  var courses= parent.Children().GroupBy(x => x.DocumentTypeAlias=="mhubTraining");
            return emptyStringArray;


        }
    }

    public static IEnumerable<IPublishedContent> GetTotalCoursePages
    {
        get
        {

            //return uQuery.GetNodesByType("trainingModule").Count();
            var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);

            var emptyStringArray = new List<string>();
            var memberRoot = MhubMemberHelper.LoggedInMember.GetValue<int>("hub");
            var parent = umbracoHelper.TypedContent(memberRoot);
            var pages = parent.DescendantsOrSelf().Where(x => x.DocumentTypeAlias == "trainingModule");

            foreach (var pg in pages)
            {
                emptyStringArray.Add(pg.Id.ToString());
            }

            //  var courses= parent.Children().GroupBy(x => x.DocumentTypeAlias=="mhubTraining");
            return pages;


        }
    }
    public static IEnumerable<IPublishedContent> GetAssessmentPages
    {
        get
        {
            var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
            var memberRoot = MhubMemberHelper.LoggedInMember.GetValue<int>("hub");
            var parent = umbracoHelper.TypedContent(memberRoot);
            var pages = parent.DescendantsOrSelf().Where(x => x.DocumentTypeAlias == "assessment");
            return pages;
        }

    }

    public static List<string> GetTotalKnowledge
    {
        get
        {
            //TODO - this needs to be fixed.

            //return uQuery.GetNodesByType("trainingModule").Count();
            var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);

            var emptyStringArray = new List<string>();
            var memberRoot = MhubMemberHelper.LoggedInMember.GetValue<int>("hub");
            var parent = umbracoHelper.TypedContent(memberRoot);

            var pages = parent.DescendantsOrSelf().Where(x => x.DocumentTypeAlias == "mhubKnowledge");

            foreach (var pg in pages)
            {
                if (pg.Children.Count() == 0)
                {
                    emptyStringArray.Add(pg.Id.ToString());
                }
            }

            //  var courses= parent.Children().GroupBy(x => x.DocumentTypeAlias=="mhubTraining");
            return emptyStringArray;
        }
    }

    public static void MarkCurrentTrainingComplete(int contentId)
    {
        var member = MhubMemberHelper.LoggedInMember;
        string completed = member.GetValue<string>("contentCompleted") + "," + contentId.ToString();
        member.SetValue("contentCompleted", completed);

        ApplicationContext.Current.Services.MemberService.Save(member);
    }


    public static void MarkCurrentKnowledgeComplete(int contentId)
    {
        var member = MhubMemberHelper.LoggedInMember;
        string completed = member.GetValue<string>("knowledgeContentCompleted") + "," + contentId.ToString();
        member.SetValue("knowledgeContentCompleted", completed);
        ApplicationContext.Current.Services.MemberService.Save(member);
    }

    public static void MarkCurrentTrainingCourseComplete(int contentId)
    {
        var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
        var parent = umbracoHelper.TypedContent(contentId).Parent;
        var sibList = new List<int>();
        bool allCompleted = true;
        var CompleteList = GetCompletedTraining();
        foreach (var children in parent.Children)
        {
            if (!CompleteList.Exists(x => x == children.Id.ToString()) && (children.DocumentTypeAlias == "trainingPage" || children.DocumentTypeAlias == "assessment"))
            { allCompleted = false; }
        }
        if (allCompleted)
        { MarkCurrentTrainingComplete(parent.Id); }


    }

    public static bool CheckCourseComplete(IPublishedContent parent, List<string> CompleteList)
    {
        var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
      //  var parent = umbracoHelper.TypedContent(contentId).Parent;
        var sibList = new List<int>();
        bool allCompleted = true;
      //  var CompleteList = GetCompletedTraining();
        foreach (var children in parent.Children)
        {
            if (!CompleteList.Exists(x => x == children.Id.ToString()) && (children.DocumentTypeAlias == "trainingPage" || children.DocumentTypeAlias == "assessment"))
            { allCompleted = false; }
        }
        

        return allCompleted;
    }
    public static List<string> GetCompletedKnowledge()
    {
        string[] emptyStringArray = new string[0];
        var contentComp = MhubMemberHelper.LoggedInMember.GetValue<string>("knowledgeContentCompleted");
        if (contentComp == "" || contentComp == null)
        {
            return emptyStringArray.ToList();
        }
        else
        {
            return MhubMemberHelper.LoggedInMember.GetValue<string>("knowledgeContentCompleted").Split(',').ToList();
        }
    }

    public static List<string> GetCompletedKnowledgeNoCache()
    {
        string[] emptyStringArray = new string[0];
        var member = MhubMemberHelper.LoggedInMember;

        var contentComp = ApplicationContext.Current.Services.MemberService.GetById(member.Id).GetValue<string>("knowledgeContentCompleted");
        if (contentComp == "" || contentComp == null)
        {
            return emptyStringArray.ToList();
        }
        else
        {
            return contentComp.Split(',').ToList();
        }
    }
    public static List<string> GetCompletedTraining()
    {
          var currentSession = HttpContext.Current.Session;
        if (currentSession != null)
        {
            currentSession["MemberProfile"] = ApplicationContext.Current.Services.MemberService.GetById(MhubMemberHelper.LoggedInMember.Id);
        }
        string[] emptyStringArray = new string[0];
        var contentComp = MhubMemberHelper.LoggedInMember.GetValue<string>("contentCompleted");
        if (contentComp == "" || contentComp == null)
        {
            return emptyStringArray.ToList();
        }
        {
            return MhubMemberHelper.LoggedInMember.GetValue<string>("contentCompleted").Split(',').ToList();
        }
    }

    public static List<eLearningModel> GetNextTraining()
    {
        var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);

        var UncompletedCourses = new List<eLearningModel>();
        var currentCourses = GetTotalCourse;
        var completedCourses = GetCompletedTraining();
        var i = 0;
        foreach (var course in currentCourses)
        {
            if (!completedCourses.Exists(x => x == course) && i < 3)
            {
                var trainingCourse = new eLearningModel();
                trainingCourse.key = umbracoHelper.TypedContent(course).Url;
                trainingCourse.value = umbracoHelper.TypedContent(course).Name;
                UncompletedCourses.Add(trainingCourse);
                ++i;
            }
        }
        return UncompletedCourses;
    }

    public static List<AssessmentAnswerSet> GetCompletedAssessments()
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select * from AssessmentAnswers where MemberId =@0 ";
        var assessments = db.Fetch<AssessmentAnswerSet>(query, MhubMemberHelper.LoggedInMember.Id);

        return assessments;
    }

    public static List<string> GetTotalAssessment
    {
        get
        {
            //return uQuery.GetNodesByType("trainingModule").Count();
            var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);

            var emptyStringArray = new List<string>();
            var memberRoot = MhubMemberHelper.LoggedInMember.GetValue<int>("hub");
            var parent = umbracoHelper.TypedContent(memberRoot);
            var pages = parent.DescendantsOrSelf().Where(x => x.DocumentTypeAlias == "assessment");

            foreach (var pg in pages)
            {
                emptyStringArray.Add(pg.Id.ToString());
            }

            //  var courses= parent.Children().GroupBy(x => x.DocumentTypeAlias=="mhubTraining");
            return emptyStringArray;
        }
    }

    public static List<userAssessment> createAssessmentReport(List<UserResults> Assessments, List<UserResults> uAssessments)
    {

        var userInfo = new List<userAssessment>();
        var uResults = new List<UserResults>();
        var i = 0;
        int assId = 0;
        var assNext = new UserResults();
        int i2 = 0;
        int p = 1;
        var cloneAss = uAssessments;
        int assCount = cloneAss.Count() - 1;
        var assmentId = 234354353;
        int k = 0;
        foreach (var u in uAssessments)
        {

            if (assCount != i)
            {
                i2 = i + 1;
                assNext = cloneAss.ElementAt(i2);
                assId = assNext.memberId;
                assmentId = assNext.Assessment;
            }
            else
            {
                assId = 1247389;
                assmentId = 234354353;
            }


            var User = ApplicationContext.Current.Services.MemberService.GetById(u.memberId);
            bool UserExists = ApplicationContext.Current.Services.MemberService.Exists(u.memberId);
            if (assId != u.memberId && UserExists)
            {
                var urResults = new List<UserResults>();
                // crap = Assessments;
                var userA = new userAssessment();
                userA.Id = u.memberId;
                userA.Name = User.Name;
                userA.Email = User.Email;
                userA.Username = User.Username;
                userA.Branch = User.GetValue<string>("branch");


                var UserResult = new UserResults();
                UserResult.Assessment = u.Assessment;
                UserResult.Score = u.Score;
                UserResult.memberId = u.memberId;
                UserResult.Grade = u.Grade;


                UserResult.Attempts = p;
                uResults.Add(UserResult);




                foreach (var ass in Assessments)
                {
                    if (uResults.Exists(x => x.Assessment == ass.Assessment))
                    {
                        var tempAssess = uResults.Single(x => x.Assessment == ass.Assessment);
                        if (k == 0)
                        {
                            tempAssess.AssessmentName = ass.AssessmentName;
                        }
                        urResults.Add(tempAssess);
                    }
                    else
                    {
                        var tempAssess = new UserResults();
                        tempAssess.Assessment = ass.Assessment;
                        tempAssess.memberId = ass.memberId;
                        if (k == 0)
                        {
                            tempAssess.AssessmentName = ass.AssessmentName;
                        }
                        //     tempAssess.AssessmentName = ass.AssessmentName;
                        urResults.Add(tempAssess);
                    }

                }

                k++;
                userA.UserAssessments = urResults;


                userInfo.Add(userA);
                userA = new userAssessment();
                uResults = new List<UserResults>();
                p = 1;


            }
            else if (assId == u.memberId && assmentId != u.Assessment)

            {
                var UserResult = new UserResults();
                UserResult.Assessment = u.Assessment;
                UserResult.Score = u.Score;
                UserResult.memberId = u.memberId;
                UserResult.Grade = u.Grade;
                UserResult.Attempts = p;
                uResults.Add(UserResult);
                UserResult = new UserResults();
                p = 1;

            }
            else
            {
                p++;
            }
            i++;


        }
        return userInfo;
    }
    //Checks if staff division is authorised to view page
    public static bool staffDivision(IPublishedContent CurrentPage, string division)
    {
        var DivisionList =  new List<string>();
        DivisionList = CurrentPage.GetPropertyValue<string>("staffDivsion") == null ? new List<string>(): CurrentPage.GetPropertyValue<string>("staffDivsion").Split(',').ToList();

        if (DivisionList.Count>0)
        {
            return DivisionList.Exists(x => x == division || x == "All");
        }
        else
        {
           return true;
        }
        return true;
    }


    public static IEnumerable<XmlNode> NewsFeed(string rssFeedURL, string siteName)
    {
        var cache = ApplicationContext.Current.ApplicationCache.RuntimeCache;

        IEnumerable<XmlNode> nodeList = (IEnumerable<XmlNode>)cache.GetCacheItem("rssFeed" + siteName);
        if (nodeList == null || !nodeList.Any())
        {
            //Get the XML from remote URL
            XmlDocument xml = new XmlDocument();

            //URL currently hardcoded - but you could use a macro param to pass in URL
            xml.Load("" + @rssFeedURL);

            //Select the nodes we want to loop through
            //XmlNodeList nodes = xml.SelectNodes("//item[position() <= 3]");
            XmlNodeList nodes = xml.SelectNodes("//item");

            nodeList = nodes.Cast<XmlNode>().Skip(0).Take(3);

            // load the data in the cache so that it times out every 2 days.
            cache.InsertCacheItem("rssFeed" + siteName, () => { return nodeList; }, timeout: TimeSpan.FromDays(1));
        }
        return nodeList;
    }
    public static bool ShowPoll(bool isHidden, int User_Id, string Poll_Name)
    {
        if (isHidden)
        {
            return true;
        }
        else
        {
            var pr = new PollRepository();
            var pollList = pr.GetUserPolls(User_Id, Poll_Name);
            if (pollList.Count()!=0)
            {
                return true;

            }
            else
            {
                return false;
            }
        }
      

    }
}
