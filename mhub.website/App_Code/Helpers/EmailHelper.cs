﻿using System;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;

/// <summary>
/// Summary description for EmailHelper
/// </summary>
public class EmailHelper
{
    public EmailHelper()
    {

    }

    public static bool SendEmail(string subject, string toAddress, string text = "", string html = "", string[] cc = null)
    {
        MailMessage mailMsg = new MailMessage();

        // To
        mailMsg.To.Add(new MailAddress(toAddress));

        if (cc != null)
        {
            foreach (var emailAddress in cc)
            {
                if (!String.IsNullOrWhiteSpace(emailAddress))
                {
                    mailMsg.CC.Add(new MailAddress(emailAddress));
                }
            }
        }

        // From
        mailMsg.From = new MailAddress("support@bakertilly.co.th", "Baker Connect");

        // Subject and multipart/alternative Body
        mailMsg.Subject = subject;

        if (!String.IsNullOrWhiteSpace(text))
        {
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
        }

        if (!String.IsNullOrWhiteSpace(html))
        {
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));
        }

        // Init SmtpClient and send
        SmtpClient smtpClient = new SmtpClient();// "smtp.sendgrid.net", Convert.ToInt32(587));
                                                 //System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("username@domain.com", "yourpassword");
                                                 //smtpClient.Credentials = credentials;

        smtpClient.Send(mailMsg);

        return false;
    }

    public static bool SendAppointment(string subject, string Description, string AppName,DateTime AppStart, DateTime AppEnd, string To,  string html = "", string[] cc = null)
    {
/// Appointment code
        StringBuilder str = new StringBuilder();
        str.AppendLine("BEGIN:VCALENDAR");
        str.AppendLine("PRODID:-//Schedule a Meeting");
        str.AppendLine("VERSION:2.0");
        str.AppendLine("METHOD:REQUEST");
        str.AppendLine("BEGIN:VEVENT");
        str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", AppStart.AddMinutes(+330)));
        str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
        str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", DateTime.Now.AddMinutes(+660)));
        str.AppendLine("LOCATION: " + "Somewhere");
        str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
        str.AppendLine(string.Format("DESCRIPTION:{0}", "Come to meeting"));
        str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", "come to meeting"));
        str.AppendLine(string.Format("SUMMARY:{0}", "Meeting now"));
        str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", "dean@bakertillythailand.com"));

        str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", "dean", "dean@bakertillythailand.com"));

        str.AppendLine("BEGIN:VALARM");
        str.AppendLine("TRIGGER:-PT15M");
        str.AppendLine("ACTION:DISPLAY");
        str.AppendLine("DESCRIPTION:Reminder");
        str.AppendLine("END:VALARM");
        str.AppendLine("END:VEVENT");
        str.AppendLine("END:VCALENDAR");



        //str.AppendLine("BEGIN:VCALENDAR");
        //str.AppendLine("PRODID:-//Schedule a Meeting");
        //str.AppendLine("VERSION:2.0");
        //str.AppendLine("METHOD:REQUEST");
        //str.AppendLine("BEGIN:VEVENT");
        //str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", AppStart));
        //str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
        //str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", AppEnd));
        //str.AppendLine("LOCATION: " + "Somewhere");
        //str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
        //str.AppendLine(string.Format("DESCRIPTION:{0}", Description));
        //str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", subject));
        //str.AppendLine(string.Format("SUMMARY:{0}", "Meeting now"));
        //str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", To));

        //str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", "dean", To));

        //str.AppendLine("BEGIN:VALARM");
        //str.AppendLine("TRIGGER:-PT15M");
        //str.AppendLine("ACTION:DISPLAY");
        //str.AppendLine("DESCRIPTION:Reminder");
        //str.AppendLine("END:VALARM");
        //str.AppendLine("END:VEVENT");
        //str.AppendLine("END:VCALENDAR");



        MailMessage mailMsg = new MailMessage();

        // To
        mailMsg.To.Add(new MailAddress(To));



        // From
        mailMsg.From = new MailAddress("NoReply@bakertilly.co.th", "Baker Connect");

        // Subject and multipart/alternative Body
        mailMsg.Subject = subject;
        var text = "Test content";
     

        if (!String.IsNullOrWhiteSpace(text))
        {
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
        }

        if (!String.IsNullOrWhiteSpace(html))
        {
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));
        }

        // Init SmtpClient and send
        SmtpClient smtpClient = new SmtpClient();// "smtp.sendgrid.net", Convert.ToInt32(587));
                                                 //System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("username@domain.com", "yourpassword");
                                                 //smtpClient.Credentials = credentials;

        mailMsg.Headers.Add("Content-class", "urn:content-classes:calendarmessage");

        System.Net.Mime.ContentType contype = new System.Net.Mime.ContentType("text/calendar");
        contype.Parameters.Add("method", "REQUEST");
        contype.Parameters.Add("name", "Meeting.ics");
        AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), contype);
        mailMsg.AlternateViews.Add(avCal);
        smtpClient.Send(mailMsg);
        return false;
    }

    public static bool SendAppointment2(string subject, string Description, string AppName, DateTime AppStart, DateTime AppEnd, string To,Guid app_id, string html1 = "", string[] cc = null)

    {

        //var now = DateTime.Now;
        //var later = now.AddHours(1);

        if (app_id== Guid.Empty)
        {
            app_id = Guid.NewGuid();
        }

        StringBuilder str = new StringBuilder();
        str.AppendLine("BEGIN:VCALENDAR");
        str.AppendLine("PRODID:-//Schedule a Meeting");
        str.AppendLine("VERSION:2.0");
        str.AppendLine("METHOD:REQUEST");
        str.AppendLine("BEGIN:VEVENT");
        str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", AppStart));
        str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
        str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", AppEnd));
        str.AppendLine("LOCATION: " + "Boardroom");
        str.AppendLine(string.Format("UID:{0}", app_id));
        str.AppendLine(string.Format("DESCRIPTION:{0}", AppName));
        str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", "come to meeting"));
        str.AppendLine(string.Format("SUMMARY:{0}", Description));
        str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", "Nicha@bakertillythailand.com"));

        str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", To, To));

        str.AppendLine("BEGIN:VALARM");
        str.AppendLine("TRIGGER:-PT15M");
        str.AppendLine("ACTION:DISPLAY");
        str.AppendLine("DESCRIPTION:Reminder");
        str.AppendLine("END:VALARM");
        str.AppendLine("END:VEVENT");
        str.AppendLine("END:VCALENDAR");


        MailMessage mailMsg = new MailMessage();

        // To
        mailMsg.To.Add(new MailAddress(To));



        // From
        mailMsg.From = new MailAddress("NoReply@bakertilly.co.th", "Training Connect");

        // Subject and multipart/alternative Body
        mailMsg.Subject = subject;
        var text = Description;
        var html = Description;

        if (!String.IsNullOrWhiteSpace(text))
        {
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
        }

        if (!String.IsNullOrWhiteSpace(html))
        {
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));
        }

        // Init SmtpClient and send
        SmtpClient smtpClient = new SmtpClient();// "smtp.sendgrid.net", Convert.ToInt32(587));
                                                 //System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("username@domain.com", "yourpassword");
                                                 //smtpClient.Credentials = credentials;

        mailMsg.Headers.Add("Content-class", "urn:content-classes:calendarmessage");

        System.Net.Mime.ContentType contype = new System.Net.Mime.ContentType("text/calendar");
        contype.Parameters.Add("method", "REQUEST");
        contype.Parameters.Add("name", "Meeting.ics");
        AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), contype);
        mailMsg.AlternateViews.Add(avCal);
        smtpClient.Send(mailMsg);

        return false;

    }

}