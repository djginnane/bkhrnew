﻿using System.Collections.Generic;
using System.Web.Security;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web.PublishedContentModels;
using System.Linq;
using System;
using System.Web;
using Umbraco.Web;


/// <summary>
/// Summary description for MhubMemberHelper
/// </summary>
public class MhubMemberHelper
{
    public static IEnumerable<IMember> GetAll(string branch = "", string division = "")
    {
        var rootId = MhubContentHelper.HubRoot.Id;
      //  var members = ApplicationContext.Current.Services.MemberService.GetMembersByPropertyValue("hub", rootId.ToString());
        var members = ApplicationContext.Current.Services.MemberService.GetMembersByMemberType("Staff");
        var member = LoggedInMember;

        switch(member.GetValue<int>("roleLevel"))
            {
            case 153:
                return members.ToList().Where(m => m.GetValue<int>("lineManager") == member.Id);
            case 154:
              
                return members.ToList().Where(m => (m.GetValue<string>("branch") == member.GetValue<string>("branch") && m.GetValue<string>("division") == member.GetValue <string>("division")) || (m.GetValue<int>("lineManager") == member.Id));
            case 155:
                return members.ToList();

            default:
                return null;

        }
        if (string.IsNullOrWhiteSpace(branch))
        {
            return members;
        }
        else if (string.IsNullOrWhiteSpace(division))
        {
            return members.ToList().Where(m => m.GetValue<string>("branch") == branch);
        }
        return members.ToList().Where(m => m.GetValue<string>("branch") == branch && m.GetValue<string>("division") == division);
    }
   
       

        /// <summary>
        /// Add on to getAll helper, accepts member.gets All staff under the injected member based on their role level, divsion, branch & root hub.
        /// </summary>
        public static IEnumerable<IMember> APIGetAll(IMember member)
    {
        var rootId = member.GetValue<int>("hub");
        var division = "";
        var branch = "";
        if (member.GetValue<int>("roleLevel") == 153)
        {
            division = member.GetValue<string>("division");
            branch = member.GetValue<string>("branch");
            //  return HubMembers.ToList().Where(m => (m.IsApproved) && (((m.GetValue<string>("branch") == branch) && (m.GetValue<string>("division") == division) && (m.GetValue<int>("roleLevel") == 152)) || (m.GetValue<int>("lineManager") == member.Id)));
            return HubMembers.ToList().Where(m => (m.IsApproved) && (m.GetValue<int>("lineManager") == member.Id));

        }
        else if (member.GetValue<int>("roleLevel") == 154)
        {
            branch = member.GetValue<string>("branch");
            division = member.GetValue<string>("division");
           

        }
        else if (member.GetValue<int>("roleLevel") == 155)
        {
            return HubMembers.ToList().Where(m => m.IsApproved);
        }
        else if (member.GetValue<int>("roleLevel") == 152)
        {
            return null;
        }

        if (string.IsNullOrWhiteSpace(branch))
        {
            return HubMembers.ToList().Where(m => m.IsApproved);
        }
        else if (string.IsNullOrWhiteSpace(division))
        {
            return HubMembers.ToList().Where(m => (((m.GetValue<string>("branch") == branch) || (m.GetValue<int>("lineManager") == member.Id)) && m.IsApproved));
        }
        branch = member.GetValue<string>("branch");
        division = member.GetValue<string>("division");
         return HubMembers.ToList().Where(m => (((m.GetValue<string>("branch") == branch && m.GetValue<string>("division") == division)|| (m.GetValue<int>("lineManager")==member.Id)) && m.IsApproved));
       // return HubMembers.ToList().Where(m => m.GetValue<string>("branch") == branch && m.GetValue<string>("division") == division );

    }
    //same as above but doesn't need to request member as one is injected(good for api calls)
    public static IEnumerable<IMember> APIGetAll2(IMember member)
    {
        var rootId = member.GetValue<int>("hub");
        var division = "";
        var branch = "";
        if (member.GetValue<int>("roleLevel") == 153)
        {
            division = member.GetValue<string>("division");
            branch = member.GetValue<string>("branch");
            return HubMembersAccepts(member).ToList().Where(m => m.GetValue<int>("lineManager") == member.Id && m.GetValue<string>("division") == division && m.IsApproved);

        }
        else if (member.GetValue<int>("roleLevel") == 154)
        {
            branch = member.GetValue<string>("branch");
            division = member.GetValue<string>("division");
        }
        else if (member.GetValue<int>("roleLevel") == 155)
        {
            return HubMembersAccepts(member).ToList().Where(m=> m.IsApproved);
        }
        else if (member.GetValue<int>("roleLevel") == 152)
        {
            return null;
        }

        if (string.IsNullOrWhiteSpace(branch))
        {
            return HubMembersAccepts(member).ToList().Where(m => m.IsApproved);
        }
        else if (string.IsNullOrWhiteSpace(division))
        {
            return HubMembersAccepts(member).ToList().Where(m => m.GetValue<string>("branch") == branch && m.IsApproved);
        }
        branch = member.GetValue<string>("branch");
        division = member.GetValue<string>("division");
        return HubMembersAccepts(member).ToList().Where(m => m.GetValue<string>("branch") == branch && m.GetValue<string>("division") == division && m.IsApproved || (m.GetValue<int>("lineManager") == member.Id && m.IsApproved));
    }

    public static IEnumerable<IMember> APIGetLineMembers()
    {var member = LoggedInMember;
        return HubMembers.ToList().Where(m => m.GetValue<int>("lineManager")==member.Id && m.IsApproved);
    }
    public static IEnumerable<IMember> LineMembers(IMember member)
    {
       
        return HubMembers.ToList().Where(m => m.GetValue<int>("lineManager") == member.Id && m.IsApproved);
    }
    public static IEnumerable<IMember> GetByDivision(string branch, string division)
    {
        var member = LoggedInMember;
        var roleNum = member.GetValue<int>("roleLevel");
        if (string.IsNullOrWhiteSpace(branch) && !string.IsNullOrWhiteSpace(division))
            
        {
            switch (roleNum) {
                case 155:
                    return HubMembers.ToList().Where(m => m.GetValue<string>("division") == division);
                    break;
                case 154:
                    return HubMembers.ToList().Where(m => m.GetValue<string>("division") == division && member.GetValue<string>("branch") == m.GetValue<string>("branch"));
                case 153:
                    return
                        HubMembers.ToList().Where(m => m.GetValue<string>("division") == division && member.GetValue<string>("branch")== m.GetValue<string>("branch") && m.GetValue<string>("division") == member.GetValue<string>("division"));
            }

            }
        else if (string.IsNullOrWhiteSpace(division))

        {
            switch (roleNum)
            {
                case 154:
                    return HubMembers.ToList().Where(m => m.GetValue<string>("branch") == branch && member.GetValue<string>("branch") == m.GetValue<string>("branch"));
                case 153:
                    return HubMembers.ToList().Where(m => m.GetValue<string>("branch") == branch && member.GetValue<string>("branch") == m.GetValue<string>("branch") && m.GetValue<string>("division") == member.GetValue<string>("division"));
                default:
                    return HubMembers.ToList().Where(m => m.GetValue<string>("branch") == branch);
            }
        }
       else if (string.IsNullOrWhiteSpace(branch) && string.IsNullOrWhiteSpace(division))

        {
            switch (roleNum)
            {
                case 155:
                    return HubMembers.ToList();
                    break;
                case 154:
                    return HubMembers.ToList().Where(m => m.GetValue<string>("branch") == member.GetValue<string>("branch"));
                case 153:
                    return  HubMembers.ToList().Where(m => m.GetValue<string>("division") == member.GetValue<string>("division") && m.GetValue<string>("branch") == member.GetValue<string>("branch"));
            }
            return HubMembers.ToList().Where(m => m.GetValue<string>("division") == member.GetValue<string>("division") && m.GetValue<string>("branch") == member.GetValue<string>("branch"));


        }
        switch (roleNum)
        {
            case 155:
                return HubMembers.ToList().Where(m => m.GetValue<string>("branch") == branch && m.GetValue<string>("division")== division);
                break;
            case 154:
                return HubMembers.ToList().Where(m => m.GetValue<string>("branch") == branch && m.GetValue<string>("division") == division && m.GetValue<string>("branch") == member.GetValue<string>("branch"));
            case 153:
               
            return HubMembers.ToList().Where(m => m.GetValue<string>("branch") == branch && m.GetValue<string>("division") == division && m.GetValue<string>("division") == member.GetValue<string>("division") && m.GetValue<string>("branch") == member.GetValue<string>("branch"));
        }
        return HubMembers.ToList().Where(m => m.GetValue<string>("branch") == branch && m.GetValue<string>("division") == division && m.GetValue<string>("division") == member.GetValue<string>("division") && m.GetValue<string>("branch") == member.GetValue<string>("branch"));

    }

    public static IEnumerable<IMember> HubMembers
    {
        get
        {
            var rootId = LoggedInMember.GetValue<int>("hub");
            // todo - this could probably be cached over a longer period of time.
            var requestCache = ApplicationContext.Current.ApplicationCache.RuntimeCache;//RequestCache;
            IEnumerable<IMember> members = (IEnumerable<IMember>)requestCache.GetCacheItem("hubMembers" + rootId.ToString());

            if (members == null || !members.Any())
            {
                members = ApplicationContext.Current.Services.MemberService.GetMembersByPropertyValue("hub", rootId);
                requestCache.InsertCacheItem("hubMembers" + rootId.ToString(), () => { return members; }, timeout: TimeSpan.FromHours(12));
            }
            members=members.ToList().Where(m => m.IsApproved);
            return members;
        }
    }

    public static IEnumerable<IMember> HubMembersAll
    {
        get
        {
            var rootId = LoggedInMember.GetValue<int>("hub");
            // todo - this could probably be cached over a longer period of time.
            var requestCache = ApplicationContext.Current.ApplicationCache.RuntimeCache;//RequestCache;
            IEnumerable<IMember> members = (IEnumerable<IMember>)requestCache.GetCacheItem("hubMembers" + rootId.ToString());

            if (members == null || !members.Any())
            {
                members = ApplicationContext.Current.Services.MemberService.GetMembersByPropertyValue("hub", rootId);
                requestCache.InsertCacheItem("hubMembers" + rootId.ToString(), () => { return members; }, timeout: TimeSpan.FromHours(12));
            }
           
            return members;
        }
    }



    public static IEnumerable<IMember> UpdateMembersCache(IMember mem)
    {
        var rootId = mem.GetValue<int>("hub");
        var requestCache = ApplicationContext.Current.ApplicationCache.RuntimeCache;
       var  members = ApplicationContext.Current.Services.MemberService.GetMembersByPropertyValue("hub",rootId);
        requestCache.ClearCacheItem("hubMembers" + rootId.ToString());
        requestCache.InsertCacheItem("hubMembers" + rootId.ToString(), () => { return members; }, timeout: TimeSpan.FromHours(12));
    
        return new List<IMember>();
    }

    public static IEnumerable<IMember> HubMembersAccepts(IMember member)
    {

        var rootId = member.GetValue<int>("hub");
        // todo - this could probably be cached over a longer period of time.
        var requestCache = ApplicationContext.Current.ApplicationCache.RuntimeCache;//RequestCache;
        IEnumerable<IMember> members = (IEnumerable<IMember>)requestCache.GetCacheItem("hubMembers" + rootId.ToString());
        if (members == null || !members.Any())
        {

            members = ApplicationContext.Current.Services.MemberService.GetMembersByPropertyValue("hub", rootId);
            requestCache.InsertCacheItem("hubMembers" + rootId.ToString(), () => { return members; }, timeout: TimeSpan.FromHours(12));
        }
        return members;

    }
    public static string GetMembersAsString(IEnumerable<IMember> mList)
    {
        string memberIdList = "";
        foreach (var m in mList)
        {
            if (memberIdList == "")
            {
                memberIdList = m.Id.ToString();
            }
            else
            {
                memberIdList = memberIdList + "," + m.Id.ToString();
            }
        }

        return memberIdList;
    }

    public static int[] GetarraylistI(IEnumerable<IMember> mList)
    {
        // var branch = currentUser.GetValue<string>("branch");
        //    var division = currentUser.GetValue<string>("division");

        var i = 0;
        int[] memberListIds;
        int[] emptymemberList = new int[1];
        bool isEmpty = !mList.Any();

        if (!isEmpty)
        {
            memberListIds = new int[mList.Count()];
            foreach (var m in mList) { memberListIds[i] = m.Id; i++; }
            return memberListIds;
        }
        return emptymemberList;
    }

    /// <summary>
    ///pretty pointless class, gets memberlist of logged in member using the helpers available
    /// </summary>
    public static int[] GetAllMemberList()
    {
        var mList = MhubMemberHelper.APIGetAll(MhubMemberHelper.LoggedInMember);
        int[] mIdList;
        if (mList != null)
        {
            mIdList = MhubMemberHelper.GetarraylistI(mList);
        }
        else { mIdList = new int[0]; }
        return mIdList;
    }


    #region "user stats"

    public static T getMemberProperty<T>(string property)
    {
        return (MhubMemberHelper.LoggedInMember.HasProperty(property)) ? MhubMemberHelper.LoggedInMember.GetValue<T>(property) : default(T);
    }

    public static int AssessmentsCompleted
    {
        get
        {
            return getMemberProperty<int>("assessmentsCompleted");
        }
    }

    public static int TrainingModulesCompleted
    {
        get
        {
            return getMemberProperty<int>("trainingCompleted");
        }
    }

    public static int AverageAssessmentScore
    {
        get
        {
            return getMemberProperty<int>("averageAssessmentScore");
        }
    }

    public static string[] GetTrainingCurrentMemberHasDone
    {
        get
        {
            string[] emptyStringArray = new string[0];
            if (MhubMemberHelper.LoggedInMember.HasProperty("contentCompleted"))
            {
                var contentComp = getMemberProperty<string>("contentCompleted");
                if (contentComp == "" || contentComp == null)
                {
                    return emptyStringArray;
                }
                else
                {
                    return contentComp.Split(',');
                }
            }
            else
            { return emptyStringArray; }
        }
    }

    public static string CurrentLoggedInMemberName
    {
        get
        {
            var currentSession = HttpContext.Current.Session;
            if (currentSession == null)
            {
                return @Membership.GetUser().UserName;
            }
            else if (currentSession["LoggedInMemberName"] == null)
            {
                currentSession["LoggedInMemberName"] = @Membership.GetUser()!=null? @Membership.GetUser().UserName:"";
            }
            return (string)currentSession["LoggedInMemberName"];
        }
    }

    /// <summary>
    ///Gets the available leave for the current year from the dictionary Item accepts IMember. If the method to retrieve availbale leave alters adjust it here
    /// </summary>
    /// <param name="member"></param>
    /// <returns></returns>
    public static int AvailableLeave(IMember member)
    {
        // var aLeave = member.GetValue<int>("holidayleave");
        if (member.GetValue("annualLeave") != null)
        {
            var aLeave = member.GetValue("annualLeave").ToString();
            var leavetypes = MhubJsonHelper<eLearningModel>.ConvertJson(aLeave);

            var aLeaveCurrent = 0;
            var LeaveString = "";
            DateTime CurrentDate = DateTime.Today;
            var CurrentYear = CurrentDate.Year.ToString();
            foreach (var entry in leavetypes)
            {
                if (CurrentYear == entry.key)
                {
                    LeaveString = entry.value;
                }
            }

            int.TryParse(LeaveString, out aLeaveCurrent);
            return aLeaveCurrent;
        }
        else
        {
            return 12;
        }

    }
    //TODO: switch availableleave references to retrieve available leave as a decimal
    public static decimal AvailableLeaveDec(IMember member)
    {
       
        if (member.GetValue("annualLeave") != null)
        {
            var aLeave = member.GetValue("annualLeave").ToString();
            var leavetypes = MhubJsonHelper<eLearningModel>.ConvertJson(aLeave);

            decimal aLeaveCurrent = 0;
            var LeaveString = "";
            DateTime CurrentDate = DateTime.Today;
            var CurrentYear = CurrentDate.Year.ToString();
            if (leavetypes == null)
            { LeaveString = "0"; }
            else
            {
                foreach (var entry in leavetypes)
                {
                    if (CurrentYear == entry.key)
                    {
                        LeaveString = entry.value;
                    }
                }
            }
            decimal.TryParse(LeaveString, out aLeaveCurrent);
            return aLeaveCurrent;
        }
        else
        {
            return 12;
        }

    }


    public static IMember LoggedInMember
    {
        get
        {
            var currentSession = HttpContext.Current.Session;
            if (currentSession == null)
            {
                return ApplicationContext.Current.Services.MemberService.GetByUsername(CurrentLoggedInMemberName);
            }
            else if (currentSession["MemberProfile"] == null)
            {
                currentSession["MemberProfile"] = ApplicationContext.Current.Services.MemberService.GetByUsername(CurrentLoggedInMemberName);

            }

            return (IMember)currentSession["MemberProfile"];
        }
    }

    public static IMember UpdateMemberCache(IMember curMem)
    {
        var loggedUsername = curMem.Username;
        var currentSession = HttpContext.Current.Session;
        currentSession["MemberProfile"] = ApplicationContext.Current.Services.MemberService.GetByUsername(loggedUsername);


        return (IMember)currentSession["MemberProfile"];
        
    }
    public static int staffLevel(IPublishedContent cPage)
    {
      var pageLevel= cPage.GetPropertyValue<string>("staffLevels");
        int staffValue=152;
        switch (pageLevel) {
            case "Division Manager":
                staffValue = 153;
                break;
            case "Branch Manager":
                staffValue = 154;
                break;
            case "Brand Manager":
                staffValue = 155;
                break;
        }
        return staffValue;
    }

    public static bool userIsActive(IMember cMem)
    {
        bool isActive = false;
        var isApproved = cMem.IsApproved;
        var inActive = cMem.GetValue<bool>("inActive");

        if (isApproved)
        {
            isActive = true;
        }

        return isActive;
    }
        #endregion

    }