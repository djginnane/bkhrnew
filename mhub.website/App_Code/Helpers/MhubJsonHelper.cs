﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;


/// <summary>
/// Converts dictionary item to list of Any Model (elearing model has generic key value property)
/// </summary>
/// <param name="json"></param>
/// <returns></returns>
public class MhubJsonHelper<T>
{

    public static List<T> ConvertJson(string json)
    {
        /*  
         *  Call this method using
          var l = MhubJsonHelper<eLearningModel>.ConvertJson<eLearningModel>("");
          */
        //
        var eLearn = new List<T>();
        if (json != "{}" && !String.IsNullOrEmpty(json))
        {
            json = json.Replace("'\"'", "");
           
            eLearn = JsonConvert.DeserializeObject<List<T>>(json);
        }
       
        return eLearn;
    }
    public MhubJsonHelper()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}