﻿using System;
using System.Collections.Generic;
using Umbraco.Core;
using Umbraco.Web;
using System.Web.Security;
using Umbraco.Core.Models;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LeaveHelpers
/// </summary>
public class LeaveHelpers
{
    /// <summary>
    /// Grabs the remaining leave for the current year for the user
    /// </summary>
    ///
    public static int AvailableLeave
    {
        get
        {
            var leaveAmount = MhubMemberHelper.AvailableLeave(MhubMemberHelper.LoggedInMember);
            return leaveAmount;
        }
    }
    public static decimal AvailableLeaveDec
    {
        get
        {
            var leaveAmount = MhubMemberHelper.AvailableLeaveDec(MhubMemberHelper.LoggedInMember);
            return leaveAmount;
        }
    }
    public static decimal AvailableLeaveDecOther(IMember member)
    {
       
            var leaveAmount = MhubMemberHelper.AvailableLeaveDec(member);
            return leaveAmount;
       
    }

    public static int PendingLeave
    {
        get
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;
            var query = "select memberID, LeaveType, createdAt, count(memberId) as TotalLeaveDays from mhubleaverequest where memberId=@0 and DATEADD(month, -1, GETDATE())<LeaveStart and ApprovedLeave=0 group by MemberId, CreatedAt, LeaveType";
            var leavePending = db.Fetch<LeaveRequest>(query, MhubMemberHelper.LoggedInMember.Id);
            return leavePending.Count();
        }
    }
    public static List<eLearningModel> GetLeaveTypes
    {
        get
        {
            var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
            var LeaveId = umbracoHelper.TypedContentAtRoot().First();
            var leaveAmount = LeaveId.GetPropertyValue("leaveTypes");
            var LeaveTypes = MhubContentHelper.ConvertJson(leaveAmount.ToString());
            return LeaveTypes;
        }
    }
    public static List<eLearningModel> GetLeaveTypesId(int pageID)
    {
      
            var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
            var LeaveId = umbracoHelper.TypedContent(pageID);
            var leaveAmount = LeaveId.GetPropertyValue("leaveTypes");
            var LeaveTypes = MhubContentHelper.ConvertJson(leaveAmount.ToString());
            return LeaveTypes;
        
    }

    public static int PendingLeaveManages
    {

        get
        {

            var db = ApplicationContext.Current.DatabaseContext.Database;
            var query = "select memberID, LeaveType, createdAt, count(memberId) as TotalLeaveDays from mhubleaverequest where memberId in(@0) and DATEADD(month, -1, GETDATE())<LeaveStart and ApprovedLeave=0 group by MemberId, CreatedAt, LeaveType";
            var leavePending = db.Fetch<LeaveRequest>(query, MhubMemberHelper.GetAllMemberList().ToArray());
            return leavePending.Count();
        }
    }


    public static decimal LeaveUsed
    {
        //Find All leave days used for the current year
        // make sure memberid, getYear are not hardcoded in
        get
        {
            var member = MhubMemberHelper.LoggedInMember;
            var testM = member.Id;
            var db = ApplicationContext.Current.DatabaseContext.Database;
            DateTime CurrentYear = DateTime.Today;
            var query = " select * from mhubLeaveRequest where memberId = @0 and ApprovedLeave=1 and Year(LeaveDay)=@1 and leaveCounted=1";
            decimal i = 0;
            decimal hDays = 0;
            var PublicHol = GrabPublicHolidays(member.Id);
            var leaveTable = db.Fetch<LeaveRequest>(query, member.Id, CurrentYear.Year);

            foreach (var ld in leaveTable)
            {

                if (!PublicHol.Exists(x => x.HoliDayDate == ld.LeaveDay))
                {
                    i++;
                    if (ld.HalfDay)
                    {
                        hDays = hDays + 0.5m;
                    }
                }
            }

            return i - hDays;
        }
    }

    public static decimal LeaveUsedByDates(DateTime DateStart, DateTime DateEnd, int Member_Id)
    {
        //Find All leave days used for the current year
        // make sure memberid, getYear are not hardcoded in
       
          //  var member = MhubMemberHelper.LoggedInMember;
            var testM = Member_Id;
            var db = ApplicationContext.Current.DatabaseContext.Database;
            DateTime CurrentYear = DateTime.Today;
            var query = " select * from mhubLeaveRequest where memberId = @0 and ApprovedLeave=1 and Year(LeaveDay)=@1 and LeaveDay>@2 and LeaveDay<@3 and leaveCounted=1";
            decimal i = 0;
            decimal hDays = 0;
            var PublicHol = GrabPublicHolidays(Member_Id);
            var leaveTable = db.Fetch<LeaveRequest>(query, Member_Id, CurrentYear.Year, DateStart, DateEnd);

            foreach (var ld in leaveTable)
            {

                if (!PublicHol.Exists(x => x.HoliDayDate == ld.LeaveDay))
                {
                    i++;
                    if (ld.HalfDay)
                    {
                        hDays = hDays + 0.5m;
                    }
                }
            }

            return i - hDays;
        
    }
    public static decimal LeaveUsedByDates(DateTime DateStart, DateTime DateEnd, int Member_Id, List<LeaveRequest> LeaveList, List<PublicHolidays> PublicHol)
    {
        //Find All leave days used for the current year
        // make sure memberid, getYear are not hardcoded in

        //  var member = MhubMemberHelper.LoggedInMember;
        var testM = Member_Id;
        var db = ApplicationContext.Current.DatabaseContext.Database;
        DateTime CurrentYear = DateTime.Today;
     //   var query = " select * from mhubLeaveRequest where memberId = @0 and ApprovedLeave=1 and Year(LeaveDay)=@1 and leaveCounted=1";
        decimal i = 0;
        decimal hDays = 0;
        //var PublicHol = PublicHol;//GrabPublicHolidays(Member_Id);
        var leaveTable = LeaveList;//db.Fetch<LeaveRequest>(query, Member_Id, CurrentYear.Year);

        foreach (var ld in leaveTable)
        {

            if (!PublicHol.Exists(x => x.HoliDayDate == ld.LeaveDay))
            {
                i++;
                if (ld.HalfDay)
                {
                    hDays = hDays + 0.5m;
                }
            }
        }

        return i - hDays;

    }
    public static string LeaveEmailMessage(IMember member, LeaveRequestStart model)
    {
        var ProRataLeave = LeaveHelpers.DetermineProRataLeave(member);
        var LeaveWithCarry = Convert.ToInt16(CarryOverLeaveToAdd(member));
        var LeaveTotalAnnual = Convert.ToInt16(LeaveHelpers.AvailableLeaveDecOther(member));
        var TotalAnnual = LeaveWithCarry + LeaveTotalAnnual;
        var LeaveRemaining = LeaveHelpers.LeaveRemaining(member.Id, TotalAnnual);
        var directManagerId = member.GetValue<int>("lineManager");
        var lineMan = ApplicationContext.Current.Services.MemberService.GetById(directManagerId);
        var proRataRemaining = ProRataLeave - (TotalAnnual - LeaveRemaining);
        string m = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        string DomainName=  m.Substring(m.LastIndexOf("//")+2);
        if (ApplicationContext.Current.Services.MemberService.Exists(directManagerId))
        {
            string leaveDetails = "<table><tr><td>Leave type:</td><td>" + model.LeaveType + "</td></tr> <tr><td>Name:</td><td>" + member.Name + "</td></tr> <tr><td>Email:</td><td>" + member.Email + "</td></tr> <tr><td>Date:</td><td>" + model.LeaveStart.ToString("MMMM dd, yyyy") + "</td></tr><tr><td>Available Annual Leave: </td><td>" + LeaveRemaining + "</td></tr><tr><td>Pro Rata leave(remaining): </td><td>" + ProRataLeave + "(" + proRataRemaining + ")</td></tr><tr><td>Go to website:</td><td><a href=\" "+ DomainName + " \" >" + DomainName + "</a></td></tr></table>";
            string eMessage = "<html><body><p>Hello " + lineMan.Name + ", <p>A holiday request has been submitted or changed, and is pending approval:</p> " + leaveDetails + "</body ></html> ";
            return eMessage;
        }
        else
        {
            string leaveDetails = "<table><tr><td>Leave type:</td><td>" + model.LeaveType + "</td></tr> <tr><td>Name:</td><td>" + member.Name + "</td></tr> <tr><td>Email:</td><td>" + member.Email + "</td></tr> <tr><td>Date:</td><td>" + model.LeaveStart.ToString("MMMM dd, yyyy") + "</td></tr><tr><td>Available Annual Leave: </td><td>" + LeaveRemaining + "</td></tr><tr><td>Pro Rata leave(remaining): </td><td>" + ProRataLeave + "("+ proRataRemaining + ")</td></tr><tr><td>Go to website:</td><td><a href=\" "+ DomainName +" \" >" + DomainName + "</a></td></tr></table>";

            string eMessage = "<html><body> <p>Hello Line Manager, <p>A holiday request has been submitted or changed, and is pending approval:</p> " + leaveDetails + "<body></html> ";
            return eMessage;
        }
    }

    public static decimal LeaveRemaining(int MemberId, int HolidaysLeft)
    {
        var PublicHol = GrabPublicHolidays(MemberId);
        DateTime lDate = new DateTime(2016, 05, 29);
        var db = ApplicationContext.Current.DatabaseContext.Database;
        DateTime today = DateTime.Today;
        int CurrentYear = today.Year;

        var query = "select *  from mhubLeaveRequest where memberId = @0 and DATEPART(yyyy,LeaveDay)>=@1 and ApprovedLeave=@2 and leaveCounted=@2";
        decimal i = 0;

        var leaveTable = db.Fetch<LeaveRequest>(query, MemberId, CurrentYear, 1, 1);
        // Loop through leave days and add only if its not on a holiday
        decimal p = 0;
        decimal hd = (decimal)0.50;
        foreach (var ld in leaveTable)
        {
            if (!PublicHol.Exists(x => x.HoliDayDate == ld.LeaveDay))
            {
                i++;
                if (ld.HalfDay)
                { p = p + hd; }
            }


        }
        decimal holleft = HolidaysLeft;
        var remLeave = HolidaysLeft - i + p;
        return remLeave;
    }

    public static decimal filterHolidays(DateTime leaveStart, DateTime LeaveEnd,List<PublicHolidays> PublicHol)
    {
     
        decimal p = 0;
       // decimal hd = (decimal)0.50;
        decimal i = 0;
        var loopDate = leaveStart;
        while (loopDate <= LeaveEnd)
        {
            if (!PublicHol.Exists(x => x.HoliDayDate == loopDate))
            {
                i++;
            }

            loopDate = loopDate.AddDays(1);
        }
        //foreach (var ld in leaveTable)
        //{
        //    if (!PublicHol.Exists(x => x.HoliDayDate == ld.LeaveDay))
        //    {
        //        i++;
        //        if (ld.HalfDay)
        //        { p = p + hd; }
        //    }


        //}
       
        var leaveUsed =i;
        return leaveUsed;
    }
    public static decimal filterAllRequests(List<LeaveRequest> leaveTable, List<PublicHolidays> PublicHol)
    {

        decimal p = 0;
        decimal hd = (decimal)0.50;
        decimal i = 0;
       
        foreach (var ld in leaveTable)
        {
            if ((ld.LeaveDay.DayOfWeek != DayOfWeek.Saturday) && (ld.LeaveDay.DayOfWeek != DayOfWeek.Sunday))
            {
                if (!PublicHol.Exists(x => x.HoliDayDate == ld.LeaveDay))
                {
                    i++;
                    if (ld.HalfDay)
                    {
                        p = p + hd;
                    }
                }

            }
            }

            var leaveUsed = i-p;
        return leaveUsed;
    }

    // leave remaining based on date
    public static decimal LeaveRemainingDate(int MemberId, decimal HolidaysLeft, DateTime SearchEndDate)
    {
        var PublicHol = GrabPublicHolidays(MemberId);
        DateTime lDate = new DateTime(2016, 05, 29);
        var db = ApplicationContext.Current.DatabaseContext.Database;
        DateTime today = DateTime.Today;
        int CurrentYear = SearchEndDate.Year;


        var query = "select *  from mhubLeaveRequest where memberId = @0 and DATEPART(yyyy,LeaveDay)>=@1  and LeaveDay<=@4 and ApprovedLeave=@2 and leaveCounted=@3";
        decimal i = 0;

        var leaveTable = db.Fetch<LeaveRequest>(query, MemberId, CurrentYear, 1, 1, SearchEndDate);
        // Loop through leave days and add only if its not on a holiday
        decimal p = 0;
        decimal hd = (decimal)0.50;
        foreach (var ld in leaveTable)
        {
            if (!PublicHol.Exists(x => x.HoliDayDate == ld.LeaveDay))
            {
                i++;
                if (ld.HalfDay)
                { p = p + hd; }
            }


        }
        decimal holleft = HolidaysLeft;
        var remLeave = HolidaysLeft - i + p;
        return remLeave;
    }

    // grab all public holidays based on user location
    //toDo Redo grabbing holidays once it is rebuilt on system
    public static List<PublicHolidays> GrabPublicHolidays(int MemberId)
    {
        // TODO:Fix location to branch
        var member = ApplicationContext.Current.Services.MemberService.GetById(MemberId);
        //  var Location = member.Location;
        var Location = member.GetValue<string>("location");

        if (String.IsNullOrWhiteSpace(Location))
        {
            Location = member.GetValue<string>("branch");
        }
        var retList = new List<PublicHolidays>();
        DateTime today = DateTime.Today;
        int CurrentYear = today.Year;

        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select *  from mhubPublicHolidays where DATEPART(yyyy,HolidayDate)>=@1 and Location=@2 ";
        retList = db.Fetch<PublicHolidays>(query, MemberId, CurrentYear, Location);
        return retList;
    }

    public static List<LeaveReport> GenerateLeaveReport(List<LeaveRequest> leaveReq, List<LeaveReport> leaveReport)
    {
        var lReport = new List<LeaveReport>();
        var lCounts = new List<LeaveCounts>();
        var tempReport = new LeaveReport();
        var templCount = new LeaveCounts();

        foreach (var Leave in leaveReq)
        {
            var Userleave = leaveReport.Single(e => e.MemberId == Leave.MemberId);
            var UserTypes = Userleave.lCounts.Single(u => u.LeaveType == Leave.LeaveType);
            decimal decHalfDays = Leave.LeaveCounted;
            UserTypes.TotalDays = Leave.TotalDays - (decHalfDays / 2);
        }
        //Load the leave Report structure


        /*  foreach (var lRequest in leaveReq)
          {

              if (lRequest.MemberId!=MemberId )
              {
                  if (i != 0)
                  {
                      tempReport.lCounts = lCounts;
                      lReport.Add(tempReport);
                  }
                  tempReport = new LeaveReport();
                  templCount = new LeaveCounts();
                  lCounts= new List<LeaveCounts>();
                  lTotals = 0;
                  tempReport.MemberId = lRequest.MemberId;
                  // get membername from helper
                  var member = ApplicationContext.Current.Services.MemberService.GetById(lRequest.MemberId);
                  var leaveAmount = MhubMemberHelper.AvailableLeave(member);
                  tempReport.MemberName = member.Name;
  // adds holiday leave allowance to report
                  templCount.MemberId = lRequest.MemberId;
                  templCount.LeaveType = "Leave Allowance";
                  templCount.TotalDays = leaveAmount;
                  templCount.LeaveCounted = lRequest.LeaveCounted;
                  templCount.HalfDay =false;
                  lCounts.Add(templCount);

                  templCount = new LeaveCounts();
                 var lRemaining= LeaveRemaining(member.Id, leaveAmount);
                  // adds leave remaining to report
                  templCount.MemberId = lRequest.MemberId;
                  templCount.LeaveType = "Remaining Leave";
                  templCount.TotalDays = lRemaining;
                  templCount.LeaveCounted = lRequest.LeaveCounted;
                  templCount.HalfDay = false;
                  lCounts.Add(templCount);

                 templCount = new LeaveCounts();

                  // Load the leave counts
                  templCount.MemberId = lRequest.MemberId;
                  templCount.LeaveType = lRequest.LeaveType;
                  templCount.TotalDays = lRequest.TotalDays;
                  templCount.LeaveCounted = lRequest.LeaveCounted;
                  templCount.HalfDay = lRequest.HalfDay;
                  lCounts.Add(templCount);
                  lTotals= lRequest.TotalDays;

              }
              else
              {
                  templCount = new LeaveCounts();
                  // Load the leave counts
                  templCount.MemberId = lRequest.MemberId;
                  templCount.LeaveType = lRequest.LeaveType;
                  templCount.TotalDays = lRequest.TotalDays;
                  templCount.LeaveCounted = lRequest.LeaveCounted;
                  templCount.HalfDay = lRequest.HalfDay;
                 lTotals = lTotals+lRequest.TotalDays;
                  lCounts.Add(templCount);
              }
              MemberId = lRequest.MemberId;
              ++i;
          }

          var templCount2 = new LeaveCounts();
          templCount2.LeaveType = "Totals Days";
          templCount2.TotalDays= lTotals;

            lCounts.Add(templCount2);
          tempReport.lCounts = lCounts;
          lReport.Add(tempReport);
          */
        return leaveReport;
    }
    public static List<LeaveReport> ReportStructure(List<LeaveRequest> leaveReq, List<LeaveCounts> leaveTypes, List<LeaveRequest> RemainingList)
    {
        var lReport = new List<LeaveReport>();
        
        var lCounts = new List<LeaveCounts>();
        var tempReport = new LeaveReport();
        var templCount = new LeaveCounts();
        int MemberId = 0;
        int i = 0;
        foreach (var lRequest in leaveReq)
        {

            if (lRequest.MemberId != MemberId)
            {
                if (i != 0)
                {
                    lReport.Add(tempReport);
                }
                ++i;

                //this removes members from original list leaving only members with no leave taken


                MemberId = lRequest.MemberId;
                var member = ApplicationContext.Current.Services.MemberService.GetById(MemberId);
                decimal leaveAmount = MhubMemberHelper.AvailableLeaveDec(member);
                decimal carryOver = LeaveHelpers.CarryOverLeaveToAdd(member);
                leaveAmount = leaveAmount + carryOver;
                tempReport = new LeaveReport();
                templCount = new LeaveCounts();
                lCounts = new List<LeaveCounts>();
                var nick = member.GetValue<string>("nickName");
                tempReport.MemberId = lRequest.MemberId;
                tempReport.MemberName = member.Name+ " ("+nick+")";
                tempReport.carryOverLeave = carryOver;
                tempReport.substituteLeave = LeaveHelpers.GetSubstitute(member);

                // adds holiday leave allowance to report
                templCount.MemberId = lRequest.MemberId;
                templCount.LeaveType = "Leave Allowance";
                templCount.TotalDays = leaveAmount;
                templCount.LeaveCounted = lRequest.LeaveCounted;
                templCount.HalfDay = false;
                lCounts.Add(templCount);
                templCount = new LeaveCounts();

                // var lRemaining = LeaveRemaining(member.Id, leaveAmount);
                var leaveRow = new LeaveRequest();
                bool exists = RemainingList.Any(e => e.MemberId == lRequest.MemberId);
                if (exists)
                {
                    leaveRow = RemainingList.Single(e => e.MemberId == lRequest.MemberId);
                }


                //if (leaveRow != null)
                //{
                decimal halfDays = (decimal)leaveRow.LeaveCounted;
                var lRemaining = leaveRow.TotalDays - (halfDays / 2);
                decimal lAmount = (decimal)leaveAmount;
                //}

                // adds leave remaining to report
                templCount.MemberId = lRequest.MemberId;
                templCount.LeaveType = "Remaining Leave";
                templCount.TotalDays = lAmount - lRemaining;
                templCount.LeaveCounted = lRequest.LeaveCounted;
                templCount.HalfDay = false;
                lCounts.Add(templCount);

                foreach (var leave in leaveTypes)
                {
                    templCount = new LeaveCounts();
                    templCount.MemberId = lRequest.MemberId;
                    templCount.LeaveType = leave.LeaveType;
                    templCount.TotalDays = 0;
                    templCount.LeaveCounted = 0;
                    templCount.HalfDay = false;
                    lCounts.Add(templCount);

                }
                tempReport.lCounts = lCounts;

            }

        }
        lReport.Add(tempReport);


        return lReport;
    }

    public static List<LeaveReport> AddNoLeave(List<LeaveReport> Result, List<LeaveCounts> LeaveTypes, int[] memberList)
    {

        foreach (int m in memberList)
        {
            if (!Result.Exists(x => x.MemberId == m))
            {
                var EmptyLeave = new LeaveReport();
                var member = ApplicationContext.Current.Services.MemberService.GetById(m);
                EmptyLeave.MemberId = m;
                EmptyLeave.MemberName = member.Name;
                /// need to add carry over in two spots also line 474 to cover all members. Copy for substitute leave in the future
                decimal carryOver = LeaveHelpers.CarryOverLeaveToAdd(member);
                EmptyLeave.carryOverLeave = carryOver;
                var templCount = new LeaveCounts();
                var lCounts = new List<LeaveCounts>();


                // adds holiday leave allowance to report
                templCount.MemberId = m;
                templCount.LeaveType = "Leave Allowance";
                templCount.TotalDays = MhubMemberHelper.AvailableLeaveDec(member) + carryOver;
                templCount.LeaveCounted = 1;
                templCount.HalfDay = false;
                lCounts.Add(templCount);
                templCount = new LeaveCounts();
                templCount.MemberId = m;
                templCount.LeaveType = "Remaining Leave";
                templCount.TotalDays = MhubMemberHelper.AvailableLeaveDec(member)+carryOver;
                templCount.LeaveCounted = 1;
                templCount.HalfDay = false;
                lCounts.Add(templCount);

                foreach (var leave in LeaveTypes)
                {
                    templCount = new LeaveCounts();
                    templCount.MemberId = member.Id;
                    templCount.LeaveType = leave.LeaveType;
                    templCount.TotalDays = 0;
                    templCount.LeaveCounted = 0;
                    templCount.HalfDay = false;
                    lCounts.Add(templCount);

                }
                EmptyLeave.lCounts = lCounts;
                Result.Add(EmptyLeave);
            }
        }
        return Result;
    }

    /// <summary>
    /// Populates the model for User Report Page
    /// </summary>
    public static LeaveUserTotals UserLeaveReport(List<LeaveRequest> leaveReq, List<LeaveRequest> sickleave, LeaveRangeDates data)
    {
        var MemberId = data.MemberId;
        var member = ApplicationContext.Current.Services.MemberService.GetById(MemberId);
        var YearlyHol = MhubMemberHelper.AvailableLeaveDec(member);//member.GetValue<int>("holidayLeave");
       YearlyHol= YearlyHol+ LeaveHelpers.CarryOverLeaveToAdd(member);
        var lReport = new LeaveUserTotals();
        decimal Holidays = 0;
        decimal SickTot = 0;
        decimal AnnualTot = 0;
        decimal OtherTot = 0;
        decimal TotalDays = 0;
        decimal OtherPaidLeave = 0;
        decimal OtherUnPaidLeave = 0;
        decimal CurrentProRata = LeaveHelpers.DetermineProRataLeave(member);

        var remLeave = LeaveRemainingDate(MemberId, YearlyHol, data.EndDate);
        decimal hDays = 0;
        foreach (var totals in leaveReq)

        {
            if (totals.LeaveType == "Holiday")
            {
                hDays = (decimal)totals.ApprovedLeave / 2;
                Holidays = totals.TotalDays - hDays;
            }
            else if (totals.LeaveCounted == 2 && totals.LeaveType == "Sick")
            {
                hDays = (decimal)totals.ApprovedLeave / 2;
                SickTot = totals.TotalDays - hDays + SickTot;
            }
            else if (totals.LeaveCounted == 2)
            {
                hDays = (decimal)totals.ApprovedLeave / 2;
                OtherPaidLeave = totals.TotalDays-hDays+ OtherPaidLeave;
            }
            else if (totals.LeaveCounted == 1)
            {
                hDays = (decimal)totals.ApprovedLeave / 2;
                AnnualTot = totals.TotalDays + AnnualTot - hDays;
            }
            else if (totals.LeaveCounted != 0)
            {
                OtherTot = OtherTot + totals.TotalDays;
            }
            else if (totals.LeaveCounted == 0)
            {
                hDays = (decimal)totals.ApprovedLeave / 2;
                OtherUnPaidLeave = OtherUnPaidLeave + totals.TotalDays -hDays;
            }

        }
        TotalDays = Holidays + SickTot + AnnualTot + OtherTot+ OtherUnPaidLeave;

        lReport.Holidays = Holidays;
        lReport.otherLeave = OtherTot;
        lReport.TotalAnnual = AnnualTot + Holidays;
        lReport.TotalDays = TotalDays;
        lReport.SickLeave = SickTot;
        lReport.AvailableLeave = YearlyHol;
        lReport.OtherPaid = OtherPaidLeave;
        lReport.OtherUnPaid = OtherUnPaidLeave;

        lReport.Memberid = MemberId;
        lReport.username = member.Username;
        lReport.fullname = member.Name;
        lReport.LeaveRemaining = remLeave; //YearlyHol;
        lReport.leaveDays = sickleave;
        lReport.ProRataLeave = CurrentProRata;


        return lReport;
    }
    //Filters out public holidays based on location, then branch if location is empty
    public static List<LeaveRequest> RemovePublicHol(List<LeaveRequest> leaveList, IMember member)
    {
        var filteredLeave = new List<LeaveRequest>();
        var holList=GrabPublicHolidays(member.Id);
        foreach (var ld in leaveList)
        {
            if (!holList.Exists(x => x.HoliDayDate == ld.LeaveDay))
            {
                filteredLeave.Add(ld);
            }
        }
        return filteredLeave;
    }

    public static decimal CarryOverLeaveWithList(IMember member, List<LeaveRequest> LeaveList, List<PublicHolidays> PublicHol)
    {
        /*** 437 means expires at the end of january, 438 means it expires at end of the year   **/
        var CarryOverToAdd = 0m;
        var CarryOverAmount = member.GetValue<Decimal>("leaveCarryOver");
        var CarryOverType = member.GetValue<Decimal>("carryOverExpir");
        if (CarryOverType == 437 && CarryOverAmount != 0)
        {
            ///get leave used after checking the month
            CarryOverToAdd = DateTime.UtcNow.Month > 1 ? DetermineJanLeaveUsed(member) : CarryOverAmount;
        }
        else
        {
            CarryOverToAdd = CarryOverAmount;
        }
        return CarryOverToAdd;
    }

    public static decimal CarryOverLeaveToAdd(IMember member)
    {
        /*** 437 means expires at the end of january, 438 means it expires at end of the year   **/
        if (member.Id==2526)
            {
            var x = 1;
        }
        var CarryOverToAdd = 0m;
        var CarryOverAmount = member.GetValue<Decimal>("leaveCarryOver");
        var CarryOverType = member.GetValue<Decimal>("carryOverExpir");
        if (CarryOverType == 437 && CarryOverAmount != 0)
        {
            ///get leave used after checking the month
            CarryOverToAdd = DateTime.UtcNow.Month > 1? DetermineJanLeaveUsed(member): CarryOverAmount;
        }
        else
        {
            CarryOverToAdd = CarryOverAmount;
        }
        return CarryOverToAdd;
    }

    public static decimal DetermineJanLeaveUsed(IMember member)
    {
     
        var CurrentYear = DateTime.UtcNow.Year;
        var startDate = "01-01-" + CurrentYear;
        var endDate = "02-01-" + CurrentYear;
        var realSDate = Convert.ToDateTime(startDate);
        var realEDate = Convert.ToDateTime(endDate );



        return LeaveUsedByDates(realSDate, realEDate, member.Id);
    }
    public static decimal DetermineJanLeaveUsed(IMember member, List<LeaveRequest> LeaveList, List<PublicHolidays> PublicHol)
    {

        var CurrentYear = DateTime.UtcNow.Year;
        var startDate = "01-01-" + CurrentYear;
        var endDate = "02-01-" + CurrentYear;
        var realSDate = Convert.ToDateTime(startDate);
        var realEDate = Convert.ToDateTime(endDate);



        return LeaveUsedByDates(realSDate, realEDate, member.Id, LeaveList, PublicHol);
    }
    public static decimal GetSubstitute(IMember member)
    {
        var substituteLeave = 3m;
        return substituteLeave;
    }
    public static decimal DetermineProRataLeave(IMember member)
    {

        var LeaveFromYearly = System.Convert.ToDecimal(MhubMemberHelper.AvailableLeaveDec(member));
        ///get 24 instead of 12 because it is calulated in half monthly segments
        var CareerStart = member.GetValue<DateTime>("companyStart");
        var CareerStartMonth =CareerStart.Month;
        var currentDate = DateTime.UtcNow;
        var currentYear = currentDate.Year;
        var Months = CareerStart.Year== currentDate.Year?12-CareerStart.Month:12;
        decimal ProRataAnnualLeave = LeaveFromYearly / (Months*2);
        var CurrentMonth = System.Convert.ToDecimal(DateTime.UtcNow.Month);
        var DayOfMonth = DateTime.UtcNow.Day;
        decimal ProRataMultiplier = 0m;
        if (CareerStart.Year== currentYear)
        {
            if (CurrentMonth == CareerStartMonth)
            {
                ProRataMultiplier = 0m;
            }
            else
            {
                ProRataMultiplier = DayOfMonth >= 15 ? (CurrentMonth - CareerStartMonth) * 2 : ((CurrentMonth - CareerStartMonth) * 2) - 1;
            }
        }
        else
        {
             ProRataMultiplier = DayOfMonth >= 15 ? CurrentMonth * 2 : CurrentMonth * 2 - 1;
        }
        ProRataAnnualLeave = ProRataAnnualLeave * ProRataMultiplier;
        ProRataAnnualLeave =  ProRataAnnualLeave > 0 ? Math.Ceiling(ProRataAnnualLeave / 0.5m) * 0.5m : 0m;
        var LeaveFromCarryOver = CarryOverLeaveToAdd(member);
        ProRataAnnualLeave = ProRataAnnualLeave + LeaveFromCarryOver;



        return ProRataAnnualLeave;
    }

    public static List<LeaveUserTotals> MonthlyUnpaidLeave()
    {
        var LR = new LeaveRepository();
        var LastMonth = DateTime.UtcNow.AddMonths(-1);
        var StartDate = new DateTime(LastMonth.Year, LastMonth.Month , 01 );
        var EndDate = new DateTime(LastMonth.Year, LastMonth.AddMonths(1).Month, 01);
        var GetReport = LR.UnpaidLeaveReport(StartDate,EndDate);
        var UnpaidList = new List<LeaveUserTotals>();
        foreach (var R in GetReport)
        {
            var LeaveList = new LeaveUserTotals();
            if (Convert.ToInt16(R.value)!=0)
            {
                var Member = ApplicationContext.Current.Services.MemberService.GetById(Convert.ToInt16(R.key));
                LeaveList.otherLeave = Convert.ToDecimal(R.value);
                LeaveList.fullname = Member.Name;
                LeaveList.username = Member.Username;
                UnpaidList.Add(LeaveList);
            }
           
        }
        return UnpaidList;
    }
    public LeaveHelpers()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}