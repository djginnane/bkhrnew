﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using System.Net;
using UmbracoWithMvc.Entities;

/// <summary>
/// Summary description for AppraisalHelpers
/// </summary>
public class AppraisalHelpers
{

    public static bool AppraisalCheck(Umbraco.Core.Models.IMember M)
    {
        bool aCheck = false;
        DateTime CurrentDate = DateTime.Today;
        int month = DateTime.Now.Month;
        int quarter = ((month + 2) / 3);
        //var quarter = 3;
        int year = DateTime.Now.Year;
        var HalfYearly = new List<int>();
        if (month <= 6)
        {
            HalfYearly.Add(1);
            HalfYearly.Add(4);
        }
        else
        {
            HalfYearly.Add(3);
            HalfYearly.Add(2);
        }
        var query = "";
        var qResult = new MemberAppraisal();
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var AppraisSched = M.GetValue<int>("appraisalSchedule");

        switch (AppraisSched)
        {
            case 112:

                query = "select top 1 * from mhubMemberAppraisal   where Month([CreatedDate]) = @0 and Year([CreatedDate])=@2 and Member_Id =@1";
                qResult = db.SingleOrDefault<MemberAppraisal>(query, month, M.Id, year);
                Console.WriteLine("Check by month");
                break;
            case 113:
                query = "select top 1 * from mhubMemberAppraisal   where  DatePart(Quarter,[CreatedDate]) = @0 and Year([CreatedDate])=@2 and Member_Id =@1";
                qResult = db.SingleOrDefault<MemberAppraisal>(query, quarter, M.Id, year);
                Console.WriteLine("Check by quarter (Use range)");
                break;
            case 114:
                query = "select top 1 * from mhubMemberAppraisal   where Year([CreatedDate]) = @0 and Member_Id =@1";
                qResult = db.SingleOrDefault<MemberAppraisal>(query, year, M.Id);

                break;
            case 313:
                query = "select top 1 * from mhubMemberAppraisal   where DatePart(Quarter,[CreatedDate]) in (@0) and Year([CreatedDate])=@2 and Member_Id =@1";
                qResult = db.SingleOrDefault<MemberAppraisal>(query, HalfYearly.ToArray(), M.Id, year);

                break;
            case 4:
                Console.WriteLine("Check by have year (Use Range)");
                break;
            default:
                qResult = null;
                break;
        }

        if (qResult == null)
        {
            aCheck = false;
        }
        else
        {
            aCheck = true;
        }

        return aCheck;
    }
    public static string AppraisalScAlias(int AppSched)
    {
       string AppString = "The appraisal schedule is";
       DateTime createDate = DateTime.Today;
        int appType;
       string appMonth;
        int appYear;
        switch (AppSched)
        {
            case 112: //Monthly
                createDate = createDate.AddMonths(-1);
                appYear = createDate.Year;
                appMonth = createDate.ToString("MMMM");
                AppString = "Appraisal for "+ appMonth+" " +appYear;
                break;
            case 113:   //Quarteryly
                appYear = createDate.Year;
                appType = (createDate.Month + 2)/ 3;
                
                appType = appType - 1;
                if (appType==0)
                    { appType = 4;
                    appYear = appYear - 1;
                }
                AppString = "Appraisal for quarter " + appType + " " + appYear;
                break;
           case 313: //6 monthly
                appYear = createDate.Year;
                appType = createDate.Month;
                if (appType<6)
                {
                    appYear = appYear - 1;
                    AppString = "Appraisal for second half of "+ appYear;
                }
                else
                {
                    AppString = "Appraisal for first half of "+ appYear;
                }
                break;
                
            case 114:   //Yearly
                createDate = createDate.AddYears(-1);
                appYear = createDate.Year;
                AppString = "Yearly Appraisal for "+ appYear;
                break;

        }


        return AppString;
    }
    /// <summary>
    ///  Helper to check wether the employee just started at the company, so no appraisal is created.
    /// </summary>
    /// <param name="M"></param>
    /// <returns></returns>
    public static bool CheckStartDate(Umbraco.Core.Models.IMember M)
    {
        var CurrentDate = DateTime.Today;
        var startDate = M.GetValue<DateTime>("companyStart");
        var AppraisSched = M.GetValue<int>("appraisalSchedule");
          var dateDiff= CurrentDate.Date.Subtract(startDate.Date).Days;
        bool createAppraisal = false;
        switch (AppraisSched)
        {
            case 112:
                if (dateDiff>28)
                {
                    createAppraisal = true;
                }
                break;
            case 113:   //Quarteryly

                if (dateDiff > 70)
                {
                    createAppraisal = true;
                }
                break;
            case 313: //6 monthly
                if (dateDiff > 150)
                {
                    createAppraisal = true;
                }
                break;
            case 114:   //Yearly
                if (dateDiff > 220)
                {
                    createAppraisal = true;
                }
                break;
        }


        return createAppraisal;

    }

    public static int PendingAppraisalsManages
    {

        get
        {
            var mList = MhubMemberHelper.GetAllMemberList().ToArray();
            if (mList.Count() == 0)
            {
                return 0;
            }
            else {
                var db = ApplicationContext.Current.DatabaseContext.Database;
                var query = "select Member_Id from mhubMemberAppraisal where Member_Id in (@0) and AppStatus in(2,3)";
                var appPending = db.Fetch<MemberAppraisal>(query, mList);
                return appPending.Count();
            }
        }
    }

    public static int PendingAppraisals
    {

        get
        {

            var db = ApplicationContext.Current.DatabaseContext.Database;
            var query = "select Member_Id from mhubMemberAppraisal where Member_Id =@0 and AppStatus in(0,1)";
            var appPending = db.Fetch<MemberAppraisal>(query, MhubMemberHelper.LoggedInMember.Id);
            return appPending.Count();
        }
    }
    public static List<AppraisalReport> AppraisalReportRange(DateTime StartRange, DateTime EndRange)
    {
        var memberLogged = MhubMemberHelper.LoggedInMember;
       // var userList= MhubMemberHelper.APIGetAll2(memberLogged);
       // var mIdList = MhubMemberHelper.GetarraylistI(userList);
        var mIdList = MhubMemberHelper.GetAllMemberList();
        if (mIdList==null)
        {
            mIdList[0] =memberLogged.Id;
        }
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select Member_Id, count(Id) as AppraisalCount, case WHEN AppStatus IN (0,1) then count(AppStatus) end as StaffIncomplete,  case WHEN AppStatus IN (2,3) then count(AppStatus) end as ManagerIncomplete, case WHEN AppStatus IN (4,5) then count(AppStatus) end as TotalCompleted, cmsmember.LoginName as UserName, STUFF((    SELECT ', ' + CAST([ScheduleAlias] AS VARCHAR(MAX))    FROM mhubmemberappraisal    WHERE (member_id = Results.member_ID and appstatus=Results.appstatus and createdDate>@0 and createdDate<@1)     FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)') ,1,2,'') AS  AppraisalConcats   from mhubmemberappraisal as Results, cmsmember where member_Id=nodeId and createdDate>@0 and createdDate<@1 and member_id in(@2) group by Member_Id, AppStatus, cmsmember.LoginName  order by Member_Id";
        var mApprais = db.Fetch<AppraisalReport>(query, StartRange, EndRange, mIdList.ToArray());
        var mId = 0;
        var AppraisalReport = new List<AppraisalReport>();
        var appReport = new AppraisalReport();
        foreach (var A in mApprais)
        {
            if (A.Member_Id != mId)
            {
                if (mId != 0) { AppraisalReport.Add(appReport); }
                appReport = new AppraisalReport();
                appReport = A;
                mId = A.Member_Id;
                var staff = ApplicationContext.Current.Services.MemberService.GetById(mId);
                appReport.Name = staff.Name;
                if (ApplicationContext.Current.Services.MemberService.Exists(staff.GetValue<int>("lineManager")))
                {
                    var mManager = ApplicationContext.Current.Services.MemberService.GetById(staff.GetValue<int>("lineManager"));

                    appReport.ManagerName = mManager.Name;
                    appReport.ManagerId = mManager.Id;
                }
                else { appReport.ManagerName = "Manager not Assigned"; }
              
                if (A.ManagerIncomplete != 0)
                {
                    appReport.ManagerIncomplete = A.ManagerIncomplete;
                    appReport.AppraisalConcatsMan = A.AppraisalConcats;
                }
                if (A.StaffIncomplete != 0)
                {
                    appReport.StaffIncomplete = A.StaffIncomplete;
                    appReport.AppraisalConcatsStaff = A.AppraisalConcats;
                }
                if (A.TotalCompleted != 0)
                {
                    appReport.TotalCompleted = A.TotalCompleted;
                    appReport.AppraisalConcatsCom = A.AppraisalConcats;
                }
            }
            else
            {
                appReport.AppraisalCount = appReport.AppraisalCount + A.AppraisalCount;
                if (A.ManagerIncomplete != 0) { appReport.ManagerIncomplete = A.ManagerIncomplete;
                    appReport.AppraisalConcatsMan = A.AppraisalConcats;
                }
                if (A.StaffIncomplete != 0) { appReport.StaffIncomplete = A.StaffIncomplete;
                    appReport.AppraisalConcatsStaff = A.AppraisalConcats;
                }
                if (A.TotalCompleted != 0) { appReport.TotalCompleted = A.TotalCompleted;
                    appReport.AppraisalConcatsCom = A.AppraisalConcats;
                }
            }
        }

        if (mApprais.Count!=0) { AppraisalReport.Add(appReport); }

        return AppraisalReport;
    }
    public AppraisalHelpers()
    {


        //
        // TODO: Add constructor logic here
        //
    }
}