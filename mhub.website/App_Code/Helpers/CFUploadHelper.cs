﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Umbraco.Web;
using Umbraco.Core;
using System.Diagnostics;
using Umbraco.Core.Models;
using System.Data.OleDb;
using System.Data;

using System.Web.Mvc;

using Umbraco.Web.Mvc;

using Newtonsoft.Json;


/// <summary>
/// Summary description for CFUploadHelper
/// </summary>
public class CFUploadHelper
{


    public static List<CFPayment> Main(string FileName)
    {
        var LineTxt = "";
        List<string> listA = new List<string>();
        var CustomerList = new List<CFPayment>();
        string path = @FileName;
        //try
        //{
            //if (File.Exists(path))
            //{
            //    File.Delete(path);
            //}

            //using (StreamWriter sw = new StreamWriter(path))
            //{
            //    sw.WriteLine("This");
            //    sw.WriteLine("is some text");
            //    sw.WriteLine("to test");
            //    sw.WriteLine("Reading");
            //}
            var i = 0;


            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    i++;
                    
                        
                        LineTxt = sr.ReadLine();

                        int delimeter = LineTxt.LastIndexOf('\t');
                        LineTxt = Regex.Replace(LineTxt, @"\B""\b[^""]+\b""\B", m => m.Value.Replace(",", ""));
                        var values = new string[44];
                    if (i != 1 )
                    {

                        if (delimeter != -1)
                        {
                            values = LineTxt.Split('\t');
                        }
                        else
                        {
                            LineTxt = LineTxt.Replace("\"", "");
                            values = LineTxt.Split(',');
                        }
                        var sizeVal = values.Length;
                    for (int j = 0; j < sizeVal-1; j++)
                    {
                        values[j] = values[j].Replace("|", "");
                        if (values[0]=="")
                        {
                            break;
                        }
                    }
                    if (sizeVal<40)
                    {
                      //  break;
                    }
                   // break;
                    CFPayment importList = new CFPayment();
                        importList.Type_Payment = values[0];
                        importList.Supplier_Name = values[1];
                        importList.Location = values[3];
                        importList.Date_Created = CreateDate(values[4]);
                        importList.Supplier_Code = values[5];
                        ///no check no. in system values[6]
                        importList.PO_Number = values[7];
                        importList.Pr_Number = values[8];
                        importList.Invoice_Number = values[9];
                        importList.Tax_Invoice_Number = values[10];
                        importList.Invoice_Date = values[11];
                        importList.Description = values[12];
                        importList.Amount_Currency = values[13];
                        importList.Amount_FXAmount = Convert.ToDecimal(values[14].Replace(",", ""));
                        importList.Amount_ExchangeRate =Convert.ToDecimal(values[16].Replace(",", ""));
                        importList.Amount = values[17] == "" ? 0m : Convert.ToDecimal(values[17].Replace(",", ""));
                   importList.NetAmount_Cash_Discount = values[18] == "" ? 0m : Convert.ToDecimal(values[18].Replace(",",""));
                   // importList.Actual_Discount_Amount = values[18] == "" ? 0m : Convert.ToDecimal(values[16]);
                        importList.Amount_Before_Discount = string.IsNullOrWhiteSpace(values[19])?0m:Convert.ToDecimal(values[19].Replace(",", ""));
                        var vatString = values[20];
                        var indexVat = vatString.IndexOf("%");
                        importList.Vat_Number = indexVat > 0 ? Convert.ToDecimal(vatString.Remove(indexVat, indexVat)):0m;
                        importList.Vat_Amount = values[21]=="-" ? 0m : Convert.ToDecimal(values[21].Replace(",", ""));
                        importList.AmountplusVat = String.IsNullOrEmpty(values[22]) ? 0m:Convert.ToDecimal(values[22].Replace(",", ""));
                        var WHTRate = values[23];
                        var indexWHT = WHTRate.IndexOf("%");
                        importList.Withholding_Tax = indexWHT > 0 ? Convert.ToDecimal(WHTRate.Remove(indexWHT, indexWHT).Replace(",", "")) : 0m;
                        importList.Withholding_Tax_Amount = values[24]=="-"?0m:Convert.ToDecimal(values[24]);
                        importList.NetAmount_Cash_Discount= String.IsNullOrEmpty(values[25]) ? 0m : Convert.ToDecimal(values[25].Replace(",", ""));
                        importList.NetAmount_Retention= String.IsNullOrEmpty(values[26]) ? 0m : Convert.ToDecimal(values[26].Replace(",", ""));
                        importList.NetAmount_Bank_Fee= String.IsNullOrEmpty(values[27]) ? 0m : Convert.ToDecimal(values[27].Replace(",", ""));
                        importList.NetAmount_Rounding= String.IsNullOrEmpty(values[28]) ? 0m : Convert.ToDecimal(values[28].Replace(",", ""));
                        importList.NetAmount_Total= String.IsNullOrEmpty(values[29]) ? 0m : Convert.ToDecimal(values[29].Replace(",", ""));
                        importList.Advanced = values[30] == "Yes" ? true : false;
                        // Need to add Reviewd Inv Date values[31]
                        importList.Blue_PO = values[32];
                        importList.Blue_Invoice = values[33];
                        importList.Blue_Contract = values[34];
                        importList.Blue_Receipt = values[35];
                        importList.Blue_Tax_Invoice = values[36];
                        importList.Blue_ApprovePV = values[37];
                        importList.Blue_ApprovePi = values[38];
                        importList.Blue_WHT_Cert = values[39];
                        importList.Estimated_Amount = values[40] == "Yes" ? true : false;

                        ///need to convert groupings to group id and expense
                        importList.Expense_Type = values[41];
                        importList.Group_Of_Expenses_Name = values[42];
                        importList.SubGroup_Of_Expenses_name = values[43];
                        importList.Payment_letterNo= Convert.ToInt16(values[44]);
                        CustomerList.Add(importList);
                    }
                }
            }
            return CustomerList;
       // }
        //catch (Exception e)
        //{
        //    CustomerList = new List<CFPayment>();
        //    return CustomerList;
        //}
    }
public static Boolean AddPayments(List<CFPayment> ImportedList)
    {
        var Repo = new CashFlowRepository();
        try
        {
            foreach (var il in ImportedList)
            {
                Repo.AddPayment(il);
            }
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }
    public static DateTime CreateDate(string someDate)
    {
        try
        {
            return Convert.ToDateTime(someDate);
        }
        catch
        {
            string[] arrayTest = someDate.Split('-');

            var day = arrayTest[0].Length == 1 ? "0" + arrayTest[0] : arrayTest[0];
            var convertedDate = DateTime.ParseExact(day + "-" + arrayTest[1] + "-20" + arrayTest[2], "dd-MMMM-yyyy", null);
            return convertedDate;
        }
    }

    public static List<CFPayment> ReadExcelFile(string fPath="")
    {
        var PaymentList = new List<CFPayment>();
        var szFilePath = "";
        DataRow test;
        var dirPath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/TEMP/FileUploads/"));
        var tableSize = 0;
      
        szFilePath = dirPath + "PaymentTest2.xlsx";
        var   szConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source='" + szFilePath + "';Extended Properties=\"Excel 12.0;HDR=YES;\"";
        using (OleDbConnection conn = new OleDbConnection(szConnectionString))
        {
            conn.Open();
            OleDbDataAdapter objDA = new System.Data.OleDb.OleDbDataAdapter
            ("select * from [Sheet1$]", conn);
            DataSet excelDataSet = new DataSet();
            objDA.Fill(excelDataSet);
          //  tableSize= excelDataSet.Tables[0];
           var dataGridView1 = excelDataSet.Tables[0];
            var sizeRows = dataGridView1.Rows.Count;
           test = dataGridView1.Rows[1];
            for (int j = 0; j < sizeRows - 1; j++)
            {
                if (dataGridView1.Rows[j][2] is DBNull)
                {
                    break;
                }
                    PaymentList.Add(ConvertToPayment(dataGridView1.Rows[j]));
                
            }
        }
        return PaymentList;
    }

    public static CFPayment ConvertToPayment(DataRow ExcelRow)
    {
        var PayList = new List<CFPayment>();
        var Pay = new CFPayment();
        Pay.Type_Payment = ExcelRow[0].ToString();
        Pay.Supplier_Name = ExcelRow[1].ToString();
        Pay.Location = ExcelRow[3].ToString(); ;
        Pay.Date_Created = CreateDate(ExcelRow[4].ToString());
        Pay.Supplier_Code = ExcelRow[5].ToString();
        /////no check no. in system ExcelRow[6]
        Pay.PO_Number = ExcelRow[7].ToString();
        Pay.Pr_Number = ExcelRow[8].ToString();
        Pay.Invoice_Number = ExcelRow[9].ToString();
        Pay.Tax_Invoice_Number = ExcelRow[10].ToString();
        Pay.Invoice_Date = ExcelRow[11].ToString();
        Pay.Description = ExcelRow[12].ToString();
        Pay.Amount_Currency = ExcelRow[13].ToString();
        Pay.Amount_FXAmount = Convert.ToDecimal(ExcelRow[14]);
        Pay.Amount_ExchangeRate = Convert.ToDecimal(ExcelRow[15]);
        Pay.Amount = ExcelRow[16] is DBNull ? 0m : Convert.ToDecimal(ExcelRow[16]);
        Pay.NetAmount_Cash_Discount = ExcelRow[17] is DBNull ? 0m : Convert.ToDecimal(ExcelRow[17]);
        // Pay.Actual_Discount_Amount = ExcelRow[18] == "" ? 0m : Convert.ToDecimal(ExcelRow[16]);
        Pay.Amount_Before_Discount = string.IsNullOrWhiteSpace(ExcelRow[18].ToString()) ? 0m : Convert.ToDecimal(ExcelRow[18].ToString());
      //  var vatString = ExcelRow[19];
      //  var indexVat = vatString.IndexOf("%");
        Pay.Vat_Number = Convert.ToDecimal(ExcelRow[19]); //indexVat > 0 ? Convert.ToDecimal(vatString.Remove(indexVat, indexVat)) : 0m;
        Pay.Vat_Amount = ExcelRow[20].ToString() == "-" ? 0m : Convert.ToDecimal(ExcelRow[20]);
        Pay.AmountplusVat = String.IsNullOrEmpty(ExcelRow[21].ToString()) ? 0m : Convert.ToDecimal(ExcelRow[21].ToString());
       // var WHTRate = ExcelRow[22];
       // var indexWHT = WHTRate.IndexOf("%");
        Pay.Withholding_Tax = ExcelRow[22] is DBNull?0m: Convert.ToDecimal(ExcelRow[22]); //indexWHT > 0 ? Convert.ToDecimal(WHTRate.Remove(indexWHT, indexWHT).Replace(",", "")) : 0m;
        Pay.Withholding_Tax_Amount = ExcelRow[23].ToString() == "-" ? 0m : Convert.ToDecimal(ExcelRow[23]);
        Pay.NetAmount_Cash_Discount = String.IsNullOrEmpty(ExcelRow[24].ToString()) ? 0m : Convert.ToDecimal(ExcelRow[24]);
        Pay.NetAmount_Retention = String.IsNullOrEmpty(ExcelRow[25].ToString()) ? 0m : Convert.ToDecimal(ExcelRow[25]);
        Pay.NetAmount_Bank_Fee = String.IsNullOrEmpty(ExcelRow[26].ToString()) ? 0m : Convert.ToDecimal(ExcelRow[26]);
        Pay.NetAmount_Rounding = String.IsNullOrEmpty(ExcelRow[27].ToString()) ? 0m : Convert.ToDecimal(ExcelRow[27]);
        Pay.NetAmount_Total = String.IsNullOrEmpty(ExcelRow[28].ToString()) ? 0m : Convert.ToDecimal(ExcelRow[28]);
        Pay.Advanced = ExcelRow[29].ToString() == "Yes" ? true : false;
        // Need to add Reviewd Inv Date ExcelRow[30]
        Pay.Blue_PO = ExcelRow[31].ToString();
        Pay.Blue_Invoice = ExcelRow[32].ToString();
        Pay.Blue_Contract = ExcelRow[33].ToString();
        ////add store recieving sheet
        Pay.Blue_Receipt = ExcelRow[35].ToString();
        Pay.Blue_Tax_Invoice = ExcelRow[36].ToString();
        Pay.Blue_ApprovePV = ExcelRow[37].ToString();
        Pay.Blue_ApprovePi = ExcelRow[38].ToString();
        Pay.Blue_WHT_Cert = ExcelRow[39].ToString();
        Pay.Estimated_Amount = ExcelRow[40].ToString() == "Yes" ? true : false;

        /////need to convert groupings to group id and expense
        Pay.Expense_Type = ExcelRow[41].ToString();
        Pay.Group_Of_Expenses_Name = ExcelRow[42].ToString();
        Pay.SubGroup_Of_Expenses_name = ExcelRow[43].ToString();
        Pay.Payment_letterNo = Convert.ToInt16(ExcelRow[44]);
        return Pay;
    }
    public CFUploadHelper()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}