﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using umbraco;
using Umbraco.Core;
using Umbraco.Core.Models;
using System.Web.Mvc;
using Umbraco.Core.Persistence;


/// <summary>
/// Summary description for DataHelpers
/// </summary>
public class DataHelpers
{

    public static IEnumerable<SelectListItem> GetStationaryOptions()

    {
        var CategoriesList = new List<SelectListItem>();
        var cat = GetStationaryCache;
        foreach (var u in GetStationaryCache)
        {
           var singleUser = new SelectListItem();
            singleUser.Text = u.Category_Name;
            singleUser.Value = u.Id.ToString();

            CategoriesList.Add(singleUser);
        }
        return CategoriesList;
    }
    public static IEnumerable<Stationary> GetStationaryCache
    {
        get
        {
            var StationaryRepo = new StationaryRepository();

            var requestCache = ApplicationContext.Current.ApplicationCache.RuntimeCache;//RequestCache;
            IEnumerable<Stationary> ProductList = (IEnumerable<Stationary>)requestCache.GetCacheItem("StationaryCategory");
            if (ProductList == null || !ProductList.Any())
            {
                ProductList  = StationaryRepo.GetAllCategories();
                requestCache.InsertCacheItem("StationaryCategory", () => { return ProductList; }, timeout: TimeSpan.FromHours(1));
            }
            return ProductList;



        }
    }

    public static IMember GetStationaryManager
    {
        get
            {
        var SiteRoot = MhubContentHelper.HubRootFromId(MhubMemberHelper.LoggedInMember.GetValue<int>("hub"));
        var trainerId = SiteRoot.GetProperty("stationaryManager").Value.ToString();
        var trainer = ApplicationContext.Current.Services.MemberService.GetById(Int32.Parse(trainerId));
        return trainer;
            }

    }

    public static string GetPollName
    {
        get
        {
            var SiteRoot = MhubContentHelper.HubRootFromId(1057);
            var pollName= SiteRoot.GetProperty("pollName").Value.ToString();
           // var trainer = ApplicationContext.Current.Services.MemberService.GetById(Int32.Parse(trainerId));
            return pollName;
        }

    }
    public DataHelpers()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}