﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Extracts colours for various themes
/// </summary>
public class ThemeHelpers
{
    public static ThemeColours knobColours(string themeName)
    {
        var tCol = new ThemeColours();
        switch (themeName)
        {
            case "theme1":
                tCol.assAverage = "804099";
                tCol.assCompleted = "C0274C";
                tCol.Knowledge = "f6ad3d";
                break;
            case "theme3":
                tCol.assAverage = "#4895c2";
                tCol.assCompleted = "#464899";
                tCol.Knowledge = "#3f9e96";
                break;
        }
     
        tCol.assAverage = "#4895c2";
        tCol.assCompleted = "#464899";
        tCol.Knowledge = "#3f9e96";
        return tCol;
    }
    public ThemeHelpers()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}