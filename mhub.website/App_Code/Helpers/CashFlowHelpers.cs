﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CashFlowHelpers
/// </summary>
public class CashFlowHelpers
{

    public static CFPayment HandleSuppliers(CFPayment Payment)
    {
        //add supplier if new supplier entered
        if (Payment.Supplier_Id == 0)
        {
            var Sup = new Supplier();
            Sup.Supplier_Name = Payment.Supplier_Name;
            Sup.Supplier_Title = Payment.Supplier_Title;
            Sup.Job_Name = Payment.Job_Name;
            Sup.Job_Id = Payment.Job_Id.ToString();
            Sup.Supplier_Code = Payment.Supplier_Code;
            var repo = new CashFlowRepository();

            /// check again if supplier exists
            var supExist = repo.CheckSupplierExists(Sup, "Popcorn");



            if (supExist.Id != 0)
            {
                Payment.Supplier_Id = supExist.Id;
            }
            else
            {
                Sup = repo.AddSupplier(Sup);
                Payment.Supplier_Id = Sup.Id;
            }

        }
        return Payment;
    }

    public static List<CFPayment> AddpaymentsToGroups(List<CFPayment> GroupList, int LetterNo, int Month, int Year)
    {
        var groupedList = new List<CFPayment>();
        var AllPayments = PaymentList(LetterNo, Month, Year);
        var subGroups = SubGroupSummary(LetterNo, Month);
       

        var i = 1;
        var k = 1;

        /****** add the correct payments into the groups ******/
        foreach (var G in GroupList)
        {
            var groupp = G;
            groupp.Type_Payment = "Group";
            groupp.Payment_Year = i;
            groupedList.Add(groupp);
            var SubgroupList = subGroups.FindAll(x => x.Group_Of_Expenses == G.Group_Of_Expenses);

            /***** Add payments based on subgroups ********/
            foreach (var sl in SubgroupList)
            {
                var listofPayments = AllPayments.Where(x => x.SubGroup_Of_Expenses == sl.SubGroup_Of_Expenses).OrderByDescending(x => x.NetAmount_Total);
                sl.Type_Payment = "SubGroup";
                groupedList.Add(sl);
                groupedList.AddRange(listofPayments);
                foreach (var ap in listofPayments)
                {
                    AllPayments.Remove(ap);
                }
               
            }
            /***** If no subgroups add based on groups ********/
            if (SubgroupList.Count() < 1)
            {
                var listofPayments = AllPayments.Where(x => x.Group_Of_Expenses == G.Group_Of_Expenses).OrderByDescending(x => x.NetAmount_Total);
                groupedList.AddRange(listofPayments);
                foreach (var ap in listofPayments)
                {
                    AllPayments.Remove(ap);
                }
            }



            i++;
        }
        foreach (var gl in groupedList)
        {
            if (gl.Type_Payment != "Group" && gl.Type_Payment != "SubGroup")
            {
                gl.Payment_Year = k;
                k++;
            }
        }
        return groupedList;
    }
    public static List<CFPayment> PaymentList(int LetterNo, int Month, int Year)
    {
        var payList = new List<CFPayment>();
        var repo = new CashFlowRepository();
        payList = repo.GetPaymentList(LetterNo, Month, Year);
        return payList;
    }
    public static List<CFPayment> SubGroupSummary(int LetterNo, int Month)
    {
        var payList = new List<CFPayment>();
        var repo = new CashFlowRepository();
        payList = repo.GetSubGroups(LetterNo, Month);
        return payList;
    }
    /// <summary>
    /// Separts all Budgets into each group based on paymnet letter and month (date added is used as a unique identifier for the group)
    /// </summary>
    /// <param name="budgets"></param>
    /// <returns></returns>
    public static List<BudgetGroups> OrganiseBudget( List<BudgetTotals> budgets)
    {
        var bgList = new List<BudgetGroups>();

        var bg = new BudgetGroups();
        var tempDate = DateTime.UtcNow;
        var i = 0;
        foreach (var b in budgets)
        {

            var bt = new BudgetTotals();
            if (b.Date_Created==tempDate)
            {
                bt = b;
                bg.Budget.Add(bt);
            }
            else
            {
                if (i != 0)
                {
                    
                    bgList.Add(bg);
                }
                bg = new BudgetGroups();
                bg.Budget = new List<BudgetTotals>();
                bg.Payment_Letter = b.Payment_Letter;
                bg.Payment_Month = b.Payment_Month;
                bg.Payment_Year = b.Payment_Year;
                bg.Date_Added = b.Date_Created;
                bg.Budget.Add(b);
                i++;
            }
            tempDate = b.Date_Created;
        }
        if (bg.Payment_Letter != 0)
        {
            bgList.Add(bg);
        }

        return bgList;
    }

    public static void RemoveBudgetGroup(BudgetTotals bt)
    {
        var CFrepo= new CashFlowRepository();
        CFrepo.RemoveBudgetTotal(bt);
    }
    public static bool CompareToBudgetTotals(int Payment_Letter, int Payment_Month, int Payment_Year, DateTime Created_Date= new DateTime())
    {
       
        var CFrepo = new CashFlowRepository();
        var btotal = new BudgetTotals();
        var Version2Active = false;
    var totals = CFrepo.BudgetIndivTotal(Payment_Letter, Payment_Month, Payment_Year, Created_Date);
        foreach (var bt in totals)
        {
            btotal= bt;
        }
         // if we need to know if overbudget use OverBudget variable
        var OverBudget = false;
       if (btotal.Version2>0)
        {
            OverBudget = btotal.Version2 > btotal.Actual_Amount ? false : true;
            Version2Active = true;
        }
       else
        {
            OverBudget = btotal.Version1 > btotal.Actual_Amount ? false : true;
        }
     
        return Version2Active;


    }
    public static List<string> splitTitle(string myString)
    {
        var titleList = new List<String>();
        
        int pos = myString.IndexOf(' ');
        if (pos > 0)
        {
            string first = myString.Substring(0, pos);
            string second = myString.Substring(pos);
            titleList.Add(first);
            titleList.Add(second);
        }
        else
        {
            titleList.Add(myString);
        }
       
        return titleList;
    }

    
    public CashFlowHelpers()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}