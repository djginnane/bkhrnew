﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UmbracoWithMvc.Entities;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;

/// <summary>
/// Summary description for TrainingHelper
/// </summary>
public class TrainingHelper
{
    /// <summary>
    /// Gets list of people joining a training event
    /// </summary>
    /// <param name="trainingDayId"></param>
    /// <returns></returns>
    public static List<trainingRequest> TrainingJoin(int trainingDayId)
    {

        var trainingList = new List<trainingRequest>();
        var db = ApplicationContext.Current.DatabaseContext.Database;
        trainingList = db.Fetch<trainingRequest>("select *, loginName as Name from mhubtrainingRequest join cmsMember on cmsMember.NodeId = mhubtrainingRequest.member_id where trainingDay_Id=@0", trainingDayId);
        return trainingList;
    }
    public static string EmailString(List<trainingRequest> tRequests)
    {
        var emailList = "";
        foreach (var request in tRequests)
        {
            emailList = emailList + request.Email + ";";
        }
        return emailList;
    }

    public static int pendingTrainingManages()
    {
        // var mList = MhubMemberHelper.GetAllMemberList().ToArray();
        var EmployeesUnder = MhubMemberHelper.APIGetLineMembers();
        var mList = MhubMemberHelper.GetarraylistI(EmployeesUnder);
        if (mList.Count() == 0)
        {
            return 0;
        }
        else
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;
            var query = "select * from mhubtrainingRequest where trainingDay_id !=0 and member_id in(@0) and trainingApproved=0";
            var trainingPending = db.Fetch<MemberAppraisal>(query, mList);
            return trainingPending.Count();
        }
    }

    public static int pendingTraining()
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select * from mhubtrainingRequest where trainingDay_id !=0 and member_id =@0 and trainingApproved=0";
        var trainingPending = db.Fetch<MemberAppraisal>(query, MhubMemberHelper.LoggedInMember.Id);
        return trainingPending.Count();
    }
    public static List<TrainingDays> pendingTrainingList()
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query = "select *, trainingApproved as trainingStatus from mhubtrainingRequest where trainingDay_id !=0 and member_id =@0 and start>=dateadd(month, datediff(month, 0, getdate())-1, 0)";
        var trainingPending = db.Fetch<TrainingDays>(query, MhubMemberHelper.LoggedInMember.Id);
        return trainingPending;
    }
    public static List<TrainingDays> managerTrainingList()
    {
        var EmployeesUnder = MhubMemberHelper.APIGetLineMembers();
        if (EmployeesUnder.Count()!=0)
        {
            var mList = MhubMemberHelper.GetarraylistI(EmployeesUnder);
            var db = ApplicationContext.Current.DatabaseContext.Database;
            var query = "select *, trainingApproved as trainingStatus from mhubtrainingRequest where trainingDay_id !=0 and member_id in (@0) and trainingApproved=0";
            var trainingPending = db.Fetch<TrainingDays>(query, mList);
            return trainingPending;
        }

        return new List<TrainingDays>();
    }

    public static IMember GetTrainer()
    {
       
            var SiteRoot = MhubContentHelper.HubRootFromId(MhubMemberHelper.LoggedInMember.GetValue<int>("hub"));
            var trainerId = SiteRoot.GetProperty("trainingManager").Value.ToString();
            var trainer = ApplicationContext.Current.Services.MemberService.GetById(Int32.Parse(trainerId));
            return trainer;
        
    }

}