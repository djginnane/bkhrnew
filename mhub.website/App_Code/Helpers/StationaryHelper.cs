﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;
using UmbracoWithMvc.Entities;

/// <summary>
/// Summary description for StationaryHelper
/// </summary>
public class StationaryHelper
{
    private readonly StationaryRepository StRepo;
    public static List<StationaryRequisition> GetRequests()
    {
       var StationaryRepo = new StationaryRepository();
        var member = MhubMemberHelper.LoggedInMember;
        return StationaryRepo.GetStationeryRequests(member.Id);
    }
    public static List<StationaryRequisition> GetRequestsById()
    {
        var StationaryRepo = new StationaryRepository();
        var member = MhubMemberHelper.LoggedInMember;
        return StationaryRepo.GetStationeryRequestsById(member.Id);
    }
    public static StationaryTotals GetTotalsById()
    {
        var StationaryRepo = new StationaryRepository();
        var member = MhubMemberHelper.LoggedInMember;
        return StationaryRepo.GetTotalsById(member.Id);
    }
    public StationaryHelper()
    {
        //
       StRepo= new StationaryRepository();
    }
}