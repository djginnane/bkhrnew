﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;
using UmbracoWithMvc.Entities;

/// <summary>
/// Summary description for AlertHelpers
/// </summary>
public class AlertHelpers
{
    public static void emailAlert(string eMessage, string uEmail, string eSubject, string[] uEmailcc = null)
    {
        EmailHelper.SendEmail(eSubject, uEmail, "", eMessage, uEmailcc);
    }

    public static string emailBodyWarmest(string eMessage, IMember member)
    {
        var messageBody = "<html><body><p>Dear " + member.Name + ",</p><p>" + eMessage + "</p><p>Warmest Regards, <br>Monroe Management</p></body></html>";
        return messageBody;
    }
    public static string emailBody(string eMessage, IMember member)
    {
        var messageBody = "<html><body><p>Dear "+member.Name+",</p><p>"+eMessage+ "</p><p>Thank you</p></body></html>";
        return messageBody;
    }
    public static string emailBodyBasic(string eMessage)
    {
        var messageBody = "<html><body>" + eMessage + "<p>Thank you</p></body></html>";
        return messageBody;
    }

    public static bool alertExist(string alertType,int memberId, int dayDiff)
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var query= "select * from mhubAlerts where AlertType=@0 and MemberId=@1 and DateDiff(day,alertDate,getDate())<=@2";
       // var leaveTable = db.Fetch<Alerts>(query, "appraisal", 1527, 7);
          var leaveTable = db.Fetch<Alerts>(query,alertType, memberId, dayDiff);
        if (leaveTable.Count() != 0)
        { return true; }
        else
        { return false; }

    }

    public static void createAlert(string alertType, int memberId, string message, string title, int appId=0)
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var alert = new Alerts();
        alert.AlertType = alertType;
        alert.MemberId = memberId;
        alert.Message = message;
        alert.Title = title;
        alert.AlertDate = DateTime.Today;
        alert.AlertsSent = 1;
             alert.AppraisalId = appId;
        alert.Read = false;
        db.Insert(alert);
    }
public static void UnpaidLeaveEmail()
    {

      var  ReportData = LeaveHelpers.MonthlyUnpaidLeave();
        var MonthName = DateTime.UtcNow.AddMonths(-1).ToString("MMMM");
        var ListUnpaidHtml = "<table cellpadding=7><tr><td>Employee Name</td><td>Unpaid Leave Days</td></tr>";
        foreach (var RD in ReportData)
        {
            ListUnpaidHtml = ListUnpaidHtml + "<tr><td>"+RD.fullname+ "</td><td>" + RD.otherLeave + "</td></tr>";
        }
        ListUnpaidHtml = ListUnpaidHtml + "</table>";
        var Body = "<p>Dear Payroll Administrator,</p><p>The following list shows all Approved Unpaid leave requests for the month of "+MonthName+". Please See below</p>";
        Body = Body + ListUnpaidHtml;
        var EmailBody = emailBodyBasic(Body);
        var Subject = "Unpaid Leave Report For " + MonthName;
        var CC = new List<string>();
        CC.Add("dean@bakertillythailand.com");
        CC.Add("Siriporn@bakertillythailand.com"); 
        // EmailHelper.SendEmail(Subject, "nicha@bakertillythailand.com", "", EmailBody, CC.ToArray());
    }

    public AlertHelpers()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}