﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;
namespace UmbracoWithMvc.Entities
{
    /// <summary>
    /// Summary description for Class1
    /// </summary>
    [TableName("mhubAlerts")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class Alerts
    {

        [PrimaryKeyColumn(AutoIncrement = true, IdentitySeed = 1)]
        public int Id { get; set; }

        public int MemberId { get; set; }
        public string AlertType { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public int RootHub { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public DateTime LastAlertDate { get; set; }
        public DateTime AlertDate { get; set; }
        public bool Read { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Message { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Title { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public int AppraisalId { get; set; }
       
        public int AlertsSent { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        bool Completed { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public int CurrentYear { get; set; }

        public Alerts()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}