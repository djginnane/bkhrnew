﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// Stores the cashflow payment forms
/// </summary>
/// 
[TableName("BKCFPayment")]
[PrimaryKey("Id", autoIncrement = true)]

/// <summary>
/// Summary description for CFPayment
/// </summary>
public class CFPayment
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }

    public string Expense_Type { get; set; }
    public int Expense_Type_Id { get; set; }
    public int MemberId { get; set; }
    [ResultColumn]
    public bool Version2 { get; set; }
    public int Supplier_Id { get; set; }
    [ResultColumn]
    public string Supplier_Name{ get; set; }
    [ResultColumn]
    public string Supplier_Title { get; set; }
    [ResultColumn]
    public string Supplier_Title_First { get; set; }
    [ResultColumn]
    public string Supplier_Title_Last { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Supplier_Code { get; set; }
    public string PO_Number{ get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Location { get; set; }

    public DateTime PO_Date { get; set; }
 
    public Boolean Advanced { get; set; }
    public Boolean Advance_Completed { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Pr_Number { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime Pr_Date { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Invoice_Number { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Tax_Invoice_Number { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]

    public string Invoice_Date{ get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Amount_Currency { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal Amount { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal Amount_FXAmount { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal Amount_ExchangeRate { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal Amount_Before_Discount { get; set; }
   
    public decimal Vat_Number { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal Vat_Amount { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    /****   vat_amount +  Amount_Before_Discount    **/
    public decimal AmountplusVat { get; set; }
    public decimal Withholding_Tax { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal Withholding_Tax_Excluded { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    /****   withholding tax +  Amount_Before_Discount    **/
    public decimal Withholding_Tax_Amount { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal NetAmount_Cash_Discount { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal NetAmount_Retention { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal NetAmount_Bank_Fee { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal NetAmount_Rounding { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    /****   AmountplusVat- Withholding_Tax_Amount -NetAmount_Cash_Discount - NetAmount_Retention -NetAmount_Bank_Fee - NetAmount_Rounding  **/
    public decimal NetAmount_Total { get; set; }

    public string Type_Payment { get; set; }

    public string Description { get; set; }

    public int Group_Of_Expenses { get; set; }
    [ResultColumn]
    public string Group_Of_Expenses_Name { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int SubGroup_Of_Expenses { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    [ResultColumn]
    public string SubGroup_Of_Expenses_name { get; set; }

    public Boolean Estimated_Amount { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int Estimate_Table { get; set; }
    public string Payment_Account { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int Payment_letterNo { get; set; }
    public int Job_Id { get; set; }
    public string Job_Name { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int Payment_Month { get; set; }
      [NullSetting(NullSetting = NullSettings.Null)]
    public int Payment_Year { get; set; }

    /****** Estimaate table add ons *****/
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal Actual_Amount { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal Actual_Exchange_Rate { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal Actual_Discount_Amount { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal Actual_Net_Amount { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal Variance_Difference { get; set; }

    /**** Personal information ****/
    [NullSetting(NullSetting = NullSettings.Null)]
    public int Created_By { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime Date_Created  { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int Last_Edit_By{ get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime Date_Last_edit { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime Date_Actual_Payment { get; set; }

    /**** Blue Check Forms ****/
    [NullSetting(NullSetting = NullSettings.Null)]
    public String Blue_PO { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public String Blue_Invoice { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public String Blue_Contract { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public String Blue_Delivery_Note { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public String Blue_Receipt{ get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public String Blue_Tax_Invoice { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public String Blue_ApprovePV { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public String Blue_ApprovePi { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public String Blue_WHT_Cert { get; set; }
    /********    Yes/No    **********/
    [NullSetting(NullSetting = NullSettings.Null)]
    public String Blue_Amount_Estimate{ get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime Upload_Date { get; set; }
    public CFPayment()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
[TableName("BKSupplier")]
[PrimaryKey("Id", autoIncrement = true)]
public class Supplier
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public string Supplier_Title {  get;set;}
    public string Supplier_Name { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Job_Name { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]

    public string Job_Id { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Supplier_Code { get; set; }

  
}
/*** ignore this for now, may need to link later ***/
[TableName("BKPO")]
[PrimaryKey("Id", autoIncrement = true)]
public class PO
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public int PO_Number{ get; set; }
    public string PO_Date { get; set; }
}