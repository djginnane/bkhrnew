﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;



/// <summary>
/// Summary description for PublicHolidays
/// </summary>
/// 
[TableName("mhubPublicHolidays")]
[PrimaryKey("Id", autoIncrement = true)]
public class PublicHolidays
    {

        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }

        public DateTime HoliDayDate { get; set; }

        public string Location { get; set; }

        public string Description { get; set; }
        public int HubRoot { get; set; }



    public PublicHolidays()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
[TableName("mhubCountryList")]
[PrimaryKey("Id", autoIncrement = true)]
public class CountryList
{

    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }

  

    public string Country { get; set; }

    public bool Show { get; set; }
    public int HubRoot { get; set; }



    public CountryList()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

