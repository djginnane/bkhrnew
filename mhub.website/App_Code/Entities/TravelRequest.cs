﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;
namespace UmbracoWithMvc.Entities
{
    [TableName("mhubTravelRequest")]
    [PrimaryKey("Id", autoIncrement = true)]


    /// <summary>
    /// Summary description for TravelRequest
    /// </summary>
    public class TravelRequest
    {

        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string EmpTraveling { get; set; }
        public DateTime TravelDate { get; set; }
        public DateTime ReturnDate { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string ArrivalTime { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string DepartureTime { get; set; }

        public Boolean RailcardNeed { get; set; }
        public Boolean HotelNeed{ get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string HotelLocation { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string HotelName { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string OtherNotes { get; set; }

        public int member_id { get; set; }

        public TravelRequest()
        {

          
            TravelDate = DateTime.Today;
            ReturnDate = TravelDate.AddDays(4);
            //
            // TODO: Add constructor logic here
            //
        }
    }
}