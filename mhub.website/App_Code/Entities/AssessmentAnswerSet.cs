﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// Summary description for AnswerSet
/// </summary>
[TableName("AssessmentAnswers")]
[PrimaryKey("Id", autoIncrement = true)]

public class AssessmentAnswerSet
{
    [PrimaryKeyColumn(AutoIncrement = true, IdentitySeed = 1)]
    public int Id { get; set; }

    public int MemberId { get; set; }
    public int AssessmentId { get; set; }
    public string Answers { get; set; }
    public string Assessment { get; set; }
    public string Grade { get; set; }
    public string Course { get; set; }
    public string Score { get; set; }
    public DateTime CreatedAt { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime CompletedAt { get; set; }
    

    public AssessmentAnswerSet()
    {
        CreatedAt = DateTime.UtcNow;
    }
}