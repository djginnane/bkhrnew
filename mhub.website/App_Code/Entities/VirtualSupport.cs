﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;
/// <summary>
/// Summary description for VirtualSupport
/// </summary>
namespace UmbracoWithMvc.Entities
{
    [TableName("mhubVirtualSupport")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class VirtualSupport
    {

        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        public int Member_Id { get; set; }
        public string SupportNeeded { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Training { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Assessment { get; set; }

        public VirtualSupport()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}