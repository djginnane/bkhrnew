﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// Summary description for Stationary
/// </summary>
[TableName("HRSStationary")]
[PrimaryKey("Id", autoIncrement = true)]
public class Stationary
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public string Category_Name { get; set; }
    public Stationary()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
[TableName("HRSStationaryItem")]
[PrimaryKey("Item_Id", autoIncrement = true)]
public class StationaryItem
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Item_Id { get; set; }
    public int Category_Id { get; set; }
    public string Description { get; set; }
    public string Type { get; set; }
    public bool IsActive{ get; set; }
    public bool IsAvailable { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int Quantity { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime Last_Update { get; set; }
    [ResultColumn]
    public String Category_Name { get; set; }
    public StationaryItem()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}