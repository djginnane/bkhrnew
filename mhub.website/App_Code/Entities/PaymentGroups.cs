﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// Just stored payment groups that are displyed in the cashflow drop down
/// </summary>
[TableName("BKCFPaymentGroups")]
[PrimaryKey("Id", autoIncrement = true)]
public class PaymentGroups
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public string Group_Name { get; set; }
    public PaymentGroups()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
[TableName("BKCFPaymentSubGroups")]
[PrimaryKey("Id", autoIncrement = true)]
public class PaymentSubGroups
{

    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public string Group_Name { get; set; }
    public int Parent_Group { get; set; }
    public PaymentSubGroups()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}