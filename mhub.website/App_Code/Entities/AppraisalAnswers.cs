﻿using System;
using System.Collections.Generic;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// Summary description for AppraisalAnswers
/// </summary>
namespace UmbracoWithMvc.Entities
{
    [TableName("mhubAppraisalAnswer")]
    [PrimaryKey("Id", autoIncrement = true)]

    public class AppraisalAnswer
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        public string QWording { get; set; }
        public string QType { get; set; }

        public int QTemplate { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string QAnswer { get; set; }

        public int Member_Id { get; set; }
        public int QCalc { get; set; }
        public int QSection { get; set; }


        public AppraisalAnswer()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
    [TableName("mhubMemberAppraisal")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class MemberAppraisal
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        public int TemplateId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModified { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string TemplateDescription { get; set; }
        public string TemplateName { get; set; }
        public int AppraisalSchedule { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string ScheduleAlias { get; set; }
        public int Member_Id { get; set; }
        public int AppStatus { get; set; }
       

        [ResultColumn]
        public List<AppraisalAnswer> AppQuest { get; set; }
        [ResultColumn]
        public string UserName
        { get; set; }

        [ResultColumn]
        public string FName
        { get; set; }

        [ResultColumn]
        public int daysSince { get; set; }
        [ResultColumn]
        public int daysSinceMod { get; set; }
        [ResultColumn]
        public string Email
        { get; set; }
        public MemberAppraisal()
        {
            CreatedDate = DateTime.UtcNow;
            LastModified = DateTime.UtcNow;
            // TODO: Add constructor logic here
            //
        }
    }

    public class UserwAppraisal

    {
        public int Member_Id { get; set; }
        public String UserName { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public List<MemberAppraisal> mAppraisals { get; set; }

    }
}