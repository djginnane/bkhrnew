﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// Summary description for StationaryRequisition
/// </summary>
[TableName("HRSStationaryRequisition")]
[PrimaryKey("Form_Id", autoIncrement = true)]
public class StationaryRequisition
{

    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Form_Id { get; set; }
    public int Stationary_Category_Id { get; set; }
    public int Stationary_Item_Id { get; set; }
    public int Status { get; set; }
    public int Quantity_Ordered { get; set; }
    public int User_Id { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int Job_Code_Id { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Job_Name { get; set; }

    public bool Approved { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int Approved_By { get; set; }
    public DateTime Date_Subumited { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime Date_Approved { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime Date_Processed { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Comments { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string AdminComments { get; set; }
    [ResultColumn]
    public string Item_Name { get; set; }
    [ResultColumn]
    public string Category_Name { get; set; }
    [ResultColumn]
    public string LoginName { get; set; }
    [ResultColumn]
    public bool PickUp { get; set; }

    public StationaryRequisition()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}