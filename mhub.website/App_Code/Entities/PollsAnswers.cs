﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// Summary description for PollResults
/// </summary>
[TableName("HRSPollsAnswers")]
[PrimaryKey("Id", autoIncrement = true)]
public class PollsAnswers
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public string PollAnswer { get; set; }
    public int User_Id { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string User_Name { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Poll_Name { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int Poll_Id { get; set; }
    public PollsAnswers()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}