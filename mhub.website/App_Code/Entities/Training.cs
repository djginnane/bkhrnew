﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;
/// <summary>
/// Summary description for Training
/// </summary>
namespace UmbracoWithMvc.Entities
{
    public class Training
    {
        public Training()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }


    [TableName("mhubTrainingDays")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class TrainingDays
    {
        [NullSetting(NullSetting = NullSettings.Null)]
        public string LoginName { get; set; }
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string comment { get; set; }
       
        public string title { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public int rootHub { get; set; }
       
        [NullSetting(NullSetting = NullSettings.Null)]
        public string startTime { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string endTime { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Description {get; set;}
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Branch { get; set;}
        [NullSetting(NullSetting = NullSettings.Null)]
        public string CourseType { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Duration { get; set; }
        public int trainingStatus { get; set; }
        public DateTime CreatedAt { get; set; }
        public int member_id { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string member_group { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public bool allDay { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public Guid appointment_id { get; set; }

        [ResultColumn]
        public string color { get; set; }
        [ResultColumn]
        public string textcolor { get; set; }
        [ResultColumn]
        public bool stick { get; set; }
        [ResultColumn]
        public List<trainingRequest> tRequests { get; set; }
        [ResultColumn]
        public string emailList { get; set; }



    }

    [TableName("mhubtrainingRequest")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class trainingRequest

    {

        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public int trainingApproved { get; set; }
        public int trainer_id { get; set; }
        public string title { get; set; }

        public int member_id { get; set; }

        public string member_group { get; set; }

        public int trainingDay_id { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public int rootHub { get; set; }

        public DateTime registerDate { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string comments { get; set; }
        [ResultColumn]
        public string newTraining { get; set; }
        [ResultColumn]
        public string TrainingName { get; set; }
        [ResultColumn]
        public string Email { get; set; }
        [ResultColumn]
        public string Name { get; set; }
        [ResultColumn]
        public Guid appointment_id { get; set; }


    }
}