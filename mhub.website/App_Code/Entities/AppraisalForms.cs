﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace UmbracoWithMvc.Controllers
{
    [TableName("mhubAppraisalForms")]
    [PrimaryKey("Id", autoIncrement = true)]

  
    /// <summary>
    /// Stores the appraisal templates

    public class AppraisalForms
    {

        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        public string TemplateName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string TemplateDescription { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public int RootNode { get; set; }

        [SpecialDbType(SpecialDbTypes.NTEXT)]      
        public string AppraisalQuestions { get; set; }
        [ResultColumn]
        public List<QuestionSet> AppQuest { get; set; }

        public AppraisalForms()
        {
            CreatedDate = DateTime.UtcNow;
            ModifiedDate = DateTime.UtcNow;
            //
            // TODO: Add constructor logic here
            //
        }
    }

    [TableName("mhubQuestionSet")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class QuestionSet {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        public string QWording { get; set; }
        public string QType { get; set; }

        public int QTemplate { get; set; }
        public int QSection { get; set; }
        public int QCalc { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public int QOrder { get; set; }
       
    }

}