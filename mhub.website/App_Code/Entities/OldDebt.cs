﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// Summary description for OldDebt
/// </summary>
[TableName("BKCFOldDebt")]
[PrimaryKey("Id", autoIncrement = true)]

public class OldDebt
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public decimal Amount { get; set; }
    public int Supplier_Code { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Supllier_Name { get; set; }
    public string Invoice_No { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Company_Name { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int Supp_Id { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int Rehab_Group { get; set; }
    [ResultColumn]
    public decimal Amount_Remaining { get; set; }
    [ResultColumn]
    public string Payments { get; set; }
    public OldDebt()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}