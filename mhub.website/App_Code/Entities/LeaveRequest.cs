﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// Object that stores days when leave is requested
/// </summary>
/// 
[TableName("mhubLeaveRequest")]
[PrimaryKey("Id", autoIncrement = true)]
public class LeaveRequest
{

    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }

    public int MemberId { get; set; }

    public DateTime LeaveDay { get; set; }

    public string LeaveType { get; set; }
    public int LeaveCounted { get; set; }

    public Boolean HalfDay { get; set; }
    [ResultColumn]
    public String LoginName { get; set; }
    public DateTime LeaveStart { get; set; }
    public DateTime LeaveEnd { get; set; }

    public int ApprovedLeave { get; set; }

    public int ApprovedBy { get; set; }
    public int hubRoot { get; set; }
    [ResultColumn]
    public decimal TotalDays { get; set; }
    [ResultColumn]
    public Decimal HolidaysCurrYear { get; set; }

    [ResultColumn]
    public string Name { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Comment { get; set; }
    [ResultColumn]
    public int LineManager { get; set; }
    [ResultColumn]
    public string LineEmail { get; set; }
    [ResultColumn]
    public decimal TotalHalfDays { get; set; }
    [ResultColumn]
    public decimal daysMinusHalf { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public Guid Unique_Approver { get; set; }
    public DateTime CreatedAt { get; set; }
    public LeaveRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }


}