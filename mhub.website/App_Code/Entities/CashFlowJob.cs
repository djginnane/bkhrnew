﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// used to separate cashflow jobs in the future
/// </summary>
[TableName("BKCashFlowJob")]
[PrimaryKey("Id", autoIncrement = true)]
public class CashFlowJob
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Job_Id { get; set; }
    public string Job_Name { get; set; }
    public int Job_Code { get; set; }
    public CashFlowJob()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}