﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// Summary description for Templeave
/// </summary>
[TableName("mhubTempLeave")]
[PrimaryKey("Id", autoIncrement = true)]
public class Templeave
{

    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }

    public int UserId { get; set; }

   

    public string Name{ get; set; }
    public decimal Holiday { get; set; }

    public decimal Sick { get; set; }
   
    public string fname { get; set; }
    public decimal remaining { get; set; }
   

    public decimal pending { get; set; }
    public float Duvet { get; set; }


public Templeave()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}