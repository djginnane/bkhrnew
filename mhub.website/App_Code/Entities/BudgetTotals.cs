﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// used to compare budget to expenses
/// </summary>
[TableName("BKCFBudgetTotals")]
[PrimaryKey("Id", autoIncrement = true)]

/// <summary>
/// Summary description for BudgetTotals
/// </summary>
public class BudgetTotals
{

    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public DateTime Date_Created { get; set; }
    public string Job_Name{ get; set; }
    public decimal Version1 { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal Version2 { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public decimal Actual_Amount { get; set; }
    public int Group_Id{ get; set; }
    [ResultColumn]
    public string Group_Name { get; set; }

    public int Payment_Month { get; set; }
    public int Payment_Year { get; set; }
    public int Payment_Letter { get; set; }
    [ResultColumn]
    public Boolean Delete_Group { get; set; }
    
    public BudgetTotals()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}

[TableName("BKCFBudgetGroupings")]
[PrimaryKey("Id", autoIncrement = true)]
public class BudgetGroupings
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public int Group_Id { get; set; }
    [ResultColumn]
    public string Group_Name { get; set; }
    public int Payment_Month { get; set; }
    public int Payment_Year { get; set; }
    public Guid Budget_ID { get; set; }
    public DateTime Date_Created { get; set; }
    public int Created_By { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int Edited_By { get; set; }
}