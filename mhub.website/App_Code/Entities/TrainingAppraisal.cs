﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// Summary description for TrainingAppraisal
/// </summary>
namespace UmbracoWithMvc.Entities
{
    [TableName("mhubTrainingAppraisal")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class TrainingAppraisal
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        public int Member_Id { get; set; }
        public string SupportNeeded { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Training { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
     
        public string Evaluation { get; set; }
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Comments { get; set; }
        public TrainingAppraisal()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}